﻿
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register tagprefix="DivHospControls" namespace="Convex.Clientes.Apifarma.Extranet.Controls" assembly="Convex.Clientes.Apifarma.Extranet , Version=1.0.0.0, Culture=neutral, PublicKeyToken=d1e3b6c2eefc4c4f" %>
<%@ Register TagPrefix="DivHospControls" TagName="UploadInquerito2" Src="~/_layouts/APIFARMA/Controls/UploadInquerito2.ascx" %>
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Convex" Namespace="Convex.Sharepoint.Foundation.Webparts" Assembly="Convex.Sharepoint.Foundation.Webparts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=39eae81de3c872a1" %>
<%@ Register tagprefix="Webparts" namespace="Convex.Clientes.Apifarma.Internet.Webparts" assembly="Convex.Clientes.Apifarma.Internet.Webparts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d1e3b6c2eefc4c4f" %>
<%@ Register assembly="AjaxControlToolkit, Version=3.0.30930.28736, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>
<%@ Page Language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:webpartpageexpansion="full" meta:progid="SharePoint.WebPartPage.Document" MasterPageFile="~masterurl/Internet_v4.master" %>



<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
      <PublishingWebControls:editmodepanel runat="server" id="editmodestyles">
			<!-- Styles for edit mode only-->
			<SharePointWebControls:CssRegistration ID="CssRegistration1" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/zz2_editMode.css %>" runat="server"/>
	</PublishingWebControls:editmodepanel>
	<SharePointWebControls:CssRegistration ID="CssRegistration2" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/rca.css %>" runat="server"/>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <SharePointWebControls:CssRegistration ID="CssRegistration3" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/pageLayouts.css %>" runat="server"/>

	
	<div class="content_box_wide">
	<div class="content_box_corner_1"></div>
	<div class="content_box_corner_2"></div>
	<div class="content_box_corner_3"></div>
	<div class="content_box_corner_4"></div>

    <!-- Start: Breadcrumb -->
	<div class="content_box_wide_title">
	    <Convex:Breadcrumb ID="cvxBreadcrumb" runat="server" />
	</div>
    <!-- End: Breadcrumb -->
    
    <!-- Start: Content area (inside) -->
	<div class="content_box_wide_text">			
	    <PublishingWebControls:EditModePanel runat="server" id="EditModePanel1">
            <SharePointWebControls:TextField ID="txtTitle" runat="server" FieldName="Title" ></SharePointWebControls:TextField>
	    </PublishingWebControls:EditModePanel>
        <PublishingWebControls:RichHtmlField id="txtContent" runat="server" FieldName="PublishingPageContent" /> 
        <DivHospControls:UploadInquerito2 runat="server" Id="uploadInquerito2" />
        <WebPartPages:WebPartZone id="g_2223D714879C4F39A023977455B264C6" runat="server" title="Zona Footer">
		</WebPartPages:WebPartZone>
    </div>
    <!-- End: Content area (inside) -->				
</div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
<SharePointWebControls:TextField runat="server" id="TitleField" FieldName="Title"/>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderTitleBreadcrumb" runat="server">
	<WebPartPages:SPProxyWebPartManager runat="server" id="ProxyWebPartManager"></WebPartPages:SPProxyWebPartManager>	
</asp:Content>
