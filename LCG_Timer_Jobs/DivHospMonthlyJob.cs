﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using System.Globalization;
using System.Threading;
using Convex.Clientes.Apifarma.DivHosp;

namespace LCG_Timer_Jobs
{
    class DivHospMonthlyJob : SPJobDefinition
    {
        public static string JobName = "DivHosp_MonthlyJob19102013";

        public DivHospMonthlyJob() : base() { }

        public DivHospMonthlyJob(string name, SPWebApplication webApplication)
            : base(name, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = name;
        }

        public DivHospMonthlyJob(SPWebApplication webApplication)
            : base(JobName, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = JobName;
        }

        private void Run_TransporValores()
        {
            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\JobLogs.txt", true))
                {
                    file.WriteLine("Início do processamento de resultados..." + " " + System.DateTime.Now.ToString());
                }

                //Resultados inseridos na BD são sempre referentes ao mês anterior...
                DateTime dtMonth = DateTime.Now.AddMonths(-1);
                Inquerito.TransporValores(dtMonth.Year, dtMonth.Month);
            }
            catch (Exception ex)
            {
                //Adicionado por RSILVA para adicionar a excepção aos logs de sharepoint
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\JobLogs.txt", true))
                {
                    file.WriteLine("ERRO (Transpor Valores): " + ex.Message + " " + System.DateTime.Now.ToString());
                }
            }
        }
        private void Run_InqueritoNotification(SPSite site)
        {
            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\JobLogs.txt", true))
                {
                    file.WriteLine("Run_InqueritoNotification" + " " + System.DateTime.Now.ToString());
                }

                Inquerito.NewInqueritoNotification(site);
            }
            catch (Exception ex)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\JobLogs.txt", true))
                {
                    file.WriteLine("ERRO (Notificação): " + ex.Message + " " + System.DateTime.Now.ToString());
                }
            }
        }
        public override void Execute(Guid targetInstanceId)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\JobLogs.txt", true))
            {
                file.WriteLine("Iniciando Job..."+ " " + System.DateTime.Now.ToString());
            }
            

            CultureInfo ptCulture = new CultureInfo("pt-PT");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ptCulture;

            try
            {

                SPWebApplication webApplication = this.Parent as SPWebApplication;
                SPContentDatabase contentDb = webApplication.ContentDatabases[targetInstanceId];

                using (SPSite site = contentDb.Sites["/"])
                {
                    //DS: 20110627 - Apifarma decidiu que a transposição dos valores deve ocorrer no inicio do periodo de entrega. Processo deverá correr no dia 1 de cada mês.
                    //if (DateTime.Now.Day == DivHospConfig.Inquerito.DiaMesFimPrazoEntrega+1)
                    if (DateTime.Now.Day == 1) 
                    {
                        Run_TransporValores();

                        Run_InqueritoNotification(site);
                    }

                }
            }
            catch (Exception ex)
            {
                ////Adicionado por RSILVA para adicionar a excepção aos logs de sharepoint
                //Microsoft.Office.Server.Diagnostics.PortalLog.LogString("Exception – {0} – {1} – {2}","User Friendly Message", ex.Message, ex.StackTrace);

                //LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs, System.Diagnostics.TraceEventType.Error, "DivHospMonthlyJob", "Erro: " + ex.ToString());
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\JobLogs.txt", true))
                {
                    file.WriteLine("ERRO (Execute): " + ex.Message + " " + System.DateTime.Now.ToString());
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = currentCulture;
            }
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\JobLogs.txt", true))
            {
                file.WriteLine("Fim do job " + System.DateTime.Now.ToString());
            }
        }

    }
}

