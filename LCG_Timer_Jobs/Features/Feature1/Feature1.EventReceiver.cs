using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Security;
using Microsoft.SharePoint.Administration;

namespace LCG_Timer_Jobs.Features.Feature1
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("af2be400-26c2-4632-90d8-c4519d89849d")]
    public class Feature1EventReceiver : SPFeatureReceiver
    {
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            #region Install Job Definitions

            SPJobDefinition job = null;
            //SPMinuteSchedule minuteSchedule = null;
            // SPDailySchedule dailySchedule = null;

            job = new DivHospMonthlyJob(((SPSite)properties.Feature.Parent).WebApplication);

            job.Schedule = SPDailySchedule.FromString("daily at 01:00:00"); ;

            job.Update();

            #endregion
        }
        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            #region Uninstall Job Definitions
            foreach (SPJobDefinition job in ((SPSite)properties.Feature.Parent).WebApplication.JobDefinitions)
            {
                if (job.Title.StartsWith("DivHosp_"))
                {
                    job.Delete();
                }
            }
            #endregion
        }
        public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        {
            //throw new NotImplementedException();
        }

        public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        {
            //throw new NotImplementedException();
        }
    }
}
