﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;

namespace LCG.Simposium.PesquisaAvancada
{
    [ToolboxItemAttribute(false)]
    public class PesquisaAvancada : Microsoft.SharePoint.WebPartPages.WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/LCG.Simposium/PesquisaAvancada/PesquisaAvancadaUserControl.ascx";
        private const string defaultSite = "/";
        public PesquisaAvancada()
        {
            this.AllowEdit = false;
            this.AllowConnect = false;
            this.ChromeState = PartChromeState.Normal;
            this.ChromeType = PartChromeType.None;
            this.ExportMode = WebPartExportMode.All;
        }

        private string siteUrl;
        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("SiteUrl")]
        [Description("Url Site Simposio")]
        [DefaultValue(defaultSite)]
        [Personalizable]
        [WebBrowsable]
        public string SiteUrl
        {
            get
            {
                return siteUrl;
            }
            set
            {
                siteUrl = value;
            }
        }
        protected override void CreateChildControls()
        {
            try
            {
                if (String.IsNullOrEmpty(SiteUrl))
                {
                    this.Controls.Clear();
                    this.Controls.Add(new LiteralControl("Missing Webpart Property 'SiteUrl'"));
                }
                else
                {

                    PesquisaAvancadaUserControl _childControl = Page.LoadControl(_ascxPath) as PesquisaAvancadaUserControl;
                    if (_childControl != null)
                    {
                        _childControl.SiteUrl = SiteUrl;
                        Controls.AddAt(0, _childControl);
                    }
                }
            }
            catch (Exception ex)
            {
                //Display any error that comes up
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
            }
        }
    }
}

