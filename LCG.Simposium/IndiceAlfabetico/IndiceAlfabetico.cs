﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebPartPages;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI;
using System.Web.UI.WebControls;
using Convex.Clientes.Apifarma.Simposium.Controls;
using System.ComponentModel;

namespace LCG.Simposium.IndiceAlfabetico
{
    [ToolboxItemAttribute(false)]
    public class IndiceAlfabetico : System.Web.UI.WebControls.WebParts.WebPart

    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/LCG.Simposium/IndiceAlfabetico/IndiceAlfabeticoUserControl.ascx";
        private const string defaultSite = "/";
        public IndiceAlfabetico()
        {
            this.AllowEdit = false;
            this.AllowConnect = false;
            this.ChromeState = PartChromeState.Normal;
            this.ChromeType = PartChromeType.None;
            this.ExportMode = WebPartExportMode.All;
        }

        private string siteUrl;
        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("SiteUrl")]
        [Description("Url Site Simposio")]
        [DefaultValue(defaultSite)]
        [Personalizable]
        [WebBrowsable]
        public string SiteUrl
        {
            get
            {
                return siteUrl;
            }
            set
            {
                siteUrl = value;
            }
        }
        protected override void CreateChildControls()
        {
            try
            {
                if (String.IsNullOrEmpty(SiteUrl))
                {
                    this.Controls.Clear();
                    this.Controls.Add(new LiteralControl("Missing Webpart Property 'SiteUrl'"));
                }
                else
                {
                    IndiceAlfabeticoUserControl _childControl = Page.LoadControl(_ascxPath) as IndiceAlfabeticoUserControl;
                    if (_childControl != null)
                    {
                        _childControl.SiteUrl = SiteUrl;

                        Controls.AddAt(0, _childControl);
                    }
                }
            }
            catch (Exception ex)
            {
                //Display any error that comes up
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
            }
        }
    }
}
