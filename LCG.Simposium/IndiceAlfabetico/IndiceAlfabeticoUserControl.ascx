﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SPWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndiceAlfabeticoUserControl.ascx.cs" Inherits="LCG.Simposium.IndiceAlfabetico.IndiceAlfabeticoUserControl" %>

<style type="text/css">
       .LetraAlfabeto
    {
    	padding-right: 4px; 
    	text-decoration: underline !important;
    	font-size: 11pt;
    }
</style>
<table cellpadding="3" cellspacing="0" border="0" width="100%">
    <tr>
        <td align="left">
            <div class="list_thumbs_associado_header">
                <asp:PlaceHolder ID="LetterPlaceHolder" runat="server"></asp:PlaceHolder>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <SPWebControls:SPGridView runat="server" ID="SPGridViewResults" AutoGenerateColumns="false"></SPWebControls:SPGridView>
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:HyperLink ID="HLPrint" Visible="false" NavigateUrl="javascript:window.print()" ToolTip="Imprimir" runat="server">
            <asp:Literal ID="Literal9" runat="server" Text="<%$Resources:Simposium, lbl_print%>"></asp:Literal></asp:HyperLink>
            
        </td>
    </tr>
</table>

<asp:Literal ID="LiteralMsg" runat="server"></asp:Literal>