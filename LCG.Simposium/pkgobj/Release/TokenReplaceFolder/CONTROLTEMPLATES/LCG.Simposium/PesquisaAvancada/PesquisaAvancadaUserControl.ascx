<%@ Assembly Name="LCG.Simposium, Version=1.0.0.0, Culture=neutral, PublicKeyToken=4678421964c8b47c" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register TagPrefix="SharePoint" Assembly="Microsoft.SharePoint,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" Namespace="Microsoft.SharePoint.WebControls" %>
<%@ Register Tagprefix="SPWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PesquisaAvancadaUserControl.ascx.cs" Inherits="LCG.Simposium.PesquisaAvancada.PesquisaAvancadaUserControl" %>

<table align="center" border="0" cellpadding="3" cellspacing="0">
    <tr>
        <td align="center">
            <asp:Literal ID="Literal8" runat="server" Text="<%$Resources:Simposium, lbl_advsearch_search%>"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td width="150" align="right">
                        <strong><asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Simposium, lbl_name%>"></asp:Literal>:</strong>
                    </td>
                    <td width="350" align="left">
                        <asp:TextBox ID="TBName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal1" runat="server" Text="<%$Resources:Simposium, lbl_actprincip%>"></asp:Literal>:</strong>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="DDLActiveP" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal3" runat="server" Text="<%$Resources:Simposium, lbl_classif%>"></asp:Literal>:</strong>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="DDLClassification" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal4" runat="server" Text="<%$Resources:Simposium, lbl_lab%>"></asp:Literal>:</strong>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="DDLLabs" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal5" runat="server" Text="<%$Resources:Simposium, lbl_species%>"></asp:Literal>:</strong>
                    </td>
                    <td align="left">
                        <asp:CheckBoxList ID="CBLSpecies" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal6" runat="server" Text="<%$Resources:Simposium, lbl_shapes%>"></asp:Literal>:</strong>
                    </td>
                    <td  align="left">
                        <asp:CheckBoxList ID="CBLTypes" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                 <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal7" runat="server" Text="<%$Resources:Simposium, lbl_codclassif%>"></asp:Literal>:</strong>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="TBCode" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Button ID="Button1" runat="server" Text="Pesquisar" OnClick="Button1_Click" />
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Label ID="LBResults" runat="server"></asp:Label>
        </td>
        
    </tr>
    <tr>
        <td>
            <SPWebControls:SPGridView runat="server" ID="SPGridViewResults" AutoGenerateColumns="false"></SPWebControls:SPGridView>
        </td>
    </tr>
     <tr>
        <td align="center">
            <asp:HyperLink ID="HLPrint" Visible="false" NavigateUrl="javascript:window.print()" ToolTip="Imprimir" runat="server">
            <asp:Literal ID="Literal9" runat="server" Text="<%$Resources:Simposium, lbl_print%>"></asp:Literal></asp:HyperLink>
        </td>
    </tr>
</table>
<asp:Literal ID="LiteralMsg" runat="server"></asp:Literal>
