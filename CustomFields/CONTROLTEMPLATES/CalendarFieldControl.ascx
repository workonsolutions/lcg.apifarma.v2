﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" %>

<SharePoint:RenderingTemplate ID="CalendarFieldControl" runat="server">
  <Template>    
        <script language="javascript" type="text/javascript" src="../_layouts/CVXGeneric.CustomFields/js/jquery-ui.multidatespicker.js"></script>    
        <link rel="stylesheet" type="text/css" href="../_layouts/CVXGeneric.CustomFields/css/mdp.css">
        <div id="page" class="ui-widget ui-widget-content ui-corner-all">        
        <asp:Literal ID="jQueryLiteralCalendar" runat="server"></asp:Literal>
        <div runat="server" ID="jQueryDivCalendar"></div>    
        <div>
            <asp:TextBox ID="jQuerySelectedDates" runat="server" Wrap="true" TextMode="MultiLine" style="display:none;"></asp:TextBox>
        </div>
    </div>  
  </Template>
</SharePoint:RenderingTemplate>
