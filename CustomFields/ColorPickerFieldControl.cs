﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace CustomFields.Sharepoint.WebControls
{
    class ColorPickerFieldControl : BaseFieldControl
    {
        protected HtmlInputControl backgroundInput;
        protected HtmlInputControl foregroundInput;
        protected TextBox tbSampleText;
        private ColorPickerFieldValue fieldValue;
        protected override string DefaultTemplateName
        {
            //Get the RenderingTemplate in the ascx file
            get { return "ColorsPicker"; }
        }
        public override object Value
        {
            get
            {
                EnsureChildControls();
                if (this.ControlMode != SPControlMode.Display)
                {
                    GetValues();
                }
                return fieldValue;
            }
            set
            {
                EnsureChildControls();
                fieldValue = value as ColorPickerFieldValue;
            }
        }
        private void GetValues()
        {
            //set this instances value to whats been selected
            //fieldValue.FontColor = ddlFont.SelectedItem.Text;
            fieldValue.FontColor = foregroundInput.Value;
            //fieldValue.BackgroundColor = ddlBackground.SelectedItem.Text;
            fieldValue.BackgroundColor = backgroundInput.Value;
        }
        private void SetValues()
        {
            //set the controls values to whats currently stored in the field for an existing item
            //ddlFont.SelectedItem.Text = fieldValue.FontColor;
            //ddlBackground.SelectedItem.Text = fieldValue.BackgroundColor;
            //tbSampleText.ForeColor = Color.FromName(fieldValue.FontColor);
            //tbSampleText.BackColor = Color.FromName(fieldValue.BackgroundColor);

            foregroundInput.Value = fieldValue.FontColor;
            backgroundInput.Value = fieldValue.BackgroundColor;

            tbSampleText.Style.Add("color", foregroundInput.Value);
            tbSampleText.Style.Add("background-color", backgroundInput.Value);
        }
        protected override void OnInit(EventArgs e)
        {
            //either get the value from an existing item or set it as a new value
            if (ControlMode == SPControlMode.Edit || ControlMode == SPControlMode.Display)
            {
                if (this.ListItemFieldValue != null)
                    fieldValue = this.ListItemFieldValue as ColorPickerFieldValue;
                else
                    fieldValue = new ColorPickerFieldValue();
            }
            if (ControlMode == SPControlMode.New)
            {
                fieldValue = new ColorPickerFieldValue();
            }
            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!Page.IsPostBack && ControlMode != SPControlMode.Display)
            {

                ////register scripts to update the preview client side
                //ClientScriptManager cs = this.Page.ClientScript;
                //if (!cs.IsClientScriptBlockRegistered(this.GetType(), "ChangeColors"))
                //{
                //    StringBuilder csText = new StringBuilder();
                //    csText.Append(" <SCRIPT type=text/javascript>");
                //    csText.Append("function changeTextColor(dropdown, textboxID)");
                //    csText.Append("{");
                //    csText.Append("var x = document.getElementById(textboxID);");
                //    csText.Append("var myindex = dropdown.selectedIndex;");
                //    csText.Append("var SelValue = dropdown.options[myindex].value;");
                //    csText.Append("x.style.color = SelValue;");
                //    csText.Append("}");
                //    csText.Append("function changeBackgroundColor(dropdown, textboxID)");
                //    csText.Append("{");
                //    csText.Append("var x = document.getElementById(textboxID);");
                //    csText.Append("var myindex = dropdown.selectedIndex;");
                //    csText.Append("var SelValue = dropdown.options[myindex].value;");
                //    csText.Append("x.style.backgroundColor = SelValue;");
                //    csText.Append("}");
                //    csText.Append("</SCRIPT> ");
                //    cs.RegisterClientScriptBlock(this.GetType(), "ChangeColors", csText.ToString(), false);
                //}
                SetValues();
            }
        }
        protected override void CreateChildControls()
        {
            if (Field == null)
                return;
            base.CreateChildControls();
            if (ControlMode == SPControlMode.Display)
                return;
            //set the local vaiables
            //ddlFont = (DropDownList)TemplateContainer.FindControl("ddlFont");
            //ddlBackground = (DropDownList)TemplateContainer.FindControl("ddlBackground");
            tbSampleText = (TextBox)TemplateContainer.FindControl("tbSampleText");
            foregroundInput = (HtmlInputControl)TemplateContainer.FindControl("foregroundInput");
            backgroundInput = (HtmlInputControl)TemplateContainer.FindControl("backgroundInput");
            if (!Page.IsPostBack)
            {
                foregroundInput.Attributes.Add("OnChange", string.Format("updateSampleColor('{0}', '{1}', '{2}');",
                    foregroundInput.ClientID, backgroundInput.ClientID, tbSampleText.ClientID));

                backgroundInput.Attributes.Add("OnChange", string.Format("updateSampleColor('{0}', '{1}', '{2}');",
                   foregroundInput.ClientID, backgroundInput.ClientID, tbSampleText.ClientID));

                //add attributes to call the javascript functions when we choose different colors from the dropdown box's
                //to update the preview client side
                //string textboxID = this.tbSampleText.ClientID;
                //ddlBackground.Attributes.Add("OnChange", string.Format("changeBackgroundColor(this,'{0}');", textboxID));
                //ddlFont.Attributes.Add("OnChange", string.Format("changeTextColor(this,'{0}');", textboxID));
            }
        }
        //control how the field is rendered in display mode, doesn't overide list view display which is controlled
        //by the DisplayPattern in the fldtypes xml file
        protected override void RenderFieldForDisplay(HtmlTextWriter output)
        {
            StringBuilder sbldr = new StringBuilder();
            sbldr.Append(" Amostra");
            output.Write(sbldr.ToString());
        }
    }
}
