﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Security;
using System.Security.Permissions;
using CustomFields.Sharepoint.WebControls;

namespace CustomFields.Sharepoint
{
    class ColorPickerField : SPFieldMultiColumn
    {
        //required constructor
        public ColorPickerField(SPFieldCollection fields, string fieldName)
            : base(fields, fieldName)
        {
        }
        //required constructor
        public ColorPickerField(SPFieldCollection fields, string typeName, string displayName)
            : base(fields, typeName, displayName)
        {
        }
        //tell it which control to use for rendering this field
        public override BaseFieldControl FieldRenderingControl
        {

            [SharePointPermission(SecurityAction.LinkDemand, ObjectModel = true)]
            get
            {
                BaseFieldControl fieldControl = new ColorPickerFieldControl();
                fieldControl.FieldName = this.InternalName;
                return fieldControl;
            }
        }
        //returns the value of this field
        public override object GetFieldValue(string value)
        {
            if (String.IsNullOrEmpty(value))
                return null;
            return new ColorPickerFieldValue(value);
        }
    }
}
