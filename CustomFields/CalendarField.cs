﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Security;
using System.Security.Permissions;
using CustomFields.Sharepoint.WebControls;

namespace CustomFields.Sharepoint
{
    class CalendarField : SPFieldMultiLineText
    {
        public CalendarField(SPFieldCollection fields, string fieldName)
            : base(fields, fieldName)
        { }

        public CalendarField(SPFieldCollection fields, string typeName, string displayName)
            : base(fields, typeName, displayName)
        { }

        public override BaseFieldControl FieldRenderingControl
        {
            [SharePointPermission(SecurityAction.LinkDemand, ObjectModel = true)]
            get
            {
                CalendarFieldControl ctr = new CalendarFieldControl();
                ctr.FieldName = this.InternalName;
                return ctr;
            }
        }


    }
}