﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;

namespace CustomFields.Sharepoint
{
    class ColorPickerFieldValue : SPFieldMultiColumnValue
    {
        //how many properties are we storing
        private const int numberOfFields = 2;
        //required constructor
        public ColorPickerFieldValue()
            : base(numberOfFields)
        {
        }
        //required constructor
        public ColorPickerFieldValue(string value)
            : base(value)
        {
        }

        //property for storing the font color to use
        public string FontColor
        {
            get
            {
                if (string.IsNullOrEmpty(this[0]))
                    //return "Black";
                    return "FFFFFF";
                else
                    return this[0];
            }
            set { this[0] = value; }
        }
        //property for storing the background color to use
        public string BackgroundColor
        {
            get
            {
                if (string.IsNullOrEmpty(this[1]))
                    //return "White";
                    return "000000";
                else
                    return this[1];
            }
            set { this[1] = value; }
        }
    }
}
