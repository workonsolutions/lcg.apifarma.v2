﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;


namespace CustomFields.Sharepoint.WebControls
{
    class EmailFieldControl : BaseFieldControl
    {
        protected TextBox EmailTextBox;
        protected Label EmailValidationText;

        protected override string DefaultTemplateName
        {
            get
            {
                return "EmailFieldControl";
            }
        }

        public override object Value
        {
            get
            {
                EnsureChildControls();

                if (String.IsNullOrEmpty(EmailTextBox.Text))
                    return null;
                else
                    return EmailTextBox.Text;
            }
            set
            {
                EnsureChildControls();
                string codeValue = this.ItemFieldValue as string;
                if (codeValue != null)
                {
                    EmailTextBox.Text = codeValue;
                }
            }
        }

        public override void Validate()
        {
            base.Validate();

            if (Field.Required && (Value == null || Value.ToString().Length == 0))
            {
                this.ErrorMessage = "Tem de especificar um valor para este campo necessário.";
                IsValid = false;
                return;
            }
            else
            {
                if (Value == null || Value.ToString() == string.Empty)
                {
                    return;
                }
                if (!isEmail(Value.ToString()))
                {
                    this.ErrorMessage = Value.ToString() + " não é um endereço de email válido.";
                    IsValid = false;
                    return;
                }

            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void CreateChildControls()
        {
            try
            {
                base.CreateChildControls();

                if (this.ControlMode == SPControlMode.New || this.ControlMode == SPControlMode.Edit)
                {
                    this.EmailTextBox = (TextBox)TemplateContainer.FindControl("EmailTextBox");
                    this.EmailValidationText = (Label)TemplateContainer.FindControl("EmailValidationText");
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                EmailValidationText.Visible = true;
                EmailValidationText.Text = ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputEmail"></param>
        /// <returns></returns>
        private bool isEmail(string inputEmail)
        {
            Regex re = new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$", RegexOptions.IgnoreCase);
            return re.IsMatch(inputEmail);
        }
    }
}