﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Security;
using System.Security.Permissions;
using CustomFields.Sharepoint.WebControls;

namespace CustomFields.Sharepoint
{
    class EmailField : SPFieldText
    {
        public EmailField(SPFieldCollection fields, string fieldName)
            : base(fields, fieldName)
        { }

        public EmailField(SPFieldCollection fields, string typeName, string displayName)
            : base(fields, typeName, displayName)
        { }

        public override BaseFieldControl FieldRenderingControl
        {
            [SharePointPermission(SecurityAction.LinkDemand, ObjectModel = true)]
            get
            {
                EmailFieldControl ctr = new EmailFieldControl();
                ctr.FieldName = this.InternalName;
                return ctr;
            }
        }


    }
}