<%@ Assembly Name="CustomFields.Sharepoint, Version=1.0.0.0, Culture=neutral, PublicKeyToken=0970ae1b710a2637" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" %>

<%-- jscolor --%> 
<SharePoint:RenderingTemplate ID="ColorsPicker" runat="server">
    <Template>
        <script type="text/javascript" src="/_layouts/CVXGeneric.CustomFields/js/jscolor/jscolor.js"></script>
        <script type="text/javascript">
            function updateSampleColor(foregroundInputId, backgroundInputId, sampleInputId) {
                var foregroundInput = document.getElementById(foregroundInputId);
                var backgroundInput = document.getElementById(backgroundInputId);
                var sampleInput = document.getElementById(sampleInputId);

                if (foregroundInput == null || backgroundInput == null || sampleInput == null) {
                    alert('ColorPicker error: Missing elements.');
                    return;
                }

                sampleInput.style.color = foregroundInput.value;
                sampleInput.style.backgroundColor = backgroundInput.value;

            }
        </script>
        <table>
            <tr>
                <td class="ms-formlabel">
                    Cor Texto
                </td>
                <td>
                    <input type="text" runat="server" id="foregroundInput" class="color" value="#000000" />
                </td>
            </tr>
            <tr>
                <td class="ms-formlabel">
                    Cor Fundo
                </td>
                <td>
                    <input type="text" runat="server" id="backgroundInput" class="color" value="#FFFFFF" />
                </td>
            </tr>
        </table>
        <br />
        <asp:TextBox ID="tbSampleText" runat="server" Font-Bold="True" ReadOnly="True">Amostra</asp:TextBox>
    </Template>
</SharePoint:RenderingTemplate>

