﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomFieldsV4._0.Sharepoint
{
    class NumeroAssociadoField : SPFieldText
    {
        public NumeroAssociadoField(SPFieldCollection fields, string fieldName)
            : base(fields, fieldName)
        { }

        public NumeroAssociadoField(SPFieldCollection fields, string typeName, string displayName)
            : base(fields, typeName, displayName)
        { }

        public override string DefaultValue
        {
            get { return "0000"; }
        }
    }
}
