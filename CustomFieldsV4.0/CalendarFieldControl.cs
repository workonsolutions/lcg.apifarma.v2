﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace CustomFieldsV4._0.Sharepoint.WebControls
{
    class CalendarFieldControl : BaseFieldControl
    {
        protected System.Web.UI.WebControls.TextBox jQuerySelectedDates;
        protected Literal jQueryLiteralCalendar;
        protected HtmlGenericControl jQueryDivCalendar;

        protected override string DefaultTemplateName
        {
            get
            {
                return "CalendarFieldControl";
            }
        }

        public override object Value
        {
            get
            {
                EnsureChildControls();

                if (String.IsNullOrEmpty(jQuerySelectedDates.Text))
                    return null;
                else
                    return jQuerySelectedDates.Text;
            }
            set
            {
                EnsureChildControls();
                string codeValue = this.ItemFieldValue as string;
                if (codeValue != null)
                {
                    jQuerySelectedDates.Text = codeValue;
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (this.ControlMode == SPControlMode.New || this.ControlMode == SPControlMode.Edit)
            {
                string codeValue = this.ItemFieldValue as string;

                if (Page.IsPostBack && !String.IsNullOrEmpty(jQuerySelectedDates.Text))
                    codeValue = jQuerySelectedDates.Text;

                string datesAdded = string.Empty;

                if (!String.IsNullOrEmpty(codeValue))
                {
                    datesAdded = codeValue.Replace(", ", "','");
                }

                StringBuilder sb = new StringBuilder();
                sb.Append(@"<script type=""text/javascript"">
                            $(function() {			
				                $(""#" + jQueryDivCalendar.ClientID + @""").multiDatesPicker({					            
                                onSelect: function(date) {
						            var arrayDates = $(this).multiDatesPicker(""getDates"");						
						            var builderDates = """";   
                                    var firstDate = true;                                 
						            for(var i=0; i<arrayDates.length; i++) {
                                        if(firstDate) {
								            builderDates = builderDates + arrayDates[i];
								            firstDate = false;
							            }
							            else {
								            builderDates = builderDates + "", "" + arrayDates[i];
							            }
						            }
						            $(""#" + jQuerySelectedDates.ClientID + @""").val(builderDates);						
					            },
					            monthNames: [""Janeiro"",""Fevereiro"",""Março"",""Abril"",""Maio"",""Junho"",""Julho"",""Agosto"",""Setembro"",""Outubro"",""Novembro"",""Dezembro""],
					            dayNamesMin: [""D"",""S"",""T"",""Q"",""Q"",""S"",""S""],
					            numberOfMonths: [1, 3]");

                if (!String.IsNullOrEmpty(datesAdded))
                    sb.Append(@", addDates: ['" + datesAdded + @"']");
                sb.Append(@"});	});</script>");

                jQueryLiteralCalendar.Text = sb.ToString();
            }
        }
        protected override void CreateChildControls()
        {
            try
            {
                base.CreateChildControls();

                if (this.ControlMode == SPControlMode.New || this.ControlMode == SPControlMode.Edit)
                {
                    this.jQuerySelectedDates = (System.Web.UI.WebControls.TextBox)TemplateContainer.FindControl("jQuerySelectedDates");
                    this.jQueryLiteralCalendar = (Literal)TemplateContainer.FindControl("jQueryLiteralCalendar");
                    this.jQueryDivCalendar = (HtmlGenericControl)TemplateContainer.FindControl("jQueryDivCalendar");
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                jQueryLiteralCalendar.Text = ex.Message + Environment.NewLine + ex.StackTrace;
            }
        }
    }
}