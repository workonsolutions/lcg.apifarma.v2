﻿using Microsoft.SharePoint.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace CustomFieldsV4._0.Sharepoint
{
    class NumberAssociadoFieldControl : TextField
    {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        public override object ItemFieldValue
        {
            get
            {
                return "teste";
            }
            set
            {
                base.ItemFieldValue = "teste";
            }
        }

        public override object Value
        {
            get
            {
                return "teste";
            }
            set
            {
                base.Value = "teste";
            }
        }

        //protected override void CreateChildControls()
        //{
        //    base.CreateChildControls();

        //    tbxNrAssociado = new TextBox();

        //    if (this.ControlMode == SPControlMode.Edit || this.ControlMode == SPControlMode.New)
        //    {

        //        tbxNrAssociado.Text = "teste";

        //        this.ItemFieldValue = tbxNrAssociado.Text;
        //    }
        //}
    }
}
