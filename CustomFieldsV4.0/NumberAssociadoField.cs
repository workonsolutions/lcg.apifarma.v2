﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Security;
using Microsoft.SharePoint.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;

namespace CustomFieldsV4._0.Sharepoint
{
    class NumberAssociadoField : SPFieldText
    {

        public NumberAssociadoField(SPFieldCollection fields, string fieldName)
            : base(fields, fieldName)
        { }

        public NumberAssociadoField(SPFieldCollection fields, string typeName, string displayName)
            : base(fields, typeName, displayName)
        { }

        public override object DefaultValueTyped
        {
            get
            {
                { return "0000"; }
            }
        }
        public override BaseFieldControl FieldRenderingControl
        {
            [SharePointPermission(SecurityAction.LinkDemand, ObjectModel = true)]
            get
            {
                NumberAssociadoFieldControl ctr = new NumberAssociadoFieldControl();
                ctr.FieldName = this.InternalName;
                return ctr;
            }
        }
    }
}
