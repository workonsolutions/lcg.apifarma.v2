﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Convex.Sharepoint.Foundation.Extensions;

namespace Webparts.Destaque
{
    [ToolboxItemAttribute(false)]
    public class Destaque : Microsoft.SharePoint.WebPartPages.WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/Destaque/DestaqueUserControl.ascx";

        private bool _error = false;
        private string _myProperty = null;

        private string imageLibName = null;
        private const string sTitle = "Title";
        private const string sline2 = "Linha2";
        private const string sUrl = "Url";
        private const string sActive = "Activo";
        private int destaquesLimit = 1;

        [Personalizable(PersonalizationScope.Shared)]
        [WebBrowsable(true)]
        [System.ComponentModel.Category("My Property Group")]
        [WebDisplayName("MyProperty")]
        [WebDescription("Meaningless Property")]
        public string MyProperty
        {
            get
            {
                if (_myProperty == null)
                {
                    _myProperty = "Hello SharePoint";
                }
                return _myProperty;
            }
            set { _myProperty = value; }
        }

        public int DestaquesLimit
        {
            get { return destaquesLimit; }
            set { destaquesLimit = value; }
        }

        public string ImageLibName
        {
            get { return imageLibName; }
            set { imageLibName = value; }
        }

        private SPList GetList(SPWeb web, string listTitle)
        {
            try
            {
                return web.Lists[listTitle];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Destaque()
        {
            this.ExportMode = WebPartExportMode.All;
        }

        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        protected override void CreateChildControls()
        {
            if (!_error)
            {
                try
                {
                    //only render the control when the page is in display mode
                    if (SPContext.Current.FormContext.FormMode == SPControlMode.Display)
                    {
                        if (string.IsNullOrEmpty(this.imageLibName))
                        {
                            LiteralControl html = new LiteralControl("Click <a href=\"javascript:MSOTlPn_ShowToolPane2Wrapper('Edit', this, '" + this.ID + "')\">open tool pane</a> to set webpart properties");
                            this.Controls.Add(html);
                        }
                        else
                        {
                            SPWeb rootWeb = SPContext.Current.Site.RootWeb;
                            StringBuilder sb = new StringBuilder();
                            SPList list = GetList(rootWeb, ImageLibName);
                            if (list != null)
                            {
                                //query list to get sorted items
                                SPQuery oQuery = new SPQuery();
                                StringBuilder sbQuery = new StringBuilder();
                                //sbQuery.Append("<Where><And><And><IsNotNull><FieldRef Name='" + sTitle + "' /></IsNotNull><IsNotNull><FieldRef Name='" + sUrl + "' /></IsNotNull></And><Eq><FieldRef Name='" + sActive + "' /><Value Type=\"Boolean\">1</Value></Eq></And></Where><OrderBy><FieldRef Name='" + sOrder + "' Ascending='TRUE' /></OrderBy>");
                                sbQuery.Append("<Where><Eq><FieldRef Name='" + sActive + "' /><Value Type=\"Boolean\">1</Value></Eq></Where><OrderBy><FieldRef Name='" + SPBuiltInFieldId.Modified + "' Ascending='FALSE' /></OrderBy>");
                                oQuery.RowLimit = (uint)destaquesLimit;
                                oQuery.Query = sbQuery.ToString();
                                SPListItemCollection sortedList = list.GetItems(oQuery);
                                sb.Append("<div class=\"eyecandy\">");


                                for (int i = 0; i < sortedList.Count; i++)
                                //foreach (SPListItem listItem in sortedList)
                                {
                                    SPListItem listItem = sortedList[i];

                                    string line1 = string.Empty;
                                    string line2 = string.Empty;
                                    string url = string.Empty;
                                    bool active = false;

                                    active = listItem.GetTypedValue<bool>(sActive);

                                    if (active)
                                    {
                                        if (!string.IsNullOrEmpty(listItem.Title))
                                            line1 = listItem.Title;


                                        if (!string.IsNullOrEmpty(listItem.GetTypedValue<string>(sline2)))
                                            line2 = listItem.GetTypedValue<string>(sline2);

                                        url = listItem.GetTypedValue<string>(sUrl);

                                        if (!string.IsNullOrEmpty(line1) || !string.IsNullOrEmpty(line2))
                                        {
                                            sb.Append("<div class=\"eyecandy_text\">");
                                            if (!string.IsNullOrEmpty(line1))
                                                sb.Append(String.Format("<p class=\"eyecandy_text_blue\"><span></span>{0}</p>", line1));
                                            if (!string.IsNullOrEmpty(line2))
                                                sb.Append(String.Format("<p class=\"eyecandy_text_green\"><span></span>{0}</p>", line2));

                                            sb.Append("</div>");
                                        }


                                        sb.Append("<div class=\"eyecandy_spotlight\">");
                                        sb.Append(String.Format("<img alt=\"\" src=\"{0}\" />", url));
                                        sb.Append("</div>");
                                    }
                                }

                                sb.Append("</div>");
                                this.Controls.Add(new LiteralControl(sb.ToString()));
                            }
                            else
                            {
                                this.Controls.Clear();
                                this.Controls.Add(new LiteralControl("The list with the name: '" + ImageLibName + "' cannot be found in RootWeb."));
                                LiteralControl html = new LiteralControl("Click <a href=\"javascript:MSOTlPn_ShowToolPane2Wrapper('Edit', this, '" + this.ID + "')\">open tool pane</a> to set webpart properties");
                                this.Controls.Add(html);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Display any error that comes up
                    this.Controls.Clear();
                    this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
                }
            }
        }

        /// <summary>
        /// Ensures that the CreateChildControls() is called before events.
        /// Use CreateChildControls() to create your controls.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            if (!_error)
            {
                try
                {
                    base.OnLoad(e);
                    this.EnsureChildControls();

                    // Your code here...
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        /// <summary>
        /// Clear all child controls and add an error message for display.
        /// </summary>
        /// <param name="ex"></param>
        private void HandleException(Exception ex)
        {
            this._error = true;
            this.Controls.Clear();
            this.Controls.Add(new LiteralControl(ex.Message));
        }
    }
}
