﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.SharePoint.Publishing.Navigation;
using System.Text;

namespace Webparts.LeftNavigationMenu
{
    [ToolboxItemAttribute(false)]
    public class LeftNavigationMenu : Microsoft.SharePoint.WebPartPages.WebPart
    {
        public LeftNavigationMenu()
        {
            this.AllowEdit = false;
            this.AllowConnect = false;
            this.ChromeState = PartChromeState.Normal;
            this.ChromeType = PartChromeType.None;
            this.ExportMode = WebPartExportMode.All;
        }

        private List<string> alwaysOpenSites = null;
        public List<string> AlwaysOpenSites
        {
            get
            {
                if (alwaysOpenSites == null)
                {
                    alwaysOpenSites = new List<string>();
                    if (ConfigurationManager.AppSettings["AlwaysOpenSites"] != null)
                    {
                        string[] split = (ConfigurationManager.AppSettings["AlwaysOpenSites"] as string).Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string s in split)
                        {
                            alwaysOpenSites.Add(s.ToLower());
                        }
                    }
                }
                return alwaysOpenSites;
            }
        }
        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        protected override void CreateChildControls()
        {
            try
            {
                SiteMapNode smnRoot = PortalSiteMapProvider.CurrentNavSiteMapProvider.RootNode;
                SiteMapNode smnCurrent = PortalSiteMapProvider.CurrentNavSiteMapProvider.CurrentNode;

                StringBuilder sb = new StringBuilder();

                sb.Append("<ul>");
                //foreach (SiteMapNode childNode in smnRoot.ChildNodes)
                for (int i = 0; i < smnRoot.ChildNodes.Count; ++i)
                {
                    SiteMapNode childNode = smnRoot.ChildNodes[i];

                    /* Para o caso de sites novos, por defeito, a navegação fica confgiurada para mostrar o actual (no fim).
                    De forma a manter a coerencia e evitar a re-configuração na navegação não vamos renderizar na mesma  */
                    if (i == smnRoot.ChildNodes.Count - 1 && smnRoot != smnCurrent)
                    {
                        if (childNode == smnCurrent)
                            break;
                    }

                    sb.Append("<li>");
                    sb.AppendFormat("<a title=\"{1}\" class=\"vertical_nav_BT\" href=\"{0}\">{1}</a>", childNode.Url, childNode.Title.Trim());

                    if (AlwaysOpenSites.Contains(childNode.Key.ToLower()))
                    {
                        if (childNode.ChildNodes.Count != 0)
                        {
                            sb.Append("<ul style=\"display:block\">");

                            foreach (SiteMapNode subChildNode in childNode.ChildNodes)
                            {
                                sb.AppendFormat("<li><a title=\"{1}\" class=\"vertical_SUBnav_BT\" href=\"{0}\">{1}</a></li>", subChildNode.Url, subChildNode.Title.Trim());
                            }
                            sb.Append("</ul>");
                        }
                    }
                }
                sb.Append("</ul>");
                sb.Append("</div>");
                this.Controls.Add(new LiteralControl(sb.ToString()));

            }
            catch (Exception ex)
            {
                //Display any error that comes up
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
            }
        }
    }
}

