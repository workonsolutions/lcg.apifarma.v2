﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Publishing.Navigation;
using System.Text;
using System.Configuration;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Web.Configuration;
using System.Xml.Serialization;

namespace Webparts.CurrentNavigationLinks
{
    [ToolboxItemAttribute(false)]
    public class CurrentNavigationLinks : Microsoft.SharePoint.WebPartPages.WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/CurrentNavigationLinks/CurrentNavigationLinksUserControl.ascx";

        public CurrentNavigationLinks()
        {
            //this.AllowEdit = false;
            //this.AllowConnect = false;
            //this.ChromeState = PartChromeState.Normal;
            //this.ChromeType = PartChromeType.None;
            //this.ExportMode = WebPartExportMode.All;
        }


        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        protected override void CreateChildControls()
        {
            try
            {
                SiteMapNode smn = PortalSiteMapProvider.CurrentNavSiteMapProvider.CurrentNode;
                if (smn.HasChildNodes)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (SiteMapNode childNode in smn.ChildNodes)
                    {
                        sb.Append("<a class=\"content_box_full_height_link\" href=\"" + childNode.Url + "\">" + childNode.Title.Trim() + "</a><br/><br/>");
                    }
                    this.Controls.Add(new LiteralControl(sb.ToString()));
                }
            }
            catch (Exception ex)
            {
                //Display any error that comes up
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
            }
        }
    }
}
