﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;

namespace Webparts.SimpAlfIndexWebPart
{
    public partial class SimpAlfIndex : System.Web.UI.UserControl
    {
        public string SiteUrl { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

            InitLetterPH();
        }

        private void InitLetterPH()
        {
            const int endLetter = (int)'Z';
            int initLetter = (int)'A';
            while (initLetter <= endLetter)
            {
                char c = (char)initLetter;
                LinkButton lb = new LinkButton();
                //lb.CssClass = "LetraAlfabeto";
                lb.CssClass = "list_thumbs_associado_letter";
                lb.Text = c.ToString();
                lb.ToolTip = c.ToString();
                lb.Click += new EventHandler(Letter_Click);
                LetterPlaceHolder.Controls.Add(lb);
                ++initLetter;
            }
        }

        private const string QueryStr = "<Where><And><Or>" +
            "<BeginsWith><FieldRef Name=\"Title\"/><Value Type=\"Text\">{0}</Value></BeginsWith>" +
            "<BeginsWith><FieldRef Name=\"Title\"/><Value Type=\"Text\">{1}</Value></BeginsWith>" +
            "</Or><Eq><FieldRef Name='_ModerationStatus'/><Value Type='ModStat'>{2}</Value></Eq></And></Where>" + HlprSimp.OrderByTitle;
        private const string QueryStrModDisabled = "<Where><Or>" +
          "<BeginsWith><FieldRef Name=\"Title\"/><Value Type=\"Text\">{0}</Value></BeginsWith>" +
          "<BeginsWith><FieldRef Name=\"Title\"/><Value Type=\"Text\">{1}</Value></BeginsWith>" +
          "</Or></Where>" + HlprSimp.OrderByTitle;
        protected void Letter_Click(object sender, EventArgs e)
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    // implementation details omitted
                    using (SPSite site = new SPSite(SiteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            string text = ((LinkButton)sender).Text;
                            SPList drugsList = web.Lists[SimpLists.Medicamentos];
                            SPQuery query = new SPQuery();
                            if (drugsList.EnableModeration)
                                query.Query = String.Format(QueryStr, text, text.ToLower(), IntResources.ModerationStatusApproved);
                            else
                                query.Query = String.Format(QueryStrModDisabled, text, text.ToLower());
                            SPListItemCollection res = drugsList.GetItems(query);

                            if (res.Count > 0)
                            {
                                HlprSimp.SetDrugsGridValues(res, SPGridViewResults, SiteUrl);
                                HLPrint.Visible = SPGridViewResults.Visible = true;
                            }
                            else
                                HLPrint.Visible = SPGridViewResults.Visible = false;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                LiteralMsg.Text = "Error:" + ex.Message;
            }
        }
    }
}