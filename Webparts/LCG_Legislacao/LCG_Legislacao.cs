﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;

namespace Webparts.LCG_Legislacao
{
    [ToolboxItemAttribute(false)]
    public class LCG_Legislacao : Microsoft.SharePoint.WebPartPages.WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/LCG_Legislacao/LCG_LegislacaoUserControl.ascx";

        #region Fields

        #region Constants

        #endregion Constants



        #endregion Fields

        #region Properties


        private string listaDisposicao;
        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Lista Disposicao")]
        [Description("Nome Lista Disposicao")]
        [DefaultValue("Disposicao")]
        [Personalizable]
        [WebBrowsable]
        public string ListaDisposicao
        {
            get
            {
                return listaDisposicao;
            }
            set
            {
                listaDisposicao = value;
            }
        }

        private string bibliotecaLegislacao;
        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Biblioteca Legislacao")]
        [Description("Nome Biblioteca Legislacao")]
        [DefaultValue("Legislacao")]
        [Personalizable]
        [WebBrowsable]
        public string BibliotecaLegislacao
        {
            get
            {
                return bibliotecaLegislacao;
            }
            set
            {
                bibliotecaLegislacao = value;
            }
        }

        private string bibliotecaLegislacaoTraduzida;
        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Biblioteca Legislacao Traduzida")]
        [Description("Nome Biblioteca Legislacao Traduzida")]
        [DefaultValue("LegislacaoTraduzida")]
        [Personalizable]
        [WebBrowsable]
        public string BibliotecaLegislacaoTraduzida
        {
            get
            {
                return bibliotecaLegislacaoTraduzida;
            }
            set
            {
                bibliotecaLegislacaoTraduzida = value;
            }
        }


        private string web = string.Empty;
        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Web")]
        [Description("Web onde procurar a lista")]
        [Personalizable]
        [WebBrowsable]
        public string Web
        {
            get
            {
                return web;
            }
            set
            {
                web = value;
            }
        }


        #endregion Properties


        public LCG_Legislacao()
        {
            this.ExportMode = WebPartExportMode.All;
            this.ChromeType = PartChromeType.None;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            //get the existing ScriptManager if it exists on the page
            ScriptManager _AjaxManager = ScriptManager.GetCurrent(this.Page);

            if (_AjaxManager == null)
            {
                //create new ScriptManager and EnablePartialRendering
                _AjaxManager = new ScriptManager();
                _AjaxManager.EnablePartialRendering = true;
                _AjaxManager.EnableScriptLocalization = true;

                // Fix problem with postbacks and form actions (DevDiv 55525)
                // Page.ClientScript.RegisterStartupScript(typeof(AJAXSmartPart), this.ID, "_spOriginalFormAction = document.forms[0].action;", true);

                //tag:"form" att:"onsubmit" val:"return _spFormOnSubmitWrapper()" blocks async postbacks after the first one
                //not calling "_spFormOnSubmitWrapper()" breaks all postbacks
                //returning true all the time, somewhat defeats the purpose of the _spFormOnSubmitWrapper() which is to block repetitive postbacks, but it allows MS AJAX Extensions to work properly
                //its a hack that hopefully has minimal effect
                if (this.Page.Form != null)
                {
                    //string formOnSubmitAtt = this.Page.Form.Attributes["onsubmit"];
                    //if (!string.IsNullOrEmpty(formOnSubmitAtt) && formOnSubmitAtt == "return _spFormOnSubmitWrapper();")
                    //{
                    //    this.Page.Form.Attributes["onsubmit"] = "_spFormOnSubmitWrapper();";
                    //}

                    //add the ScriptManager as the first control in the Page.Form
                    //I don't think this actually matters, but I did it to be consistent with how you are supposed to place the ScriptManager when used declaritevly
                    this.Page.Form.Controls.AddAt(0, _AjaxManager);
                }
            }
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            try
            {

                if (String.IsNullOrEmpty(ListaDisposicao))
                {
                    this.Controls.Clear();
                    this.Controls.Add(new LiteralControl("Missing Webpart Property 'ListaDisposicao'"));
                    return;
                }
                if (String.IsNullOrEmpty(BibliotecaLegislacao))
                {
                    this.Controls.Clear();
                    this.Controls.Add(new LiteralControl("Missing Webpart Property 'BibliotecaLegislacao'"));
                    return;
                }

                LCG_LegislacaoUserControl _childControl = Page.LoadControl(_ascxPath) as LCG_LegislacaoUserControl;
                if (_childControl != null)
                {
                    _childControl.LegislacaoLib = BibliotecaLegislacao;
                    _childControl.DisposicaoList = ListaDisposicao;
                    _childControl.LegislacaoTraduzidaLib = BibliotecaLegislacaoTraduzida;
                    Controls.Add(_childControl);
                }
            }
            catch (Exception ex)
            {
                //Display any error that comes up
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
            }
        }


        //protected DropDownList ddlDisposicao1;

        //protected ScriptManager scriptManager1;
        //protected UpdatePanel updatePanelLegislacao;

        protected void LoadControls()
        {

            //SPWeb myWeb = null;
            //bool dispose = false;
            //try
            //{
            //    //scriptManager1 = new ScriptManager();
            //    //scriptManager1.ID = "scriptManager1";
            //    //this.Controls.Add(scriptManager1);

            //    //updatePanelLegislacao = new UpdatePanel();
            //    //updatePanelLegislacao.ID = "updatePanelLegislacao";

            //    //UpdateProgress updateProgressLegislacao1 = new UpdateProgress();
            //    //updateProgressLegislacao1.ID = "updateProgressLegislacao1";
            //    //updateProgressLegislacao1.DisplayAfter = 10;
            //    //updateProgressLegislacao1.DynamicLayout = true;
            //    //updateProgressLegislacao1.AssociatedUpdatePanelID = "updatePanelLegislacao";


            //    ddlDisposicao1 = new DropDownList();
            //    ddlDisposicao1.ID = "ddlDisposicao1";
            //    ddlDisposicao1.AutoPostBack = true;
            //    ddlDisposicao1.SelectedIndexChanged += new EventHandler(ddlDisposicao_SelectedIndexChanged);

            //   // updatePanelLegislacao.ContentTemplateContainer.Controls.Add(ddlDisposicao1);

            //    this.Controls.Add(ddlDisposicao1);

            //    if (String.IsNullOrEmpty(Web))
            //        myWeb = SPContext.Current.Web;
            //    else
            //    {
            //        if (SPContext.Current.Site.RootWeb.Webs[Web].Exists)
            //        {
            //            myWeb = SPContext.Current.Site.RootWeb.Webs[Web];
            //            dispose = true;
            //        }
            //        else
            //        {
            //            this.Controls.Add(new LiteralControl("Web não foi encontrada."));
            //            return;
            //        }
            //    }

            //    SPList listaDisposicao = Utils.VerificaLista(myWeb.Lists, ListaDisposicao);
            //    SPDocumentLibrary bibliotecaLegislacao = (SPDocumentLibrary)Utils.VerificaLista(myWeb.Lists, BibliotecaLegislacao);

            //    string whiteSpaces = "  ".Replace(" ", HttpUtility.HtmlDecode("&nbsp;"));
            //    if (listaDisposicao != null && bibliotecaLegislacao != null)
            //    {

            //        if (SelectedDisposicao == null)
            //        {
            //            var rootItems = from i in listaDisposicao.Items.Cast<SPListItem>()
            //                            orderby i.Title
            //                            where String.IsNullOrEmpty(i.GetFormattedValue("Pai"))
            //                            select i;

            //            foreach (var v in rootItems)
            //            {
            //                ddlDisposicao1.Items.Add(new ListItem(v.Title, v.ID.ToString()));

            //                var childItems = from i in listaDisposicao.Items.Cast<SPListItem>()
            //                                 orderby i.Title
            //                                 where i.GetFormattedValue("Pai") == v.Title
            //                                 select i;

            //                foreach (var child in childItems)
            //                {
            //                    ddlDisposicao1.Items.Add(new ListItem(whiteSpaces + child.Title, child.ID.ToString()));
            //                }

            //            }
            //            if (!Page.IsPostBack)
            //                ddlDisposicao1.Items.Insert(0, new ListItem("", "-1"));
            //        }


            //    }

            //}
            //catch (SPException es)
            //{
            //    this.Controls.Add(new LiteralControl(String.Format("Erro: {0} </br> {1}", es.Message, es.StackTrace)));
            //}
            //finally
            //{
            //    if (myWeb != null && dispose)
            //        myWeb.Dispose();
            //}
        }

        /* void ddlDisposicao_SelectedIndexChanged(object sender, EventArgs e)
         {
             SPWeb myWeb = null;
             bool dispose = false;
             try
             {
                 EnsureChildControls();

                 string idDisposicao = ddlDisposicao1.SelectedValue;

                 if (idDisposicao == "-1") return;

                 if (String.IsNullOrEmpty(Web))
                     myWeb = SPContext.Current.Web;
                 else
                 {
                     if (SPContext.Current.Site.RootWeb.Webs[Web].Exists)
                     {
                         myWeb = SPContext.Current.Site.RootWeb.Webs[Web];
                         dispose = true;
                     }
                     else
                     {
                         this.Controls.Add(new LiteralControl("Web não foi encontrada."));
                         return;
                     }
                 }

                 SPDocumentLibrary bibliotecaLegislacao = (SPDocumentLibrary)Utils.VerificaLista(myWeb.Lists, BibliotecaLegislacao);

                 SPQuery query = new SPQuery();

                 query.Query = String.Format("<Where><Eq><FieldRef Name=\"Disposicao\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq></Where><OrderBy><FieldRef Name=\"Title\" Ascending=\"False\" /></OrderBy>", idDisposicao);

                 SPListItemCollection res = bibliotecaLegislacao.GetItems(query);
                 StringBuilder contentArea = new StringBuilder();

                 contentArea.Append("<div style='width: 100%; padding-top: 15px; margin-left: 5px;'>");

                 foreach (SPListItem legisl in res)
                 {
                    contentArea.AppendLine("<div style='border-bottom: #999999 1px dotted; padding-bottom: 5px; width: 100%; float: left; padding-top: 5px;'>");
                    contentArea.AppendLine(
                             String.Format("<a class='content_title' title='{0}' href='{1}'>{0}</a><p class='content_legisl_descr'>{2}</p>", legisl.Title, SPUrlUtility.CombineUrl(bibliotecaLegislacao.ParentWebUrl, legisl.Url), legisl["Descricao"]));
                    contentArea.AppendLine("</div>");
                   // 

                 }
                 contentArea.Append("</div>");
                 Controls.Add(new LiteralControl(contentArea.ToString()));

                // this.Page.Title = SPContext.Current.ListItem.Title;
             }
             catch (SPException es)
             {
                 this.Controls.Add(new LiteralControl(String.Format("Erro: {0} </br> {1}", es.Message, es.StackTrace)));
             }
             finally
             {
                 if (myWeb != null && dispose)
                     myWeb.Dispose();

             }            
         }
         * */
    }
}
