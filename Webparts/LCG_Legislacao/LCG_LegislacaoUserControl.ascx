﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LCG_LegislacaoUserControl.ascx.cs" Inherits="Webparts.LCG_Legislacao.LCG_LegislacaoUserControl" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlDisposicao1_indexChanged" ID="ddlDisposicao1" runat="server">
        </asp:DropDownList>
        <asp:UpdateProgress DynamicLayout="true" DisplayAfter="10" ID="updatePanelLegislacao" runat="server">
            <ProgressTemplate>
               <span>A carregar...</span>
            </ProgressTemplate>
        </asp:UpdateProgress>
         <asp:Literal runat="server" ID="LiteralInfo" visible="false" Text="badjouras"></asp:Literal>
    </ContentTemplate>
    
</asp:UpdatePanel>