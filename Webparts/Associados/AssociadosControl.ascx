﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssociadosControl.ascx.cs" Inherits="Webparts.Associados.AssociadosControl" %>

<style type="text/css">
       .LetraAlfabeto
    {
    	padding-right: 4px; 
    	text-decoration: underline !important;
    	font-size: 11pt;
    }
</style>
<table cellpadding="3" cellspacing="0" border="0" width="100%">
    <tr>
        <td align="center">
            <asp:PlaceHolder ID="LetterPlaceHolder" runat="server"></asp:PlaceHolder>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Repeater ID="AssociadosRepeater" runat="server">
                <HeaderTemplate>
                    <table border=0>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                             <p class="content_title_sub"><%# DataBinder.Eval(Container.DataItem, "Nome") %></p>
                             <span><%# DataBinder.Eval(Container.DataItem, "Morada") %></span><br />
                             <span><%# DataBinder.Eval(Container.DataItem, "CodPostal") %></span><br />
                             <%# String.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "Telefone") as string) ? String.Empty : "Telefone: " + DataBinder.Eval(Container.DataItem, "Telefone")+ "<br/>"%>
                             <%# String.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "Fax") as string) ? String.Empty : "Fax: " + DataBinder.Eval(Container.DataItem, "Fax") + "<br/>"%>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="height: 0px; width: 100%; margin-bottom: 2px; margin-top: 2px; border-bottom: #999999 1px dotted;"></div>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            
        </td>
    </tr>
</table>

<asp:Literal ID="LiteralMsg" Visible= "false" runat="server"></asp:Literal>