﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Xml.Serialization;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Linq;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Administration;

namespace Webparts.Associados
{
    public partial class AssociadosControl : UserControl
    {
        private void InitLetterPH()
        {
            const int endLetter = (int)'Z';
            int initLetter = (int)'A';
            while (initLetter <= endLetter)
            {
                char c = (char)initLetter;
                LinkButton lb = new LinkButton();
                lb.CssClass = "LetraAlfabeto";
                lb.Text = c.ToString();
                lb.ToolTip = c.ToString();
                lb.Click += new EventHandler(Letter_Click);
                LetterPlaceHolder.Controls.Add(lb);
                ++initLetter;
            }
        }
        protected void Letter_Click(object sender, EventArgs e)
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    // implementation details omitted
                    using (SPSite site = new SPSite(HlpInternet.EntidadesSiteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPList entidades = web.Lists["lists/entidades"];
                            string text = ((LinkButton)sender).Text;

                            StringBuilder caml = new StringBuilder();
                            caml.Append("<Where>");
                            caml.Append("<And>");
                            caml.Append("  <And>");
                            caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq>", SPBuiltInFieldId.ContentType, entidades.ContentTypes["AF_Associado"].Name);
                            caml.AppendFormat("<BeginsWith><FieldRef Name=\"Title\"/><Value Type=\"Text\">{0}</Value></BeginsWith>", text);
                            //caml.Append("   <Or>");
                            //caml.AppendFormat( "<BeginsWith><FieldRef Name=\"Title\"/><Value Type=\"Text\">{0}</Value></BeginsWith>" +
                            //                   "<BeginsWith><FieldRef Name=\"Title\"/><Value Type=\"Text\">{1}</Value></BeginsWith>", text, text.ToLower());
                            //caml.Append("   </Or>");
                            caml.Append("  </And>");
                            caml.Append("   <Or>");
                            /*<Query><Where><Gt><FieldRef Name="AF_DataCancelamento" /><Value IncludeTimeValue="TRUE" Type="DateTime">2010-06-21T12:58:08Z</Value></Gt></Where></Query>*/
                            caml.Append("       <Gt><FieldRef Name=\"AF_DataCancelamento\" /><Value Type=\"DateTime\"><Today/></Value></Gt>");
                            caml.Append("       <IsNull><FieldRef Name=\"AF_DataCancelamento\"/></IsNull>");
                            caml.Append("   </Or>");
                            caml.Append("</And>");
                            caml.Append("</Where><OrderBy><FieldRef Name='Title' /></OrderBy>");

                            SPQuery query = new SPQuery();
                            query.Query = caml.ToString();
                            SPListItemCollection res = entidades.GetItems(query);

                            var associados = from a in res.Cast<SPListItem>()
                                             select new
                                             {
                                                 Nome = a.Title,
                                                 Morada = a.GetFormattedValue("AF_Morada1"),
                                                 CodPostal = a.GetFormattedValue("AF_CodigoPostal"),
                                                 Telefone = a.GetFormattedValue("AF_Telefone"),
                                                 Fax = a.GetFormattedValue("AF_Fax")
                                             };

                            AssociadosRepeater.DataSource = associados;
                            AssociadosRepeater.DataBind();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                LiteralMsg.Text = "Error:" + ex.Message;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                InitLetterPH();

            }
            catch (Exception ex)
            {
                LiteralMsg.Text = String.Format("Erro: {0} </br> {1}", ex.Message, ex.StackTrace);
                LiteralMsg.Visible = true;
                //this.Controls.Add(new LiteralControl(String.Format("Erro: {0} </br> {1}", es.Message, es.StackTrace)));
            }
            finally
            {

            }
        }

        protected void test()
        {

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite rootSiteCollection = new SPSite(SPContext.Current.Site.Url))
                {
                    SPWebApplication webApplication = rootSiteCollection.WebApplication;
                    SPSiteCollection siteCollections = webApplication.Sites;

                    foreach (SPSite siteCollection in siteCollections)
                    {
                        try
                        {
                            // Do stuff
                        }
                        finally
                        {
                            if (siteCollection != null)
                                siteCollection.Dispose();
                        }
                    }
                }
            });

        }
    }
}
