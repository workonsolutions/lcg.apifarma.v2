﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace Webparts.Associados
{
    [ToolboxItemAttribute(false)]
    public class Associados : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/Associados/AssociadosControl.ascx";

        public Associados()
        {
            this.ExportMode = WebPartExportMode.All;
            this.ChromeType = PartChromeType.None;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            //get the existing ScriptManager if it exists on the page
            ScriptManager _AjaxManager = ScriptManager.GetCurrent(this.Page);

            if (_AjaxManager == null)
            {
                //create new ScriptManager and EnablePartialRendering
                _AjaxManager = new ScriptManager();
                _AjaxManager.EnablePartialRendering = true;
                _AjaxManager.EnableScriptLocalization = true;

                // Fix problem with postbacks and form actions (DevDiv 55525)
                // Page.ClientScript.RegisterStartupScript(typeof(AJAXSmartPart), this.ID, "_spOriginalFormAction = document.forms[0].action;", true);

                //tag:"form" att:"onsubmit" val:"return _spFormOnSubmitWrapper()" blocks async postbacks after the first one
                //not calling "_spFormOnSubmitWrapper()" breaks all postbacks
                //returning true all the time, somewhat defeats the purpose of the _spFormOnSubmitWrapper() which is to block repetitive postbacks, but it allows MS AJAX Extensions to work properly
                //its a hack that hopefully has minimal effect
                if (this.Page.Form != null)
                {
                    //string formOnSubmitAtt = this.Page.Form.Attributes["onsubmit"];
                    //if (!string.IsNullOrEmpty(formOnSubmitAtt) && formOnSubmitAtt == "return _spFormOnSubmitWrapper();")
                    //{
                    //    this.Page.Form.Attributes["onsubmit"] = "_spFormOnSubmitWrapper();";
                    //}

                    //add the ScriptManager as the first control in the Page.Form
                    //I don't think this actually matters, but I did it to be consistent with how you are supposed to place the ScriptManager when used declaritevly
                    this.Page.Form.Controls.AddAt(0, _AjaxManager);
                }
            }
        }


        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            try
            {


                AssociadosControl _childControl = Page.LoadControl(_ascxPath) as AssociadosControl;
                if (_childControl != null)
                {

                    Controls.Add(_childControl);
                }
            }
            catch (Exception ex)
            {
                //Display any error that comes up
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
            }
        }
    }
}
