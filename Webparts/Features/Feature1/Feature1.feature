﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="0a8008fb-901f-4c2d-a017-25711de70eaa" alwaysForceInstall="true" description="All webparts for Apifarma Website" featureId="0a8008fb-901f-4c2d-a017-25711de70eaa" imageUrl="" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="Apifarma WebParts" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="b56b84b2-1ae9-4275-8ea8-0332d1f2de61" />
    <projectItemReference itemId="07d4954d-cace-4a44-b86e-9a6e88000744" />
    <projectItemReference itemId="46af2416-54a2-4bc2-9007-d0dc05f396e2" />
    <projectItemReference itemId="d3c03e19-403c-4bf1-8070-06898b6ad460" />
    <projectItemReference itemId="5bf38fa5-822b-4538-b733-9cfa4f8181ab" />
    <projectItemReference itemId="456b796a-c630-4c8e-bfc5-1842bf5cdd8e" />
    <projectItemReference itemId="680e8ea9-b74e-4780-b710-de89d980104e" />
    <projectItemReference itemId="69cdb3ad-4e93-4fa7-be7f-b0c5479ec618" />
    <projectItemReference itemId="7078868b-7c25-4a74-9916-db84a9933ce6" />
    <projectItemReference itemId="152930b1-d2e6-4fb8-b299-78f29fa9ebb7" />
    <projectItemReference itemId="7dd41a60-f711-4ca7-bdb4-0bb44d09fd12" />
    <projectItemReference itemId="f58f0df5-08f3-4f4c-80b7-db9d5189f8fd" />
    <projectItemReference itemId="a883227d-e0cf-4b67-bd7d-7cf9358027d9" />
    <projectItemReference itemId="d3284f20-f881-4fec-9c22-b27dd2480e5c" />
    <projectItemReference itemId="83a21089-8927-49b9-a619-0c247edfcd00" />
    <projectItemReference itemId="5062bf58-79e9-4a0f-8803-157e2c2ceaa0" />
    <projectItemReference itemId="55ff7d97-6d38-4179-aece-dcc7c9579d44" />
    <projectItemReference itemId="042511bd-f98e-41ca-8432-a737be825fa7" />
  </projectItems>
</feature>