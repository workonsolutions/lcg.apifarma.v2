﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Webparts.HomeBanner
{
    public partial class UC_FlashPlayer : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public string BannerUrl
        {
            get;
            set;
        }

        public int BannerHeight
        {
            get;
            set;
        }

        public int BannerWidth
        {
            get;
            set;
        }
    }
}
