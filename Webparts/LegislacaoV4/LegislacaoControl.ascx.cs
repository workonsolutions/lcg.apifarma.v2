﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Web;
using System.Text;
using Microsoft.SharePoint.Utilities;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Xml.Serialization;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using Convex.Sharepoint.Foundation.Extensions;

namespace Webparts.Legislacao
{
    public partial class LegislacaoControl : UserControl
    {
        public string LegislacaoLib { get; set; }
        public string LegislacaoTraduzidaLib { get; set; }
        public string DisposicaoList { get; set; }
        public string Web { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
                  {
                      LiteralInfo.Visible = false;
                      if (!IsPostBack)
                      {
                          SPWeb myWeb = null;
                          bool dispose = false;
                          try
                          {


                              // this.Controls.Add(ddlDisposicao1);

                              if (String.IsNullOrEmpty(Web))
                                  myWeb = SPContext.Current.Web;
                              else
                              {
                                  if (SPContext.Current.Site.RootWeb.Webs[Web].Exists)
                                  {
                                      myWeb = SPContext.Current.Site.RootWeb.Webs[Web];
                                      dispose = true;
                                  }
                                  else
                                  {
                                      LiteralInfo.Text = "Web não foi encontrada.";
                                      LiteralInfo.Visible = true;
                                      // this.Controls.Add(new LiteralControl("Web não foi encontrada."));
                                      return;
                                  }
                              }

                              SPList listaDisposicao = Utils.VerificaLista(myWeb.Lists, DisposicaoList);


                              SPDocumentLibrary bibliotecaLegislacao = (SPDocumentLibrary)Utils.VerificaLista(myWeb.Lists, LegislacaoLib);

                              string whiteSpaces = "    ".Replace(" ", HttpUtility.HtmlDecode("&nbsp;"));
                              if (listaDisposicao != null && bibliotecaLegislacao != null)
                              {
                                  var rootItems = from i in listaDisposicao.Items.Cast<SPListItem>()
                                                  orderby i.Title
                                                  where String.IsNullOrEmpty(i.GetFormattedValue("Pai")) || i.GetFormattedValue("Pai") == i.Title
                                                  select i;

                                  //List<SPListItem> rootItems = (from SPListItem x in listaDisposicao.Items
                                  //                              orderby x.Title
                                  //                              where String.IsNullOrEmpty(x["Pai"].ToString())
                                  //                              select x).ToList();


                                  
                                  foreach (var v in rootItems)
                                  {
                                      ddlDisposicao1.Items.Add(new ListItem(v.Title.ToString(), v.ID.ToString()));

                                      var childItems = from i in listaDisposicao.Items.Cast<SPListItem>()
                                                       orderby i.Title
                                                       where i.GetFormattedValue("Pai") == v.Title && i.ID != v.ID
                                                       select i;

                                      //List<SPListItem> childItems = (from SPListItem item in listaDisposicao.Items

                                      //                               where item.GetFormattedValue("Pai") == v.GetFormattedValue("Title") && item.ID != v.ID

                                      //                               select item).ToList();

                                      foreach (var child in childItems)
                                      {
                                          ddlDisposicao1.Items.Add(new ListItem(whiteSpaces + child.Title.ToString(), child.ID.ToString()));
                                      }
                                      

                                  }
                                  ddlDisposicao1.Items.Insert(0, new ListItem("", "-1"));
                                  

                              }

                              

                          }
                          catch (Exception ex)
                          {
                              LiteralInfo.Text = String.Format("Erro: {0} </br> {1}", ex.Message, ex.StackTrace);
                              LiteralInfo.Visible = true;
                              //this.Controls.Add(new LiteralControl(String.Format("Erro: {0} </br> {1}", es.Message, es.StackTrace)));
                          }
                          finally
                          {
                              if (myWeb != null && dispose)
                                  myWeb.Dispose();
                          }
                      }
                  });

        }



        protected void ddlDisposicao1_indexChanged(object sender, EventArgs e)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
                  {
                      SPWeb myWeb = null;
                      bool dispose = false;
                      try
                      {
                          string idDisposicao = ddlDisposicao1.SelectedValue;

                          if (idDisposicao == "-1") return;

                          if (String.IsNullOrEmpty(Web))
                              myWeb = SPContext.Current.Web;
                          else
                          {
                              if (SPContext.Current.Site.RootWeb.Webs[Web].Exists)
                              {
                                  myWeb = SPContext.Current.Site.RootWeb.Webs[Web];
                                  dispose = true;
                              }
                              else
                              {
                                  LiteralInfo.Text = "Web não foi encontrada.";
                                  LiteralInfo.Visible = true;
                              }
                          }

                          SPDocumentLibrary bibliotecaLegislacao = (SPDocumentLibrary)Utils.VerificaLista(myWeb.Lists, LegislacaoLib);

                          //SPQuery query = new SPQuery();

                          //query.Query = String.Format("<Where><Eq><FieldRef Name=\"Disposicao\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq></Where><OrderBy><FieldRef Name=\"Data_x0020_Diploma\" Ascending=\"False\" /></OrderBy>", idDisposicao);
                          //query.ViewAttributes = "Scope=\"RecursiveAll\"";
                          //SPListItemCollection res = bibliotecaLegislacao.GetItems(query);

                          //Altered by RS
                          var res = from item in bibliotecaLegislacao.Items.Cast<SPListItem>()
                                    where item.GetTypedValue<SPFieldLookupValue>("Disposicao").LookupValue.Equals(idDisposicao)
                                    orderby item["Data Diploma"] descending
                                    select item;


                          StringBuilder contentArea = new StringBuilder();

                          //contentArea.Append("<div style='width: 100%; padding-top: 15px; margin-left: 5px;'>");


                          if (res.Count() > 0)
                          {
                              SPDocumentLibrary traduzida = (SPDocumentLibrary)Utils.VerificaLista(myWeb.Lists, LegislacaoTraduzidaLib);
                              var legislTraduzida = from l in traduzida.Items.Cast<SPListItem>()
                                                    where l["Legislacao"] != null
                                                    select new
                                                    {
                                                        Legisl = l["Legislacao"] == null ? null : new SPFieldLookupValue(l["Legislacao"].ToString()),
                                                        Lingua = l["Lingua"] as string,
                                                        Url = SPUrlUtility.CombineUrl(traduzida.ParentWebUrl, l.Url)
                                                    };
                              foreach (SPListItem legisl in res)
                              {
                                  var traducoes = from l in legislTraduzida
                                                  where l.Legisl.LookupId == legisl.ID
                                                  select l;

                                  string t = String.IsNullOrEmpty(legisl.Title) ? legisl.File.Name : legisl.Title;
                                  contentArea.Append("<div class='list_legis'><img alt='' src='/_layouts/apifarma/images/icon_links_lvl2.png' class='icon_list'>");
                                  contentArea.AppendLine(String.Format("<a target='_blank' class='legis' title='{0}' href='{1}'>{0}</a>",
                                      t, SPUrlUtility.CombineUrl(bibliotecaLegislacao.ParentWebUrl, legisl.Url)));

                                  contentArea.Append(legisl["Descricao"]);
                                  //contentArea.AppendLine(
                                  //     String.Format("<p class='content_legisl_descr'>{0}</p>", legisl["Descricao"]));

                                  if (traducoes.Count() > 0)
                                  {
                                      contentArea.Append("<div class='legis_flags'>");
                                      foreach (var traducao in traducoes)
                                      {
                                          contentArea.AppendFormat("<a target='_blank' class='legis_flags_icon' href='{0}'><img class='legis_flags_img' src='_layouts/apifarma/images/icon_{1}.gif' alt='{1}' /></a>", traducao.Url, traducao.Lingua);
                                          // contentArea.AppendFormat("<a target='_blank' href='{0}' style='padding-right: 5px'><img border='0' src='/_layouts/Apifarma/images/Legisl/{1}-icon.png' alt='Tradução {1}' /></a>", traducao.Url, traducao.Lingua);
                                      }
                                      contentArea.AppendLine("</div>");
                                  }

                                  contentArea.AppendLine("</div>");
                              }
                          }
                          else
                              contentArea.Append("<br/><strong>Não foi encontrada Legislação.</strong>");
                          //contentArea.Append("</div>");
                          UpdatePanel1.ContentTemplateContainer.Controls.Add(new LiteralControl(contentArea.ToString()));

                          this.Page.Title = SPContext.Current.ListItem.Title;
                      }
                      catch (Exception ex)
                      {
                          LiteralInfo.Text = String.Format("Erro: {0} </br> {1}", ex.Message, ex.StackTrace);
                          LiteralInfo.Visible = true;
                          //this.Controls.Add(new LiteralControl(String.Format("Erro: {0} </br> {1}", es.Message, es.StackTrace)));
                      }
                      finally
                      {
                          if (myWeb != null && dispose)
                              myWeb.Dispose();

                      }

                  });
        }
    }
}