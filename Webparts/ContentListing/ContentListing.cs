﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Text;
using System.Globalization;
using System.Web.UI.HtmlControls;
using Microsoft.SharePoint.WebPartPages;
using System.Data;
using Microsoft.SharePoint.Utilities;

namespace Webparts.ContentListing
{
    [ToolboxItemAttribute(false)]
    public class ContentListing : Microsoft.SharePoint.WebPartPages.WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/ContentListing/ContentListingUserControl.ascx";

        const String defaultPaginas = "10";
        const String defaultItens = "5";
        const String defaultItensRecursivos = "15";
        const String defaultSubTitulo = "";
        //const String defaultCharsNumber = "200";

        private String _numeroPaginas = string.Empty;
        private String _numeroItens = string.Empty;
        private String _numeroItensRecursivos = string.Empty;
        //private String _query = string.Empty;
        private String _subTitulo = string.Empty;
        private string _web = string.Empty;
        private String _charsNumber = string.Empty;

        private bool _conteudoSite = true;
        private bool _conteudoSiteRecursivo = false;
        private bool _titulosWebpart = false;

        //private SPList listaPaginas;
        //private SPWeb myWeb;
        private StringBuilder renderLista = new StringBuilder();
        //private DataTable dt;
        //private SPQuery query = new SPQuery();
        private NumberFormatInfo provider = new NumberFormatInfo();

        //private int paginaActual = 1;
        //private int totalPaginas = 0;
        //private int numeroSeccao = 0;

        //control to render by the webpart
        private HtmlGenericControl dv = new HtmlGenericControl("div");
        #region Properties
        //TODO: Definir contenttypes..
        public enum ContentTypeName
        {
            Noticias = 0,
            Comunicados,
            Discursos,
            Eventos,
            Newsletters,
            Publicacoes,
            NoticExtranet
        };
        private string[] contentTypes = new string[] { HlpInternet.ContentTypes.Noticia, HlpInternet.ContentTypes.Comunicado,
            HlpInternet.ContentTypes.Discurso, HlpInternet.ContentTypes.Evento, HlpInternet.ContentTypes.Newsletter, HlpInternet.ContentTypes.Publicacao, HlpInternet.ContentTypes.NoticiaExtranet };
        protected ContentTypeName tipoConteudo;
        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Conteúdo a listar")]
        [Description("Tipo de Conteúdo a listar.")]
        [Personalizable]
        [WebBrowsable]
        public ContentTypeName TipoConteudo
        {
            get
            {
                return tipoConteudo;
            }
            set
            {
                tipoConteudo = value;
            }
        }

        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Web")]
        [Description("Web onde procurar a lista")]
        [Personalizable]
        [WebBrowsable]
        public string Web
        {
            get
            {
                return _web;
            }
            set
            {
                _web = value;
            }
        }
        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Ver Titulo e SubTitulo da Webpart")]
        [Description("Ver o titulo e subtitulo escrito pela webpart")]
        [Personalizable]
        [WebBrowsable]
        public bool TitulosWebpart
        {
            get
            {
                return _titulosWebpart;
            }
            set
            {
                _titulosWebpart = value;
            }
        }

        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Sub Título")]
        [Description("Altera o Sub Título")]
        [DefaultValue(defaultSubTitulo)]
        [Personalizable]
        [WebBrowsable]
        public string SubTitulo
        {
            get
            {
                return _subTitulo;
            }
            set
            {
                _subTitulo = value;
            }
        }

        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Numero de páginas")]
        [Description("Numero de paginas na paginação")]
        [DefaultValue(defaultPaginas)]
        [Personalizable]
        [WebBrowsable]
        public string NumeroPaginas
        {
            get
            {
                return _numeroPaginas;
            }
            set
            {
                _numeroPaginas = value;
            }
        }

        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Numero de Itens")]
        [Description("Numero de Itens por página")]
        [DefaultValue(defaultItens)]
        [Personalizable]
        [WebBrowsable]
        public string NumeroItens
        {
            get
            {
                return _numeroItens;
            }
            set
            {
                _numeroItens = value;
            }
        }

        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Ver só Páginas do Site")]
        [Description("Ver só lista de Páginas do Site")]
        [Personalizable]
        [WebBrowsable]
        public bool ConteudoSite
        {
            get
            {
                return _conteudoSite;
            }
            set
            {
                _conteudoSite = value;
            }
        }

        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Ver Páginas do Site e SubSites")]
        [Description("Ver todas listas de Páginas do Site e dos seu SubSites")]
        [Personalizable]
        [WebBrowsable]
        public bool ConteudoSiteRecursivo
        {
            get
            {
                return _conteudoSiteRecursivo;
            }
            set
            {
                _conteudoSiteRecursivo = value;
            }
        }

        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Número de Itens Recursivos")]
        [Description("Número de Itens da procura recursiva a mostrar")]
        [DefaultValue(defaultItensRecursivos)]
        [Personalizable]
        [WebBrowsable]
        public string NumeroItensRecursivos
        {
            get
            {
                return _numeroItensRecursivos;
            }
            set
            {
                _numeroItensRecursivos = value;
            }
        }

        //[Category("Campos")]
        //[WebPartStorage(Storage.Personal)]
        //[FriendlyNameAttribute("Número limite de caracteres na descrição")]
        //[Description("Número limite de caracteres na descrição")]
        //[DefaultValue(defaultCharsNumber)]
        //[Personalizable]
        //[WebBrowsable]
        //public string CharsNumber
        //{
        //    get
        //    {
        //        return _charsNumber;
        //    }
        //    set
        //    {
        //        _charsNumber = value;
        //    }
        //}

        #endregion
        public ContentListing()
        {
            this.ExportMode = WebPartExportMode.All;
            this.ChromeType = PartChromeType.None;
        }

        int? pagActual = null;
        private int PaginaActual
        {
            get
            {
                if (pagActual == null)
                {
                    if (this.Page.Request.QueryString["pg"] != null)
                        pagActual = Int32.Parse(this.Page.Request.QueryString["pg"].ToString(), provider);
                }
                return pagActual.Value;
            }
            set
            {
            }
        }

        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        protected override void CreateChildControls()
        {
            if (String.IsNullOrEmpty(NumeroPaginas)) NumeroPaginas = defaultPaginas;
            if (String.IsNullOrEmpty(NumeroItens)) NumeroItens = defaultItens;
            if (String.IsNullOrEmpty(NumeroItensRecursivos)) NumeroItensRecursivos = defaultItensRecursivos;
            if (String.IsNullOrEmpty(SubTitulo)) SubTitulo = defaultSubTitulo;
            //if (String.IsNullOrEmpty(CharsNumber)) CharsNumber = defaultCharsNumber;

            if (SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                LoadControls();
            }
            else
            {

                renderLista.Append("<div class='modulo'><div class='modulo_header'>");
                renderLista.Append("<div class='title'>");
                renderLista.Append(SPContext.Current.Web.Title);
                renderLista.Append("<br /><span class='subtitle'>");
                renderLista.Append(SubTitulo);
                renderLista.Append(" </span></div></div>");
                renderLista.Append("<div class='modulo_conteudo'>");


                renderLista.Append(" <div class='list_item'>");
                renderLista.Append(" <div class='link-item'>");
                renderLista.Append(" <a href='#'>Item 1</a>");
                renderLista.Append(" <div class='description'>Descrição Item 1</div>");
                renderLista.Append("</div></div>");
                renderLista.Append(" <div class='list_item'>");
                renderLista.Append(" <div class='link-item'>");
                renderLista.Append(" <a href='#'>Item 2</a>");
                renderLista.Append(" <div class='description'>Descrição Item 2</div>");
                renderLista.Append("</div></div>");
                renderLista.Append(" <div class='list_item'>");
                renderLista.Append(" <div class='link-item'>");
                renderLista.Append(" <a href='#'>Item 3</a>");
                renderLista.Append(" <div class='description'>Descrição Item 3</div>");
                renderLista.Append("</div></div>");

                renderLista.Append("</div>");

                renderLista.Append("<div id='pager'></div></div>");


            }
        }

        protected void LoadControls()
        {
            const string queryByContentType = "<OrderBy><FieldRef Name='ArticleStartDate' Ascending='False' /></OrderBy><Where><Eq><FieldRef Name='ContentType' /><Value Type='Choice'>{0}</Value></Eq></Where>";
            SPWeb myWeb = null;
            bool dispose = false;
            try
            {
                string queryStr = String.Format(queryByContentType, contentTypes[(int)TipoConteudo]);

                if (String.IsNullOrEmpty(Web))
                    myWeb = SPContext.Current.Web;
                else
                {
                    if (SPContext.Current.Site.RootWeb.Webs[Web].Exists)
                    {
                        myWeb = SPContext.Current.Site.RootWeb.Webs[Web];
                        dispose = true;
                    }
                    else
                    {
                        renderLista.Append("Web " + Web + " não foi encontrada");
                        return;
                    }
                }
                DataTable dt = null;
                int paginaActual = 1;
                int totalPaginas = 0;
                int numeroSeccao = 0;
                if (ConteudoSite)
                {
                    String NomeLista = VerificarLista(myWeb.Lists);
                    SPList listaPaginas = myWeb.Lists[NomeLista];
                    SPQuery query = new SPQuery();
                    query.Query = queryStr;
                    dt = listaPaginas.GetItems(query).GetDataTable();

                }
                else if (ConteudoSiteRecursivo)
                {
                    SPSiteDataQuery quer = new SPSiteDataQuery();
                    quer.Lists = "<Lists ServerTemplate='850'/>";
                    quer.Query = queryStr;
                    quer.ViewFields = "<FieldRef Name='Title' /><FieldRef Name='FileLeafRef' /><FieldRef Name='FileDirRef' /><FieldRef Name='PublishingRollupImage' /><FieldRef Name='Sinopse' /><FieldRef Name='PublishingPageContent' /><FieldRef Name='ArticleStartDate' />";
                    quer.Webs = "<Webs Scope='Recursive'/>";

                    quer.RowLimit = UInt32.Parse(_numeroItensRecursivos, provider);

                    dt = myWeb.GetSiteData(quer);

                }

                if (this.Page.Request.QueryString["pg"] != null)
                    paginaActual = Int32.Parse(this.Page.Request.QueryString["pg"].ToString(), provider);

                if ((dt != null) && (dt.Rows.Count > 0))
                {
                    numeroSeccao = (paginaActual / Int32.Parse(NumeroPaginas, provider));
                    if ((paginaActual % Int32.Parse(NumeroPaginas, provider)) > 0) // nº seccoes de paginas
                        numeroSeccao++;

                    int items = Int32.Parse(NumeroItens, provider);

                    totalPaginas = (dt.Rows.Count / items);//calcula o nº de paginas 
                    if ((dt.Rows.Count % Int32.Parse(NumeroItens, provider)) > 0)
                        totalPaginas++;


                    if (paginaActual <= totalPaginas)
                    {

                        if (paginaActual == totalPaginas)
                        {
                            //se estou na ultima pagina e tenho o numero de items a apresentar tantos como os que são apresentados por pagina
                            int numItemsDT = dt.Rows.Count;
                            int numItemsRestPages = (totalPaginas - 1) * Int32.Parse(NumeroItens, provider);
                            if ((dt.Rows.Count - numItemsRestPages) == Int32.Parse(NumeroItens, provider))
                                items = Int32.Parse(NumeroItens, provider);
                            else
                                items = (dt.Rows.Count % Int32.Parse(NumeroItens, provider));        //verifica se esta na ultima pagina se sim altera o nº de items aparecer
                        }

                        if (dt.Rows.Count <= Int32.Parse(NumeroItens, provider)) //se o nº items for inferior ao da pagina 
                            items = dt.Rows.Count;


                        renderLista.Append("<table width='100%' cellspacing='0' cellpadding='0' border='0'>");

                        //Debug.Write("pagertop");
                        #region "PAGERTOP"


                        if ((dt != null) && (dt.Rows.Count > 0))
                        {


                            int limiteSeccao = (numeroSeccao * Int32.Parse(NumeroPaginas, provider));

                            int totalSeccoes = (totalPaginas / Int32.Parse(NumeroPaginas, provider));

                            if (((totalPaginas) % (Int32.Parse(NumeroPaginas, provider))) > 0)
                                totalSeccoes++;


                            if (numeroSeccao == totalSeccoes)
                                limiteSeccao = totalPaginas;

                            if (totalPaginas > 1)
                            {
                                renderLista.Append("<tr><td>");

                                renderLista.Append("<div id='pagerTop' class='content_number'>");
                                renderLista.Append("<span class=\"content_page\">Página: </span>");
                                //Navegacao de paginacao
                                if ((totalPaginas > Int32.Parse(NumeroPaginas, provider)) && (!(numeroSeccao == totalSeccoes)))
                                {
                                    renderLista.Append("<div id='fwd' class='page_btn' onmouseover='JavaScript:pagerBtn('fwd','OVER');' onfocus='JavaScript:pagerBtn('fwd','OVER');' onmouseout='JavaScript:pagerBtn('fwd','OUT');' onblur='JavaScript:pagerBtn('fwd','OUT');'><a href='?pg=");
                                    renderLista.Append(((numeroSeccao * Int32.Parse(NumeroPaginas, provider)) + 1));
                                    renderLista.Append("'>»</a></div>");
                                    //renderLista.Append("'><img src='/SiteCollectionImages/arrow_fwd.gif' alt='avançar' id='fwd_img'/></a></div>");
                                }


                                for (int i = ((numeroSeccao - 1) * Int32.Parse(NumeroPaginas, provider)) + 1; i <= limiteSeccao; i++)
                                {

                                    if (i == paginaActual)
                                    {
                                        renderLista.AppendFormat("<span class=\"content_page_number\">{0}</span>", i);
                                    }
                                    else
                                    {
                                        renderLista.AppendFormat("<a class=\"content_page_number\"href='?pg={0}'>{0}</a>", i);
                                    }

                                }
                                renderLista.Append("</div>");
                                //pager
                                //renderLista.Append("<div id='pagerTop' class='pagerListagem'>");
                                ////Navegacao de paginacao
                                //if ((totalPaginas > Int32.Parse(NumeroPaginas, provider)) && (!(numeroSeccao == totalSeccoes)))
                                //{
                                //    renderLista.Append("<div id='fwd' class='page_btn' onmouseover='JavaScript:pagerBtn('fwd','OVER');' onfocus='JavaScript:pagerBtn('fwd','OVER');' onmouseout='JavaScript:pagerBtn('fwd','OUT');' onblur='JavaScript:pagerBtn('fwd','OUT');'><a href='?pg=");
                                //    renderLista.Append(((numeroSeccao * Int32.Parse(NumeroPaginas, provider)) + 1));
                                //    renderLista.Append("'>»</a></div>");
                                //    //renderLista.Append("'><img src='/SiteCollectionImages/arrow_fwd.gif' alt='avançar' id='fwd_img'/></a></div>");
                                //}

                                //renderLista.Append("<div id='paginacaoTop' ><ul class='content_paging'>");
                                //for (int i = ((numeroSeccao - 1) * Int32.Parse(NumeroPaginas, provider)) + 1; i <= limiteSeccao; i++)
                                //{

                                //    if (i == paginaActual)
                                //    {
                                //        renderLista.Append("<li class='pageSelected'>");
                                //        renderLista.Append(i);
                                //        renderLista.Append("</li>");
                                //    }
                                //    else
                                //    {
                                //        renderLista.Append("<li><a href='?pg=");
                                //        renderLista.Append(i);
                                //        renderLista.Append("'>");
                                //        renderLista.Append(i);
                                //        renderLista.Append("</a></li>");
                                //        //renderLista.Append("<div class='separador_V'><img src='/_layouts/images/blank.gif' alt='' /></div>");
                                //    }

                                //}
                                //renderLista.Append("</ul></div>");

                                //Navegacao de paginacao
                                if (!(paginaActual <= Int32.Parse(NumeroPaginas, provider)))
                                {
                                    renderLista.Append("<div id='bcktop' class='page_btn' onmouseover='JavaScript:pagerBtn('bck','OVER');' onfocus='JavaScript:pagerBtn('bck','OVER');' onmouseout='JavaScript:pagerBtn('bck'','OUT');' onblur='JavaScript:pagerBtn('bck'','OUT');'><a href='?pg=");
                                    renderLista.Append(((numeroSeccao - 1) * Int32.Parse(NumeroPaginas, provider)));
                                    //renderLista.Append("'><img src='/SiteCollectionImages/arrow_bck_end.gif' alt='retroceder' id='bck_img'/></a></div>");
                                    renderLista.Append("'>«</a></div>");
                                    renderLista.Append("<div class='separador_V'><img src='/_layouts/images/blank.gif' alt='' /></div>");
                                }
                                renderLista.Append("</div>");
                                renderLista.Append("<br/>");
                            }
                        }





                        renderLista.Append("</td></tr>");
                        renderLista.Append("<tr><td class='linebreakListagem'><img alt='' src='/_layouts/images/blank.gif' /></td></tr>");
                        #endregion

                        //Debug.Write("items");
                        #region "Items"
                        renderLista.Append("<tr><td>");


                        //clearfix
                        renderLista.Append("<div class='clearfix'>");

                        // Debug.Write("TitulosWebpart");
                        //Titulos da webpart
                        if (TitulosWebpart)
                        {
                            renderLista.Append("<div >");
                            renderLista.Append("<div class='titleListagem'>");
                            renderLista.Append("<div class='title'>");
                            renderLista.Append(myWeb.Title);
                            renderLista.Append("<br /><span class='subtitleListagem'>");
                            renderLista.Append(SubTitulo);
                            renderLista.Append(" </span></div></div>");
                        }

                        //renderLista.Append("<br/>");

                        //Conteudo - items da lista da consulta - div modulo_conteudo
                        renderLista.Append("<div class='conteudoListagem'>");

                        renderLista.Append("<table cellpadding='0' cellspacing='0' border='0' width='100%'>");


                        for (int i = ((Int32.Parse(NumeroItens, provider)) * (paginaActual - 1)); i < (((Int32.Parse(NumeroItens, provider)) * (paginaActual - 1)) + items); i++)
                        {
                            //item of list
                            string url = string.Empty;
                            if (ConteudoSite)
                            {
                                url = dt.Rows[i].ItemArray[dt.Columns["LinkFilename"].Ordinal].ToString();

                            }
                            else
                                if (ConteudoSiteRecursivo)
                                {
                                    ////renderLista.Append(myWeb.Url);
                                    ////renderLista.Append("/");
                                    //int gy = dt.Columns["ListId"].Ordinal;
                                    //string ui = dt.Rows[i].ItemArray[gy].ToString().Replace("{", "").Replace("}", "");
                                    //Guid gui = new Guid(ui);
                                    //SPList lit = myWeb.Lists[gui];
                                    //url = lit.GetItemById(Int32.Parse(dt.Rows[i].ItemArray[dt.Columns["ID"].Ordinal].ToString(), provider)).Url;
                                    string leafRef = dt.Rows[i].ItemArray[dt.Columns["FileLeafRef"].Ordinal].ToString().Split(new char[] { '#' })[1];
                                    string leafDirRef = dt.Rows[i].ItemArray[dt.Columns["FileDirRef"].Ordinal].ToString().Split(new char[] { '#' })[1];
                                    url = "/" + leafDirRef + "/" + leafRef;
                                }

                            //renderLista.Append(string.Format(@"<tr class=""list_item"" onmouseover=""this.className='list_itemOVER';"" onmouseout=""this.className='list_item';"" onclick=""document.location.href = '{0}';"" >", url));
                            renderLista.Append(@"<tr class=""list_item"">");


                            //Image Item
                            //renderLista.Append("<td class='ImageItemListagem'>");
                            //if (dt.Rows[i].ItemArray[dt.Columns["PublishingRollupImage"].Ordinal] != null && !dt.Rows[i].ItemArray[dt.Columns["PublishingRollupImage"].Ordinal].ToString().Equals(""))
                            //    renderLista.Append(dt.Rows[i].ItemArray[dt.Columns["PublishingRollupImage"].Ordinal].ToString());
                            //else
                            //    renderLista.Append("<img alt='' src='/_layouts/images/blank.gif' width='100px' height='64px' />");
                            //renderLista.Append("</td>");



                            //link Item
                            renderLista.Append(@"<td style='width:100%;' class='list_itemNoticias'>");
                            renderLista.Append(" <div class='content_list'>");

                            //renderLista.Append(" <a class='content_box_full_height_link' href='");
                            //renderLista.Append(url);
                            //renderLista.Append("'>");

                            renderLista.Append("<p class=\"content_title\">");

                            if (dt.Rows[i].ItemArray[dt.Columns["Title"].Ordinal] != null)
                                renderLista.Append(String.Format("<a class='content_title' href='{0}'>{1}</a>", url, SPEncode.HtmlEncode(dt.Rows[i].ItemArray[dt.Columns["Title"].Ordinal].ToString())));


                            if ((dt.Columns.Contains("ArticleStartDate")) && (dt.Rows[i].ItemArray[dt.Columns["ArticleStartDate"].Ordinal] != null))
                            {
                                string articleDate = dt.Rows[i].ItemArray[dt.Columns["ArticleStartDate"].Ordinal].ToString();
                                if (!String.IsNullOrEmpty(articleDate))
                                    renderLista.AppendFormat("<br/><span class=\"content_title_data\"> {0}</span>", DateTime.Parse(articleDate).ToShortDateString());
                                //if (articleDate.Length > 9)
                                //    renderLista.Append(dt.Rows[i].ItemArray[dt.Columns["ArticleStartDate"].Ordinal].ToString().Substring(0, 10));
                            }

                            //<span class="content_title_data"> | 05/03/2010</span>
                            renderLista.Append("</p>");


                            //Description


                            string image = null;
                            if (dt.Columns.Contains("PublishingRollupImage") && !String.IsNullOrEmpty(image = dt.Rows[i].ItemArray[dt.Columns["PublishingRollupImage"].Ordinal] as string))
                            {
                                if (TipoConteudo == ContentTypeName.Newsletters || TipoConteudo == ContentTypeName.Publicacoes)
                                {
                                    renderLista.AppendFormat(image);

                                }
                                else
                                {
                                    //substring-before(substring-after(@PublishingRollupImage, 'src=&quot;'), '&quot;')
                                    image = image.ToLower().Substring(image.IndexOf("src=\"") + 5);
                                    image = image.Substring(0, image.IndexOf("\""));
                                    //Alteração pedida pelo cliente 27082010
                                    //renderLista.AppendFormat("<div class=\"content_image\"><img src='{0}' width='126px' height='81px'/></div>", image);
                                    renderLista.AppendFormat("<div class=\"content_image\"><img src='{0}'/></div>", image);
                                }
                            }
                            else
                            {
                                renderLista.Append("<div class=\"content_image\"><img src=\"/_layouts/apifarma/images/sample_img_07.jpg\" alt=\"\"/></div>");
                            }


                            string text = null;

                            if (TipoConteudo != ContentTypeName.Newsletters && TipoConteudo != ContentTypeName.Publicacoes)
                            {
                                if (dt.Columns.Contains("Sinopse") && !String.IsNullOrEmpty(text = dt.Rows[i].ItemArray[dt.Columns["Sinopse"].Ordinal] as string))
                                {
                                    renderLista.Append(text);
                                }
                            }
                            else
                            {
                                if ((dt.Columns.Contains("PublishingPageContent")) && !String.IsNullOrEmpty(text = dt.Rows[i].ItemArray[dt.Columns["PublishingPageContent"].Ordinal] as string))
                                {
                                    renderLista.Append(text);
                                }
                            }
                            #region old
                            //else if ((dt.Columns.Contains("PublishingPageContent")) && !String.IsNullOrEmpty(text = dt.Rows[i].ItemArray[dt.Columns["PublishingPageContent"].Ordinal] as string))
                            //{

                            //    string textHtml = HttpUtility.HtmlDecode(text);
                            //    renderLista.Append(Utils.GetDescriptionFromBody(textHtml, CharsNumber));
                            //};
                            //if ((dt.Columns.Contains("Sinopse")) && (dt.Rows[i].ItemArray[dt.Columns["Sinopse"].Ordinal] != null))
                            //{
                            //    renderLista.Append(dt.Rows[i].ItemArray[dt.Columns["Sinopse"].Ordinal].ToString());
                            //}
                            //if(TipoConteudo != ContentTypeName.Newsletters)
                            #endregion

                            renderLista.Append("<div style='padding-bottom: 5px; padding-top: 5px; border-bottom: #999999 1px dotted; width: 100%; float: left'>");
                            if (TipoConteudo != ContentTypeName.Newsletters && TipoConteudo != ContentTypeName.Publicacoes)
                                renderLista.AppendFormat("<a title='Ler mais' class=\"content_link\" href=\"{0}\">Ler mais</a>", url);
                            renderLista.Append("</div>");
                            //renderLista.Append("<br/>");

                            //ArticleStartDate
                            //renderLista.Append("<div>");

                            //renderLista.Append("</div>");

                            renderLista.Append("</div>");

                            renderLista.Append("</td>");
                            renderLista.Append("</tr>");
                        }

                        renderLista.Append("</table>");
                        //end div modulo_conteudo
                        renderLista.Append("</div>");
                        renderLista.Append("<br/>");
                        //end of div clearfix
                        renderLista.Append("</div>");



                        renderLista.Append("</td></tr>");
                        #endregion

                        //Debug.Write("pagerbottom");
                        #region "PAGER"
                        renderLista.Append("<tr><td>");


                        //pager
                        renderLista.Append("<div id='pager' class='pagerListagem'>");

                        if ((dt != null) && (dt.Rows.Count > 0))
                        {
                            int limiteSeccao = (numeroSeccao * Int32.Parse(NumeroPaginas, provider));

                            int totalSeccoes = (totalPaginas / Int32.Parse(NumeroPaginas, provider));

                            if (((totalPaginas) % (Int32.Parse(NumeroPaginas, provider))) > 0)
                                totalSeccoes++;


                            if (numeroSeccao == totalSeccoes)
                                limiteSeccao = totalPaginas;

                            if (totalPaginas > 1)
                            {
                                //Navegacao de paginacao
                                if ((totalPaginas > Int32.Parse(NumeroPaginas, provider)) && (!(numeroSeccao == totalSeccoes)))
                                {
                                    renderLista.Append("<div id='fwd' class='page_btn' onmouseover='JavaScript:pagerBtn('fwd','OVER');' onfocus='JavaScript:pagerBtn('fwd','OVER');' onmouseout='JavaScript:pagerBtn('fwd','OUT');' onblur='JavaScript:pagerBtn('fwd','OUT');'><a href='?pg=");
                                    renderLista.Append(((numeroSeccao * Int32.Parse(NumeroPaginas, provider)) + 1));
                                    renderLista.Append("'>»</a></div>");
                                    //renderLista.Append("'><img src='/SiteCollectionImages/arrow_fwd.gif' alt='avançar' id='fwd_img'/></a></div>");
                                }

                                renderLista.Append("<div id='paginacao' class='content_number'>");
                                renderLista.Append("<span class=\"content_page\">Página: </span>");


                                for (int i = ((numeroSeccao - 1) * Int32.Parse(NumeroPaginas, provider)) + 1; i <= limiteSeccao; i++)
                                {

                                    if (i == paginaActual)
                                    {
                                        renderLista.AppendFormat("<span class=\"content_page_number\">{0}</span>", i);
                                    }
                                    else
                                    {
                                        renderLista.AppendFormat("<a class=\"content_page_number\"href='?pg={0}'>{0}</a>", i);
                                    }

                                }
                                renderLista.Append("</div>");

                                //Navegacao de paginacao
                                if (!(paginaActual <= Int32.Parse(NumeroPaginas, provider)))
                                {
                                    renderLista.Append("<div id='bcktop' class='page_btn' onmouseover='JavaScript:pagerBtn('bck','OVER');' onfocus='JavaScript:pagerBtn('bck','OVER');' onmouseout='JavaScript:pagerBtn('bck'','OUT');' onblur='JavaScript:pagerBtn('bck'','OUT');'><a href='?pg=");
                                    renderLista.Append(((numeroSeccao - 1) * Int32.Parse(NumeroPaginas, provider)));
                                    //renderLista.Append("'><img src='/SiteCollectionImages/arrow_bck_end.gif' alt='retroceder' id='bck_img'/></a></div>");
                                    renderLista.Append("'>«</a></div>");
                                    renderLista.Append("<div class='separador_V'><img src='/_layouts/images/blank.gif' alt='' /></div>");
                                }
                            }
                        }


                        renderLista.Append("</div>");

                        renderLista.Append("</td></tr>");
                        #endregion
                        renderLista.Append("</table>");


                    }
                }
                else
                {
                    renderLista.Append("<div>" + Utils.LoadResourceFromContext("lbl_listing_noresults") + "</div>");
                }

            }
            catch (Exception es)
            {
                renderLista.Append(es.Message);
                renderLista.Append("</br>");
                renderLista.Append(es.Source);
                renderLista.Append("</br>");
            }
            finally
            {
                if (myWeb != null && dispose)
                    myWeb.Dispose();
            }
        }

        private static String VerificarLista(SPListCollection Lista)
        {

            String NomeLista = "Páginas";

            if (Lista != null)
                foreach (SPList lista in Lista)
                {
                    if (lista.Title == "Pages")
                    {
                        NomeLista = lista.Title;
                        break;
                    }
                }
            return NomeLista;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.Write(renderLista);

            //render Div to html in page
            dv.RenderControl(writer);
        }



        /// <summary>
        /// Clear all child controls and add an error message for display.
        /// </summary>
        /// <param name="ex"></param>
        private void HandleException(Exception ex)
        {

            this.Controls.Clear();
            this.Controls.Add(new LiteralControl(ex.Message));
        }
    }
}
