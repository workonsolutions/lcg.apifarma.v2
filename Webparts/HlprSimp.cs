﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;
using Convex.Sharepoint.Foundation.Extensions;
using System.Diagnostics;
using System.Web.Caching;
using Jayrock.Json;

namespace Webparts
{
    public class SimpLists
    {
        public const string Familias = "Familias";
        public const string Especies = "Espécies";
        public const string Formas = "Formas";
        public const string PrincipActivos = "Princípios Activos";
        public const string Classificacoes = "Classificações";
        public const string Labs = "Laboratórios";
        public const string Medicamentos = "Medicamentos";
        public const string MedicamentosDraft = "Medicamentos (Drafts)";
        public const string ATCVet = "ATCVet";
        public const string XSLTemplates = "XSLTemplates";
    }
    public class Fields
    {
        public const string Id = "ID";
        public const string Titulo = "Title";
        public const string Codigo = "Codigo";
        public const string Classificacao = "Classificacao";
        public const string Laboratorio = "Laboratorio";
        public const string Especie = "Especies";
        public const string Formas = "Formas";
        public const string PrincipActivos = "PrincipiosActivos";
        public const string ATCVet = "ATCVet";
        public const string Nome = "Nome";
        public const string Morada = "Morada";
        public const string Telefone = "Telefone";
        public const string Fax = "Fax";
        public const string Site = "Site";
        public const string Email = "Email";
        public const string GrupoAcesso = "GrupoAcesso";

    }
    public class HlprSimp
    {
        public const string ApifarmaSimposiumGroupName = "Apifarma Simposium";

        public static string LoadResourceFromContext(string key)
        {
            return HttpContext.GetGlobalResourceObject("Simposium", key).ToString();
        }
        /// <summary>
        /// Função auxiliar que preenche uma SPGridView com um conjunto de medicamentos.
        /// </summary>
        /// <param name="drugs"></param>
        /// <param name="grid"></param>
        public static void SetDrugsGridValues(SPListItemCollection drugs, SPGridView grid, string siteUrl)
        {
            DataTable dt = new DataTable("Medicamentos");
            dt.Columns.Add(new DataColumn(Fields.Id, typeof(int)));
            dt.Columns.Add(new DataColumn(Fields.Titulo, typeof(string)));
            dt.Columns.Add(new DataColumn(Fields.Codigo, typeof(string)));
            dt.Columns.Add(new DataColumn(Fields.Laboratorio, typeof(string)));
            dt.Columns.Add(new DataColumn(Fields.Classificacao, typeof(string)));
            dt.Columns.Add(new DataColumn(Fields.Formas, typeof(string)));
            dt.Columns.Add(new DataColumn(Fields.Especie, typeof(string)));
            dt.Columns.Add(new DataColumn(Fields.PrincipActivos, typeof(string)));
            dt.Columns.Add(new DataColumn("Url"));
            grid.Columns.Clear();

            BoundField col;
            //BoundField col = new BoundField();
            //col.DataField = Fields.Titulo;
            //col.HeaderText = "Nome";
            //grid.Columns.Add(col);

            // O nome tem uma hiperligação para o Resumo das Caracteristicas do Medicamento (pdf?)
            HyperLinkField linkField = new HyperLinkField();
            linkField.HeaderText = LoadResourceFromContext("lbl_name");
            linkField.DataNavigateUrlFields = new string[] { "Url" };
            linkField.DataNavigateUrlFormatString = Path.Combine(siteUrl, "{0}"); //Path.Combine(drugs.List.ParentWebUrl, "{0}"));
            linkField.DataTextField = Fields.Titulo;
            linkField.Target = "_blank";
            grid.Columns.Add(linkField);

            //col = new BoundField();
            //col.DataField = Fields.Classificacao;
            //col.HeaderText = "Classificação";
            //grid.Columns.Add(col);

            col = new BoundField();
            col.DataField = Fields.Codigo;
            col.HeaderText = LoadResourceFromContext("lbl_code_column");
            grid.Columns.Add(col);

            col = new BoundField();
            col.DataField = Fields.PrincipActivos;
            col.HeaderText = LoadResourceFromContext("lbl_actprincips_column");
            grid.Columns.Add(col);

            col = new BoundField();
            col.DataField = Fields.Laboratorio;
            col.HeaderText = LoadResourceFromContext("lbl_lab");
            grid.Columns.Add(col);

            col = new BoundField();
            col.DataField = Fields.Especie;
            col.HeaderText = LoadResourceFromContext("lbl_species");
            grid.Columns.Add(col);

            col = new BoundField();
            col.DataField = Fields.Formas;
            col.HeaderText = LoadResourceFromContext("lbl_shapes");
            grid.Columns.Add(col);

            //HyperLinkField linkField = new HyperLinkField();
            //linkField.HeaderText = "RCM"; // Resumo das Caracteristicas do Medicamentos
            //linkField.DataNavigateUrlFields = new string[] { "Url" };
            //linkField.DataNavigateUrlFormatString = "/{0}";
            //linkField.Text = "Abrir";
            //linkField.Target = "_blank";
            //grid.Columns.Add(linkField);

            foreach (SPListItem item in drugs)
            {
                
                DataRow row = dt.NewRow();
                row[Fields.Id] = item.ID;
                row[Fields.Titulo] = item.Title.ToUpper();
                row[Fields.Codigo] = item[Fields.Codigo];
                row[Fields.Classificacao] = item.GetValue(Fields.Classificacao, SPListItemExtension.ValuePartEnum.Description);
                row[Fields.Laboratorio] = item.GetValue(Fields.Laboratorio, SPListItemExtension.ValuePartEnum.Description);
                row[Fields.Especie] = item.GetMultiLookupDisplayString(Fields.Especie);
                row[Fields.Formas] = item.GetMultiLookupDisplayString(Fields.Formas);
                row[Fields.PrincipActivos] = item.GetMultiLookupDisplayString(Fields.PrincipActivos);
                row["Url"] = item.Url;
                dt.Rows.Add(row);
            }
            grid.DataSource = dt;
            grid.DataBind();

        }
        public static string GetFamilyJSONDataNonCached()
        {
            StringBuilder sb = new StringBuilder();

            SPList list = SPContext.Current.Web.Lists[SimpLists.Familias];

            List<FamilyEntity> families = new List<FamilyEntity>();
            foreach (SPListItem item in list.Items)
            {
                families.Add(new FamilyEntity() { Id = item.ID, Title = item.Title, Code = item["Codigo"] as string, ParentCode = item["Parent"] as string });
            }

            using (JsonWriter writer = new JsonTextWriter(new StringWriter(sb)))
            {
                writer.WriteStartObject();
                writer.WriteMember("type"); writer.WriteString("json");
                writer.WriteMember("opts");
                writer.WriteStartObject();
                writer.WriteMember("static");
                writer.WriteStartArray();
                TransverseFamilyNode(families, null, writer);
                writer.WriteEndArray();
                writer.WriteEndObject();
                writer.WriteEndObject();
            }
            return sb.ToString();
        }
        private static object _lock = new object();
        /// <summary>
        /// Transforma a Lista das Familias em nós JSON para a JSTree ler.
        /// </summary>
        /// <returns>String com resultado.</returns>
        public static string GetFamilyJSONData()
        {
            const string FamilyJSONCacheName = "FamilyJSONData";
            const int FamilyJSONCacheMinutes = 15;
            string cachedJSON = HttpContext.Current.Cache[FamilyJSONCacheName] as string;
            if (cachedJSON == null)
            {
                lock (_lock)
                {
                    //Ensure that the data was not loaded by a concurrent thread while waiting for lock.
                    cachedJSON = HttpContext.Current.Cache[FamilyJSONCacheName] as string;
                    if (cachedJSON == null)
                    {
                        Debug.Write("Inserting into cache");
                        cachedJSON = GetFamilyJSONDataNonCached();
                        HttpContext.Current.Cache.Add(FamilyJSONCacheName,
                            cachedJSON, null, DateTime.UtcNow.AddMinutes(FamilyJSONCacheMinutes), Cache.NoSlidingExpiration,
                            CacheItemPriority.High, null);
                    }
                }
            }
            else
                Debug.Write("CACHED!");
            return cachedJSON;
        }
        private static void TransverseFamilyNode(List<FamilyEntity> families, string parentCode, JsonWriter writer)
        {
            //Usar Linq para devolver os filhos do nó corrente.
            List<FamilyEntity> childFamilies = (from f in families where parentCode == f.ParentCode select f).ToList<FamilyEntity>();
            foreach (FamilyEntity family in childFamilies)
            {
                //Realizar a familia corrente.
                writer.WriteStartObject();
                writer.WriteMember("data"); writer.WriteString(family.Title);
                writer.WriteMember("attributes");
                writer.WriteStartObject();
                //O id do nó passa a ser o código.
                writer.WriteMember("id"); writer.WriteString(family.Code);
                writer.WriteEndObject();
                //Realizar filhos
                writer.WriteMember("children");
                writer.WriteStartArray();
                TransverseFamilyNode(families, family.Code, writer);
                writer.WriteEndArray();
                writer.WriteEndObject();
            }
        }
        /// <summary>
        /// Query a list, ordered by title, from the current Web.
        /// </summary>
        /// <param name="listName"></param>
        /// <returns></returns>
        public static SPListItemCollection GetOrderedListItems(string listName, SPWeb web)
        {
            SPList list = web.Lists[listName];
            SPQuery q = new SPQuery();
            q.Query = OrderByTitle;
            return list.GetItems(q);
        }
        public static SPListItemCollection GetOrderedActiveListItems(string listName, SPWeb web)
        {
            SPList list = web.Lists[listName];
            SPQuery q = new SPQuery();
            q.Query = ActiveOnly;
            return list.GetItems(q);
        }
        public const string ActiveOnly = "<Where><Eq><FieldRef Name=\"Activo\"></FieldRef><Value Type=\"Boolean\">1</Value></Eq></Where>" + OrderByTitle;
        public const string OrderByTitle = "<OrderBy><FieldRef Name=\"Title\" /></OrderBy>";
        public const string OrderByClassif = "<OrderBy><FieldRef Name=\"" + Fields.Classificacao + "\" /></OrderBy>";

    }
}
