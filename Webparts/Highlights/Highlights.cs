﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using Convex.Clientes.Apifarma.Internet.Webparts.Helper;
using Microsoft.SharePoint.Publishing.Navigation;
using Convex.Sharepoint.Foundation.Extensions;
using Convex.Sharepoint.Foundation.Webparts.Helper;
using System.IO;

namespace Webparts.Highlights
{
    [ToolboxItemAttribute(false)]
    public class Highlights : System.Web.UI.WebControls.WebParts.WebPart
    {
        internal class HighlightItem
        {
            public string NavUrl { get; set; }
            public string Text1 { get; set; }
            public string Text2 { get; set; }
            public bool Active { get; set; }
            public SPFieldUrlValue Image { get; set; }
        }
        #region Properties

        internal uint _itemLimit = 1;
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("Item Limit"), WebDescription("Items to return")]
        public uint ItemLimit
        {
            get { return _itemLimit; }
            set { _itemLimit = value; }
        }

        internal string _xsltFile = "/Style%20Library/XSL%20Style%20Sheets/Highlights.xslt";
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("Xslt file"), WebDescription("Url path to xslt transformation file")]
        public string XsltFile
        {
            get { return _xsltFile; }
            set { _xsltFile = value; }
        }

        internal bool _debugMode = false;
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("Debug Mode"), WebDescription("Render debug steps instead of layout")]
        public bool DebugMode
        {
            get { return _debugMode; }
            set { _debugMode = value; }
        }

        #endregion

        #region Error Control

        StringBuilder _debugMessage = new StringBuilder();
        private bool _showDebugMessage = false;

        #endregion

        private string _listName = "Highlights";
        private string _htmlOutput = "";

        protected override void CreateChildControls()
        {
            _debugMessage.AppendLine(this.Title + ": Start CreateChildControls()");
            _debugMessage.AppendLine(this.Title + ": ItemLimit=" + _itemLimit.ToString());
            _debugMessage.AppendLine(this.Title + ": XsltFile=" + _xsltFile);

            try
            {
                #region Get data

                _debugMessage.AppendLine(this.Title + ": Start getting active items");

                SPList list = SPContext.Current.Site.RootWeb.Lists[_listName];

                var items = from x in list.Items.Cast<SPListItem>()
                            select new HighlightItem
                            {
                                Active = x.GetTypedValue<bool>("Active"),
                                NavUrl = x.Title.ToLower(),
                                Text1 = x.GetTypedValue<string>("Text1"),
                                Text2 = x.GetTypedValue<string>("Text2"),
                                Image = new SPFieldUrlValue((string)x["Image"])
                            };
                items = from x in items
                        where x.Active == true
                        orderby x.NavUrl descending
                        select x;

                string navKey = PortalSiteMapProvider.CurrentNavSiteMapProvider.CurrentNode.Key.Remove(0, 1).ToLower();

                HighlightItem matchedItem = null;

                if (String.IsNullOrEmpty(navKey))
                {
                    matchedItem = (from x in items
                                   where string.IsNullOrEmpty(x.NavUrl)
                                   select x).FirstOrDefault();
                }
                else
                {
                    matchedItem = (from x in items
                                   where !String.IsNullOrEmpty(x.NavUrl) && navKey.StartsWith(x.NavUrl)
                                   select x).FirstOrDefault();
                }
                #endregion

                #region Create xml data

                _debugMessage.AppendLine(this.Title + ": Start creating xml data");
                XElement xDoc = new XElement("highlights");
                if (matchedItem != null)
                {
                    string strText1 = string.Empty;
                    string strText2 = string.Empty;
                    if (navKey.StartsWith("associados"))
                    {
                        strText1 = matchedItem.Text1;
                        SPUser user = SPContext.Current.Web.CurrentUser;
                        strText2 = user.Name;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(matchedItem.Text1))
                            strText1 = matchedItem.Text1;

                        if (!string.IsNullOrEmpty(matchedItem.Text2))
                            strText2 = matchedItem.Text2;

                        strText2 = strText2.Replace("\r\n", "<br/>");
                    }

                    bool isFlashContent = false;
                    if (matchedItem.Image != null || matchedItem.Image.Url != null)
                    {
                        if (Path.GetExtension(matchedItem.Image.Url.ToLower()) == ".swf")
                            isFlashContent = true;
                    }

                    xDoc.Add(new XElement("item",
                        new XElement("title", matchedItem.NavUrl),
                        new XElement("url", ""),
                        new XElement("text1", strText1),
                        new XElement("text2", strText2),
                        new XElement("image", matchedItem.Image.Url),
                        new XElement("isFlashContent", isFlashContent)
                        ));
                }
                _debugMessage.AppendLine(this.Title + ": End creating xml data");
                #endregion
                #region Metodo Antigo

                /*
                string key = PortalSiteMapProvider.CurrentNavSiteMapProvider.CurrentNode.Key.Remove(0, 1);
                string caml = string.IsNullOrEmpty(key) ? "<IsNull><FieldRef Name='Title' /></IsNull>" : string.Format("<Eq><FieldRef Name='Title' /><Value Type='Text'>{0}</Value></Eq>", key);

                SPList list = SPContext.Current.Site.RootWeb.Lists[_listName];
                SPListItemCollection items = list.GetItems(new SPQuery
                {
                    Query = string.Format("<Where><And>{0}<Eq><FieldRef Name='Active' /><Value Type='Boolean'>1</Value></Eq></And></Where><OrderBy><FieldRef Name='Modified' Ascending='FALSE' /></OrderBy>", caml),
                    RowLimit = _itemLimit
                });
              
                _debugMessage.AppendLine(this.Title + ": End getting active items");
                
                #endregion

                #region Create xml data

                _debugMessage.AppendLine(this.Title + ": Start creating xml data");
                XElement xDoc = new XElement("highlights");
                foreach (SPListItem item in items)
                {
                    string strText1 = string.Empty;
                    string strText2 = string.Empty;
                    if(key.ToLower().Equals("associados")){
                        strText1 = (string)item["Text1"];
                        SPUser user = SPContext.Current.Web.CurrentUser;
                        strText2 = user.Name;
                    }else{
                        if (item["Text1"] != null)
                            strText1 = item["Text1"].ToString();

                        if (item["Text2"] != null)
                            strText2 = item["Text2"].ToString();

                        strText2 = strText2.Replace("\r\n", "<br/>");
                    }
                    SPFieldUrlValue imageUrl = new SPFieldUrlValue((string)item["Image"]);

                    bool isFlashContent = false;
                    if(imageUrl != null || imageUrl.Url != null) {
                        if (Path.GetExtension(imageUrl.Url.ToLower()) == ".swf")
                            isFlashContent = true;
                    }

                    xDoc.Add(new XElement("item",
                        new XElement("title", item.Title),
                        new XElement("url", ""),
                        new XElement("text1", strText1),
                        new XElement("text2", strText2),
                        new XElement("image", imageUrl.Url),
                        new XElement("isFlashContent", isFlashContent)
                        ));
                }
                _debugMessage.AppendLine(this.Title + ": End creating xml data");
                
                #endregion
                   * */
                #endregion

                #region Convert xml data into html

                _debugMessage.AppendLine(this.Title + ": Start converting xml into html");
                string xsltContents = SPContext.Current.Site.RootWeb.GetFileAsString(_xsltFile);
                _htmlOutput = Xslt.Xml2Html(xDoc.ToString(), xsltContents);
                _debugMessage.AppendLine(this.Title + ": End converting xml into html");

                #endregion
            }
            catch (Exception ex)
            {
                _debugMessage.AppendLine(string.Format("<strong>{0}</strong>", ex.Message));
                _showDebugMessage = true;
            }

            _debugMessage.AppendLine(this.Title + ": End CreateChildControls()");
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (_debugMode || _showDebugMessage)
            {
                _debugMessage.Replace(Environment.NewLine, "<br/>");
                _debugMessage.Insert(0, "<div class=\"ms-error\">");
                _debugMessage.Append("</div>");

                writer.Write(_debugMessage.ToString());
            }
            else
            {
                writer.Write(_htmlOutput);
            }
        }
    }
}
