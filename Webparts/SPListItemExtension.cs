﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace Webparts
{
    public static class SPListItemExtension
    {
        public enum ValuePartEnum
        {
            Id,
            Description
        }

        public static object GetValue(this SPListItem item, string fieldName)
        {
            return GetValue(item, fieldName, ValuePartEnum.Id);
        }
        public static string GetMultiLookupDisplayString(this SPListItem item, string fieldName)
        {
            if (item[fieldName] != null)
            {
                SPField field = item.ParentList.Fields.GetFieldByInternalName(fieldName);
                if (field.Type == SPFieldType.Lookup)
                {
                    SPFieldLookup lField = field as SPFieldLookup;
                    if (!lField.AllowMultipleValues)
                        throw new Exception("Field " + fieldName + " is not of type MultiLookup");

                    SPFieldLookupValueCollection values = new SPFieldLookupValueCollection(item[fieldName].ToString());
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < values.Count; ++i)
                    {
                        sb.Append(values[i].LookupValue);
                        if (i != values.Count - 1)
                            sb.Append(", ");
                    }
                    return sb.ToString();
                }
                else
                    throw new Exception("Field " + fieldName + " is not of type MultiLookup");
            }
            else return String.Empty;
        }
        public static object GetValue(this SPListItem item, string fieldName, ValuePartEnum valuePart)
        {
            object value = null;

            if (item[fieldName] != null)
            {
                SPField field = item.ParentList.Fields.GetFieldByInternalName(fieldName);
                switch (field.Type)
                {
                    case SPFieldType.Text:
                        value = Convert.ToString(item[fieldName]);
                        break;

                    case SPFieldType.Number:
                        SPFieldNumber fieldNumber = field as SPFieldNumber;
                        if (fieldNumber.DisplayFormat == SPNumberFormatTypes.NoDecimal)
                        {
                            value = Convert.ToInt32(item[fieldName]);
                        }
                        else
                        {
                            value = Convert.ToDouble(item[fieldName]);
                        }
                        break;

                    case SPFieldType.Currency:
                        SPFieldNumber fieldCurrency = field as SPFieldNumber;
                        if (fieldCurrency.DisplayFormat == SPNumberFormatTypes.NoDecimal)
                        {
                            value = Convert.ToInt32(item[fieldName]);
                        }
                        else
                        {
                            value = Convert.ToDouble(item[fieldName]);
                        }
                        break;

                    case SPFieldType.DateTime:
                        value = Convert.ToDateTime(item[fieldName]);
                        break;

                    case SPFieldType.Boolean:
                        value = Convert.ToBoolean(item[fieldName]);
                        break;

                    case SPFieldType.Lookup:
                        SPFieldLookup fieldLookup = field as SPFieldLookup;
                        // Single Lookup Value
                        if (!fieldLookup.AllowMultipleValues)
                        {
                            SPFieldLookupValue lv = new SPFieldLookupValue(Convert.ToString(item[fieldName]));
                            if (lv.LookupId > 0)
                            {
                                switch (valuePart)
                                {
                                    case ValuePartEnum.Id:
                                        value = lv.LookupId;
                                        break;

                                    case ValuePartEnum.Description:
                                        value = lv.LookupValue;
                                        break;
                                }
                            }
                        }
                        // Multiple Lookup Value
                        else
                        {
                            List<int> valuesId = new List<int>();
                            List<string> valuesDescription = new List<string>();

                            SPFieldLookupValueCollection lvc = new SPFieldLookupValueCollection(Convert.ToString(item[fieldName]));
                            foreach (SPFieldLookupValue lv in lvc)
                            {
                                if (lv.LookupId > 0)
                                {
                                    switch (valuePart)
                                    {
                                        case ValuePartEnum.Id:
                                            if (!valuesId.Contains(lv.LookupId)) { valuesId.Add(lv.LookupId); }
                                            break;

                                        case ValuePartEnum.Description:
                                            if (!valuesDescription.Contains(lv.LookupValue)) { valuesDescription.Add(lv.LookupValue); }
                                            break;
                                    }
                                }
                            }

                            switch (valuePart)
                            {
                                case ValuePartEnum.Id:
                                    value = valuesId;
                                    break;

                                case ValuePartEnum.Description:
                                    value = valuesDescription;
                                    break;
                            }
                        }
                        break;


                    default:
                        throw new Exception("SPFieldType '" + field.Type.ToString() + "' não parametrizado na extensão!");
                }
            }

            return value;
        }


        public static bool DoesCurrentUserHavePermissions(this SPListItem item, SPBasePermissions permissionMask)
        {
            bool hasPermissions = false;

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite elevatedSite = new SPSite(item.Web.Site.ID))
                {
                    using (SPWeb elevatedWeb = elevatedSite.OpenWeb(item.Web.ID))
                    {
                        SPList elevatedList = elevatedWeb.Lists[item.ParentList.ID];

                        SPListItem elevatedItem = elevatedList.Items.GetItemById(item.ID);

                        hasPermissions = elevatedItem.DoesUserHavePermissions(SPContext.Current.Web.CurrentUser, permissionMask);
                    }
                }
            });

            return hasPermissions;
        }

    }
}
