﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Text;
using System.Xml.Linq;
using System.Xml.Xsl;
using Convex.Sharepoint.Foundation.Webparts;
using Convex.Sharepoint.Foundation.Webparts.Helper;

namespace Webparts.AssociadosFullList
{
    [ToolboxItemAttribute(false)]
    public class AssociadosFullList : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/AssociadosFullList/AssociadosFullListUserControl.ascx";

        //public AssociadosFullList()
        //{
        //    this.ExportMode = WebPartExportMode.All;
        //    this.ChromeType = PartChromeType.None;
        //}

        #region Properties

        internal string _siteUrl = "";
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("Site Url"), WebDescription("Intranet site url")]
        public string SiteUrl
        {
            get { return _siteUrl; }
            set { _siteUrl = value; }
        }

        internal string _webUrl = "";
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("Web Url"), WebDescription("Intranet web 'Entidades' url")]
        public string WebUrl
        {
            get { return _webUrl; }
            set { _webUrl = value; }
        }

        internal string _listName = "";
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("List Name"), WebDescription("'Associados' list name")]
        public string ListName
        {
            get { return _listName; }
            set { _listName = value; }
        }

        internal string _xsltFile = "/Style%20Library/XSL%20Style%20Sheets/ListAssociados.xslt";
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("Xslt file"), WebDescription("Url path to xslt transformation file")]
        public string XsltFile
        {
            get { return _xsltFile; }
            set { _xsltFile = value; }
        }

        internal bool _debugMode = false;
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("Debug Mode"), WebDescription("Render debug steps instead of layout")]
        public bool DebugMode
        {
            get { return _debugMode; }
            set { _debugMode = value; }
        }

        #endregion

        #region Error Control

        StringBuilder _debugMessage = new StringBuilder();
        private bool _showDebugMessage = false;

        #endregion

        private string _queryStringDisplayPage = "laPage";
        private string _htmlOutput = "";
        private string _htmlPagingOutput = "";

        protected override void CreateChildControls()
        {
            _debugMessage.AppendLine(this.Title + ": Start CreateChildControls()");
            _debugMessage.AppendLine(this.Title + ": SiteUrl=" + _siteUrl);
            _debugMessage.AppendLine(this.Title + ": WebUrl=" + _webUrl);
            _debugMessage.AppendLine(this.Title + ": ListName=" + _listName);
            _debugMessage.AppendLine(this.Title + ": XsltFile=" + _xsltFile);

            try
            {
                SPFile templateFile = SPContext.Current.Site.RootWeb.GetFile(_xsltFile);
                if (!templateFile.Exists)
                {
                    _debugMessage.AppendLine(string.Format("Template file not found at '{0}'", _xsltFile));
                    _showDebugMessage = true;
                    return;
                }


                #region Get data

                _debugMessage.AppendLine(this.Title + ": Start getting list items");
                XElement xDoc = new XElement("query");

                #region Items

                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(_siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb(_webUrl))
                        {
                            StringBuilder caml = new StringBuilder();
                            caml.Append("<Where>");
                            caml.Append("<And>");
                            caml.Append("<Eq><FieldRef Name='ContentType' /><Value Type='Text'>Associado</Value></Eq>");
                            caml.Append("<Eq><FieldRef Name='AF_Activo' /><Value Type='Boolean'>1</Value></Eq>");
                            caml.Append("</And>");
                            caml.Append("</Where>");
                            caml.Append("<OrderBy><FieldRef Name='Title' Ascending='TRUE' /></OrderBy>");

                            SPList list = web.Lists[_listName];
                            SPListItemCollection items = list.GetItems(new SPQuery { Query = caml.ToString() });

                            #region Create xml data

                            _debugMessage.AppendLine(this.Title + ": Start creating xml item data");
                            XElement xItems = new XElement("items");
                            foreach (SPListItem item in items)
                            {
                                _debugMessage.AppendLine(this.Title + ": ItemID=" + item.ID.ToString());
                                _debugMessage.AppendLine(this.Title + ": ItemTitle=" + item.Title);

                                XElement xItem = new XElement("item",
                                    new XElement("id", item.ID),
                                    new XElement("title", item.Title),
                                    new XElement("morada", (string)item["AF_Morada1"]),
                                    new XElement("codigoPostal", (string)item["AF_CodigoPostal"]),
                                    new XElement("telefone", (string)item["AF_Telefone"]),
                                    new XElement("fax", (string)item["AF_Fax"]),
                                    new XElement("website", new SPFieldUrlValue((string)item["AF_Website"]).Url)
                                    );

                                xItems.Add(xItem);
                            }
                            xDoc.Add(xItems);
                            _debugMessage.AppendLine(this.Title + ": End creating xml item data");

                            #endregion
                        }
                    }
                }
                );

                #endregion

                #region Convert xml data into html

                _debugMessage.AppendLine(this.Title + ": Start converting xml item data into html");
                XslCompiledTransform xslTemplate = Xslt.LoadXsltFromSPFile(templateFile);
                _htmlOutput = Xslt.XmlTransform(new XDocument(xDoc), xslTemplate);
                _debugMessage.AppendLine(this.Title + ": End converting xml item data into html");

                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                _debugMessage.AppendLine(string.Format("<strong>{0}</strong>", ex.Message));
                _showDebugMessage = true;
            }

            _debugMessage.AppendLine(this.Title + ": End CreateChildControls()");
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (_debugMode || _showDebugMessage)
            {
                _debugMessage.Replace(Environment.NewLine, "<br/>");
                _debugMessage.Insert(0, "<div class=\"ms-error\">");
                _debugMessage.Append("</div>");

                writer.Write(_debugMessage.ToString());
            }
            else
            {
                writer.Write(_htmlPagingOutput);
                writer.Write(_htmlOutput);
            }
        }

        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        //        protected override void CreateChildControls()
        //        {
        //            base.CreateChildControls();
        //            try
        //            {

        //                if (SPContext.Current.FormContext.FormMode == SPControlMode.Display)
        //                {

        //                    StringBuilder sb = new StringBuilder();

        //                    try
        //                    {

        //                        SPSecurity.RunWithElevatedPrivileges(delegate()
        //                 {
        //                     // implementation details omitted
        //                     using (SPSite site = new SPSite(HlpInternet.EntidadesSiteUrl))
        //                     {
        //                         using (SPWeb web = site.OpenWeb())
        //                         {
        //                             SPList entidades = web.Lists.GetList("lists/entidades");

        //                             StringBuilder caml = new StringBuilder();
        //                             caml.Append("<Where>");
        //                             caml.Append("<And>");
        //                             //caml.Append("  <And>");
        //                             caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq>", SPBuiltInFieldId.ContentType, entidades.ContentTypes.GetContentType("AF_Associado").Name);
        //                             //caml.AppendFormat("<BeginsWith><FieldRef Name=\"Title\"/><Value Type=\"Text\">{0}</Value></BeginsWith>", text);
        //                             //caml.Append("  </And>");
        //                             caml.Append("   <Or>");
        //                             caml.Append("       <Gt><FieldRef Name=\"AF_DataCancelamento\" /><Value Type=\"DateTime\"><Today/></Value></Gt>");
        //                             caml.Append("       <IsNull><FieldRef Name=\"AF_DataCancelamento\"/></IsNull>");
        //                             caml.Append("   </Or>");
        //                             caml.Append("</And>");
        //                             caml.Append("</Where><OrderBy><FieldRef Name='Title' /></OrderBy>");

        //                             SPQuery query = new SPQuery();
        //                             query.Query = caml.ToString();
        //                             SPListItemCollection res = entidades.GetItems(query);

        //                             var associados = from a in res.Cast<SPListItem>()
        //                                              select new
        //                                              {
        //                                                  Nome = a.Title,
        //                                                  Morada = a.GetFormattedValue("AF_Morada1"),
        //                                                  CodPostal = a.GetFormattedValue("AF_CodigoPostal"),
        //                                                  Telefone = a.GetFormattedValue("AF_Telefone"),
        //                                                  Fax = a.GetFormattedValue("AF_Fax")
        //                                              };

        //                             sb.AppendLine("<table border='0'>");
        //                             foreach (var assoc in associados)
        //                             {
        //                                 sb.AppendFormat("<tr><td><p class='content_title_sub'>{0}</p>", assoc.Nome);
        //                                 if(!String.IsNullOrEmpty(assoc.Morada))
        //                                    sb.AppendFormat("<span>{0}</span></br>", assoc.Morada);
        //                                 if (!String.IsNullOrEmpty(assoc.CodPostal))
        //                                     sb.AppendFormat("<span>{0}</span></br>", assoc.CodPostal);
        //                                 if (!String.IsNullOrEmpty(assoc.Telefone))
        //                                     sb.AppendFormat("Telefone: {0} <br/>", assoc.Telefone);
        //                                 if (!String.IsNullOrEmpty(assoc.Fax))
        //                                     sb.AppendFormat("Fax: {0} <br/>", assoc.Fax);

        //                                 sb.AppendLine(@" <tr>
        //                                        <td>
        //                                            <div style=""height: 0px; width: 100%; margin-bottom: 2px; margin-top: 2px; border-bottom: #999999 1px dotted;""></div>
        //                                        </td>
        //                                    </tr>");

        //                             }
        //                             sb.AppendLine("</table>");

        //                             this.Controls.Add(new LiteralControl(sb.ToString()));
        //                         }
        //                     }
        //                 });
        //                    }
        //                    catch (Exception) { }
        //                }
        //                else
        //                {
        //                    this.Controls.Add(new LiteralControl("Listagem completa de associados"));
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                //Display any error that comes up
        //                this.Controls.Clear();
        //                this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
        //            }
        //        }

    }
}
