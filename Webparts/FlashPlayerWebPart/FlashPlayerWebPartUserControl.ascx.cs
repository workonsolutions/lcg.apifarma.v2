﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Webparts.FlashPlayerWebPart
{
    public partial class FlashPlayerWebPartUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public string BannerUrl
        {
            get;
            set;
        }

        public int BannerHeight
        {
            get;
            set;
        }

        public int BannerWidth
        {
            get;
            set;
        }

        public string TitleBox
        {
            get;
            set;
        }
    }
}