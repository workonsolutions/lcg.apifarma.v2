﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Xml.Serialization;

namespace Webparts.FlashPlayerWebPart
{
    [ToolboxItemAttribute(false)]
    public class FlashPlayerWebPart : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/FlashPlayerWebPart/FlashPlayerWebPartUserControl.ascx";
        const int DefaultHeight = 180;
        const int DefaultWidth = 938;
        const string DefaultUrl = "http://spfte01apifarma.cloudapp.net/Flash/new_header_revamp_v3b.swf";
       
        protected override void CreateChildControls()
        {
            FlashPlayerWebPartUserControl control = Page.LoadControl(_ascxPath) as FlashPlayerWebPartUserControl;
            control.BannerHeight = this.BannerHeight;
            control.BannerWidth = this.BannerWidth;
            control.BannerUrl = this.BannerUrl;
            control.TitleBox = this.TitleBox;
            Controls.Add(control);
        }

        private int _BannerWidth;
        private int _BannerHeight;
        private string _BannerUrl;
        private string _titlebox;

        public FlashPlayerWebPart()
        {
            _BannerHeight = DefaultHeight;
            _BannerWidth = DefaultWidth;
            _BannerUrl = DefaultUrl;
            _titlebox = "";
        }

        
        [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("Width"),
        WebDescription("Please Enter Banner Width"), DefaultValue(DefaultWidth),
        XmlElement(ElementName = "_BannerWidth")]
        public int BannerWidth
        {
            get { return _BannerWidth; }
            set { _BannerWidth = value; }
        }

        
        [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("Height"),
        WebDescription("Please Enter Banner Height"), DefaultValue(DefaultHeight),
        XmlElement(ElementName = "_BannerHeight")]
        public int BannerHeight
        {
            get { return _BannerHeight; }
            set { _BannerHeight = value; }
        }

        
        [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("Banner URL"),
        WebDescription("Please Enter a Sample Text"), DefaultValue(DefaultUrl),
        XmlElement(ElementName = "_BannerUrl")]
        public string BannerUrl
        {
            get { return _BannerUrl; }
            set
            {
                _BannerUrl = value;
            }
        }

        [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("WebPartTitle"),
        WebDescription("Please Enter the title"), DefaultValue("")]
        public string TitleBox
        {
            get { return _titlebox; }
            set
            {
                _titlebox = value;
            }
        }

    }
}