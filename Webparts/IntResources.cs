﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Resources;

namespace Webparts
{
    class IntResources
    {
        static IntResources()
        {
            var intlAsssembly = Assembly.Load("Microsoft.SharePoint.intl, Version=12.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c");

            IntlResources = new ResourceManager("Microsoft.SharePoint", intlAsssembly);
        }

        private static ResourceManager IntlResources { get; set; }

        public static string ModerationStatusApproved
        {
            get
            {
                return IntlResources.GetString("ModerationStatusApproved");
            }
        }

        public static string ModerationStatusPending
        {
            get
            {
                return IntlResources.GetString("ModerationStatusPending");
            }
        }

        public static string ModerationStatusRejected
        {
            get
            {
                return IntlResources.GetString("ModerationStatusRejected");
            }
        }
    }
}
