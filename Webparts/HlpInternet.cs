﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Webparts
{
    public partial struct HlpInternet
    {
        public static string EntidadesSiteUrl = ConfigurationSettings.AppSettings["EntidadesSiteUrl"] as string;
        
        public partial struct ContentTypes
        {
            public const string Noticia = "Noticia";
            public const string Discurso = "Discurso";
            public const string Evento = "Evento Apifarma";
            public const string Newsletter = "Newsletter";
            public const string Comunicado = "Comunicado Imprensa";
            public const string Publicacao = "Publicacao";
            public const string NoticiaExtranet = "Noticia Extranet";
        }
    }
}
