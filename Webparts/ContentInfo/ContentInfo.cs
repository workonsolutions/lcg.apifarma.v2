﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using Microsoft.SharePoint.Publishing;
using Microsoft.SharePoint.Publishing.Fields;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.Linq;
using System.IO;

namespace Webparts.ContentInfo
{
    [ToolboxItemAttribute(false)]
    public class ContentInfo : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/ContentInfo/ContentInfoUserControl.ascx";

        internal class ContentInfos
        {
            internal string title;
            internal DateTime articleStart;
            internal string sinopse;
            internal string imageUrl;
            internal string linkUrl;
            internal string publishingRollupImage;
            internal string target;
            internal string exibicao;
            internal string publishingPageContent;
        }

        #region Properties
        private string imageContentInfoTitleUrl = null;
        [Personalizable(PersonalizationScope.Shared),
           WebBrowsable(true),
           WebDisplayName("Image Info Title in content (mandatory)"),
           WebDescription("Enter Image Info Title in content (mandatory)")]
        public string ImageContentInfoTitleUrl
        {
            get { return imageContentInfoTitleUrl; }
            set { imageContentInfoTitleUrl = value; }
        }

        private int infoLimit = 1;
        [Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("InfoLimit"),
        WebDescription("Maximum number of Info to show.)")]
        public int InfoLimit
        {
            get { return infoLimit; }
            set { infoLimit = value; }
        }

        private string contentInfoTitle = null;
        [Personalizable(PersonalizationScope.Shared),
           WebBrowsable(true),
           WebDisplayName("Title of Content Info (mandatory)"),
           WebDescription("Enter title (mandatory)")]
        public string ContentInfoTitle
        {
            get { return contentInfoTitle; }
            set { contentInfoTitle = value; }
        }

        private string contentImageUrl = string.Empty;
        [Personalizable(PersonalizationScope.Shared),
           WebBrowsable(true),
           WebDisplayName("URL For Content Info Image"),
           WebDescription("URL For Content Info Image")]
        public string ContentImageUrl
        {
            get { return contentImageUrl; }
            set { contentImageUrl = value; }
        }

        private string _xsltFile = "/Style%20Library/XSL%20Style%20Sheets/ContentInfoNews.xslt";
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("Xslt file"), WebDescription("Url path to xslt transformation file")]
        public string XsltFile
        {
            get { return _xsltFile; }
            set { _xsltFile = value; }
        }

        #endregion

        #region Error Control

        private string _errorMessageException = "";
        private bool _showErrorMessageException = false;

        #endregion

        List<ContentInfos> _allInfo = new List<ContentInfos>();
        private string _xsltOutput = "";

        public ContentInfo()
        {
            this.ExportMode = WebPartExportMode.All;
        }

        internal XElement CreateXmlData(string boxImageUrl, string boxTitle)
        {
            XElement xContent = new XElement("content");

            if (!string.IsNullOrEmpty(boxImageUrl))
                xContent.Add(new XElement("boxImageUrl", boxImageUrl));

            if (!string.IsNullOrEmpty(boxTitle))
                xContent.Add(new XElement("boxTitle", boxTitle));

            xContent.Add(new XElement("contentImageUrl", contentImageUrl));

            foreach (ContentInfos infos in _allInfo)
            {
                XElement xInfo = new XElement("info");

                // Add node element
                xInfo.Add(
                    new XElement("title", infos.title),
                    new XElement("startDate", infos.articleStart),
                    new XElement("sinopse", infos.sinopse),
                    new XElement("imageUrl", infos.imageUrl),
                    new XElement("linkUrl", infos.linkUrl),
                    new XElement("rollUpImage", infos.publishingRollupImage),
                    new XElement("PublishingPageContent", infos.publishingPageContent)
                    );

                xContent.Add(xInfo);
            }

            return xContent;
        }


        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        protected override void CreateChildControls()
        {

            try
            {
                if (string.IsNullOrEmpty(this.contentInfoTitle))
                {
                    LiteralControl html = new LiteralControl("Click <a href=\"javascript:MSOTlPn_ShowToolPane2Wrapper('Edit', this, '" + this.ID + "')\">open tool pane</a> to set webpart properties");
                    this.Controls.Add(html);
                }
                else
                {
                    using (SPWeb rootWeb = SPContext.Current.Site.RootWeb)
                    {
                        #region Get content Info
                        SPSiteDataQuery query = new SPSiteDataQuery();

                        //Ask for all lists created from the document libray.
                        query.Lists = "<Lists BaseType=\"1\" />";

                        // Get the Title (Last Name) and FirstName fields.
                        query.ViewFields = "<FieldRef Name=\"Title\" />" +
                                            "<FieldRef Name=\"ArticleStartDate\" Nullable=\"TRUE\" />" +
                                            "<FieldRef Name=\"Sinopse\" Nullable=\"TRUE\"/>" +
                            //"<FieldRef Name=\"ImageUrl\" Nullable=\"TRUE\"/>" +
                            //"<FieldRef Name=\"LinkUrl\" Nullable=\"TRUE\"/>" +
                                            "<FieldRef Name=\"FileRef\" Nullable=\"TRUE\"/>" +
                                            "<FieldRef Name=\"PublishingRollupImage\" Nullable=\"TRUE\"/>" +
                                            "<FieldRef Name=\"Exibicao\" Nullable=\"TRUE\" Type=\"Text\"/>" +
                                            "<FieldRef Name=\"SafeLinkUrl\" Nullable=\"TRUE\" />" +
                                            "<FieldRef Name=\"LinkTarget\" Nullable=\"TRUE\" />" +
                                            "<FieldRef Name=\"PublishingPageContent\" Nullable=\"TRUE\" />";

                        query.Query = "<Where>" +
                                        "<And>" +
                                            "<BeginsWith>" +
                                              "<FieldRef Name=\"ContentTypeId\" />" +
                                              "<Value Type=\"Text\">0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D</Value>" +
                                            "</BeginsWith>" +
                                            "<Eq>" +
                                                "<FieldRef Name=\"Exibicao\" />" +
                                              "<Value Type=\"Text\">" + contentInfoTitle + "</Value>" +
                                            "</Eq>" +
                                        "</And>" +
                                      "</Where>" +
                                      "<OrderBy>" +
                                          "<FieldRef Name=\"ArticleStartDate\" Ascending=\"False\"/>" +
                                      "</OrderBy>";

                        query.RowLimit = (uint)infoLimit;

                        // Query all Web sites 
                        query.Webs = "<Webs Scope=\"Recursive\" />";

                        DataTable dt = rootWeb.GetSiteData(query);
                        DataRowCollection coll = dt.Rows;

                        int numCols = dt.Columns.Count;
                        //DateTime? daTi = null;
                        foreach (DataRow dr in coll)
                        {


                            ContentInfos ci = new ContentInfos();
                            ci.title = dr.Field<string>("Title");
                            if (!string.IsNullOrEmpty(dr.Field<string>("ArticleStartDate")))
                                ci.articleStart = DateTime.Parse(dr.Field<string>("ArticleStartDate"));
                            ci.sinopse = dr.Field<string>("Sinopse");
                            //ci.imageUrl = dr.Field<string>("ImageUrl");
                            if (!string.IsNullOrEmpty(dr.Field<string>("FileRef")) && dr.Field<string>("FileRef").Contains('#'))
                                ci.linkUrl = new Uri(new Uri(SPContext.Current.Site.Url), dr.Field<string>("FileRef").Substring(dr.Field<string>("FileRef").IndexOf("#") + 1)).AbsoluteUri;
                            ci.publishingRollupImage = new ImageFieldValue(dr.Field<string>("PublishingRollupImage")).ImageUrl;
                            ci.target = dr.Field<string>("LinkTarget");
                            ci.publishingPageContent = dr.Field<string>("PublishingPageContent");
                            _allInfo.Add(ci);
                        }

                        _errorMessageException += "Done!<br />";

                        #endregion

                        #region Create xml data

                        XElement xDoc = CreateXmlData(imageContentInfoTitleUrl, contentInfoTitle);

                        #endregion

                        // Get Xslt file
                        string xsltContents = SPContext.Current.Site.RootWeb.GetFileAsString(_xsltFile);

                        // Prepare and trasnform Xml in Html                   
                        StringReader srInput = new StringReader(xDoc.ToString());
                        XmlReader xmlrImput = XmlReader.Create(srInput);

                        StringReader srTransform = new StringReader(xsltContents);
                        XmlReader xmlrTransform = XmlReader.Create(srTransform);

                        using (StringWriter sw = new StringWriter())
                        {
                            XslCompiledTransform xslt = new XslCompiledTransform();
                            xslt.Load(xmlrTransform);

                            using (XmlWriter xmlwOutput = XmlWriter.Create(sw, xslt.OutputSettings))
                            {
                                xslt.Transform(xmlrImput, xmlwOutput);
                            }

                            _xsltOutput = sw.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Display any error that comes up
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
            }
        }

        /// <summary>
        /// Clear all child controls and add an error message for display.
        /// </summary>
        /// <param name="ex"></param>
        private void HandleException(Exception ex)
        {
            this.Controls.Clear();
            this.Controls.Add(new LiteralControl(ex.Message));
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!_showErrorMessageException)
            {
                writer.Write(_xsltOutput);
            }

            // Mostra a mensagem de erros completa (erro + log) caso seja necessário
            if (_showErrorMessageException)
            {
                writer.Write(string.Format("<div class=\"ms-error\">{0}</div>", _errorMessageException));
            }

        }
    }
}
