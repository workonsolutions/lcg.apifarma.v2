﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimpAdvSearch.ascx.cs" Inherits="Webparts.SimpAdvSearchWebPart.SimpAdvSearch" %>


<table align="center" border="0" cellpadding="3" cellspacing="0">
    <tr>
        <td align="center">
            <asp:Literal ID="Literal8" runat="server" Text="Por favor preencha os critérios da sua pesquisa e clique em pesquisar."></asp:Literal>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td width="150" align="right">
                        <strong><asp:Literal ID="Literal2" runat="server" Text="Nome"></asp:Literal>:</strong>
                    </td>
                    <td width="350" align="left">
                        <asp:TextBox ID="TBName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal1" runat="server" Text="Princípio Activo"></asp:Literal>:</strong>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="DDLActiveP" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal3" runat="server" Text="Classificação"></asp:Literal>:</strong>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="DDLClassification" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal4" runat="server" Text="Laboratório"></asp:Literal>:</strong>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="DDLLabs" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal5" runat="server" Text="Espécies"></asp:Literal>:</strong>
                    </td>
                    <td align="left">
                        <asp:CheckBoxList ID="CBLSpecies" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal6" runat="server" Text="Formas"></asp:Literal>:</strong>
                    </td>
                    <td  align="left">
                        <asp:CheckBoxList ID="CBLTypes" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                 <tr>
                    <td align="right">
                        <strong><asp:Literal ID="Literal7" runat="server" Text="Escolha a classificação"></asp:Literal>:</strong>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="TBCode" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Button ID="Button1" runat="server" Text="Pesquisar" OnClick="Button1_Click" />
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Label ID="LBResults" runat="server"></asp:Label>
        </td>
        
    </tr>
    <tr>
        <td>
            <Sharepoint:SPGridView runat="server" ID="SPGridViewResults" AutoGenerateColumns="false"></Sharepoint:SPGridView>
        </td>
    </tr>
     <tr>
        <td align="center">
            <asp:HyperLink ID="HLPrint" Visible="false" NavigateUrl="javascript:window.print()" ToolTip="Imprimir" runat="server">
            <asp:Literal ID="Literal9" runat="server" Text="Imprimir"></asp:Literal></asp:HyperLink>
        </td>
    </tr>
</table>
<asp:Literal ID="LiteralMsg" runat="server"></asp:Literal>

