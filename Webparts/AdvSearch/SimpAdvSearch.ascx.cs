﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Webparts.SimpAdvSearchWebPart
{
    public partial class SimpAdvSearch : System.Web.UI.UserControl
    {
        public string SiteUrl { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LiteralMsg.Text = String.Empty;

                if (!IsPostBack)
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        using (SPSite site = new SPSite(SiteUrl))
                        {
                            using (SPWeb web = site.OpenWeb())
                            {
                                #region Databind Controls
                                CBLSpecies.DataSource = HlprSimp.GetOrderedListItems(SimpLists.Especies, web);
                                CBLSpecies.DataTextField = "Title";
                                CBLSpecies.DataValueField = "ID";
                                CBLSpecies.DataBind();

                                CBLTypes.DataSource = HlprSimp.GetOrderedListItems(SimpLists.Formas, web);
                                CBLTypes.DataTextField = "Title";
                                CBLTypes.DataValueField = "ID";
                                CBLTypes.DataBind();

                                DDLClassification.DataSource = HlprSimp.GetOrderedListItems(SimpLists.Classificacoes, web);
                                DDLClassification.DataTextField = "Title";
                                DDLClassification.DataValueField = "ID";
                                DDLClassification.DataBind();
                                DDLClassification.Items.Insert(0, new ListItem(HlprSimp.LoadResourceFromContext("lbl_choose_classif"), "-1"));

                                DDLLabs.DataSource = HlprSimp.GetOrderedActiveListItems(SimpLists.Labs, web);
                                DDLLabs.DataTextField = "Title";
                                DDLLabs.DataValueField = "ID";
                                DDLLabs.DataBind();
                                DDLLabs.Items.Insert(0, new ListItem(HlprSimp.LoadResourceFromContext("lbl_choose_lab"), "-1"));

                                DDLActiveP.DataSource = HlprSimp.GetOrderedListItems(SimpLists.PrincipActivos, web);
                                DDLActiveP.DataTextField = "Title";
                                DDLActiveP.DataValueField = "ID";
                                DDLActiveP.DataBind();
                                DDLActiveP.Items.Insert(0, new ListItem(HlprSimp.LoadResourceFromContext("lbl_choose_actprincip"), "-1"));
                                #endregion
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {

                LiteralMsg.Text = "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// Permite construir as condições em CAML para uma Check Box List
        /// Encadeamento de "OR's" com as várias opções seleccionadas.
        /// </summary>
        /// <param name="field">Nome da coluna a comparar</param>
        /// <param name="cbl">A CheckBoxList</param>
        /// <returns></returns>
        private string GetCBLCAMLCondition(string field, CheckBoxList cbl)
        {
            string Condition = "<Eq><FieldRef Name=\"" + field + "\" LookupId=\"TRUE\" /><Value Type=\"LookupMulti\">{0}</Value></Eq>";
            //Obter os items seleccionados na CBL.
            List<ListItem> selectedItems = new List<ListItem>();
            foreach (ListItem item in cbl.Items)
            {
                if (item.Selected) selectedItems.Add(item);
            }
            if (selectedItems.Count == 0) return null;
            else if (selectedItems.Count == 1)
            {
                return String.Format(Condition, selectedItems[0].Value);

            }
            else
            {
                StringBuilder sb = new StringBuilder();
                //Aplicar <Or>
                for (int i = 0; i < selectedItems.Count - 1; ++i)
                {
                    sb.Append("<Or>");
                }
                sb.Append(String.Format(Condition, selectedItems[0].Value));
                for (int i = 1; i < selectedItems.Count; ++i)
                {
                    sb.Append(String.Format(Condition, selectedItems[i].Value));
                    sb.Append("</Or>");
                }
                return sb.ToString();
            }

        }
        /// <summary>
        /// Gerar a Query String CAML para efectuar a pesquisa.
        /// </summary>
        /// <returns></returns>
        private string GetCAMLQueryString(bool moderationEnabled)
        {

            StringBuilder sb = new StringBuilder();

            // Se for especificado Código de Classificação pesquisar apenas por este.
            if (!String.IsNullOrEmpty(TBCode.Text))
            {
                return "<Where><Eq><FieldRef Name=\"" + Fields.Codigo + "\" /><Value Type = \"Text\">" + TBCode.Text + "</Value></Eq></Where><OrderBy><FieldRef Name='Title' /></OrderBy>";
            }
            else
            {
                #region Gerar as várias condições e colocar numa lista
                List<String> conditions = new List<string>();

                /*
        * var queryString = @"<Where>
                      <Eq>
                        <FieldRef Name=’_ModerationStatus’/>
                        <Value Type=’ModStat’>{0}</Value>
                      </Eq>
                   </Where>";
queryString = string.Format(queryString,  
                           Resources.ModerationStatusApproved);*/
                if (moderationEnabled)
                    conditions.Add(String.Format("<Eq><FieldRef Name='_ModerationStatus'/><Value Type='ModStat'>{0}</Value></Eq>", IntResources.ModerationStatusApproved));
                if (!String.IsNullOrEmpty(TBName.Text))
                {
                    conditions.Add("<Contains><FieldRef Name=\"" + Fields.Titulo + "\" /><Value Type = \"Text\">" + TBName.Text + "</Value></Contains>");
                }
                if (DDLActiveP.SelectedValue != "-1")
                {
                    conditions.Add("<Eq><FieldRef Name=\"" + Fields.PrincipActivos + "\" LookupId=\"TRUE\" /><Value Type=\"LookupMulti\">" + DDLActiveP.SelectedValue + "</Value></Eq>");
                }
                if (DDLClassification.SelectedValue != "-1")
                {
                    conditions.Add("<Eq><FieldRef Name=\"" + Fields.Classificacao + "\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">" + DDLClassification.SelectedValue + "</Value></Eq>");
                }
                if (DDLLabs.SelectedValue != "-1")
                {
                    conditions.Add("<Eq><FieldRef Name=\"" + Fields.Laboratorio + "\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">" + DDLLabs.SelectedValue + "</Value></Eq>");
                }

                string speciesCond = GetCBLCAMLCondition(Fields.Especie, CBLSpecies);
                string typesCond = GetCBLCAMLCondition(Fields.Formas, CBLTypes);

                if (!String.IsNullOrEmpty(speciesCond))
                    conditions.Add(speciesCond);

                if (!String.IsNullOrEmpty(typesCond))
                    conditions.Add(typesCond);

                #endregion

                // Produzir o CAML propriamente dito com a concatenação de AND's entre as condições.
                if (conditions.Count > 0)
                {
                    sb.Append("<Where>");
                    if (conditions.Count == 1)
                        sb.Append(conditions[0]);
                    else
                    {
                        //Aplicar <Ands>
                        for (int i = 0; i < conditions.Count - 1; ++i)
                        {
                            sb.Append("<And>");
                        }
                        sb.Append(conditions[0]);
                        for (int i = 1; i < conditions.Count; ++i)
                        {
                            sb.Append(conditions[i]);
                            sb.Append("</And>");
                        }
                    }
                    sb.Append("</Where>");
                }
            }
            //sb.Append(HlprSimp.OrderByClassif);
            //Ordenar por titulo.
            sb.Append(HlprSimp.OrderByTitle);

            return sb.ToString();

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    // implementation details omitted

                    using (SPSite site = new SPSite(SiteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPList drugsList = web.Lists[SimpLists.Medicamentos];
                            SPQuery query = new SPQuery();
                            query.Query = GetCAMLQueryString(drugsList.EnableModeration);
                            Debug.Write(query.Query, "SimpAdvSearch");
                            SPListItemCollection res = drugsList.GetItems(query);
                            Debug.Write("Res: " + res.Count, "SimpAdvSearch");
                            if (res.Count == 0)
                                LBResults.Text = HlprSimp.LoadResourceFromContext("lbl_norecords");
                            else if (res.Count == 1)
                                LBResults.Text = HlprSimp.LoadResourceFromContext("lbl_onerecord");
                            else
                                LBResults.Text = String.Format(HlprSimp.LoadResourceFromContext("lbl_nrecords"), res.Count);

                            if (res.Count > 0)
                            {
                                //SPGridViewResults.EnableViewState = false;
                                //SPGridViewResults.AllowGrouping = true;
                                //SPGridViewResults.AllowGroupCollapse = true;
                                //SPGridViewResults.GroupField = Fields.Classificacao;
                                //SPGridViewResults.GroupFieldDisplayName = "Classificação";
                                HlprSimp.SetDrugsGridValues(res, SPGridViewResults, SiteUrl);
                                HLPrint.Visible = SPGridViewResults.Visible = true;
                            }
                            else
                                HLPrint.Visible = SPGridViewResults.Visible = false;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                LiteralMsg.Text = "Error:" + ex.Message;
            }
        }
    }
}

//HlprSimp; SimpLists; Fields