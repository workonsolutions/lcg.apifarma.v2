﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Text;
using System.Xml.Linq;
using System.Xml.Xsl;
using Convex.Sharepoint.Foundation.Webparts.Helper;

namespace Webparts.ContentInfoBoxStatic
{
    [ToolboxItemAttribute(false)]
    public class ContentInfoBoxStatic : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/ContentInfoBoxStatic/ContentInfoBoxStaticUserControl.ascx";

        #region Properties

        internal bool _debugMode = false;
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("Debug Mode"), WebDescription("Render debug steps instead of layout")]
        public bool DebugMode
        {
            get { return _debugMode; }
            set { _debugMode = value; }
        }

        private string imageContentInfoTitleUrl = null;
        [Personalizable(PersonalizationScope.Shared),
           WebBrowsable(true),
           WebDisplayName("Image Info Title in content (mandatory)"),
           WebDescription("Enter Image Info Title in content (mandatory)")]
        public string ImageContentInfoTitleUrl
        {
            get { return imageContentInfoTitleUrl; }
            set { imageContentInfoTitleUrl = value; }
        }

        private string contentInfoTitle = null;
        [Personalizable(PersonalizationScope.Shared),
           WebBrowsable(true),
           WebDisplayName("Title of Content Info (mandatory)"),
           WebDescription("Enter title (mandatory)")]
        public string ContentInfoTitle
        {
            get { return contentInfoTitle; }
            set { contentInfoTitle = value; }
        }

        private string contentInfoImageUrl = null;
        [Personalizable(PersonalizationScope.Shared),
           WebBrowsable(true),
           WebDisplayName("URL (location) of Content Info Image"),
           WebDescription("Enter URL of Image")]
        public string ContentInfoImageUrl
        {
            get { return contentInfoImageUrl; }
            set { contentInfoImageUrl = value; }
        }

        private string contentInfoImageHyperlink = null;
        [Personalizable(PersonalizationScope.Shared),
           WebBrowsable(true),
           WebDisplayName("Hyperlink for Content Info Image"),
           WebDescription("Enter Hyperlink for Image")]
        public string ContentInfoImageHyperlink
        {
            get { return contentInfoImageHyperlink; }
            set { contentInfoImageHyperlink = value; }
        }

        private string xsltFile = "/Style%20Library/XSL%20Style%20Sheets/ContentInfoBoxStatic.xslt";
        [WebBrowsable(true), Personalizable(true), System.ComponentModel.Category("Config"), WebDisplayName("Xslt file"), WebDescription("Url path to xslt transformation file")]
        public string XsltFile
        {
            get { return xsltFile; }
            set { xsltFile = value; }
        }

        #endregion

        #region Error Control

        private string _errorMessageException = "";
        private bool _showErrorMessageException = false;

        StringBuilder _debugMessage = new StringBuilder();
        private bool _showDebugMessage = false;

        #endregion

        private string _htmlOutput = "";
        private string _xsltOutput = "";

        public ContentInfoBoxStatic()
        {
            this.ExportMode = WebPartExportMode.All;
        }

        internal XElement CreateXmlData(string boxImageUrl, string boxTitle)
        {
            XElement xContent = new XElement("content");

            if (!string.IsNullOrEmpty(boxImageUrl))
                xContent.Add(new XElement("boxImageUrl", boxImageUrl));

            if (!string.IsNullOrEmpty(boxTitle))
                xContent.Add(new XElement("boxTitle", boxTitle));

            if (!string.IsNullOrEmpty(contentInfoImageUrl))
                xContent.Add(new XElement("contentInfoImageUrl", contentInfoImageUrl));

            if (!string.IsNullOrEmpty(contentInfoImageHyperlink))
                xContent.Add(new XElement("contentInfoImageHyperlink", contentInfoImageHyperlink));

            return xContent;
        }


        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        protected override void CreateChildControls()
        {
            _debugMessage.AppendLine(this.Title + ": Start CreateChildControls()");
            _debugMessage.AppendLine(this.Title + ": XsltFile=" + xsltFile);
            _debugMessage.AppendLine(this.Title + ": ContentInfoImageUrl=" + contentInfoImageUrl);
            _debugMessage.AppendLine(this.Title + ": ContentInfoImageHyperlink=" + contentInfoImageHyperlink);

            try
            {
                //only render the control when the page is in display mode
                if (SPContext.Current.FormContext.FormMode == SPControlMode.Display)
                {



                    #region Create xml data
                    _debugMessage.AppendLine(this.Title + ": Start creating xml data");


                    XElement xDoc = CreateXmlData(imageContentInfoTitleUrl, contentInfoTitle);

                    _debugMessage.AppendLine(this.Title + ": End creating xml data");

                    #endregion

                    _debugMessage.AppendLine(this.Title + ": Start converting xml into html");

                    XslCompiledTransform transform = Xslt.LoadXsltFromSPFile(SPContext.Current.Site.RootWeb.GetFile(xsltFile));

                    _xsltOutput = Xslt.XmlTransform(new XDocument(xDoc), transform);

                    _debugMessage.AppendLine(this.Title + ": End converting xml into html");
                }

                //}
            }
            catch (Exception ex)
            {
                //Display any error that comes up
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
                _debugMessage.AppendLine(string.Format("<strong>{0}</strong>", ex.Message));
                _showDebugMessage = true;
            }
            _debugMessage.AppendLine(this.Title + ": End CreateChildControls()");
        }

        /// <summary>
        /// Clear all child controls and add an error message for display.
        /// </summary>
        /// <param name="ex"></param>
        private void HandleException(Exception ex)
        {
            this.Controls.Clear();
            this.Controls.Add(new LiteralControl(ex.Message));
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!_showErrorMessageException)
            {
                writer.Write(_xsltOutput);
            }

            // Mostra a mensagem de erros completa (erro + log) caso seja necessário
            if (_showErrorMessageException)
            {
                writer.Write(string.Format("<div class=\"ms-error\">{0}</div>", _errorMessageException));
            }

            if (_debugMode || _showDebugMessage)
            {
                _debugMessage.Replace(Environment.NewLine, "<br/>");
                _debugMessage.Insert(0, "<div class=\"ms-error\">");
                _debugMessage.Append("</div>");

                writer.Write(_debugMessage.ToString());
            }
            else
            {
                writer.Write(_htmlOutput);
            }

        }
    }
}
