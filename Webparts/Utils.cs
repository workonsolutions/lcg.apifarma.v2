﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using System.Text.RegularExpressions;
using System.Web;

namespace Webparts
{
    public class Utils
    {
        public static string LoadResourceFromContext(string key)
        {
            return HttpContext.GetGlobalResourceObject("ApifarmaInternet", key).ToString();
        }

        /// <summary>
        /// get substring from body of page
        /// </summary>
        /// <param name="sText"></param>
        /// <param name="iLength"></param>
        /// <returns></returns>
        public static string GetDescriptionFromBody(string sText, string lenght)
        {
            if (String.IsNullOrEmpty(sText)) return String.Empty;
            sText = Regex.Replace(sText, @"<(.|\n)*?>", string.Empty);

            int iLength = 0;
            if (int.TryParse(lenght, out iLength))
            {
                int iSpot = 0;
                if (sText.Length > (iLength + 3))
                { sText = sText.Substring(0, iLength + 3); }

                if (sText.Length > iLength)
                    while (sText.Length > iLength)
                    {
                        iSpot = sText.LastIndexOf(" ");
                        iSpot = ((sText.LastIndexOf(".") < iSpot && sText.LastIndexOf(".") != -1) ? sText.LastIndexOf(".") : iSpot);
                        sText = sText.Substring(0, iSpot);
                    }
                return sText + " ...";
            }
            return string.Empty;
        }
        /// <summary>
        /// Verifies if list exists
        /// </summary>
        /// <param name="listas"></param>
        /// <param name="listName"></param>
        /// <returns></returns>
        public static SPList VerificaLista(SPListCollection listas, string listName)
        {
            try
            {
                return listas[listName];
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Verifies if view exists
        /// </summary>
        /// <param name="listas"></param>
        /// <param name="listName"></param>
        /// <returns></returns>
        public static SPView VerificaVista(SPList lista, string viewName)
        {
            try
            {
                return lista.Views[viewName];
            }
            catch
            {
                return null;
            }
        }

    }
}
