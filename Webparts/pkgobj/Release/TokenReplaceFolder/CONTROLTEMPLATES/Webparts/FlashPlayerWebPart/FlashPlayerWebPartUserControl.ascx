﻿<%@ Assembly Name="Webparts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=233ea1bde501e127" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FlashPlayerWebPartUserControl.ascx.cs" Inherits="Webparts.FlashPlayerWebPart.FlashPlayerWebPartUserControl" %>


<div class="content_box_info" xmlns:ms="urn:schemas-microsoft-com:xslt">
<div class="content_icon_container">
<img src=" /_layouts/APIFARMA/IMAGES/icon_links.png" >
</div>
<div class="content_box_info_corner_1"></div>
<div class="content_box_info_corner_2"></div>
<div class="content_box_info_corner_3"></div>
<div class="content_box_info_corner_4"></div>
<div class="content_box_info_title"><%= this.TitleBox %></div>
<div class="content_box_info_text">

<div id="flash" runat="server" style="margin: 0 auto 0 13%;">
    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"
        width="<%= this.BannerWidth %>" height="<%= this.BannerHeight %>" id="Banner" style=" left: 50%;">
        <param name="movie" value="<%= this.BannerUrl %>" />
        <param name="quality" value="high" />
        <param name="bgcolor" value="#FFFFFF" />
        <embed src="<%= this.BannerUrl %>"
            quality="high" bgcolor="#FFFFFF" width="<%= this.BannerWidth %>" height="<%= this.BannerHeight %>" name="Banner" align=""
            type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer">
        </embed>
    </object>
</div>

</div>
</div>
<div class="content_box_info_spacer" xmlns:ms="urn:schemas-microsoft-com:xslt"></div>


