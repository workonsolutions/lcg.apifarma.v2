﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Text;
using System.Xml.Linq;
using Convex.Sharepoint.Foundation.Webparts.Helper;

namespace Webparts.BannersLinks
{
    [ToolboxItemAttribute(false)]
    public class BannersLinks : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/BannersLinks/BannersLinksUserControl.ascx";

        private string imageLibName = null;
        private string imageUrl = null;
        private string bannersTitle = null;

        private const string sTitle = "Title";
        private const string sUrl = "Url";
        private const string sOrder = "NrOrdem";
        private const string sActive = "Activo";
        private const string sOpenNew = "AbrirNova";

        [Personalizable(PersonalizationScope.Shared),
            WebBrowsable(true),
            WebDisplayName("Image Library Name in RootWeb (mandatory)"),
            WebDescription("Enter Image Library in RootWeb (mandatory)")]
        public string ImageLibName
        {
            get { return imageLibName; }
            set { imageLibName = value; }
        }
        private int bannersLimit = 4;
        [Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("BannersLimit"),
        WebDescription("Maximum number of Banners Images to show.)")]
        public int BannersLimit
        {
            get { return bannersLimit; }
            set { bannersLimit = value; }
        }

        [Personalizable(PersonalizationScope.Shared),
           WebBrowsable(true),
           WebDisplayName("Title of Banners links (mandatory)"),
           WebDescription("Enter title (mandatory)")]
        public string BannersTitle
        {
            get { return bannersTitle; }
            set { bannersTitle = value; }
        }
        [Personalizable(PersonalizationScope.Shared),
           WebBrowsable(true),
           WebDisplayName("Title image url of Banners links (mandatory)"),
           WebDescription("Enter title image url(mandatory)")]
        public string ImageUrl
        {
            get { return imageUrl; }
            set { imageUrl = value; }
        }
        private string xsltFile = "/Style%20Library/XSL%20Style%20Sheets/Banners.xslt";
        [WebBrowsable(true), Personalizable(PersonalizationScope.Shared), WebDisplayName("Xslt file"), WebDescription("Url path to xslt transformation file")]
        public string XsltFile
        {
            get { return xsltFile; }
            set { xsltFile = value; }
        }
        private bool debugMode = false;
        [WebBrowsable(true), Personalizable(PersonalizationScope.Shared), WebDisplayName("Debug Mode"), WebDescription("Render debug steps instead of layout")]
        public bool DebugMode
        {
            get { return debugMode; }
            set { debugMode = value; }
        }
        public BannersLinks()
        {
            this.ExportMode = WebPartExportMode.All;
        }

        StringBuilder debugMessage = new StringBuilder();
        private bool showDebugMessage = false;
        //private string listName = "Banners";
        private string htmlOutput = "";

        private SPList GetList(SPWeb web, string listTitle)
        {
            try
            {
                return web.Lists[listTitle];
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        /// 
        /*
        protected override void CreateChildControls()
        {
            try
            {
                if (string.IsNullOrEmpty(this.imageLibName))
                {
                    LiteralControl html = new LiteralControl("Click <a href=\"javascript:MSOTlPn_ShowToolPane2Wrapper('Edit', this, '" + this.ID + "')\">open tool pane</a> to set webpart properties");
                    this.Controls.Add(html);
                }
                else
                {
                    SPWeb rootWeb = SPContext.Current.Site.RootWeb;
                    StringBuilder sb = new StringBuilder();
                    SPList list = GetList(rootWeb, ImageLibName);
                    if (list != null)
                    {
                        //query list to get sorted items
                        SPQuery oQuery = new SPQuery();
                        StringBuilder sbQuery = new StringBuilder();
                        //sbQuery.Append("<Where><And><And><IsNotNull><FieldRef Name='" + sTitle + "' /></IsNotNull><IsNotNull><FieldRef Name='" + sUrl + "' /></IsNotNull></And><Eq><FieldRef Name='" + sActive + "' /><Value Type=\"Boolean\">1</Value></Eq></And></Where><OrderBy><FieldRef Name='" + sOrder + "' Ascending='TRUE' /></OrderBy>");
                        sbQuery.Append("<Where><Eq><FieldRef Name='" + sActive + "' /><Value Type=\"Boolean\">1</Value></Eq></Where><OrderBy><FieldRef Name='" + sOrder + "' Ascending='TRUE' /></OrderBy>");
                        oQuery.RowLimit = (uint)BannersLimit;
                        oQuery.Query = sbQuery.ToString();
                        SPListItemCollection sortedList = list.GetItems(oQuery);

                        sb.Append("<div class=\"content_box_info\">");
                        sb.Append(String.Format("<div class=\"content_icon_container\"><img src=\"{0}\" alt=\"\" /></div>", imageUrl));
                        sb.Append("<div class=\"content_box_info_corner_1\"></div>");
                        sb.Append("<div class=\"content_box_info_corner_2\"></div>");
                        sb.Append("<div class=\"content_box_info_corner_3\"></div>");
                        sb.Append("<div class=\"content_box_info_corner_4\"></div>");
                        sb.Append(String.Format("<div class=\"content_box_info_title\">{0}</div>", bannersTitle));

                        sb.Append("<div class=\"content_box_info_text\">");
                        //DS: 27-09-2011 - Flash da Gripe - TODO: Alterar esta webpart para gerador de XSLT. 
                        sb.Append(@"<a href=""http://www.convex.pt"" style=""border:0px; padding:0px 10px 0px 10px; margin:0px 0px 5px 0px; display:block; height:210px; width:220px; background-color:#0099ff;""><script type=""text/javascript"">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','220','height','210','src','Apifarma_Gripe_215x205','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','wmode','transparent','movie','Apifarma_Gripe_215x205' ); //end AC code
</script><noscript><object classid=""clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"" codebase=""http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0"" width=""220"" height=""210"">
    <param name=""movie"" value=""Apifarma_Gripe_215x205.swf"" />
    <param name=""quality"" value=""high"" />
    <param name=""wmode"" value=""transparent"" />
    <embed src=""Apifarma_Gripe_215x205.swf"" width=""220"" height=""210"" quality=""high"" pluginspage=""http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash"" type=""application/x-shockwave-flash"" wmode=""transparent""></embed>
  </object>
</noscript>
</noscript></a>");
                        for (int i = 0; i < sortedList.Count; i++)
                        //foreach (SPListItem listItem in sortedList)
                        {
                            SPListItem listItem = sortedList[i];

                            string imgurl = listItem.File.ServerRelativeUrl;
                            string title = string.Empty;
                            string url = string.Empty;
                            bool active = false;
                            bool openNew = true;
                            Boolean.TryParse(listItem["AbrirNova"].ToString(), out openNew);
                            Boolean.TryParse(listItem[sActive].ToString(), out active);

                            if (active)
                            {
                                if (!string.IsNullOrEmpty(listItem.Title))
                                {
                                    title = listItem.Title;
                                }
                                url = listItem[sUrl] as string;
                                if (!String.IsNullOrEmpty(url))
                                {
                                    url = url.Substring(0, url.IndexOf(","));
                                }
                                else url = string.Empty;
                                sb.Append("<div class=\"relevant_link\">");

                                if (openNew)
                                    sb.Append(String.Format("<a target='_blank' href=\"{2}\"><img alt=\"{0}\" src=\"{1}\" class=\"relevant_link_img\" complete=\"complete\" /></a>", title, imgurl, url));
                                else
                                    sb.Append(String.Format("<a href=\"{2}\"><img alt=\"{0}\" src=\"{1}\" class=\"relevant_link_img\" complete=\"complete\" /></a>", title, imgurl, url));

                                sb.Append("</div>");
                            }
                        }
                        sb.Append("</div>");
                        sb.Append("</div>");
                        this.Controls.Add(new LiteralControl(sb.ToString()));
                    }
                    else
                    {
                        this.Controls.Clear();
                        this.Controls.Add(new LiteralControl("The list with the name: '" + ImageLibName + "' cannot be found in RootWeb."));
                        LiteralControl html = new LiteralControl("Click <a href=\"javascript:MSOTlPn_ShowToolPane2Wrapper('Edit', this, '" + this.ID + "')\">open tool pane</a> to set webpart properties");
                        this.Controls.Add(html);
                    }
                }
            }
            catch (Exception ex)
            {
                //Display any error that comes up
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("An error occurred: " + ex.Message.ToString()));
            }
        }
         * */

        protected override void Render(HtmlTextWriter writer)
        {
            if (DebugMode || showDebugMessage)
            {
                debugMessage.Replace(Environment.NewLine, "<br/>");
                debugMessage.Insert(0, "<div class=\"ms-error\">");
                debugMessage.Append("</div>");

                writer.Write(debugMessage.ToString());
            }
            else
            {
                writer.Write(htmlOutput);
            }
        }
        protected override void CreateChildControls()
        {
            try
            {
                debugMessage.AppendLine(this.Title + ": Start CreateChildControls()");
                debugMessage.AppendLine(this.Title + ": BannersLimit=" + this.BannersLimit);
                debugMessage.AppendLine(this.Title + ": XsltFile=" + this.XsltFile);

                //only render the control when the page is in display mode
                if (SPContext.Current.FormContext.FormMode == SPControlMode.Display)
                {
                    if (string.IsNullOrEmpty(this.imageLibName))
                    {
                        LiteralControl html = new LiteralControl("Click <a href=\"javascript:MSOTlPn_ShowToolPane2Wrapper('Edit', this, '" + this.ID + "')\">open tool pane</a> to set webpart properties");
                        this.Controls.Add(html);
                    }
                    else
                    {
                        SPWeb rootWeb = SPContext.Current.Site.RootWeb;
                        StringBuilder sb = new StringBuilder();
                        SPList list = GetList(rootWeb, ImageLibName);
                        if (list != null)
                        {
                            //query list to get sorted items
                            SPQuery oQuery = new SPQuery();
                            StringBuilder sbQuery = new StringBuilder();
                            //sbQuery.Append("<Where><And><And><IsNotNull><FieldRef Name='" + sTitle + "' /></IsNotNull><IsNotNull><FieldRef Name='" + sUrl + "' /></IsNotNull></And><Eq><FieldRef Name='" + sActive + "' /><Value Type=\"Boolean\">1</Value></Eq></And></Where><OrderBy><FieldRef Name='" + sOrder + "' Ascending='TRUE' /></OrderBy>");
                            sbQuery.Append("<Where><Eq><FieldRef Name='" + sActive + "' /><Value Type=\"Boolean\">1</Value></Eq></Where><OrderBy><FieldRef Name='" + sOrder + "' Ascending='TRUE' /></OrderBy>");
                            oQuery.RowLimit = (uint)BannersLimit;
                            oQuery.Query = sbQuery.ToString();
                            SPListItemCollection sortedList = list.GetItems(oQuery);

                            #region Create XML Data
                            debugMessage.AppendLine(this.Title + ": Start creatinsg xml data");
                            XElement xDoc = new XElement("banners");
                            foreach (SPListItem item in sortedList)
                            {
                                string imgurl = item.File.ServerRelativeUrl;
                                string title = item.Title;
                                string url = string.Empty;
                                bool active = false;
                                bool openNew = true;

                                if (item[sOpenNew] != null)
                                    Boolean.TryParse(item[sOpenNew].ToString(), out openNew);
                                if (item[sActive] != null)
                                    Boolean.TryParse(item[sActive].ToString(), out active);

                                url = item[sUrl] as string;
                                if (!String.IsNullOrEmpty(url))
                                {
                                    url = url.Substring(0, url.IndexOf(","));
                                }
                                else url = string.Empty;

                                xDoc.Add(new XElement("banner",
                                    new XElement("title", item.Title),
                                    new XElement("url", url),
                                    new XElement("imgurl", imgurl),
                                    new XElement("opennew", openNew)));

                            }
                            debugMessage.AppendLine(this.Title + ": End creating xml data");

                            #endregion

                            #region Convert xml data into html

                            debugMessage.AppendLine(this.Title + ": Start converting xml into html");
                            string xsltContents = SPContext.Current.Site.RootWeb.GetFileAsString(XsltFile);
                            htmlOutput = Xslt.Xml2Html(xDoc.ToString(), xsltContents);
                            debugMessage.AppendLine(this.Title + ": End converting xml into html");

                            #endregion

                        }
                        else
                        {
                            debugMessage.AppendLine(string.Format("<strong>The list with the name: '{0}' cannot be found in RootWeb.</strong>", ImageLibName));
                            showDebugMessage = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                debugMessage.AppendLine(string.Format("<strong>{0}</strong>", ex.Message));
                showDebugMessage = true;
            }

            debugMessage.AppendLine(this.Title + ": End CreateChildControls()");
        }
    }
}
