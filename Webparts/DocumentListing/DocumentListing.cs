﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Text;
using System.Globalization;
using System.Web.UI.HtmlControls;
using Microsoft.SharePoint.WebPartPages;
using System.Data;
using Convex.Sharepoint.Foundation;
using Microsoft.SharePoint.Utilities;

namespace Webparts.DocumentListing
{
    [ToolboxItemAttribute(false)]
    public class DocumentListing : Microsoft.SharePoint.WebPartPages.WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Webparts/DocumentListing/DocumentListingUserControl.ascx";

        const String defaultPaginas = "10";
        const String defaultItens = "10";

        //const String defaultCharsNumber = "200";

        private String _numeroPaginas = string.Empty;
        private String _numeroItens = string.Empty;
        private String _numeroItensRecursivos = string.Empty;
        private string _web = string.Empty;
        private string _nomeBiblioteca = string.Empty;
        private string _titulo = string.Empty;

        private StringBuilder renderLista = new StringBuilder();
        private NumberFormatInfo provider = new NumberFormatInfo();

        //control to render by the webpart
        private HtmlGenericControl dv = new HtmlGenericControl("div");
        #region Properties

        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Biblioteca")]
        [Description("Nome da biblioteca")]
        [Personalizable]
        [WebBrowsable]
        public string NomeBiblioteca
        {
            get
            {
                return _nomeBiblioteca;
            }
            set
            {
                _nomeBiblioteca = value;
            }
        }

        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Web")]
        [Description("Web onde procurar a biblioteca")]
        [Personalizable]
        [WebBrowsable]
        public string Web
        {
            get
            {
                return _web;
            }
            set
            {
                _web = value;
            }
        }
        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Inserir Titulo")]
        [Description("Inserir Titulo")]
        [Personalizable]
        [WebBrowsable]
        public string Titulo
        {
            get
            {
                return _titulo;
            }
            set
            {
                _titulo = value;
            }
        }


        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Numero de páginas")]
        [Description("Numero de paginas na paginação")]
        [DefaultValue(defaultPaginas)]
        [Personalizable]
        [WebBrowsable]
        public string NumeroPaginas
        {
            get
            {
                return _numeroPaginas;
            }
            set
            {
                _numeroPaginas = value;
            }
        }

        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("Numero de Itens")]
        [Description("Numero de Itens por página")]
        [DefaultValue(defaultItens)]
        [Personalizable]
        [WebBrowsable]
        public string NumeroItens
        {
            get
            {
                return _numeroItens;
            }
            set
            {
                _numeroItens = value;
            }
        }



        #endregion
        public DocumentListing()
        {
            this.ExportMode = WebPartExportMode.All;
            this.ChromeType = PartChromeType.None;
        }

        int? pagActual = null;
        private int PaginaActual
        {
            get
            {
                if (pagActual == null)
                {
                    if (this.Page.Request.QueryString["pg"] != null)
                        pagActual = Int32.Parse(this.Page.Request.QueryString["pg"].ToString());
                }
                return pagActual.Value;
            }
            set
            {
            }
        }

        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        protected override void CreateChildControls()
        {
            if (String.IsNullOrEmpty(NumeroPaginas)) NumeroPaginas = defaultPaginas;
            if (String.IsNullOrEmpty(NumeroItens)) NumeroItens = defaultItens;
            if (String.IsNullOrEmpty(NomeBiblioteca)) NomeBiblioteca = SPContext.Current.ListItem.Title;

            if (SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                LoadControls();
            }
            else
            {

                renderLista.Append("<div class='modulo'><div class='modulo_header'>");
                renderLista.Append("<div class='title'>");
                renderLista.Append(SPContext.Current.Web.Title);
                renderLista.Append("<br /><span class='subtitle'>");
                renderLista.Append(" </span></div></div>");
                renderLista.Append("<div class='modulo_conteudo'>");


                renderLista.Append(" <div class='list_item'>");
                renderLista.Append(" <div class='link-item'>");
                renderLista.Append(" <a href='#'>Item 1</a>");
                renderLista.Append(" <div class='description'>Descrição Item 1</div>");
                renderLista.Append("</div></div>");
                renderLista.Append(" <div class='list_item'>");
                renderLista.Append(" <div class='link-item'>");
                renderLista.Append(" <a href='#'>Item 2</a>");
                renderLista.Append(" <div class='description'>Descrição Item 2</div>");
                renderLista.Append("</div></div>");
                renderLista.Append(" <div class='list_item'>");
                renderLista.Append(" <div class='link-item'>");
                renderLista.Append(" <a href='#'>Item 3</a>");
                renderLista.Append(" <div class='description'>Descrição Item 3</div>");
                renderLista.Append("</div></div>");

                renderLista.Append("</div>");

                renderLista.Append("<div id='pager'></div></div>");


            }
        }

        protected void LoadControls()
        {
            SPWeb myWeb = null;
            bool dispose = false;
            try
            {
                if (String.IsNullOrEmpty(Web))
                    myWeb = SPContext.Current.Web;
                else
                {
                    if (SPContext.Current.Site.RootWeb.Webs[Web].Exists)
                    {
                        myWeb = SPContext.Current.Site.RootWeb.Webs[Web];
                        dispose = true;
                    }
                    else
                    {
                        renderLista.Append("Web " + Web + " não foi encontrada");
                        return;
                    }
                }
                DataTable dt = null;
                int paginaActual = 1;
                int totalPaginas = 0;
                int numeroSeccao = 0;


                SPList listaPaginas = myWeb.Lists[NomeBiblioteca];
                SPQuery query = new SPQuery();
                query.Query = "<Query><OrderBy><FieldRef Name=\"Modified\" Ascending=\"False\" /></OrderBy></Query>";
                dt = listaPaginas.GetItems(query).GetDataTable();

                if (this.Page.Request.QueryString["pg"] != null)
                    paginaActual = Int32.Parse(this.Page.Request.QueryString["pg"].ToString(), provider);

                if ((dt != null) && (dt.Rows.Count > 0))
                {
                    numeroSeccao = (paginaActual / Int32.Parse(NumeroPaginas, provider));
                    if ((paginaActual % Int32.Parse(NumeroPaginas, provider)) > 0) // nº seccoes de paginas
                        numeroSeccao++;

                    int items = Int32.Parse(NumeroItens, provider);

                    totalPaginas = (dt.Rows.Count / items);//calcula o nº de paginas 
                    if ((dt.Rows.Count % Int32.Parse(NumeroItens, provider)) > 0)
                        totalPaginas++;


                    if (paginaActual <= totalPaginas)
                    {

                        if (paginaActual == totalPaginas)
                        {
                            //se estou na ultima pagina e tenho o numero de items a apresentar tantos como os que são apresentados por pagina
                            int numItemsDT = dt.Rows.Count;
                            int numItemsRestPages = (totalPaginas - 1) * Int32.Parse(NumeroItens, provider);
                            if ((dt.Rows.Count - numItemsRestPages) == Int32.Parse(NumeroItens, provider))
                                items = Int32.Parse(NumeroItens, provider);
                            else
                                items = (dt.Rows.Count % Int32.Parse(NumeroItens, provider));        //verifica se esta na ultima pagina se sim altera o nº de items aparecer
                        }

                        if (dt.Rows.Count <= Int32.Parse(NumeroItens, provider)) //se o nº items for inferior ao da pagina 
                            items = dt.Rows.Count;


                        renderLista.Append("<table width='100%' cellspacing='0' cellpadding='0' border='0'>");

                        //Debug.Write("pagertop");
                        #region "PAGERTOP"


                        if ((dt != null) && (dt.Rows.Count > 0))
                        {


                            int limiteSeccao = (numeroSeccao * Int32.Parse(NumeroPaginas, provider));

                            int totalSeccoes = (totalPaginas / Int32.Parse(NumeroPaginas, provider));

                            if (((totalPaginas) % (Int32.Parse(NumeroPaginas, provider))) > 0)
                                totalSeccoes++;


                            if (numeroSeccao == totalSeccoes)
                                limiteSeccao = totalPaginas;

                            if (totalPaginas > 1)
                            {
                                renderLista.Append("<tr><td>");

                                renderLista.Append("<div id='pagerTop' class='content_number'>");
                                renderLista.Append("<span class=\"content_page\">Página: </span>");
                                //Navegacao de paginacao
                                if ((totalPaginas > Int32.Parse(NumeroPaginas, provider)) && (!(numeroSeccao == totalSeccoes)))
                                {
                                    renderLista.Append("<div id='fwd' class='page_btn' onmouseover='JavaScript:pagerBtn('fwd','OVER');' onfocus='JavaScript:pagerBtn('fwd','OVER');' onmouseout='JavaScript:pagerBtn('fwd','OUT');' onblur='JavaScript:pagerBtn('fwd','OUT');'><a href='?pg=");
                                    renderLista.Append(((numeroSeccao * Int32.Parse(NumeroPaginas, provider)) + 1));
                                    renderLista.Append("'>»</a></div>");
                                    //renderLista.Append("'><img src='/SiteCollectionImages/arrow_fwd.gif' alt='avançar' id='fwd_img'/></a></div>");
                                }


                                for (int i = ((numeroSeccao - 1) * Int32.Parse(NumeroPaginas, provider)) + 1; i <= limiteSeccao; i++)
                                {

                                    if (i == paginaActual)
                                    {
                                        renderLista.AppendFormat("<span class=\"content_page_number\">{0}</span>", i);
                                    }
                                    else
                                    {
                                        renderLista.AppendFormat("<a class=\"content_page_number\"href='?pg={0}'>{0}</a>", i);
                                    }

                                }
                                renderLista.Append("</div>");
                                //pager
                                //renderLista.Append("<div id='pagerTop' class='pagerListagem'>");
                                ////Navegacao de paginacao
                                //if ((totalPaginas > Int32.Parse(NumeroPaginas, provider)) && (!(numeroSeccao == totalSeccoes)))
                                //{
                                //    renderLista.Append("<div id='fwd' class='page_btn' onmouseover='JavaScript:pagerBtn('fwd','OVER');' onfocus='JavaScript:pagerBtn('fwd','OVER');' onmouseout='JavaScript:pagerBtn('fwd','OUT');' onblur='JavaScript:pagerBtn('fwd','OUT');'><a href='?pg=");
                                //    renderLista.Append(((numeroSeccao * Int32.Parse(NumeroPaginas, provider)) + 1));
                                //    renderLista.Append("'>»</a></div>");
                                //    //renderLista.Append("'><img src='/SiteCollectionImages/arrow_fwd.gif' alt='avançar' id='fwd_img'/></a></div>");
                                //}

                                //renderLista.Append("<div id='paginacaoTop' ><ul class='content_paging'>");
                                //for (int i = ((numeroSeccao - 1) * Int32.Parse(NumeroPaginas, provider)) + 1; i <= limiteSeccao; i++)
                                //{

                                //    if (i == paginaActual)
                                //    {
                                //        renderLista.Append("<li class='pageSelected'>");
                                //        renderLista.Append(i);
                                //        renderLista.Append("</li>");
                                //    }
                                //    else
                                //    {
                                //        renderLista.Append("<li><a href='?pg=");
                                //        renderLista.Append(i);
                                //        renderLista.Append("'>");
                                //        renderLista.Append(i);
                                //        renderLista.Append("</a></li>");
                                //        //renderLista.Append("<div class='separador_V'><img src='/_layouts/images/blank.gif' alt='' /></div>");
                                //    }

                                //}
                                //renderLista.Append("</ul></div>");

                                //Navegacao de paginacao
                                if (!(paginaActual <= Int32.Parse(NumeroPaginas, provider)))
                                {
                                    renderLista.Append("<div id='bcktop' class='page_btn' onmouseover='JavaScript:pagerBtn('bck','OVER');' onfocus='JavaScript:pagerBtn('bck','OVER');' onmouseout='JavaScript:pagerBtn('bck'','OUT');' onblur='JavaScript:pagerBtn('bck'','OUT');'><a href='?pg=");
                                    renderLista.Append(((numeroSeccao - 1) * Int32.Parse(NumeroPaginas, provider)));
                                    //renderLista.Append("'><img src='/SiteCollectionImages/arrow_bck_end.gif' alt='retroceder' id='bck_img'/></a></div>");
                                    renderLista.Append("'>«</a></div>");
                                    renderLista.Append("<div class='separador_V'><img src='/_layouts/images/blank.gif' alt='' /></div>");
                                }
                                renderLista.Append("</div>");
                                renderLista.Append("<br/>");
                            }
                        }





                        renderLista.Append("</td></tr>");
                        renderLista.Append("<tr><td class='linebreakListagem'><img alt='' src='/_layouts/images/blank.gif' /></td></tr>");
                        #endregion

                        //Debug.Write("items");
                        #region "Items"
                        renderLista.Append("<tr><td>");


                        //clearfix
                        renderLista.Append("<div class='clearfix'>");

                        // Debug.Write("TitulosWebpart");
                        //Titulos da webpart
                        //if (TitulosWebpart)
                        //{
                        //    renderLista.Append("<div >");
                        //    renderLista.Append("<div class='titleListagem'>");
                        //    renderLista.Append("<div class='title'>");
                        //    renderLista.Append(myWeb.Title);
                        //    renderLista.Append("<br /><span class='subtitleListagem'>");
                        //    renderLista.Append(SubTitulo);
                        //    renderLista.Append(" </span></div></div>");
                        //}

                        //renderLista.Append("<br/>");

                        //Conteudo - items da lista da consulta - div modulo_conteudo
                        renderLista.Append("<div class='conteudoListagem'>");

                        renderLista.Append("<table cellpadding='0' cellspacing='0' border='0' width='100%'>");


                        for (int i = ((Int32.Parse(NumeroItens, provider)) * (paginaActual - 1)); i < (((Int32.Parse(NumeroItens, provider)) * (paginaActual - 1)) + items); i++)
                        {
                            //item of list
                            string fName = dt.Rows[i].ItemArray[dt.Columns["LinkFilename"].Ordinal].ToString();

                            string url = CvxUtility.CombineUrl(listaPaginas.ParentWebUrl, listaPaginas.RootFolder.Url, fName);
                            renderLista.Append(@"<tr class=""list_item"">");


                            //link Item
                            renderLista.Append(@"<td style='width:100%;' class='list_itemNoticias'>");
                            renderLista.Append(" <div>");


                            renderLista.Append("<p class=\"content_title\">");

                            string title;
                            if (!String.IsNullOrEmpty(dt.Rows[i].ItemArray[dt.Columns["Title"].Ordinal] as string))
                                title = dt.Rows[i].ItemArray[dt.Columns["Title"].Ordinal] as string;
                            else
                                title = fName;
                            renderLista.Append(String.Format("<img src='/_layouts/images/{0}' />", SPUtility.MapToIcon(SPContext.Current.Web, fName, fName)));
                            renderLista.Append(String.Format("&nbsp;<a class='content_title_sub' href='{0}'>{1}</a>", url, title));

                            renderLista.AppendFormat("<span class=\"content_title_data\"> {0}</span>", DateTime.Parse(dt.Rows[i].ItemArray[dt.Columns["Modified"].Ordinal].ToString()).ToShortDateString());

                            //<span class="content_title_data"> | 05/03/2010</span>
                            renderLista.Append("</p>");


                            ////Description
                            //string text = null;
                            // if (dt.Columns.Contains("Description") && !String.IsNullOrEmpty(text = dt.Rows[i].ItemArray[dt.Columns["Description"].Ordinal] as string))
                            //{
                            //    renderLista.Append(text);
                            //}



                            renderLista.Append("<div style='height: 5px; padding-bottom: 0px; padding-top: 0px; border-bottom: #999999 1px dotted; width: 100%; float: left'></div>");

                            renderLista.Append("</div>");

                            renderLista.Append("</td>");
                            renderLista.Append("</tr>");
                        }

                        renderLista.Append("</table>");
                        //end div modulo_conteudo
                        renderLista.Append("</div>");
                        renderLista.Append("<br/>");
                        //end of div clearfix
                        renderLista.Append("</div>");



                        renderLista.Append("</td></tr>");
                        #endregion

                        //Debug.Write("pagerbottom");
                        #region "PAGER"
                        renderLista.Append("<tr><td>");


                        //pager
                        renderLista.Append("<div id='pager' class='pagerListagem'>");

                        if ((dt != null) && (dt.Rows.Count > 0))
                        {
                            int limiteSeccao = (numeroSeccao * Int32.Parse(NumeroPaginas, provider));

                            int totalSeccoes = (totalPaginas / Int32.Parse(NumeroPaginas, provider));

                            if (((totalPaginas) % (Int32.Parse(NumeroPaginas, provider))) > 0)
                                totalSeccoes++;


                            if (numeroSeccao == totalSeccoes)
                                limiteSeccao = totalPaginas;

                            if (totalPaginas > 1)
                            {
                                //Navegacao de paginacao
                                if ((totalPaginas > Int32.Parse(NumeroPaginas, provider)) && (!(numeroSeccao == totalSeccoes)))
                                {
                                    renderLista.Append("<div id='fwd' class='page_btn' onmouseover='JavaScript:pagerBtn('fwd','OVER');' onfocus='JavaScript:pagerBtn('fwd','OVER');' onmouseout='JavaScript:pagerBtn('fwd','OUT');' onblur='JavaScript:pagerBtn('fwd','OUT');'><a href='?pg=");
                                    renderLista.Append(((numeroSeccao * Int32.Parse(NumeroPaginas, provider)) + 1));
                                    renderLista.Append("'>»</a></div>");
                                    //renderLista.Append("'><img src='/SiteCollectionImages/arrow_fwd.gif' alt='avançar' id='fwd_img'/></a></div>");
                                }

                                renderLista.Append("<div id='paginacao' class='content_number'>");
                                renderLista.Append("<span class=\"content_page\">Página: </span>");


                                for (int i = ((numeroSeccao - 1) * Int32.Parse(NumeroPaginas, provider)) + 1; i <= limiteSeccao; i++)
                                {

                                    if (i == paginaActual)
                                    {
                                        renderLista.AppendFormat("<span class=\"content_page_number\">{0}</span>", i);
                                    }
                                    else
                                    {
                                        renderLista.AppendFormat("<a class=\"content_page_number\"href='?pg={0}'>{0}</a>", i);
                                    }

                                }
                                renderLista.Append("</div>");

                                //Navegacao de paginacao
                                if (!(paginaActual <= Int32.Parse(NumeroPaginas, provider)))
                                {
                                    renderLista.Append("<div id='bcktop' class='page_btn' onmouseover='JavaScript:pagerBtn('bck','OVER');' onfocus='JavaScript:pagerBtn('bck','OVER');' onmouseout='JavaScript:pagerBtn('bck'','OUT');' onblur='JavaScript:pagerBtn('bck'','OUT');'><a href='?pg=");
                                    renderLista.Append(((numeroSeccao - 1) * Int32.Parse(NumeroPaginas, provider)));
                                    //renderLista.Append("'><img src='/SiteCollectionImages/arrow_bck_end.gif' alt='retroceder' id='bck_img'/></a></div>");
                                    renderLista.Append("'>«</a></div>");
                                    renderLista.Append("<div class='separador_V'><img src='/_layouts/images/blank.gif' alt='' /></div>");
                                }
                            }
                        }


                        renderLista.Append("</div>");

                        renderLista.Append("</td></tr>");
                        #endregion
                        renderLista.Append("</table>");


                    }
                }
                else
                {
                    renderLista.Append("<div>" + Utils.LoadResourceFromContext("lbl_listing_noresults") + "</div>");
                }

            }
            catch (Exception es)
            {
                renderLista.Append(es.Message);
                renderLista.Append("</br>");
                renderLista.Append(es.Source);
                renderLista.Append("</br>");
            }
            finally
            {
                if (myWeb != null && dispose)
                    myWeb.Dispose();
            }
        }

        private static String VerificarLista(SPListCollection Lista)
        {

            String NomeLista = "Páginas";

            if (Lista != null)
                foreach (SPList lista in Lista)
                {
                    if (lista.Title == "Pages")
                    {
                        NomeLista = lista.Title;
                        break;
                    }
                }
            return NomeLista;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.Write(renderLista);

            //render Div to html in page
            dv.RenderControl(writer);
        }



        /// <summary>
        /// Clear all child controls and add an error message for display.
        /// </summary>
        /// <param name="ex"></param>
        private void HandleException(Exception ex)
        {

            this.Controls.Clear();
            this.Controls.Add(new LiteralControl(ex.Message));
        }
    }
}
