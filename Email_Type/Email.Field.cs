﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Security;
using System.Windows.Controls;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.IO;
using System.Web;
using System.Web.UI;
using Email.SharePoint.WebControls;

namespace Email.SharePoint
{
    public class EmailField : SPFieldText
    {
        public EmailField(SPFieldCollection fields, string fieldName)
            : base(fields, fieldName)
        { }

        public EmailField(SPFieldCollection fields, string typeName, string displayName)
            : base(fields, typeName, displayName)
        { }

        public override BaseFieldControl FieldRenderingControl
        {
            [SharePointPermission(SecurityAction.LinkDemand, ObjectModel = true)]
            get
            {
                EmailFieldControl ctr = new EmailFieldControl();
                ctr.FieldName = this.InternalName;
                return ctr;
            }
        }


    }
}