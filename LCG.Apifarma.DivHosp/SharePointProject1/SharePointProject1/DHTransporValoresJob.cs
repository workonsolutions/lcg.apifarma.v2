﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.Globalization;
using System.Threading;
using SharePointProject1.Helper;

namespace SharePointProject1
{
    public class DHTransporValoresJob : SPJobDefinition
    {public static string JobName = "DivHosp_TransporValores";

        public DHTransporValoresJob() : base() { }

        public DHTransporValoresJob(string name, SPWebApplication webApplication)
            : base(name, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = name;
        }

        public DHTransporValoresJob(SPWebApplication webApplication)
            : base(JobName, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = JobName;
        }

        private void Run_TransporValores()
        {
            try
            {
                DateTime dtMonth = DateTime.Now.AddMonths(-1);
                Inquerito.TransporValores(dtMonth.Year, dtMonth.Month);

            }
            catch (Exception ex)
            {
                Inquerito.LogMessage("Run Transpor Valores Exception: " + ex.Message);
            }
        }

        public override void Execute(Guid targetInstanceId)
        {
            Inquerito.LogMessage("Inicio do Job Transpor Valores");
            CultureInfo ptCulture = new CultureInfo("pt-PT");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ptCulture;

            try
            {

                SPWebApplication webApplication = this.Parent as SPWebApplication;
                SPContentDatabase contentDb = webApplication.ContentDatabases[targetInstanceId];

                using (SPSite site = contentDb.Sites["/"])
                {
                    //DS: 20110627 - Apifarma decidiu que a transposição dos valores deve ocorrer no inicio do periodo de entrega. Processo deverá correr no dia 1 de cada mês.
                    //if (DateTime.Now.Day == DivHospConfig.Inquerito.DiaMesFimPrazoEntrega+1)
                    if (DateTime.Now.Day == 1) // primeiro dia do mês. --> Experimentar hoje(DateTime.Now.Day == 21): Fazer backup à BD de dh e ver se os logs são registados
                    {
                        Run_TransporValores();
                    }

                }
            }
            catch (Exception ex)
            {
                Inquerito.LogMessage("DH Transpor Valores Exception (execute): " + ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = currentCulture;
            }
        }
       
    }
}
