using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace SharePointProject1.Features.LCG.ApifarmaDH.Jobs
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("386764bb-7fb7-4538-9b43-e90110547b4a")]
    public class LCGApifarmaDHEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            #region Install Job Definitions

            SPJobDefinition JobInquerito = null, JobValores = null, TransporValores = null;

            JobInquerito = new DHInqueritoJob(((SPSite)properties.Feature.Parent).WebApplication);

            JobInquerito.Schedule = SPDailySchedule.FromString("daily at 01:00:00"); ; 

            JobInquerito.Update();

            JobValores = new DHTransporValoresJob(((SPSite)properties.Feature.Parent).WebApplication);

            JobValores.Schedule = SPDailySchedule.FromString("daily at 02:00:00"); ;

            JobValores.Update();

            #endregion
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            #region Uninstall Job Definitions
            foreach (SPJobDefinition job in ((SPSite)properties.Feature.Parent).WebApplication.JobDefinitions)
            {
                if (job.Title.StartsWith("DivHosp_"))
                {
                    job.Delete();
                }
            }
            #endregion
        }


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
