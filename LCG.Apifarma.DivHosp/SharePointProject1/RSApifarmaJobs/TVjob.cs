﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.Globalization;
using System.Threading;
using Convex.Clientes.Apifarma.DivHosp.DAL;

namespace RSApifarmaJobs
{
    class TVjob : SPJobDefinition
    {
        public static string JobName = "LCG_TransporValores";

        public TVjob() : base() { }

        public TVjob(SPWebApplication webApplication)
            : base(JobName, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = JobName;
        }

        public override void Execute(Guid targetInstanceId)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\TVLogs.txt", true))
            {
                file.WriteLine("Transpor Valores NEW");
            }
            CultureInfo ptCulture = new CultureInfo("pt-PT");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ptCulture;

            try
            {

                SPWebApplication webApplication = this.Parent as SPWebApplication;
                SPContentDatabase contentDb = webApplication.ContentDatabases[targetInstanceId];

                using (SPSite site = contentDb.Sites["/"])
                {
                    DateTime dtMonth = DateTime.Now.AddMonths(-1);

                    //Meter aqui o codigo do inquerito

                    int anoInquerito = dtMonth.Year;
                    int mesInquerito = dtMonth.Month;
                    DateTime dataMesInquerito = new DateTime(anoInquerito, mesInquerito, 1);
                    DateTime dataMesAnterior = dataMesInquerito.AddMonths(-1);

                    StringBuilder sbInfo = new StringBuilder();

                    using (DividasHospitalaresEntities context = new DividasHospitalaresEntities())
                    {

                        //Obter valores inseridos no mês
                        var resultadosMesInquerito = from r in context.ResultadoSet
                                                     where r.Data == dataMesInquerito
                                                     select r;


                        //Obter valores inseridos no mês anterior
                        var resultadosMesAnterior = from r in context.ResultadoSet.Include("Hospital").Include("AreaNegocio").Include("Associado")
                                                    where r.Data == dataMesAnterior
                                                    select r;

                        //Como no carregamento do excel com o inquérito são validados os dados do mês anterior, aqui só interessa validar associados que não responderam.

                        Associado[] associadosMesInquerito = (from r in resultadosMesInquerito group r by r.Associado into g select g.Key).ToArray();
                        Associado[] associadosMesAnterior = (from r in resultadosMesAnterior group r by r.Associado into g select g.Key).ToArray();


                        foreach (Associado associado in associadosMesAnterior)
                        {


                            if (!associadosMesInquerito.Contains(associado))//, AssociadoComparer.GetInstance()))
                            {
                                var results = from r in resultadosMesAnterior where r.Associado.AssociadoID == associado.AssociadoID select r;

                                foreach (var r in results)
                                {
                                    //Resultado "nulo" ? 
                                    if ((r.Divida90 == null || r.Divida90.Value == 0) &&
                                        (r.DividaTotal == null || r.DividaTotal.Value == 0) &&
                                        (r.PMR == null || r.PMR.Value == 0))
                                        continue;

                                    //InsertCopia
                                    Resultado novoResultado = new Resultado();
                                    novoResultado.AreaNegocio = r.AreaNegocio;
                                    novoResultado.Associado = r.Associado;
                                    novoResultado.Hospital = r.Hospital;
                                    if (r.PMR != null)
                                        novoResultado.PMR = r.PMR;
                                    if (r.Divida90 != null)
                                        novoResultado.Divida90 = r.Divida90;
                                    if (r.DividaTotal != null)
                                        novoResultado.DividaTotal = r.DividaTotal;
                                    if (r.Divida60 != null)
                                        novoResultado.Divida60 = r.Divida60;
                                    novoResultado.Data = dataMesInquerito;

                                    context.AddToResultadoSet(novoResultado);
                                }
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\TVLogs.txt", true))
                {
                    file.WriteLine(ex.Message);
                }
            }
        }
    }
}
