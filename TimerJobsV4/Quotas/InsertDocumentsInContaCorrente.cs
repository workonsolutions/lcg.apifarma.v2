﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using System.Globalization;
using System.Threading;
using Convex.Clientes.Apifarma.Intranet.Helper;
using System.Data;
using System.IO;
using Convex.Sharepoint.Foundation.Extensions;
using System.Diagnostics;

namespace TimerJobsV4.Quotas
{
    class InsertDocumentsInContaCorrente : SPJobDefinition
    {
        public InsertDocumentsInContaCorrente()
            : base()
        {
        }

        public InsertDocumentsInContaCorrente(string name, SPWebApplication webApplication)
            : base(name, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = name;
        }

        private class AFQuotasDefaultsCalculo
        {
            public int ID;
            public int Ano;
            public double JoiaMinima;
            public double JoiaPermilagem;
            public double QuotaMinima;
            public double QuotaPermilagem;
            public int QuotasAno;
            public int QuotasDiasVencimento;
        }

        private class AFQuotasVolumesFacturacao
        {
            public int ID;
            public int IDAssociado;
            public int Ano;
            public double Valor;
            public SPModerationStatusType ModerationStatus;
        }

        public override void Execute(Guid contentDbId)
        {
            CultureInfo ptCulture = new CultureInfo("pt-PT");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ptCulture;
            try
            {
                SPWebApplication webApplication = this.Parent as SPWebApplication;
                SPContentDatabase contentDb = webApplication.ContentDatabases[contentDbId];

                using (SPSite site = contentDb.Sites["/"])
                {
                    // HlpIntranet.Quotas.WebUrl = "/entidades/quotas"
                    using (SPWeb web = site.OpenWeb(HlpIntranet.Quotas.WebUrl))
                    {
                        List<string[]> errors = new List<string[]>();
                        List<AFQuotasDefaultsCalculo> defaultsCalculo = new List<AFQuotasDefaultsCalculo>();
                        List<AFQuotasVolumesFacturacao> volumesFacturacao = new List<AFQuotasVolumesFacturacao>();

                        // Para contornar os problemas de histórico.
                        // Numa situação ideal seria desnecessário.
                        DateTime dataMinimaInicioCalculo = new DateTime(2010, 4, 1);
                        DateTime today = DateTime.Today;

                        StringBuilder caml;
                        // "lists/contacorrente"
                        SPList listContaCorrente = web.Lists[HlpIntranet.Quotas.Lists.ContaCorrente.Nome];
                        // "lists/declaracaofacturacao"
                        SPList listDeclaracoesFacturacao = web.Lists[HlpIntranet.Quotas.Lists.DeclaracoesFacturacao.Nome];
                        // "lists/declaracaofacturacao_detalhe"
                        SPList listDeclaracoesFacturacaoDt = web.Lists[HlpIntranet.Quotas.Lists.DeclaracoesFacturacaoDetalhe.Nome];
                        // "lists/defaults"
                        SPList listDefaults = web.Lists[HlpIntranet.Quotas.Lists.Defaults.Nome];
                        SPListItemCollection itemsContaCorrente = null;
                        SPListItemCollection itemsDeclaracoesFacturacao = null;
                        SPListItemCollection itemsDeclaracoesFacturacaoDt = null;
                        SPListItemCollection itemsDefaults = null;
                        SPListItemCollection itemsAssociados = null;

                        #region Typed Objects

                        // lista ("entidades/quotas/lists/defaults") com os valores para cálculo das quotas
                        defaultsCalculo = (from dc in listDefaults.Items.Cast<SPListItem>()
                                           select new AFQuotasDefaultsCalculo
                                           {
                                               ID = dc.ID,
                                               Ano = int.Parse(dc.Title),
                                               JoiaMinima = dc.GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.JoiaMinima),
                                               JoiaPermilagem = dc.GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.JoiaPermilagem),
                                               QuotaMinima = dc.GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.QuotaMinima),
                                               QuotaPermilagem = dc.GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.QuotaPermilagem),
                                               QuotasAno = (int)dc.GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.QuotasAno),
                                               QuotasDiasVencimento = (int)dc.GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.QuotasDiasVencimento)
                                           }).ToList();

                        // lista ("entidades/quotas/lists/declaracaofacturacao") com as declarações de facturação de TODOS os associados
                        volumesFacturacao = (from vf in listDeclaracoesFacturacao.Items.Cast<SPListItem>()
                                             select new AFQuotasVolumesFacturacao
                                             {
                                                 ID = vf.ID,
                                                 IDAssociado = vf.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Quotas.Lists.DeclaracoesFacturacao.ContentTypes.Item.Fields.Associado).LookupId,
                                                 Ano = int.Parse(vf.Title),
                                                 Valor = vf.GetTypedValue<double>(HlpIntranet.Quotas.Lists.DeclaracoesFacturacao.ContentTypes.Item.Fields.ValorCalculoQuota),
                                                 ModerationStatus = vf.ModerationInformation.Status
                                             }).ToList();

                        #endregion

                        #region Get Associados
                        // "entidades"
                        using (SPWeb webEntidades = site.OpenWeb(HlpIntranet.Entidades.WebUrl))
                        {
                            // "lists/entidades"
                            SPList listEntidades = webEntidades.Lists.GetList(HlpIntranet.Entidades.Lists.Entidades.Nome);
                            // contentType = "AF_Associado" - os elementos da lista podem ser Associados (AF_Associado) ou Entidades (AF_Entidade)
                            SPContentType ctAssociado = listEntidades.ContentTypes.GetContentType(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Nome);

                            caml = new StringBuilder();
                            // para garantir que só vamos buscar os conteúdos da lista cujo content type é AF_Associado, ou seja, apenas vai buscar os Associados (exclui as Entidades)
                            caml.AppendFormat("<Where><Eq><FieldRef Name='ContentType' /><Value Type='Text'>{0}</Value></Eq></Where>", ctAssociado.Name);
                            // para ordenar pelo campo AF_Associado
                            caml.AppendFormat("<OrderBy><FieldRef Name='{0}' Ascending='TRUE' /></OrderBy>", HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.NumeroAssociado);

                            // a lista itemsAssociados tem apenas Associados (e não Entidades)
                            itemsAssociados = listEntidades.GetItems(new SPQuery { Query = caml.ToString() });
                        }

                        #endregion
                        string mensagem = @"\r\n" + DateTime.Now.ToString("yyyyMMddHHmm") + "STATUS DO JOB APÓS CONTA CORRENTE";

                        // Percorre todos os Associados para efectuar o cálculo das quotas.
                        foreach (SPListItem itemAssociado in itemsAssociados)
                        {
                            // Verifica se o Associado já tem registos na declaração de facturação Detalhe(Updated 30/01/12)
                            // Caso Contrários não irá emitir documentos
                            caml = new StringBuilder();
                            caml.Append("<Where>");
                            caml.AppendFormat("<Eq><FieldRef Name='{0}' LookupId='TRUE' /><Value Type='Lookup'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.DeclaracoesFacturacaoDetalhe.ContentTypes.Item.Fields.Associado, itemAssociado.ID);
                            caml.Append("</Where>");

                            itemsDeclaracoesFacturacaoDt = listDeclaracoesFacturacaoDt.GetItems(new SPQuery { Query = caml.ToString() });
                            if (itemsDeclaracoesFacturacaoDt != null)
                            {
                                try { mensagem += "\r\n passou neste associado:" + itemAssociado.ID.ToString(); } catch { }

                                #region Atribui a data de início para o cálculo de quotas.
                                // Verifica a data de inscrição - AF_DataInscricao
                                DateTime dataInscricao = itemAssociado.GetTypedValue<DateTime>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.DataInscricao);

                                // Verifica qual a última quota emitida.
                                DateTime dataUltimaQuota = DateTime.MinValue;

                                caml = new StringBuilder();
                                caml.Append("<Where>");
                                caml.Append("<And>");
                                caml.AppendFormat("<Eq><FieldRef Name='{0}' LookupId='TRUE' /><Value Type='Lookup'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Associado, itemAssociado.ID);
                                caml.Append("<And>");
                                caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq>", SPBuiltInFieldId.ContentType, listContaCorrente.ContentTypes.GetContentType(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.Quota.Nome).Name);
                                caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Boolean'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Anulado, 0);
                                caml.Append("</And>");
                                caml.Append("</And>");
                                caml.Append("</Where>");
                                caml.AppendFormat("<OrderBy><FieldRef Name='{0}' Ascending='FALSE'/><FieldRef Name='{1}' Ascending='FALSE'/></OrderBy>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.AnoQuota, HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.MesQuota);

                                // Vai buscar todos os registos da "entidades/quotas/Lists/contacorrente" para o respectivo associado, do tipo quota e NÃO anulado ordenados decrescentemente pelo ano e mês
                                itemsContaCorrente = listContaCorrente.GetItems(new SPQuery { Query = caml.ToString() });
                                if (itemsContaCorrente != null && itemsContaCorrente.Count > 0)
                                {
                                    dataUltimaQuota = new DateTime(
                                        (int)itemsContaCorrente[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.AnoQuota),
                                        (int)itemsContaCorrente[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.MesQuota),
                                        1);
                                    try { mensagem += "\r\n data da última quota: " + dataUltimaQuota.ToString("yyyy-MM-dd"); } catch { }
                                }

                                // A data de início do cálculo é a maior de entre a data de inscrição e a última quota emitida.
                                DateTime dataInicioCalculo = (dataInscricao > dataUltimaQuota) ? dataInscricao : dataUltimaQuota;

                                // Workaround dos problemas com o histórico.
                                if (dataInicioCalculo < dataMinimaInicioCalculo) { dataInicioCalculo = dataMinimaInicioCalculo; }

                                #endregion

                                #region Atribui a data de fim para o cálculo de quotas.

                                // ChangeBy: gboleo, 2011-07-19
                                // O processo automático de cálculo sofre uma pequena alteração para ficar com a possibilidade de enviar as cartas pelo correio
                                //  1.  Uma semana antes do final de cada trimestre são geradas as quotas com a data correcta do início do trimestre (permite papel e envio electrónico/disponibilização na extranet).
                                //  2.  Nos outros casos serão geradas para o próprio dia.
                                //  
                                //  ALERTA!! ALERTA!! ALERTA!!
                                //  Haverá uma alteração do "today" pois isto obriga a calcular quotas no futuro.
                                //  Os trimestres estão martelados pois nesta fase, e para minimizar alterações à forma como o cálculo foi feito, é preferivel fazer desta forma.
                                //  ALERTA!! ALERTA!! ALERTA!!
                                DateTime todaySpan = today.AddDays(7);
                                if (todaySpan.Day == 1 && (todaySpan.Month == 1 || todaySpan.Month == 4 || todaySpan.Month == 7 || todaySpan.Month == 10))
                                {
                                    today = todaySpan;
                                }
                                // /ChangeBy: gboleo, 2011-07-19

                                // Caso não existe data de cancelamento é assumida a data de hoje.
                                DateTime dataCancelamento = itemAssociado.GetTypedValue<DateTime>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.DataCancelamento);
                                if (dataCancelamento == DateTime.MinValue) { dataCancelamento = today; }
                                try { mensagem += "\r\n data de cancelamento: " + dataCancelamento.ToString("yyyy-MM-dd"); } catch { }

                                // A data de fim do cálculo é a menor de entre a data de cancelamento e o dia de hoje.
                                DateTime dataFimCalculo = (dataCancelamento < today) ? dataCancelamento : today;
                                try { mensagem += "\r\n data de fim de cálculo: " + dataFimCalculo.ToString("yyyy-MM-dd"); } catch { }

                                #endregion

                                bool continuar = true;
                                // Necessário porque a Apifarma, para novos associados, por vezes coloca Datas de Inscrição superiores à Data de Cancelamento para que o associado tenha a ficha criada mas esteja ainda desactivdo.
                                if (dataInicioCalculo <= dataFimCalculo)
                                {
                                    string nipc = itemAssociado.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.Nipc);
                                    try { mensagem += "\r\n nipc: " + nipc; } catch { }
                                    if (!string.IsNullOrEmpty(nipc))
                                    {
                                        // Percorre todos os anos de cálculo em falta.
                                        for (int anoCalculo = dataInicioCalculo.Year; anoCalculo <= dataFimCalculo.Year; anoCalculo++)
                                        {
                                            #region Atribui os defaults para o ano do cálculo

                                            double joiaMinima = 0;
                                            double joiaPermilagem = 0;
                                            double quotaMinima = 0;
                                            double quotaPermilagem = 0;
                                            int quotasAno = 0;
                                            int quotaDiasVencimento = 0;

                                            // Vai buscar os defaults para o ano de cálculo.
                                            caml = new StringBuilder();
                                            caml.Append("<Where>");
                                            caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.Ano, anoCalculo);
                                            caml.Append("</Where>");

                                            itemsDefaults = listDefaults.GetItems(new SPQuery { Query = caml.ToString() });
                                            if (itemsDefaults != null && itemsDefaults.Count == 1)
                                            {
                                                joiaMinima = itemsDefaults[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.JoiaMinima);
                                                joiaPermilagem = itemsDefaults[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.JoiaPermilagem);
                                                quotaMinima = itemsDefaults[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.QuotaMinima);
                                                quotaPermilagem = itemsDefaults[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.QuotaPermilagem);
                                                quotasAno = (int)itemsDefaults[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.QuotasAno);
                                                quotaDiasVencimento = (int)itemsDefaults[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.Defaults.ContentTypes.Item.Fields.QuotasDiasVencimento);
                                            }
                                            else
                                            {
                                                errors.Add(new string[]
                                                    {
                                                        "",
                                                        "",
                                                        anoCalculo.ToString(),
                                                        "Não existem valores para os 'Default de Cálculo'."
                                                    });
                                                break;
                                            }

                                            #endregion

                                            #region Atribui os valores defaults referentes ao próprio associado

                                            bool declaracaoFacturacaoEmFalta = false;
                                            double volumeFacturacao = 0;
                                            double quotaBase = 0;

                                            // Volume de facturação do ano anterior ao do cálculo.
                                            caml = new StringBuilder();
                                            caml.Append("<Where>");
                                            caml.Append("<And>");
                                            caml.AppendFormat("<Eq><FieldRef Name='{0}' LookupId='TRUE' /><Value Type='Lookup'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.DeclaracoesFacturacao.ContentTypes.Item.Fields.Associado, itemAssociado.ID);
                                            caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.DeclaracoesFacturacao.ContentTypes.Item.Fields.Ano, anoCalculo - 1);
                                            caml.Append("</And>");
                                            caml.Append("</Where>");

                                            itemsDeclaracoesFacturacao = listDeclaracoesFacturacao.GetItems(new SPQuery { Query = caml.ToString() });
                                            if (itemsDeclaracoesFacturacao != null && itemsDeclaracoesFacturacao.Count == 1)
                                            {
                                                if (itemsDeclaracoesFacturacao[0].ModerationInformation.Status == SPModerationStatusType.Approved)
                                                {
                                                    volumeFacturacao = itemsDeclaracoesFacturacao[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.DeclaracoesFacturacao.ContentTypes.Item.Fields.ValorCalculoQuota);
                                                }
                                                else
                                                {
                                                    // Tem declaração de facturação mas não está aprovada.
                                                    errors.Add(new string[]
                                                        {
                                                            itemAssociado.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.NumeroAssociado),
                                                            itemAssociado.Title,
                                                            anoCalculo.ToString(),
                                                            "A declaração de facturação não está aprovada."
                                                        });
                                                    continuar = false;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                // Não vai ter como calcular a quota a parir do periodo 2, inclusive.
                                                declaracaoFacturacaoEmFalta = true;
                                            }

                                            // Valor da quota base independente de ser a primeira quota do ano ou a quota de acerto.
                                            quotaBase = Math.Round(volumeFacturacao * quotaPermilagem / quotasAno, 4);
                                            if (quotaBase < quotaMinima) { quotaBase = quotaMinima; }
                                            quotaBase = Math.Round(quotaBase, 0);

                                            #endregion

                                            #region Atribui os periodos de cálculo para o ano que está a ser processado

                                            // O periodo de inicio é o seguinte ao da última quota emitida para o ano de cálculo se este for o primeiro,
                                            // ou 1 se o ano de cálculo for superior ao primeiro.
                                            int periodoInicio = 1;
                                            if (anoCalculo == dataInicioCalculo.Year)
                                            {
                                                // Procura o valor da última quota emitida para o ano de cálculo.
                                                caml = new StringBuilder();
                                                caml.Append("<Where>");
                                                caml.Append("<And>");
                                                caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Numeric'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.AnoQuota, anoCalculo);
                                                caml.Append("<And>");
                                                caml.AppendFormat("<Eq><FieldRef Name='{0}' LookupId='TRUE' /><Value Type='Lookup'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Associado, itemAssociado.ID);
                                                caml.Append("<And>");
                                                caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq>", SPBuiltInFieldId.ContentType, listContaCorrente.ContentTypes.GetContentType(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.Quota.Nome).Name);
                                                caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Boolean'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Anulado, 0);
                                                caml.Append("</And>");
                                                caml.Append("</And>");
                                                caml.Append("</And>");
                                                caml.Append("</Where>");
                                                caml.AppendFormat("<OrderBy><FieldRef Name='{0}' Ascending='FALSE'/></OrderBy>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.MesQuota);

                                                itemsContaCorrente = listContaCorrente.GetItems(new SPQuery { Query = caml.ToString(), RowLimit = 1 });
                                                if (itemsContaCorrente != null && itemsContaCorrente.Count == 1)
                                                {
                                                    periodoInicio = GetPeriodo((int)itemsContaCorrente[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.MesQuota), quotasAno) + 1;
                                                }
                                                else
                                                {
                                                    // Não havendo quota emitida para o ano de cálculo verifica ainda se o ano de cálculo é o primeiro ano de associado
                                                    // para começar no período correcto (para os outros anos o período é o 1 excepto no ano do workaround).
                                                    // Neste caso também deve ser calculada a jóia!
                                                    if (anoCalculo == dataMinimaInicioCalculo.Year)
                                                    {
                                                        periodoInicio = GetPeriodo(dataMinimaInicioCalculo.Month, quotasAno);
                                                    }

                                                    if (anoCalculo == dataInscricao.Year)
                                                    {
                                                        periodoInicio = GetPeriodo(dataInscricao.Month, quotasAno);

                                                        // gboleo, 05-04-2012, Acrescentada a multiplacação pela permilagem da joia que faltava.
                                                        double joia = quotaBase * quotasAno * joiaPermilagem;
                                                        if (joia < joiaMinima) { joia = joiaMinima; }

                                                        // Insere a jóia em conta corrente. 
                                                        SPListItem item = listContaCorrente.Items.Add();
                                                        item[SPBuiltInFieldId.ContentTypeId] = listContaCorrente.ContentTypes.GetContentType(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.Joia.Nome).Id;
                                                        item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Associado] = itemAssociado.ID;
                                                        item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.DataEmissao] = today;
                                                        item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.DataVencimento] = today.AddDays(quotaDiasVencimento);
                                                        item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Valor] = joia;
                                                        item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.OrdemCompra] = itemAssociado.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.OrdemCompra);
                                                        item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Exportado] = 0;
                                                        item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Anulado] = 0;
                                                        item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.AnoQuota] = anoCalculo;
                                                        item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.MesQuota] = 0;
                                                        item.SystemUpdate();
                                                    }
                                                }
                                            }

                                            // O periodo de fim é o último do ano caso o ano de cálculo não seja o final,
                                            // ou o período correspondente à data de fim de cálculo.
                                            int periodoFim = 0;
                                            if (anoCalculo < dataFimCalculo.Year)
                                            {
                                                periodoFim = quotasAno;
                                            }
                                            else
                                            {
                                                periodoFim = GetPeriodo(dataFimCalculo.Month, quotasAno);
                                            }

                                            #endregion

                                            #region Calculo da quota

                                            for (int periodoCalculo = periodoInicio; periodoCalculo <= periodoFim; periodoCalculo++)
                                            {
                                                double quota = 0;

                                                if (periodoCalculo == 1)
                                                {
                                                    // Cálculo da primeira quota do ano mudou
                                                    // Deixou de ser igual à última do ano anterior para ser 
                                                    // o cálculo correcto da permilagem do ano mas com a declaração de facturação de dois anos antes
                                                    // (em janeiro de 2011 calcula a quota com a permilagem de 2011 mas a declaração de 2009)

                                                    // Defaults do ano
                                                    AFQuotasDefaultsCalculo qdc = (from dc in defaultsCalculo
                                                                                   where dc.Ano == anoCalculo
                                                                                   select dc).SingleOrDefault();

                                                    // Facturação dois anos anteriores
                                                    AFQuotasVolumesFacturacao qvf = (from vf in volumesFacturacao
                                                                                     where (vf.Ano == (anoCalculo - 2)) && (vf.IDAssociado == itemAssociado.ID) && (vf.ModerationStatus == SPModerationStatusType.Approved)
                                                                                     select vf).SingleOrDefault();

                                                    if (qdc != null && qvf != null)
                                                    {
                                                        quota = Math.Round(qvf.Valor * qdc.QuotaPermilagem / qdc.QuotasAno, 4);
                                                        if (quota < qdc.QuotaMinima) { quota = qdc.QuotaMinima; }
                                                        quota = Math.Round(quota, 0);
                                                    }
                                                    else
                                                    {
                                                        continuar = false;
                                                        break;
                                                    }

                                                    /*
                                                    // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                                                    // QUOTA IGUAL Á ÚLTIMA DO ANO ANTERIOR
                                                    // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-                                    
                                                    // Se a data de inscrição for a do próprio ano de cálculo não existe quota do ano anterior e o valor é calculado normalmente.
                                                    if (dataInscricao.Year == anoCalculo)
                                                    {
                                                        quota = quotaBase;
                                                    }
                                                    else
                                                    {
                                                        // Procura o valor da última quota para o ano anterior ao de cálculo.
                                                        caml = new StringBuilder();
                                                        caml.Append("<Where>");
                                                        caml.Append("<And>");
                                                        caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Numeric'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.AnoQuota, anoCalculo - 1);
                                                        caml.Append("<And>");
                                                        caml.AppendFormat("<Eq><FieldRef Name='{0}' LookupId='TRUE' /><Value Type='Lookup'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Associado, itemAssociado.ID);
                                                        caml.Append("<And>");
                                                        caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq>", SPBuiltInFieldId.ContentType, listContaCorrente.ContentTypes.GetContentType(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.Quota.Nome).Name);
                                                        caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Boolean'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Anulado, 0);
                                                        caml.Append("</And>");
                                                        caml.Append("</And>");
                                                        caml.Append("</And>");
                                                        caml.Append("</Where>");
                                                        caml.AppendFormat("<OrderBy><FieldRef Name='{0}' Ascending='FALSE'/></OrderBy>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.MesQuota);

                                                        itemsContaCorrente = listContaCorrente.GetItems(new SPQuery { Query = caml.ToString(), RowLimit = 1 });
                                                        if (itemsContaCorrente != null && itemsContaCorrente.Count == 1)
                                                        {
                                                            quota = itemsContaCorrente[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Valor);
                                                        }
                                                    }
                                                     * */
                                                }
                                                else if (periodoCalculo == 2)
                                                {
                                                    quota = quotaBase;
                                                    // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                                                    // QUOTA DE ACERTO
                                                    // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                                                    if (!declaracaoFacturacaoEmFalta)
                                                    {
                                                        // Procura o valor da quota para o período 1 do ano de cálculo e faz os cálculos do acerto.
                                                        caml = new StringBuilder();
                                                        caml.Append("<Where>");
                                                        caml.Append("<And>");
                                                        caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Numeric'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.MesQuota, 1);
                                                        caml.Append("<And>");
                                                        caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Numeric'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.AnoQuota, anoCalculo);
                                                        caml.Append("<And>");
                                                        caml.AppendFormat("<Eq><FieldRef Name='{0}' LookupId='TRUE' /><Value Type='Lookup'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Associado, itemAssociado.ID);
                                                        caml.Append("<And>");
                                                        caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq>", SPBuiltInFieldId.ContentType, listContaCorrente.ContentTypes.GetContentType(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.Quota.Nome).Name);
                                                        caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Boolean'>{1}</Value></Eq>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Anulado, 0);
                                                        caml.Append("</And>");
                                                        caml.Append("</And>");
                                                        caml.Append("</And>");
                                                        caml.Append("</And>");
                                                        caml.Append("</Where>");
                                                        caml.AppendFormat("<OrderBy><FieldRef Name='{0}' Ascending='FALSE'/></OrderBy>", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.MesQuota);

                                                        itemsContaCorrente = listContaCorrente.GetItems(new SPQuery { Query = caml.ToString() });
                                                        if (itemsContaCorrente != null && itemsContaCorrente.Count == 1)
                                                        {
                                                            quota = Math.Round(quotaBase - Math.Round((itemsContaCorrente[0].GetTypedValue<double>(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Valor) - quotaBase), 4), 4);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // Não tem declaração de facturação para efectuar o cálculo de acerto.
                                                        errors.Add(new string[]
                                                            {
                                                                itemAssociado.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.NumeroAssociado),
                                                                itemAssociado.Title,
                                                                anoCalculo.ToString(),
                                                                "Não foi entregue a declaração de facturação."
                                                            });
                                                        continuar = false;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                                                    // QUOTA NORMAL
                                                    // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                                                    quota = quotaBase;
                                                }

                                                // Insere a quota em conta corrente. 
                                                SPListItem item = listContaCorrente.Items.Add();
                                                item[SPBuiltInFieldId.ContentTypeId] = listContaCorrente.ContentTypes.GetContentType(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.Quota.Nome).Id;
                                                item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Associado] = itemAssociado.ID;
                                                item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.DataEmissao] = today;
                                                if (quota > 0)
                                                {
                                                    // ChangeBy: gboleo, 2011-07-19
                                                    // A data de vencimento tem as seguintes regras:
                                                    //  Ex. 1: Uma quota criada a 01-07-2011 (data correcta para o trimestre) ficaria com 10 dias para vencimento (11-07-2011).
                                                    //  Ex. 2: Uma quota criada a 05-07-2011 ficaria com data de vencimento 11-07-2011.
                                                    //  Ex. 3: Uma quota criada a 20-07-2011 ficaria com data de vencimento 20-07-2011.

                                                    // Calcula a data de vencimento correcta caso o processo corra no dia correcto, o dia 1 de cada trimestre.
                                                    DateTime dataVencimentoQuotaCorrecta = new DateTime(anoCalculo, GetPrimeiroMesNoPeriodo(periodoCalculo, quotasAno), 1).AddDays(quotaDiasVencimento);

                                                    // Calcula a diferença de dias a partir da emissão da quota para o dia de vencimento correcto.
                                                    // Caso a diferença seja negativa é porque já passou a data de vencimento e fica a data da própria emissão (dayDifference = 0)
                                                    int dayDifference = (dataVencimentoQuotaCorrecta - today).Days;
                                                    if (dayDifference < 0) { dayDifference = 0; }

                                                    item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.DataVencimento] = today.AddDays(dayDifference);
                                                    // /ChangeBy: gboleo, 2011-07-19
                                                }
                                                item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Valor] = quota;
                                                item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.OrdemCompra] = itemAssociado.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.OrdemCompra);
                                                item["AF_Observacoes"] = string.Format("Quota relativa ao {0}º trimestre de {1}", periodoCalculo, anoCalculo);
                                                item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Exportado] = 0;
                                                item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Anulado] = 0;
                                                item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.AnoQuota] = anoCalculo;
                                                item[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.MesQuota] = GetPrimeiroMesNoPeriodo(periodoCalculo, quotasAno);
                                                item.SystemUpdate();
                                            }

                                            #endregion

                                            if (!continuar) { break; }
                                        }
                                    }
                                    else
                                    {
                                        errors.Add(new string[]
                                    {
                                        itemAssociado.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.NumeroAssociado),
                                        itemAssociado.Title,
                                        "",
                                        "Sem número de contribuinte para gerar documento."
                                    });
                                    }
                                }
                            }
                        }
                        try
                        {
                            InsertLogEntry(mensagem);
                        }
                        catch
                        {

                        }
                        #region Relatório de erros de processamento

                        if (errors.Count > 0)
                        {
                            string body = HlpMail.CreateMessageBody(
                                "Lançamento Quotas",
                                "Ocorreram os seguinte erros a processar as quotas.",
                                new string[] { "Número Associado", "Nome", "Ano", "Erro" },
                                errors);

                            HlpMail.SendMail(webApplication, HlpIntranet.Defaults.GetValue(site, HlpIntranet.Defaults.Keys.QuotasNotificacoesErro), "Intranet - Quotas - Erros no processamento", body);

                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = currentCulture;
            }
        }

        private void InsertLogEntry(string msg)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                StreamWriter logs;
                logs = new StreamWriter("c:\\report.txt");
                logs.Write(DateTime.Now.ToString("s") + " - " + msg + "\n");
                logs.Close();
            });
        }
        private static int GetPeriodo(int mes, int periodosPorAno)
        {
            int mesesPorPeriodo = 12 / periodosPorAno;
            return (int)Math.Ceiling((double)mes / mesesPorPeriodo);
        }
        private static int GetPrimeiroMesNoPeriodo(int periodo, int periodosPorAno)
        {
            return (periodo - 1) * (12 / periodosPorAno) + 1;
        }



    }
}
