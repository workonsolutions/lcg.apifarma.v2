﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Convex.Sharepoint.Foundation.Extensions;
using System.Diagnostics;
using Convex.Clientes.Apifarma.Intranet.Helper;

namespace TimerJobsV4.Quotas.EventReceivers
{
    class ItemDeclaracaoFacturacaoDetalhe : SPItemEventReceiver
    {
        private readonly string Title = "Apifarma_Quotas_ItemDeclaracaoFacturacaoDetalhe";

        private void UpdateValues(SPItemEventProperties properties)
        {
            this.EventFiringEnabled = false; //.DisableEventFiring();

            using (SPWeb webQuotas = properties.OpenWeb())
            {
                Convex.Clientes.Apifarma.Intranet.Helper.Quotas quotas = new Convex.Clientes.Apifarma.Intranet.Helper.Quotas(webQuotas);

                // Informação do item 
                SPListItem item = properties.ListItem;
                int idAssociado = item.GetTypedValue<SPFieldLookupValue>("AF_Entidade").LookupId;
                int ano = int.Parse(item.Title);
                Debug.WriteLine(string.Format("IdAssociado={0}, Ano={1}", idAssociado, ano), this.Title);

                // Declaração
                Convex.Clientes.Apifarma.Intranet.Helper.Quotas.DeclaracaoFacturacao declaracao = quotas.GetDeclaracoesFacturacao(idAssociado, ano);
                if (declaracao == null)
                {
                    declaracao = new Convex.Clientes.Apifarma.Intranet.Helper.Quotas.DeclaracaoFacturacao
                    {
                        IdAssociado = idAssociado,
                        Ano = ano
                    };
                }

                // Declaração (detalhe)
                List<Convex.Clientes.Apifarma.Intranet.Helper.Quotas.DeclaracaoFacturacaoDetalhe> declaracaoDetalhe = quotas.GetDeclaracoesFacturacaoDetalhe(idAssociado, ano);
                // Apaga o próprio registo da lista
                if (properties.EventType == SPEventReceiverType.ItemDeleting)
                {
                    declaracaoDetalhe = (from d in declaracaoDetalhe
                                         where d.Id != item.ID
                                         select d).ToList();
                }

                quotas.UpdateDeclaracaoFacturacao(declaracao, declaracaoDetalhe);
                quotas.UpdateDeclaracaoFacturacaoDetalheAssociado(idAssociado, properties.EventType == SPEventReceiverType.ItemDeleting ? item.ID : 0);
            }

            this.EventFiringEnabled = true; //.EnableEventFiring();
        }

        public override void ItemAdded(SPItemEventProperties properties)
        {
            Debug.WriteLine("Start ItemAdded() method", this.Title);

            // DS: 20110808 - Nova coluna AF_Recalcular que indica se o evento deve realizar o calculo da declaração de facturação resultante do detalhe
            // Serve para a Extranet controlar em que altura o calculo deve ser realizado (no último detalhe inserido)
            bool recalcular = properties.ListItem.GetTypedValue<bool>(HlpIntranet.Quotas.Lists.DeclaracoesFacturacaoDetalhe.ContentTypes.Item.Fields.Recalcular);

            if (recalcular)
                UpdateValues(properties);
            Debug.WriteLine("End ItemAdded() method", this.Title);
        }

        public override void ItemUpdated(SPItemEventProperties properties)
        {
            Debug.WriteLine("Start ItemUpdated() method", this.Title);
            UpdateValues(properties);
            Debug.WriteLine("End ItemUpdated() method", this.Title);
        }

        // No ItemDeleted já não temos acesso à informação do item 
        public override void ItemDeleting(SPItemEventProperties properties)
        {
            Debug.WriteLine("Start ItemDeleting() method", this.Title);
            UpdateValues(properties);
            Debug.WriteLine("End ItemDeleting() method", this.Title);
        }
    }
}

