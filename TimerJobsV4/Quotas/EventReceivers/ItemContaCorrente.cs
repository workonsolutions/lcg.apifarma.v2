﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.Intranet.Helper;
using Microsoft.SharePoint.Utilities;
using Convex.Sharepoint.Foundation.Extensions;

namespace TimerJobsV4.Quotas.EventReceivers
{
    class ItemContaCorrente : SPItemEventReceiver
    {
        public override void ItemAdding(SPItemEventProperties properties)
        {
            using (SPWeb web = properties.OpenWeb())
            {
                SPList list = web.Lists[properties.ListId];
                SPContentTypeId contentTypeId = new SPContentTypeId(properties.AfterProperties["ContentTypeId"].ToString());
                SPContentType contentType = list.ContentTypes[contentTypeId];

                int nextNumero = 1;
                int widthNumero = 6;

                // Verifica se a data de vencimento é igual ou superior à data de emissão (para os content types que tenham data de vencimento).
                if (properties.ErrorMessage == null)
                {
                    if (Convert.ToString(properties.AfterProperties[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.DataVencimento]).Length > 0)
                    {
                        DateTime dataEmissao = SPUtility.CreateDateTimeFromISO8601DateTimeString(properties.AfterProperties[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.DataEmissao].ToString());
                        DateTime dataVencimento = SPUtility.CreateDateTimeFromISO8601DateTimeString(properties.AfterProperties[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.DataVencimento].ToString());
                        if (dataVencimento < dataEmissao)
                        {
                            properties.ErrorMessage = string.Format("{0} tem que ser igual ou posterior à {1}.", list.Fields.GetFieldByInternalName(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.DataVencimento).Title, list.Fields.GetFieldByInternalName(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.DataEmissao).Title);
                            properties.Status = SPEventReceiverStatus.CancelWithError;
                        }
                    }
                }

                if (properties.ErrorMessage == null)
                {
                    StringBuilder caml = new StringBuilder();
                    caml.Append("<Where>");
                    caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq>", SPBuiltInFieldId.ContentType, contentType.Name);
                    caml.Append("</Where>");
                    caml.Append("<OrderBy>");
                    caml.AppendFormat("<FieldRef Name='{0}' Ascending='FALSE' />", HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.NumeroDocumento);
                    caml.Append("</OrderBy>");

                    SPListItemCollection items = list.GetItems(new SPQuery { Query = caml.ToString(), RowLimit = 1 });
                    if (items != null && items.Count == 1)
                    {
                        // Atribui o número seguinte.
                        if (int.TryParse(items[0].GetFormattedValue(HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.NumeroDocumento), out nextNumero))
                        {
                            nextNumero++;
                        }
                    }
                }

                // Caso tudo tenha corrido bem atribui o respectivo número de documento.
                if (properties.ErrorMessage == null)
                {
                    string numero = nextNumero.ToString().PadLeft(widthNumero, '0');
                    properties.AfterProperties[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.NumeroDocumento] = numero;
                }
            }
        }

        public override void ItemDeleting(SPItemEventProperties properties)
        {
            using (SPWeb web = properties.OpenWeb())
            {
                // Quem não é site admin não pode apagar items da conta corrente. Os site admin podem apenas para corrigir possíveis situações de erro.
                if (!web.AllUsers.GetByID(properties.CurrentUserId).IsSiteAdmin)
                {
                    properties.ListItem[HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Anulado] = true;
                    properties.ListItem.Update();

                    properties.Status = SPEventReceiverStatus.CancelNoError;
                }
            }
        }

        #region Gera o documento em anexo

        private void CreatePdfDocument(SPItemEventProperties properties)
        {
            if (properties.ListItem != null)
            {
                SPListItem item = properties.ListItem;

                if (item.ContentType.ResourceFolder.Name != "AF_AcertoSaldo" && item.ContentType.ResourceFolder.Name != "AF_Devolucao")
                {
                    BaseDocument document;

                    if (item.ContentType.ResourceFolder.Name == "AF_NotaCredito" || item.ContentType.ResourceFolder.Name == "AF_Pagamento" || (item.ContentType.ResourceFolder.Name != "AF_NotaCredito" && item.GetTypedValue<double>("AF_Valor") < 0))
                    {
                        document = new PdfCreditoDocument();
                    }
                    else
                        document = new PdfDebitoDocument();

                    document.Id = item.ID;
                    document.ContentType = item.ContentType.ResourceFolder.Name;

                    // Informação directa do documento
                    document.TipoDocumento = item.ContentType.Name;
                    document.Numero = item.Title;
                    document.DataEmissao = item.GetTypedValue<DateTime>("AF_DataEmissao");
                    if (item.ContentType.ResourceFolder.Name != "AF_Pagamento")
                        document.DataVencimento = item.GetTypedValue<DateTime>("AF_DataVencimento");
                    document.Valor = item.GetTypedValue<double>("AF_Valor");
                    document.OrdemCompra = item.GetTypedValue<string>("AF_OrdemCompra");
                    document.Observacoes = item.GetTypedValue<string>("AF_Observacoes");
                    document.Anulado = item.GetTypedValue<bool>("AF_Anulado");

                    using (SPWeb webQuotas = properties.OpenWeb())
                    {
                        using (SPWeb webAssociados = webQuotas.ParentWeb)
                        {
                            // Informação do associado
                            Convex.Clientes.Apifarma.Intranet.Helper.Associados associados = new Associados(webAssociados);
                            Convex.Clientes.Apifarma.Intranet.Helper.Associados.Associado associado = associados.GetAssociado(item.GetTypedValue<SPFieldLookupValue>("AF_Entidade").LookupId);
                            if (associado != null)
                            {
                                document.AssociadoNumero = associado.Numero;
                                document.AssociadoNome = associado.Nome;
                                document.AssociadoMorada = associado.Morada;
                                document.AssociadoCodigoPostal = associado.CodigoPostal;
                                document.AssociadoNipc = associado.Nipc;
                            }
                        }

                        // Informação de configuração do módulo das quotas
                        Convex.Clientes.Apifarma.Intranet.Helper.Quotas quotas = new Convex.Clientes.Apifarma.Intranet.Helper.Quotas(webQuotas);
                        document.Nibs = quotas.GetDocumentDisplayNibs();
                    }

                     //Gera o documento em pdf
                    byte[] contents = null;//document.Create();
                    if (contents != null)
                    {
                        // Coloca em anexo o documento gerado
                        this.EventFiringEnabled = false;  //.DisableEventFiring();
                        string attachmentLeafName = string.Format("{0}_{1}.pdf", item.ContentType.Name, item.Title);
                        if (item.Attachments.Count > 0) { item.Attachments.DeleteNow(item.Attachments[0]); }
                        item.Attachments.AddNow(attachmentLeafName, contents);
                        this.EventFiringEnabled = true; //.EnableEventFiring();
                    }
                }
            }
        }

        public override void ItemAdded(SPItemEventProperties properties)
        {
            CreatePdfDocument(properties);
        }

        public override void ItemUpdated(SPItemEventProperties properties)
        {
            CreatePdfDocument(properties);
        }

        #endregion

    }
}
