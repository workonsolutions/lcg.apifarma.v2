﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.Intranet.Helper;
using Convex.Sharepoint.Foundation.Extensions;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using Microsoft.SharePoint.Utilities;
using Convex.Clientes.Apifarma.Intranet.Config;
using Convex.Sharepoint.Foundation;
using System.Net.Mail;
using System.Configuration;
using System.Web;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.Xml;

namespace TimerJobsV4.Quotas
{
    class JobSendDocumentsByEmail : SPJobDefinition
    {
        public static string Nome = "Apifarma_Quotas_SendDocumentsByEmail";
        string QuotaXSL = string.Format("{0}/{1}", "XsltTemplates", "Quota.xslt");
        string JoiaXSL = string.Format("{0}/{1}", "XsltTemplates", "Joia.xslt");

        public JobSendDocumentsByEmail()
            : base()
        {
        }

        public JobSendDocumentsByEmail(string name, SPWebApplication webApplication)
            : base(name, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = name;
        }

        //Funcion para buscar el email del Director General para enviar los informes  (NO AVANZAR)
        public string GetMailRepLegais(int IdEntidade, Guid ContentDbId)
        {

            SPWebApplication webApplication = this.Parent as SPWebApplication;
            string MailAddressDir = "";
            try
            {
                SPContentDatabase contentDb = webApplication.ContentDatabases[ContentDbId];

                SPSite site = contentDb.Sites["/"];

                SPWeb webEntidades = site.OpenWeb("entidades");
                SPList list = webEntidades.Lists.GetList(HlpIntranet.Entidades.Lists.Representantes.Nome);

                StringBuilder caml = new StringBuilder();
                caml.Append("<Where>");
                caml.AppendFormat("<Eq><FieldRef Name='{0}' LookupId='TRUE' /><Value Type='Lookup'>{1}</Value></Eq>", HlpIntranet.Entidades.Lists.Representantes.ContentTypes.Item.Fields.Associado, IdEntidade);
                caml.Append("</Where>");


                SPQuery query = new SPQuery();
                query.Query = caml.ToString();

                SPListItemCollection items = list.GetItems(query);
                foreach (SPListItem item in items)
                {
                    try
                    {
                        if (item.GetTypedValue<string>("AF_Email").Contains("@"))
                            MailAddressDir += item.GetTypedValue<string>("AF_Email") + ";";
                    }
                    catch { }
                }
            }
            catch
            {
                MailAddressDir = "";
            }
            return MailAddressDir;




        }

        public override void Execute(Guid contentDbId)
        {
            HlpQuotas.LogMessage(QuotasLogCategory.Quotas_Job, TraceEventType.Information, "JobSendDocumentsByEmail", "Inicio Job: " + Nome);
            CultureInfo ptCulture = new CultureInfo("pt-PT");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ptCulture;

            try
            {
                #region Configs iniciais
                DateTime queryDate = DateTime.Parse(SPUtility.CreateISO8601DateTimeFromSystemDateTime(DateTime.Now));
                int trimester = (int)Math.Ceiling(queryDate.Month / 3.0);
                int year = queryDate.Year;
                #endregion

                SPWebApplication webApplication = this.Parent as SPWebApplication;
                SPContentDatabase contentDb = webApplication.ContentDatabases[contentDbId];

                using (SPSite site = contentDb.Sites["/"])
                {
                    using (SPWeb webEntidades = site.OpenWeb("entidades"), webQuotas = site.OpenWeb("entidades/quotas"))
                    {
                        SPList entidadesList = webEntidades.Lists.GetList(HlpIntranet.Entidades.Lists.Entidades.Nome);
                        SPList ccList = webQuotas.Lists.GetList(Convex.Clientes.Apifarma.Intranet.Helper.HlpIntranet.Quotas.Lists.ContaCorrente.Nome);
                        QuotasConfig config = new QuotasConfig(webQuotas);

                        SPQuery query = new SPQuery();

                        query.Query =
                            string.Format(@"<Where>
                                         <And>
                                            <And>
                                            <Or>
                                                 <Eq>
                                                    <FieldRef Name='ContentType' />
                                                    <Value Type='Choice'>Quota</Value>
                                                 </Eq>
                                                 <Eq>
                                                    <FieldRef Name='ContentType' />
                                                    <Value Type='Choice'>Jóia</Value>
                                                 </Eq>
                                            </Or>
                                         <Eq>
                                            <FieldRef Name='{0}' />
                                            <Value Type='DateTime'>{1}</Value>
                                         </Eq>
                                        </And>
                                        <Eq>
                                            <FieldRef Name='{2}' />
                                            <Value Type='Boolean'>{3}</Value>
                                         </Eq>
                                        </And>
                                  </Where>
                                  <OrderBy><FieldRef Name=""Title"" Ascending=""True"" /></OrderBy></Query>",
                            HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.DataEmissao, SPUtility.CreateISO8601DateTimeFromSystemDateTime(DateTime.Now), HlpIntranet.Quotas.Lists.ContaCorrente.ContentTypes.DocumentoBase.Fields.Enviado, 0);


                        SPListItemCollection res = ccList.GetItems(query);



                        if (res.Count > 0)
                        {
                            HlpQuotas.LogMessage(QuotasLogCategory.Quotas_Job, TraceEventType.Information, "JobSendDocumentsByEmail", string.Format("Total de documentos encontrados: {0}", res.Count.ToString()));

                            Associados associados = new Associados(webEntidades);
                            foreach (SPListItem item in res)
                            {
                                // Informação do associado                                
                                Associados.Associado associado = associados.GetAssociado(item.GetTypedValue<SPFieldLookupValue>("AF_Entidade").LookupId);
                                if (associado != null)
                                {
                                    if (!string.IsNullOrEmpty(associado.EmailNotificacaoContabilidade))
                                    {
                                        HlpQuotas.LogMessage(QuotasLogCategory.Quotas_Job, TraceEventType.Information, "JobSendDocumentsByEmail", string.Format("Associado {0} - A enviar quota para '{1}'...", associado.Numero, associado.EmailNotificacaoContabilidade));

                                        try
                                        {
                                            String assunto = string.Empty;
                                            String emailBody = string.Empty;
                                            string referente = string.Empty;

                                            string tipoDocumento = item.ContentType.ResourceFolder.Name;

                                            SPList templateList = webQuotas.Lists.GetList("XsltTemplates");
                                            if (templateList == null)
                                                throw new Exception("Não foi encontrada a lista de templates XSL.");

                                            if (tipoDocumento.Equals("AF_Joia"))
                                            {
                                                assunto = "ENVIO DE NOTA DE LANÇAMENTO REFERENTE À JÓIA DE INSCRIÇÃO NA APIFARMA";
                                                emailBody = HlpMail.CreateMessagebody(CvxUtility.CombineUrl(webQuotas.Url, JoiaXSL), assunto, string.Empty, associado.DebitoDirecto, templateList);
                                            }
                                            if (tipoDocumento.Equals("AF_Quota"))
                                            {
                                                assunto = String.Format("ENVIO FACTURA (QUOTA) RELATIVA AO {0}º TRIMESTRE {1}", trimester, year);
                                                referente = String.Format("{0}º trimestre de {1}", trimester, year);
                                                emailBody = HlpMail.CreateMessagebody(CvxUtility.CombineUrl(webQuotas.Url, QuotaXSL), assunto, referente, associado.DebitoDirecto, templateList);
                                            }

                                            try
                                            {
                                                MailMessage message = new MailMessage();

                                                string bccAddresses = config.GetConfigValue(QuotasConfig.Notificacao.NotificationBccDocumentosElectronicos.Key);

                                                //string ccAddresses = GetMailRepLegais(associado.Id, contentDbId);

                                                #region Colocar em Attachment os attachments do item da lista
                                                foreach (string fileName in item.Attachments)
                                                {
                                                    SPFile file = webQuotas.GetFile(item.Attachments.UrlPrefix + fileName);
                                                    if (file.Exists)
                                                    {
                                                        message.Attachments.Add(new Attachment(file.OpenBinaryStream(), file.Name, MediaTypeNames.Application.Octet));
                                                    }
                                                }
                                                #endregion
                                                if (!string.IsNullOrEmpty(associado.EmailNotificacaoContabilidade))
                                                {


                                                    try
                                                    {

                                                        Configuration configur = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/", webApplication.Name);


                                                        if (!(configur.AppSettings.Settings["Environment"].Value.ToUpper() == "PRODUCTION"))
                                                        {



                                                            //if (!string.IsNullOrEmpty(ccAddresses))

                                                            //HlpMail.SendMail(SPContext.Current.Site.WebApplication, configur.AppSettings.Settings["EmailTester"].Value.ToLower(), configur.AppSettings.Settings["EmailDev"].Value.ToLower(), assunto + ccAddresses, emailBody, message);
                                                            //else
                                                            HlpMail.SendMail(SPContext.Current.Site.WebApplication, configur.AppSettings.Settings["EmailTester"].Value.ToLower(), configur.AppSettings.Settings["EmailDev"].Value.ToLower(), assunto, emailBody, message);
                                                        }
                                                        else
                                                        {
                                                            //if (!string.IsNullOrEmpty(ccAddresses))
                                                            //    HlpMail.SendMail(site.WebApplication, associado.EmailNotificacaoContabilidade, ccAddresses, bccAddresses, assunto, emailBody, message);
                                                            //else
                                                            HlpMail.SendMail(site.WebApplication, associado.EmailNotificacaoContabilidade, bccAddresses, assunto, emailBody, message);


                                                        }
                                                    }
                                                    catch
                                                    {

                                                    }





                                                    item["AF_Enviado"] = "true";
                                                    item.Update();
                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                HlpQuotas.LogMessage(QuotasLogCategory.Quotas_Email, TraceEventType.Verbose, "JobSendDocumentsByEmail::Quotas_Email " + string.Format("Associado {0} ", associado.Numero), ex.Message);
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            HlpQuotas.LogMessage(QuotasLogCategory.Quotas_General, TraceEventType.Error, "JobSendDocumentsByEmail::BtnOk_Click() ", ex.ToString());

                                        }
                                    }
                                }
                            }
                        }
                        else
                            HlpQuotas.LogMessage(QuotasLogCategory.Quotas_Job, TraceEventType.Information, "JobSendDocumentsByEmail", "Não existem documentos a enviar");
                    }
                }
            }
            catch (Exception ex)
            {
                HlpQuotas.LogMessage(QuotasLogCategory.Quotas_Job, TraceEventType.Verbose, "QuotasJob: ", ex.Message);
                HlpQuotas.LogMessage(QuotasLogCategory.Quotas_Job, TraceEventType.Verbose, "QuotasJob: ", ex.StackTrace);
            }
            HlpQuotas.LogMessage(QuotasLogCategory.Quotas_Job, TraceEventType.Information, "JobSendDocumentsByEmail", "Fim Job: " + Nome);
        }


    }
}
