﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.Intranet.Helper;
using Convex.Sharepoint.Foundation.Extensions;
using System.Globalization;
using System.Threading;
using System.Collections;
using Microsoft.SharePoint.Utilities;
using System.IO;
using Microsoft.SharePoint.WebControls;

namespace TimerJobsV4.Correspondencia
{
    public class Drafts2Outbox : SPJobDefinition
    {
        public Drafts2Outbox()
            : base()
        {
        }

        public Drafts2Outbox(string name, SPWebApplication webApplication)
            : base(name, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = name;
        }

        private static void FillWordBindFields(SPWeb webEntidades, HlpIntranet.Entidades.Lists.GruposContacto.GrupoContactoResult contact, SPListItem docEnviadoItem)
        {
            #region Obter informação de contacto para preencher colunas Word (AF_WordBind*)

            SPListItem entidadeItem = HlpIntranet.Entidades.Lists.Entidades.GetById(webEntidades, contact.EntidadeId);
            SPListItem colaboradorItem = null;
            if (contact.ColaboradorId.HasValue)
                colaboradorItem = HlpIntranet.Entidades.Lists.Colaboradores.GetById(webEntidades, contact.ColaboradorId.Value);

            docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindEntidade] = entidadeItem[HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Entidade.Fields.Nome];

            docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindCodigoPostal] =
                   entidadeItem.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Entidade.Fields.CodigoPostal);

            docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindMorada] =
                entidadeItem.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Entidade.Fields.Morada);

            docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindPais] =
                entidadeItem.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Entidade.Fields.Pais).LookupValue;

            if (colaboradorItem != null)
            {

                docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindColaborador] =
                    colaboradorItem.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Nome);

                docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindCargo] =
                    colaboradorItem.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Titulo).LookupValue;

                string cargo = colaboradorItem.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.CargoSubstituicao);
                if (String.IsNullOrEmpty(cargo))
                    cargo = colaboradorItem.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Cargo).LookupValue;

                docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindCargo] = cargo;
                docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindTitulo] = colaboradorItem.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Titulo).LookupValue;
                docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindExmo] = colaboradorItem.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Exmo).LookupValue;

                string moradaColab = colaboradorItem.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Morada);

                if (!String.IsNullOrEmpty(moradaColab))
                {
                    docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindCodigoPostal] =
                        colaboradorItem.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.CodigoPostal);

                    docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindMorada] =
                        colaboradorItem.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Morada);

                    docEnviadoItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.WordBindPais] =
                        colaboradorItem.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Pais).LookupValue;
                }
            }

            #endregion
        }
        public override void Execute(Guid contentDbId)
        {
            CultureInfo ptCulture = new CultureInfo("pt-PT");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ptCulture;
            try
            {
                SPWebApplication webApplication = this.Parent as SPWebApplication;
                SPContentDatabase contentDb = webApplication.ContentDatabases[contentDbId];

                using (SPSite site = contentDb.Sites["/"])
                {
                    using (SPWeb web = site.OpenWeb(HlpIntranet.Correspondencia.WebUrl))
                    {
                        List<string[]> errors = new List<string[]>();
                        SPList draftsList = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.Rascunhos.Nome);
                        SPList outboxList = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.CaixaSaida.Nome);
                        SPList attachmentsList = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.Anexos.Nome);

                        // Define as constantes do processo.
                        int emailId = HlpIntranet.Correspondencia.Lists.Canais.GetCanalEmailId(web);
                        int year = DateTime.Today.Year;
                        int lastFileNumber = HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.GetLast(web, HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.DireccaoEnum.Saida, year);

                        #region SPQuery de pesquisa (todos os documentos aprovados)

                        StringBuilder caml = new StringBuilder();
                        caml.Append("<Where>");
                        caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Number'>{1}</Value></Eq>", SPBuiltInFieldId._ModerationStatus, (int)SPModerationStatusType.Approved);
                        caml.Append("</Where>");

                        SPQuery query = new SPQuery();
                        query.Query = caml.ToString();

                        #endregion

                        SPListItemCollection itemsTemp = draftsList.GetItems(query);
                        List<SPListItem> items = new List<SPListItem>();
                        foreach (SPListItem item in itemsTemp)
                        {
                            items.Add(item);
                        }
                        itemsTemp = null;

                        int stepIncrement = 100 / (items.Count > 0 ? items.Count : 1);
                        int progress = 0;
                        foreach (SPListItem item in items)
                        {
                            try
                            {
                                #region Verifica se o ficheiro tem a informação necessária ao workflow preenchida

                                string requiredFieldMessage = "";

                                if (item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Canal).LookupId == 0)
                                {
                                    requiredFieldMessage += string.Format("O campo '{0}' é de preenchimento obrigatório!<br/>", item.ParentList.Fields.GetFieldByInternalName(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Canal).Title);
                                }
                                if (item.GetTypedValue<DateTime>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.DataDocumento) < DateTime.Today.AddMonths(-3))
                                {
                                    requiredFieldMessage += string.Format("'{0}' inválida!<br/>", item.ParentList.Fields.GetFieldByInternalName(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.DataDocumento).Title);
                                }
                                if (string.IsNullOrEmpty(item.GetTypedValue<string>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Assunto)))
                                {
                                    requiredFieldMessage += string.Format("O campo '{0}' é de preenchimento obrigatório!<br/>", item.ParentList.Fields.GetFieldByInternalName(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Assunto).Title);
                                }
                                if (item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.GrupoContacto).LookupId == 0 && item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Entidade).LookupId == 0)
                                {
                                    requiredFieldMessage += string.Format("'{0}' ou '{1}' tem que estar preenchido!<br/>", item.ParentList.Fields.GetFieldByInternalName(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.GrupoContacto).Title, item.ParentList.Fields.GetFieldByInternalName(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Entidade).Title);
                                }

                                #endregion

                                if (requiredFieldMessage.Length == 0)
                                {
                                    // Inicializa a metadata dos documentos a gerar a partir do documento em draft (podem ser vários devido ao desdobramento).
                                    List<Hashtable> filesMetadata = new List<Hashtable>();
                                    // Os documentos pelo canal de e-mail ficam automaticamente aprovados para saída.
                                    SPModerationStatusType moderationStatus = (item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Canal).LookupId == emailId) ? SPModerationStatusType.Approved : SPModerationStatusType.Pending;

                                    #region Desdobramento da metadata do ficheiro pelos contactos do grupo

                                    List<HlpIntranet.Entidades.Lists.GruposContacto.GrupoContactoResult> contacts = new List<HlpIntranet.Entidades.Lists.GruposContacto.GrupoContactoResult>();
                                    int grupoContactoId = item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.GrupoContacto).LookupId;
                                    if (grupoContactoId == 0)
                                    {
                                        contacts.Add(new HlpIntranet.Entidades.Lists.GruposContacto.GrupoContactoResult
                                        {
                                            EntidadeId = item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Entidade).LookupId,
                                            ColaboradorId = item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Colaborador).LookupId
                                        });
                                    }
                                    else
                                    {
                                        using (SPWeb webEntidades = site.OpenWeb(HlpIntranet.Entidades.WebUrl))
                                        {
                                            contacts = HlpIntranet.Entidades.Lists.GruposContacto.GetContactosIndividuais(webEntidades, grupoContactoId);
                                        }
                                    }

                                    if (contacts.Count > 0)
                                    {
                                        using (SPWeb webEntidades = site.OpenWeb(HlpIntranet.Entidades.WebUrl))
                                        {
                                            foreach (HlpIntranet.Entidades.Lists.GruposContacto.GrupoContactoResult contact in contacts)
                                            {
                                                lastFileNumber++;


                                                string destinationUrl = SPUrlUtility.CombineUrl(outboxList.RootFolder.Name, DateTime.Now.ToFileTime().ToString() + Path.GetExtension(item.File.Name).ToLower());
                                                item.File.CopyTo(destinationUrl);

                                                SPFile file = web.GetFile(destinationUrl);
                                                file.Item[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.NumeroDocumento] = HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.FormatNumber(year, lastFileNumber);
                                                file.Item[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Anulado] = 0;
                                                file.Item[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Entidade] = contact.EntidadeId;
                                                file.Item[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Colaborador] = contact.ColaboradorId.HasValue ? contact.ColaboradorId.Value : 0;

                                                //DS: 06-10-2011 Adicionado o campo do CustomField Entidade/Colaborador.
                                                file.Item[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.EntidadeColaborador] = contact.EntidadeId + ";" + (contact.ColaboradorId.HasValue ? contact.ColaboradorId.Value.ToString() : string.Empty);

                                                FillWordBindFields(webEntidades, contact, file.Item);

                                                file.Item[SPBuiltInFieldId.Modified] = item.GetTypedValue<DateTime>(SPBuiltInFieldId.Modified);
                                                file.Item[SPBuiltInFieldId.Editor] = item.GetTypedValue<SPFieldUserValue>(SPBuiltInFieldId.Editor);
                                                file.Item.UpdateOverwriteVersion();

                                                // Anexos
                                                HlpIntranet.Correspondencia.CopyAttachments(attachmentsList.RootFolder, draftsList.RootFolder, item.ID, outboxList.RootFolder, file.Item.ID);
                                            }
                                        }

                                        HlpIntranet.Correspondencia.DeleteAttachments(attachmentsList.RootFolder, draftsList.RootFolder, item.ID);
                                        item.File.Delete();
                                    }
                                    else
                                    {
                                        errors.Add(new string[]
                                    {
                                        item.ContentType.Name,
                                        item.File.Name,
                                        item.GetTypedValue<DateTime>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.DataDocumento).ToString("dd-MM-yyyy"),
                                        item.GetTypedValue<string>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Assunto),
                                        item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.GrupoContacto).LookupValue,
                                        item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Entidade).LookupValue,
                                        item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Colaborador).LookupValue,
                                        "Grupo de contacto sem contactos activos."
                                    });
                                    }

                                    #endregion
                                }
                                else
                                {
                                    // Rejeita o documento por falta de preenchimento dos campos obrigatórios.
                                    item.File.Deny(string.Format("Campos obrigatórios não preenchidos ({0}).", this.Title));

                                    // Envia a mensagem para o criador/editor do documento.
                                    string body = HlpMail.CreateMessageBody(
                                        this.Title,
                                        "O seguinte documento não foi processado e continua nos rascunhos (rejeitado pelo sistema).",
                                        new string[] { "Tipo de Conteúdo", "Nome", "Canal", "Data Documento", "Assunto", "Grupo Contacto", "Entidade", "Colaborador", "Erro" },
                                        new List<string[]>
                                    {
                                        new string[]
                                        {
                                            item.ContentType.Name,
                                            string.Format("<a href='{0}'>{1}</a>", item.ParentList.GetFormPageUrl(SPControlMode.Display, item.ID, true), item.File.Name),
                                            item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Canal).LookupValue,
                                            item.GetTypedValue<DateTime>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.DataDocumento).ToString("dd-MM-yyyy"),
                                            item.GetTypedValue<string>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Assunto),
                                            item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.GrupoContacto).LookupValue,
                                            item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Entidade).LookupValue,
                                            item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Colaborador).LookupValue,
                                            requiredFieldMessage
                                        }
                                    });

                                    // Utilizadores a notificar do documento ter campos por preencher.
                                    string documentNotificationUsers = "";
                                    documentNotificationUsers += item.GetTypedValue<SPFieldUserValue>(SPBuiltInFieldId.Author).User.Email;
                                    documentNotificationUsers += ";" + item.GetTypedValue<SPFieldUserValue>(SPBuiltInFieldId.Editor).User.Email;

                                    HlpMail.SendMail(webApplication, documentNotificationUsers, "Intranet - Rascunhos - Campos obrigatórios por preencher", body);
                                }
                            }
                            catch (Exception ex)
                            {
                                errors.Add(new string[]
                            {
                                item.ContentType.Name,
                                item.File.Name,
                                "",
                                "",
                                "",
                                "",
                                "", 
                                ex.Message
                            });
                            }

                            progress += stepIncrement;
                            if (progress > 100) { progress = 100; }
                            this.UpdateProgress(progress);
                        }

                        HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.Update(web, HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.DireccaoEnum.Saida, year, lastFileNumber);

                        #region Relatório de erros dos documentos que não foram correctamente processados

                        if (errors.Count > 0)
                        {
                            string body = HlpMail.CreateMessageBody(
                                this.Title,
                                "Os seguintes documentos não foram processados e continuam nos rascunhos.",
                                new string[] { "Tipo de Conteúdo", "Nome", "Data Documento", "Assunto", "Grupo Contacto", "Entidade", "Colaborador", "Erro" },
                                errors);

                            HlpMail.SendMail(webApplication, HlpIntranet.Defaults.GetValue(site, HlpIntranet.Defaults.Keys.CorrespondenciaEnviadaNotificacoesErro), "Intranet - Correspondência Enviada - Erros no processamento de documentos de rascunho", body);
                        }

                        #endregion
                    }
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = currentCulture;
            }
        }
    }

}


