﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.Intranet.Helper;
using Convex.Sharepoint.Foundation.Extensions;
using System.Globalization;
using System.Threading;
using System.Collections;
using Microsoft.SharePoint.Utilities;
using Convex.Sharepoint.Foundation;

namespace TimerJobsV4.Correspondencia
{
    public class Inbox2Received : SPJobDefinition
    {
        public Inbox2Received()
            : base()
        {
        }

        public Inbox2Received(string name, SPWebApplication webApplication)
            : base(name, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = name;
        }

        public override void Execute(Guid contentDbId)
        {
            CultureInfo ptCulture = new CultureInfo("pt-PT");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ptCulture;
            try
            {
                SPWebApplication webApplication = this.Parent as SPWebApplication;
                SPContentDatabase contentDb = webApplication.ContentDatabases[contentDbId];

                using (SPSite site = contentDb.Sites["/"])
                {
                    using (SPWeb web = site.OpenWeb(HlpIntranet.Correspondencia.WebUrl))
                    {
                        List<string[]> errors = new List<string[]>();
                        SPList inboxList = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.CaixaEntrada.Nome);
                        SPList receivedList = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.Recebidos.Nome);

                        // Define as constantes do processo.
                        int year = DateTime.Today.Year;
                        int lastFileNumber = HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.GetLast(web, HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.DireccaoEnum.Entrada, year);

                        #region SPQuery de pesquisa (todos os documentos que já tenham data estão classificados)

                        StringBuilder caml = new StringBuilder();
                        caml.Append("<Where>");
                        caml.AppendFormat("<IsNotNull><FieldRef Name='{0}' /></IsNotNull>", HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.DataDocumento);
                        caml.Append("</Where>");

                        SPQuery query = new SPQuery();
                        query.Query = caml.ToString();

                        #endregion

                        SPListItemCollection itemsTemp = inboxList.GetItems(query);
                        List<SPListItem> items = new List<SPListItem>();
                        foreach (SPListItem item in itemsTemp)
                        {
                            items.Add(item);
                        }
                        itemsTemp = null;

                        int stepIncrement = 100 / (items.Count > 0 ? items.Count : 1);
                        int progress = 0;
                        foreach (SPListItem item in items)
                        {
                            try
                            {
                                // Atribui o folder onde vai colocar o documento pela data do mesmo.
                                DateTime documentDate = item.GetTypedValue<DateTime>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.DataDocumento);
                                SPFolder destinationFolder = receivedList.RootFolder.SubFolders.Add(documentDate.Year.ToString(), documentDate.Month.ToString().PadLeft(2, '0'));

                                // Transfere o documento para a pasta correcta.
                                string destinationUrl = CvxUtility.CombineUrl(destinationFolder.Url, item.File.Name);
                                item.File.MoveTo(destinationUrl, true);
                                // Actualiza o número de documento na metadata e repõe a data de modificação.
                                SPFile file = receivedList.ParentWeb.GetFile(destinationUrl);
                                if (file != null)
                                {
                                    lastFileNumber++;
                                    file.Item[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.NumeroDocumento] = HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.FormatNumber(year, lastFileNumber);
                                    file.Item[SPBuiltInFieldId.Modified] = item.GetTypedValue<DateTime>(SPBuiltInFieldId.Modified);
                                    file.Item[SPBuiltInFieldId.Editor] = item.GetTypedValue<SPFieldUserValue>(SPBuiltInFieldId.Editor);
                                    file.Item.UpdateOverwriteVersion();

                                    #region Atribui as permissões ao documento

                                    try
                                    {
                                        SPList listPermissions = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.RecebidosPermissoes.Nome);
                                        SPListItemCollection itemsPermissions = listPermissions.GetItems(new SPQuery { Query = string.Format("<Where><Eq><FieldRef Name='{0}'/><Value Type='Text'>{1}</Value></Eq></Where>", HlpIntranet.Correspondencia.Lists.RecebidosPermissoes.ContentTypes.Item.Fields.TipoConteudo, file.Item.ContentType.Name) });
                                        if (itemsPermissions != null && itemsPermissions.Count > 0)
                                        {
                                            // Break inheritance.
                                            file.Item.BreakRoleInheritance(false);

                                            foreach (SPListItem itemPermissions in itemsPermissions)
                                            {
                                                // Set new role assignment.
                                                SPPrincipal principal = (SPPrincipal)web.SiteGroups[itemPermissions.GetTypedValue<string>(HlpIntranet.Correspondencia.Lists.RecebidosPermissoes.ContentTypes.Item.Fields.Grupo)];
                                                SPRoleDefinition rd = web.RoleDefinitions[itemPermissions.GetTypedValue<string>(HlpIntranet.Correspondencia.Lists.RecebidosPermissoes.ContentTypes.Item.Fields.NivelPermissao)];

                                                SPRoleAssignment ra = new SPRoleAssignment(principal);
                                                ra.RoleDefinitionBindings.Add(rd);

                                                // Assign permissions.
                                                file.Item.RoleAssignments.Add(ra);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        errors.Add(new string[]
                                    {
                                        item.ContentType.Name,
                                        item.File.Name,
                                        "Erro a atribuir permissões.<br/>" + ex.Message
                                    });
                                    }

                                    #endregion

                                    #region Envia um mail para os destinatários a indicar que existe um novo documento

                                    try
                                    {
                                        List<string[]> docs = new List<string[]>();
                                        docs.Add(new string[]
                                    {
                                        file.Item.ContentType.Name,
                                        string.Format("<a href='{0}'>{1}</a>", CvxUtility.CombineUrl( file.Item.Web.Site.Url, file.ServerRelativeUrl), file.Item.GetTypedValue<string>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.NumeroDocumento)),
                                        file.Item.Title,
                                        file.Item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.Entidade).LookupValue,
                                        file.Item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.Colaborador).LookupValue,
                                    });

                                        string body = HlpMail.CreateMessageBody(
                                            this.Title,
                                            "Foi indicado como destinatário do seguinte documento.",
                                            new string[] { "Tipo de Conteúdo", "Nº Documento", "Assunto", "Remetente (Entidade)", "Remetente (Colaborador)" },
                                            docs);

                                        // Destinatários
                                        string toAddress = "";
                                        SPFieldUserValueCollection notifyUsers = file.Item.GetTypedValue<SPFieldUserValueCollection>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.Destinatario);
                                        foreach (SPFieldUserValue user in notifyUsers)
                                        {
                                            if (!string.IsNullOrEmpty(user.User.Email))
                                            {
                                                toAddress += user.User.Email;
                                            }
                                            else
                                            {
                                                errors.Add(new string[]
                                            {
                                                file.Item.ContentType.Name,
                                                file.Item.GetTypedValue<string>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.NumeroDocumento),
                                                string.Format("Destinatário {0} não tem endereço de correio electrónico válido.<br/>Documento processado mas o destinatário não foi notificado.", user.LookupValue)
                                            });
                                            }
                                        }

                                        HlpMail.SendMail(webApplication, toAddress, "Novo documento recebido!", body);
                                    }
                                    catch
                                    {
                                        errors.Add(new string[]
                                    {
                                        file.Item.ContentType.Name,
                                        file.Item.GetTypedValue<string>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.NumeroDocumento),
                                        "Erro a notificar destinatários."
                                    });
                                    }

                                    #endregion
                                }
                            }
                            catch (Exception ex)
                            {
                                errors.Add(new string[]
                            {
                                item.ContentType.Name,
                                item.File.Name,
                                ex.Message
                            });
                            }

                            progress += stepIncrement;
                            if (progress > 100) { progress = 100; }
                            this.UpdateProgress(progress);
                        }

                        HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.Update(web, HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.DireccaoEnum.Entrada, year, lastFileNumber);

                        #region Relatório de erros dos documentos que não foram correctamente processados

                        if (errors.Count > 0)
                        {
                            string body = HlpMail.CreateMessageBody(
                                this.Title,
                                "Ocorreram erros a processar os seguintes documentos.",
                                new string[] { "Tipo de Conteúdo", "Documento", "Erro" },
                                errors);

                            HlpMail.SendMail(webApplication, HlpIntranet.Defaults.GetValue(site, HlpIntranet.Defaults.Keys.CorrespondenciaRecebidaNotificacoesErro), "Intranet - Correspondência Recebida - Erros no processamento de documentos", body);
                        }

                        #endregion
                    }
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = currentCulture;
            }
        }
    }
}
