﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.Intranet.Helper;
using Convex.Sharepoint.Foundation.Extensions;
using System.Globalization;
using System.Threading;
using System.Collections;
using Microsoft.SharePoint.Utilities;
using Convex.Sharepoint.Foundation;
using System.Diagnostics;

namespace TimerJobsV4.Correspondencia
{
    public class Outbox2Sent : SPJobDefinition
    {
        public Outbox2Sent()
            : base()
        {
        }

        public Outbox2Sent(string name, SPWebApplication webApplication)
            : base(name, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = name;
        }

        public override void Execute(Guid contentDbId)
        {
            CultureInfo ptCulture = new CultureInfo("pt-PT");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ptCulture;

            Debug.Write("Starting..", "JobOutbox2Sent");
            SPWebApplication webApplication = this.Parent as SPWebApplication;
            SPContentDatabase contentDb = webApplication.ContentDatabases[contentDbId];

            try
            {
                using (SPSite site = contentDb.Sites["/"])
                {
                    using (SPWeb web = site.OpenWeb(HlpIntranet.Correspondencia.WebUrl))
                    {
                        List<string[]> errors = new List<string[]>();
                        SPList outboxList = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.CaixaSaida.Nome);
                        SPList sentList = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.Enviados.Nome);
                        SPList attachmentsList = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.Anexos.Nome);

                        // Define as constantes do processo.
                        int emailId = HlpIntranet.Correspondencia.Lists.Canais.GetCanalEmailId(web);

                        #region SPQuery de pesquisa (todos os documentos aprovados)

                        StringBuilder caml = new StringBuilder();
                        caml.Append("<Where>");
                        caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Number'>{1}</Value></Eq>", SPBuiltInFieldId._ModerationStatus, (int)SPModerationStatusType.Approved);
                        caml.Append("</Where>");

                        SPQuery query = new SPQuery();
                        query.Query = caml.ToString();

                        Debug.Write("CAML Query: " + query.Query, "JobOutbox2Sent");

                        #endregion

                        SPListItemCollection itemsTemp = outboxList.GetItems(query);

                        Debug.Write("CAML Query Res: " + itemsTemp.Count, "JobOutbox2Sent");

                        List<SPListItem> items = new List<SPListItem>();
                        foreach (SPListItem item in itemsTemp)
                        {
                            items.Add(item);
                        }
                        itemsTemp = null;

                        int stepIncrement = 100 / (items.Count > 0 ? items.Count : 1);
                        int progress = 0;
                        foreach (SPListItem item in items)
                        {
                            Debug.Write(String.Format("Processing Item ID: {0} FileName: {1}", item.ID, item.File == null ? "NULL" : item.File.Name), "JobOutbox2Sent");

                            try
                            {
                                // Atribui o folder onde vai colocar o documento pela data do mesmo.
                                DateTime documentDate = item.GetTypedValue<DateTime>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.DataDocumento);
                                SPFolder destinationFolder = sentList.RootFolder.SubFolders.Add(documentDate.Year.ToString(), documentDate.Month.ToString().PadLeft(2, '0'));

                                Debug.Write("Document Dest Folder: " + destinationFolder.ServerRelativeUrl, "JobOutbox2Sent");

                                // Transfere o documento para a pasta correcta.
                                string destinationUrl = CvxUtility.CombineUrl(destinationFolder.Url, item.File.Name);
                                item.File.MoveTo(destinationUrl, true);
                                // Actualiza o número de documento na metadata e repõe a data de modificação.
                                SPFile file = sentList.ParentWeb.GetFile(destinationUrl);
                                if (file != null)
                                {
                                    Debug.Write("Updating file metadata " + file.ServerRelativeUrl, "JobOutbox2Sent");

                                    file.Item[SPBuiltInFieldId.Modified] = item.GetTypedValue<DateTime>(SPBuiltInFieldId.Modified);
                                    file.Item[SPBuiltInFieldId.Editor] = item.GetTypedValue<SPFieldUserValue>(SPBuiltInFieldId.Editor);
                                    file.Item.UpdateOverwriteVersion();

                                    Debug.Write("Moving Attachments..", "JobOutbox2Sent");
                                    // Anexos
                                    HlpIntranet.Correspondencia.MoveAttachments(attachmentsList.RootFolder, outboxList.RootFolder, item.ID, sentList.RootFolder, file.Item.ID);
                                }
                            }
                            catch (Exception ex)
                            {
                                Debug.Write("Item Error: " + ex.Message + Environment.NewLine + ex.StackTrace, "JobOutbox2Sent");

                                errors.Add(new string[]
                            {
                                item.ContentType.Name,
                                item.File.Name,
                                item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Canal).LookupValue,
                                item.GetTypedValue<DateTime>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.DataDocumento).ToString("dd-MM-yyyy"),
                                item.GetTypedValue<string>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Assunto),
                                item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Entidade).LookupValue,
                                item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Colaborador).LookupValue,
                                ex.Message
                            });
                            }

                            #region Old
                            /*
                        #region Envia por mail o documento caso não esteja anulado e seja do canal email

                        if (item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Canal).LookupId == emailId
                            &&
                            !item.GetTypedValue<bool>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Anulado))
                        {
                            string toAddress = "";
                            int entidadeId = 0;
                            int colaboradorId = 0;

                            SPFieldLookupValue lv = null;
                            lv = item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Entidade);
                            if (lv != null) { entidadeId = lv.LookupId; }
                            lv = item.GetTypedValue<SPFieldLookupValue>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Colaborador);
                            if (lv != null) { colaboradorId = lv.LookupId; }

                            #region Valida se entidade, ou colaborador, têm endereço válido

                            // Se o colaborador estiver definido é para ele que envia o mail, caso contrário é para a entidade.
                            if (colaboradorId > 0)
                            {
                                // Verifica endereço de envio.
                                DataRow row = (from DataRow r in ds.Tables["Colaboradores"].Rows
                                               where (int)r[HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Id] == colaboradorId && r[HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Email] != DBNull.Value
                                               select r).SingleOrDefault<DataRow>();

                                toAddress = (row != null) ? Convert.ToString(row[HlpIntranet.Entidades.Lists.Colaboradores.ContentTypes.Colaborador.Fields.Email]) : "";
                            }
                            else if (entidadeId > 0)
                            {
                                // // Verifica endereço de envio.
                                DataRow row = (from DataRow r in ds.Tables["Entidades"].Rows
                                               where (int)r[HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Entidade.Fields.Id] == entidadeId && r[HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Entidade.Fields.Email] != DBNull.Value
                                               select r).SingleOrDefault<DataRow>();

                                if (row != null)
                                {
                                    toAddress = item.ContentType.ResourceFolder.Name.StartsWith("AF_DocumentoQuota")
                                        ? Convert.ToString(row[HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.EmailQuotas])
                                        : Convert.ToString(row[HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Entidade.Fields.Email]);
                                }
                            }

                            #endregion

                            #region Constroi e envia a mensagem de mail com o documento e anexos para o destinatário de email

                            if (toAddress.Length == 0)
                            {
                                dtErrors.Rows.Add(
                                    item.ContentType.Name,
                                    item.File.Name,
                                    DateTime.Parse(item.GetFormattedValue(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.DataDocumento)).ToString("dd-MM-yyyy"),
                                    item.GetFormattedValue(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.NumeroDocumento),
                                    item.GetFormattedValue(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Assunto),
                                    item.GetFormattedValue(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Entidade),
                                    item.GetFormattedValue(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Colaborador)
                                );
                            }
                            else
                            {
                                MailMessage message = new MailMessage();
                                message.To.Add(toAddress);
                                message.Subject = Convert.ToString(item.GetFormattedValue(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Assunto));

                                byte[] fileContents = item.File.OpenBinary();
                                string fileContentAsString = item.File.CharSetName != null ? Encoding.GetEncoding(item.File.CharSetName).GetString(fileContents) : Encoding.GetEncoding(0).GetString(fileContents);
                                string fileExtension = Path.GetExtension(item.File.Name).ToLower();

                                // Se for do tipo htm, html ou mht coloca o conteúdo no body, caso contrário o ficheiro fica em attachment.
                                if (fileExtension == ".htm" || fileExtension == ".html" || fileExtension == ".mht")
                                {
                                    message.Body = fileContentAsString;
                                }
                                else
                                {
                                    MemoryStream fileStream = new MemoryStream(fileContents);
                                    Attachment attachment = new Attachment(fileStream, item.File.Name, MediaTypeNames.Application.Octet);
                                    message.Attachments.Add(attachment);
                                }

                                if (attachmentsSourceFolder.Exists)
                                {
                                    foreach (SPFile fileAttachment in attachmentsSourceFolder.Files)
                                    {
                                        if (fileAttachment.Item.GetTypedValue<bool>(HlpIntranet.Correspondencia.Lists.Anexos.ContentTypes.Documento.Fields.EnvioPorMail))
                                        {
                                            MemoryStream fileStream = new MemoryStream(fileAttachment.OpenBinary());
                                            Attachment attachment = new Attachment(fileStream, "Anexo_" + fileAttachment.Name, MediaTypeNames.Application.Octet);
                                            message.Attachments.Add(attachment);
                                        }
                                    }
                                }

                                HlpMail.SendMail(message);
                            }

                            #endregion
                        }

                        #endregion
                        */
                            #endregion

                            progress += stepIncrement;
                            if (progress > 100) { progress = 100; }
                            this.UpdateProgress(progress);
                        }

                        #region Relatório de erros dos documentos que não foram correctamente processados

                        if (errors.Count > 0)
                        {
                            string body = HlpMail.CreateMessageBody(
                                this.Title,
                                "Os seguintes documentos não foram processados e continuam na caixa de saída.",
                                new string[] { "Tipo de Conteúdo", "Nome", "Canal", "Data Documento", "Assunto", "Entidade", "Colaborador", "Erro" },
                                errors);

                            HlpMail.SendMail(webApplication, HlpIntranet.Defaults.GetValue(site, HlpIntranet.Defaults.Keys.CorrespondenciaEnviadaNotificacoesErro), "Intranet - Correspondência Enviada - Erros no processamento de documentos de saída", body);
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Write("General Error: " + ex.Message + Environment.NewLine + ex.StackTrace, "JobOutbox2Sent");
                throw;
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = currentCulture;
            }
            Debug.Write("Ending..", "JobOutbox2Sent");
        }
    }
}
