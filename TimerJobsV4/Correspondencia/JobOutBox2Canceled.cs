﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.Intranet.Helper;
using Convex.Sharepoint.Foundation.Extensions;
using System.Globalization;
using System.Threading;
using System.Collections;
using Microsoft.SharePoint.Utilities;
using Convex.Sharepoint.Foundation;

namespace TimerJobsV4.Correspondencia
{
    public class OutBox2Canceled : SPJobDefinition
    {
        public OutBox2Canceled()
            : base()
        {
        }

        public OutBox2Canceled(string name, SPWebApplication webApplication)
            : base(name, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = name;
        }

        public override void Execute(Guid contentDbId)
        {
            CultureInfo ptCulture = new CultureInfo("pt-PT");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ptCulture;
            try
            {
                SPWebApplication webApplication = this.Parent as SPWebApplication;
                SPContentDatabase contentDb = webApplication.ContentDatabases[contentDbId];

                using (SPSite site = contentDb.Sites["/"])
                {
                    using (SPWeb web = site.OpenWeb(HlpIntranet.Correspondencia.WebUrl))
                    {
                        List<string[]> errors = new List<string[]>();
                        SPList outboxList = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.CaixaSaida.Nome);
                        SPList canceledList = web.Lists.GetList(HlpIntranet.Correspondencia.Lists.Anuladas.Nome);

                        // Define as constantes do processo.
                        int year = DateTime.Today.Year;
                        int lastFileNumber = HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.GetLast(web, HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.DireccaoEnum.Entrada, year);

                        #region SPQuery de pesquisa (todos os documentos que tenham sido anulados na Caixa de Saída.)

                        StringBuilder caml = new StringBuilder();
                        caml.Append("<Where>");
                        caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Boolean'>{1}</Value></Eq>", HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.Anulado);
                        caml.Append("</Where>");

                        SPQuery query = new SPQuery();
                        query.Query = caml.ToString();

                        #endregion

                        SPListItemCollection itemsTemp = outboxList.GetItems(query);
                        List<SPListItem> items = new List<SPListItem>();
                        foreach (SPListItem item in itemsTemp)
                        {
                            items.Add(item);
                        }
                        itemsTemp = null;

                        int stepIncrement = 100 / (items.Count > 0 ? items.Count : 1);
                        int progress = 0;
                        foreach (SPListItem item in items)
                        {
                            try
                            {
                                // Atribui o folder onde vai colocar o documento pela data do mesmo.
                                DateTime documentDate = item.GetTypedValue<DateTime>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.DataDocumento);
                                SPFolder destinationFolder = canceledList.RootFolder.SubFolders.Add(documentDate.Year.ToString(), documentDate.Month.ToString().PadLeft(2, '0'));

                                // Transfere o documento para a pasta correcta.
                                string destinationUrl = CvxUtility.CombineUrl(destinationFolder.Url, item.File.Name);
                                item.File.MoveTo(destinationUrl, true);
                                // Actualiza o número de documento na metadata e repõe a data de modificação.
                                SPFile file = canceledList.ParentWeb.GetFile(destinationUrl);
                                if (file != null)
                                {
                                    lastFileNumber++;
                                    file.Item[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.NumeroDocumento] = HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.FormatNumber(year, lastFileNumber);
                                    file.Item[SPBuiltInFieldId.Modified] = item.GetTypedValue<DateTime>(SPBuiltInFieldId.Modified);
                                    file.Item[SPBuiltInFieldId.Editor] = item.GetTypedValue<SPFieldUserValue>(SPBuiltInFieldId.Editor);
                                    file.Item.UpdateOverwriteVersion();

                                }
                            }
                            catch (Exception ex)
                            {
                                errors.Add(new string[]
                            {
                                item.ContentType.Name,
                                item.File.Name,
                                ex.Message
                            });
                            }

                            progress += stepIncrement;
                            if (progress > 100) { progress = 100; }
                            this.UpdateProgress(progress);
                        }

                        HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.Update(web, HlpIntranet.Correspondencia.Lists.NumeracaoDocumentos.DireccaoEnum.Entrada, year, lastFileNumber);

                        #region Relatório de erros dos documentos que não foram correctamente processados

                        if (errors.Count > 0)
                        {
                            string body = HlpMail.CreateMessageBody(
                                this.Title,
                                "Ocorreram erros a processar os seguintes documentos.",
                                new string[] { "Tipo de Conteúdo", "Documento", "Erro" },
                                errors);

                            HlpMail.SendMail(webApplication, HlpIntranet.Defaults.GetValue(site, HlpIntranet.Defaults.Keys.CorrespondenciaRecebidaNotificacoesErro), "Intranet - Correspondência Recebida - Erros no processamento de documentos", body);
                        }

                        #endregion
                    }
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = currentCulture;
            }
        }
    }
}
