﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using System.Diagnostics;
using Convex.Sharepoint.Foundation.Extensions.Permissions;
using Convex.Sharepoint.Foundation.Extensions;

namespace TimerJobsV4.Correspondencia.EventReceivers
{
    class EventUtils
    {
        private static SPGroup FindGroupByName(string name, SPGroupCollection col)
        {
            try
            {
                return col[name];
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static void CheckConfidenciality(SPItemEventProperties properties, bool conf)
        {
            if (conf)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(properties.WebUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPListItem elevatedItem = web.Lists[properties.ListTitle].GetItemById(properties.ListItemId);

                            SPGroup confGroup = FindGroupByName("Confidencial", web.SiteGroups);
                            if (confGroup == null)
                            {
                                Debug.Write("Não foi encontrado grupo Confidencial!");
                                return;
                            }
                            int creatorId = new SPFieldLookupValue(elevatedItem["Author"].ToString()).LookupId;

                            AccessCollection acs = new AccessCollection();
                            acs.Add(new Access(confGroup.ID, SPRoleType.Contributor));
                            acs.Add(new Access(creatorId, SPRoleType.Contributor));

                            elevatedItem.SetPermissions(acs, web);
                        }
                    }
                });
            }
            else
            {
                if (properties.ListItem.HasUniqueRoleAssignments)
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        using (SPSite site = new SPSite(properties.WebUrl))
                        {
                            using (SPWeb web = site.OpenWeb())
                            {
                                SPListItem elevatedItem = web.Lists[properties.ListTitle].GetItemById(properties.ListItemId);

                                elevatedItem.ResetPermissions();
                            }
                        }
                    });
                }

            }
        }
    }
}
