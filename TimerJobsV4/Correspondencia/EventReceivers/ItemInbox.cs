﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.Intranet.Helper;

namespace TimerJobsV4.Correspondencia.EventReceivers
{
    class ItemInbox : SPItemEventReceiver
    {
        public override void ItemUpdating(SPItemEventProperties properties)
        {
            int entidadeId = 0;
            int colaboradorId = 0;

            string[] splitEntidadeColaborador = properties.AfterProperties[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.EntidadeColaborador].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (splitEntidadeColaborador.Length > 0)
            {
                entidadeId = int.Parse(splitEntidadeColaborador[0]);
                if (splitEntidadeColaborador.Length > 1)
                {
                    colaboradorId = int.Parse(splitEntidadeColaborador[1]);
                }
            }

            properties.AfterProperties[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.Entidade] = entidadeId;
            properties.AfterProperties[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseRecebido.Fields.Colaborador] = colaboradorId;
        }

    }
}