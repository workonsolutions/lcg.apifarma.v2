﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.Intranet.Helper;

namespace TimerJobsV4.Correspondencia.EventReceivers
{
    class Confidenciality : SPItemEventReceiver
    {
        public override void ItemUpdated(SPItemEventProperties properties)
        {
            if (properties.ListItem != null)
            {
                bool conf = false;

                if (properties.ListItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Confidencial] != null)
                    conf = Convert.ToBoolean(properties.ListItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Confidencial]);

                EventUtils.CheckConfidenciality(properties, conf);
            }
        }

        public override void ItemAdded(SPItemEventProperties properties)
        {
            if (properties.ListItem != null)
            {
                bool conf = false;

                if (properties.ListItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Confidencial] != null)
                    conf = Convert.ToBoolean(properties.ListItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Confidencial]);

                EventUtils.CheckConfidenciality(properties, conf);
            }
        }
    }
}
