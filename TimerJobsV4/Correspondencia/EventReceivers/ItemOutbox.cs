﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.Intranet.Helper;
using Convex.Sharepoint.Foundation.Extensions;

namespace TimerJobsV4.Correspondencia.EventReceivers
{
    class ItemOutbox : SPItemEventReceiver
    {
        public override void ItemUpdated(SPItemEventProperties properties)
        {
            //DS: 2010-12-09 Removido para solucionar problema desdobramento grupos de contacto.

            // Tem que ser no Updated porque o Word mexe na metadata também e só assim garantimos que os valores ficam correctos.
            // Executado depois de tudo!

            this.EventFiringEnabled = false;

            int entidadeId = 0;
            int colaboradorId = 0;

            string[] splitEntidadeColaborador = properties.ListItem.GetTypedValue<string>(HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.EntidadeColaborador).Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (splitEntidadeColaborador.Length > 0)
            {
                entidadeId = int.Parse(splitEntidadeColaborador[0]);
                if (splitEntidadeColaborador.Length > 1)
                {
                    colaboradorId = int.Parse(splitEntidadeColaborador[1]);
                }
            }

            properties.ListItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Entidade] = entidadeId;
            properties.ListItem[HlpIntranet.Correspondencia.ContentTypes.DocumentoBaseEnviado.Fields.Colaborador] = colaboradorId;
            properties.ListItem.SystemUpdate(false);

            this.EventFiringEnabled = true; ;

        }

    }
}