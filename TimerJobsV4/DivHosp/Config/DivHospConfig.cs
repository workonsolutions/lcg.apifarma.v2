﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Convex.Sharepoint.Foundation.ConfigList;
using System.Configuration;
using Microsoft.SharePoint;
using TimerJobsV4.DivHosp.Helper;

namespace TimerJobsV4.DivHosp.Config
{
    public class DivHospConfig
    {

        public partial struct Inquerito
        {
            public const string CategoryName = "Inquerito";
            public static ConfigIdentity DiaMesFimPrazoEntrega = new ConfigIdentity(CategoryName, "DiaMesFimPrazoEntrega");
            public static ConfigIdentity ValorMaximoPMR = new ConfigIdentity(CategoryName, "ValorMaximoPMR");
            public static ConfigIdentity NotificationEnable = new ConfigIdentity(CategoryName, "EnviarNotificacao");
            public static ConfigIdentity NotificationTemplatePath = new ConfigIdentity(CategoryName, "CaminhoTemplateNotificacao");
            public static ConfigIdentity NotificationFromEmail = new ConfigIdentity(CategoryName, "EmailSaidaNotificacao");
            // Added by gboleo, 2012-03-13 - Intranet ID78
            public static ConfigIdentity NotificationBccEmail = new ConfigIdentity(CategoryName, "EmailBccNotificacao");
            // Added by gboleo, 2012-03-13 - Intranet ID78
            public static ConfigIdentity NotificationSubject = new ConfigIdentity(CategoryName, "AssuntoEmailNotificacao");

        }
        public partial struct Sistema
        {
            public const string CategoryName = "Sistema";
            public static ConfigIdentity InstituicaoGenericaSPA = new ConfigIdentity(CategoryName, "InstituicaoGenericaSPA");
            public static ConfigIdentity InstituicaoGenericaEPE = new ConfigIdentity(CategoryName, "InstituicaoGenericaEPE");
        }

        public static string GetConfigValue(ConfigIdentity config, SPWeb dividasWeb)
        {
            return ConfigList.GetValue(config, dividasWeb);
        }
        // Como a configuração está na Intranet e temos páginas na Extranet, vamos tratar internamente este detalhe
        public static string GetConfigValue(ConfigIdentity config)
        {
            string intranetUrl = ConfigurationManager.AppSettings["IntranetSiteUrl"];
            if (string.IsNullOrEmpty(intranetUrl) ||
                (SPContext.Current != null &&
                SPContext.Current.Site.Url == intranetUrl &&
                SPContext.Current.Web.ServerRelativeUrl.ToLower() == HlpDivHosp.WebUrl))
            {
                //Site actual é Intranet/DividasHosp, usar Contexto.
                return ConfigList.GetValue(config);
            }
            else
            {
                string v = string.Empty;
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    //Necessáro abrir site da Intranet/DividasHosp.
                    using (SPSite site = new SPSite(intranetUrl))
                    {
                        using (SPWeb web = site.OpenWeb(HlpDivHosp.WebUrl))
                        {
                            v = ConfigList.GetValue(config, web);
                        }
                    }
                });
                return v;
            }
        }
    }
}
