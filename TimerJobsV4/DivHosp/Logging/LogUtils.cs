﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.SharePoint;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace TimerJobsV4.DivHosp.Logging
{
    public enum DivHospCategory
    {
        DivHosp_General = 0,
        DivHosp_Jobs
    };
    public class LogUtils
    {

        public static void LogMessage(DivHospCategory cat, TraceEventType severity, string title, string message)
        {
            try
            {
                Debug.Write(String.Format("Title: {0} Message: {1}", title, message), cat.ToString());
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    LogWriter lw = EnterpriseLibraryContainer.Current.GetInstance<LogWriter>();
                    LogEntry le = new LogEntry();

                    le.Categories.Add(cat.ToString());
                    le.Message = message;
                    le.Title = title;
                    le.Severity = severity;
                    lw.Write(le);
                });
            }
            catch (Exception ex)
            {

                Debug.Write("Log Error: " + ex.Message + ex.StackTrace);
            }
        }

        //public static void LogMessage(LogWriter lw, DivHospCategory cat, TraceEventType severity, string message) 
        //{
        //    LogEntry le = new LogEntry();

        //    le.Categories.Add(cat.ToString());
        //    le.Message = message;
        //    le.Severity = severity;         
        //    lw.Write(le);
        //}
        //public static void LogMessage(DivHospCategory cat, TraceEventType severity, string message)
        //{
        //    LogWriter lw = EnterpriseLibraryContainer.Current.GetInstance<LogWriter>();

        //    LogEntry le = new LogEntry();

        //    le.Categories.Add(cat.ToString());
        //    le.Message = message;
        //    le.Severity = severity;
        //    lw.Write(le);
        //}
    }
}
