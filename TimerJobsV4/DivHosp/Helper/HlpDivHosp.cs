﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Convex.Clientes.Apifarma.DivHosp.DAL;
using TimerJobsV4.DivHosp.Logging;

namespace TimerJobsV4.DivHosp.Helper
{
    public class HlpDivHosp
    {
        public const string WebUrl = "/dividashosp";
        public struct TiposHospitais
        {

            public const int SPA_ID = 1;
            public const int EPE_ID = 2;
        }
        public static Associado EnsureAssociado(string numero, int idIntranet, string nome, DividasHospitalaresEntities context)
        {
            var query = from a in context.AssociadoSet where a.Numero == numero select a;

            if (query.Count() == 0)
            {
                Associado novoAssociado = new Associado();
                novoAssociado.IdIntranet = idIntranet;
                novoAssociado.Numero = numero;
                novoAssociado.Nome = nome;

                context.AddToAssociadoSet(novoAssociado);
                context.SaveChanges();
                LogUtils.LogMessage(DivHospCategory.DivHosp_General, System.Diagnostics.TraceEventType.Information, "Dividas Hospitalares",
                    String.Format("Inserido novo Associado '{0} - {1}' na base de dados.", numero, nome));

                return novoAssociado;
            }
            else
                return query.First();
        }
    }
}
