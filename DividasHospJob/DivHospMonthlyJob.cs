﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using System.Globalization;
using System.Threading;
using DividasHospJob.Helper;

namespace DividasHospJob.DivHosp
{
    public class DivHospMonthlyJob : SPJobDefinition
    {
        public static string JobName = "DivHosp_MonthlyJob18102013";

        public DivHospMonthlyJob() : base() { }

        public DivHospMonthlyJob(string name, SPWebApplication webApplication)
            : base(name, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = name;
        }

        public DivHospMonthlyJob(SPWebApplication webApplication)
            : base(JobName, webApplication, null, SPJobLockType.ContentDatabase)
        {
            this.Title = JobName;
        }

        private void Run_TransporValores()
        {
            try
            {

                //Resultados inseridos na BD são sempre referentes ao mês anterior...
                DateTime dtMonth = DateTime.Now.AddMonths(-1);
                Inquerito.TransporValores(dtMonth.Year, dtMonth.Month);
            }
            catch (Exception ex)
            {

            }
        }
        private void Run_InqueritoNotification(SPSite site)
        {
            try
            {

                Inquerito.NewInqueritoNotification(site);
            }
            catch (Exception ex)
            {

            }
        }
        public override void Execute(Guid targetInstanceId)
        {

            CultureInfo ptCulture = new CultureInfo("pt-PT");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ptCulture;

            try
            {

                SPWebApplication webApplication = this.Parent as SPWebApplication;
                SPContentDatabase contentDb = webApplication.ContentDatabases[targetInstanceId];

                using (SPSite site = contentDb.Sites["/"])
                {
                    //DS: 20110627 - Apifarma decidiu que a transposição dos valores deve ocorrer no inicio do periodo de entrega. Processo deverá correr no dia 1 de cada mês.
                    //if (DateTime.Now.Day == DivHospConfig.Inquerito.DiaMesFimPrazoEntrega+1)
                    if (DateTime.Now.Day == 18) // primeiro dia do mês. (Nao esquecer de repor o dia 1.RSILVA)
                    {
                        Run_TransporValores();

                        Run_InqueritoNotification(site);
                    }

                }
            }
            catch (Exception ex)
            {
                ////Adicionado por RSILVA para adicionar a excepção aos logs de sharepoint
                //Microsoft.Office.Server.Diagnostics.PortalLog.LogString("Exception – {0} – {1} – {2}","User Friendly Message", ex.Message, ex.StackTrace);

                //LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs, System.Diagnostics.TraceEventType.Error, "DivHospMonthlyJob", "Erro: " + ex.ToString());
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\logsfromVS.txt", true))
                {
                    file.WriteLine(ex.Message);
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = currentCulture;
            }
        }

    }
}

