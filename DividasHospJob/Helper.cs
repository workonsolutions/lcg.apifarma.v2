﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Convex.Clientes.Apifarma.DivHosp.DAL;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.Intranet.Helper;
using Convex.Sharepoint.Foundation.Webparts.Helper;
using System.Xml.Xsl;
using Convex.Sharepoint.Foundation.Extensions;
using Microsoft.SharePoint.Utilities;
using System.Xml.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Net.Mime;
using TimerJobsV4.DivHosp.Config;

namespace TimerJobsV4.DivHosp.Helper
{
    public class Inquerito
    {

        private static void InsertCopy(Resultado r, DateTime data, DividasHospitalaresEntities context)
        {
            Resultado novoResultado = new Resultado();
            novoResultado.AreaNegocio = r.AreaNegocio;
            novoResultado.Associado = r.Associado;
            novoResultado.Hospital = r.Hospital;
            if (r.PMR != null)
                novoResultado.PMR = r.PMR;
            if (r.Divida90 != null)
                novoResultado.Divida90 = r.Divida90;
            if (r.DividaTotal != null)
                novoResultado.DividaTotal = r.DividaTotal;
            novoResultado.Data = data;

            context.AddToResultadoSet(novoResultado);
        }
        public static void TransporValores(int anoInquerito, int mesInquerito)
        {
            DateTime dataMesInquerito = new DateTime(anoInquerito, mesInquerito, 1);
            DateTime dataMesAnterior = dataMesInquerito.AddMonths(-1);

            LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                System.Diagnostics.TraceEventType.Information, "Transposição de Valores",
                String.Format("Processar inquérito referente ao mês: {0}", dataMesInquerito));

            StringBuilder sbInfo = new StringBuilder();

            using (DividasHospitalaresEntities context = new DividasHospitalaresEntities())
            {

                //Obter valores inseridos no mês
                var resultadosMesInquerito = from r in context.ResultadoSet
                                             where r.Data == dataMesInquerito
                                             select r;


                //Obter valores inseridos no mês anterior
                var resultadosMesAnterior = from r in context.ResultadoSet.Include("Hospital").Include("AreaNegocio").Include("Associado")
                                            where r.Data == dataMesAnterior
                                            select r;

                //foreach (var resultado in resultadosMesAnterior)
                //{

                //    //Existe um resultado no mes actual para o mesmo associado/hospital/areanegocio ?

                //    var query1 = from r in resultadosMesActual
                //                 where
                //                     r.AreaNegocio.AreaNegocioID == resultado.AreaNegocio.AreaNegocioID &&
                //                     r.Associado.AssociadoID == resultado.Associado.AssociadoID &&
                //                     r.Hospital.HospitalID == resultado.Hospital.HospitalID
                //                 select r;
                //    if (query1.Count() == 0)
                //    {
                //        InsertCopy(resultado, mesCorrente, context);
                //    }
                //    else
                //    {
                //        Resultado resultadoMesCorrente = query1.First();
                //        //Validar campos não preenchidos
                //        if (resultadoMesCorrente.PMR == null && resultado.PMR != null)
                //            resultadoMesCorrente.PMR = resultado.PMR;
                //        if (r.Divida90 != null)
                //            novoResultado.Divida90 = r.Divida90;
                //        if (r.DividaTotal != null)
                //            novoResultado.DividaTotal = r.DividaTotal;
                //    }

                //}

                //Como no carregamento do excel com o inquérito são validados os dados do mês anterior, aqui só interessa validar associados que não responderam.

                Associado[] associadosMesInquerito = (from r in resultadosMesInquerito group r by r.Associado into g select g.Key).ToArray();
                Associado[] associadosMesAnterior = (from r in resultadosMesAnterior group r by r.Associado into g select g.Key).ToArray();

                LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                   System.Diagnostics.TraceEventType.Information, "Transposição de Valores",
                   String.Format("{0} Associados responderam ao inquérito deste mês.", associadosMesInquerito.Count()));

                LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                 System.Diagnostics.TraceEventType.Information, "Transposição de Valores",
                 String.Format("{0} Associados responderam ao inquérito do mês anterior.", associadosMesAnterior.Count()));

                foreach (Associado associado in associadosMesAnterior)
                {


                    if (!associadosMesInquerito.Contains(associado))//, AssociadoComparer.GetInstance()))
                    {

                        LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                         System.Diagnostics.TraceEventType.Verbose, "Transposição de Valores",
                         String.Format("Necessário transpor valores do Associado {0} - {1}", associado.Numero, associado.Nome));

                        var results = from r in resultadosMesAnterior where r.Associado.AssociadoID == associado.AssociadoID select r;

                        foreach (var r in results)
                        {
                            //Resultado "nulo" ? 
                            if ((r.Divida90 == null || r.Divida90.Value == 0) &&
                                (r.DividaTotal == null || r.DividaTotal.Value == 0) &&
                                (r.PMR == null || r.PMR.Value == 0))
                                continue;

                            InsertCopy(r, dataMesInquerito, context);
                        }
                        context.SaveChanges();
                    }
                }
            }
        }

        public static void NewInqueritoNotification(SPSite site)
        {
            using (SPWeb dividasWeb = site.OpenWeb(HlpDivHosp.WebUrl), entidadesWeb = site.OpenWeb(HlpIntranet.Entidades.WebUrl))
            {
                // 1 - Template Notificação:

                bool notificationEnable = false;

                bool.TryParse(DivHospConfig.GetConfigValue(DivHospConfig.Inquerito.NotificationEnable, dividasWeb), out notificationEnable);
                if (!notificationEnable) return;

                string templatePath = DivHospConfig.GetConfigValue(DivHospConfig.Inquerito.NotificationTemplatePath, dividasWeb);
                string fromEmail = DivHospConfig.GetConfigValue(DivHospConfig.Inquerito.NotificationFromEmail, dividasWeb);
                // Added by gboleo, 2012-03-13 - Intranet ID78
                string bccEmail = DivHospConfig.GetConfigValue(DivHospConfig.Inquerito.NotificationBccEmail, dividasWeb);
                // Added by gboleo, 2012-03-13 - Intranet ID78
                int diaLimite = Int32.Parse(DivHospConfig.GetConfigValue(DivHospConfig.Inquerito.DiaMesFimPrazoEntrega, dividasWeb));
                string tituloEmail = DivHospConfig.GetConfigValue(DivHospConfig.Inquerito.NotificationSubject, dividasWeb);
                DateTime dataLimite = new DateTime(DateTime.Now.Year, DateTime.Now.Month, diaLimite);
                string dataInquerito = String.Format("{0:MMMM}/{1}", DateTime.Now.AddMonths(-1), DateTime.Now.Year);

                tituloEmail = String.Format(tituloEmail, dataInquerito);

                LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                        System.Diagnostics.TraceEventType.Verbose, "Notificação Inquerito",
                        String.Format("Carregar template email em '{0}'", templatePath));


                SPFile templateFile = dividasWeb.GetFile(templatePath);
                if (!templateFile.Exists)
                {
                    LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                       System.Diagnostics.TraceEventType.Warning, "Notificação Inquerito",
                       String.Format("Template não encontrado em '{0}'", templatePath));

                    return;
                }

                XslCompiledTransform xslTemplate = Xslt.LoadXsltFromSPFile(templateFile);


                // 2 - Query Associados activos com acesso à área das dívidas hospitalares na Extranet

                SPList entidadesList = entidadesWeb.Lists.GetList(HlpIntranet.Entidades.Lists.Entidades.Nome);

                int year = DateTime.Now.Year;

                StringBuilder caml = new StringBuilder();
                caml.Append("<Where>");
                caml.Append("<And>");
                caml.Append("<IsNotNull><FieldRef Name='AF_UtilizadoresExtranetDividasH' /></IsNotNull>");
                caml.Append("<And>");
                caml.AppendFormat("<Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq>", SPBuiltInFieldId.ContentType, entidadesList.ContentTypes.GetContentType(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Nome).Name);
                caml.Append("<And>");
                caml.AppendFormat("<Or><Leq><FieldRef Name='{0}' /><Value Type='DateTime'>{1}</Value></Leq><IsNull><FieldRef Name='{0}' /></IsNull></Or>", HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.DataInscricao, SPUtility.CreateISO8601DateTimeFromSystemDateTime(new DateTime(year, 12, 31, 23, 59, 59)));
                caml.AppendFormat("<Or><Geq><FieldRef Name='{0}' /><Value Type='DateTime'>{1}</Value></Geq><IsNull><FieldRef Name='{0}' /></IsNull></Or>", HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Associado.Fields.DataCancelamento, SPUtility.CreateISO8601DateTimeFromSystemDateTime(new DateTime(year, 1, 1, 0, 0, 0)));
                caml.Append("</And>");
                caml.Append("</And>");
                caml.Append("</And>");
                caml.Append("</Where><OrderBy><FieldRef Name='Title' Ascending='True' /></OrderBy>");

                SPQuery query = new SPQuery();
                query.Query = caml.ToString();

                SPListItemCollection items = entidadesList.GetItems(query);

                var associados = from x in items.Cast<SPListItem>()
                                 select new
                                 {
                                     Nome = x.Title,
                                     Email = x.GetTypedValue<string>(HlpIntranet.Entidades.Lists.Entidades.ContentTypes.Entidade.Fields.EmailDividasHosp)
                                 };

                XElement notification = new XElement("NotificacaoInquerito",
                new XAttribute("Assunto", "APIFARMA - Inquérito Dívidas Hospitalares"),
                new XAttribute("DataInquerito", dataInquerito),
                new XAttribute("DataLimite", dataLimite.ToShortDateString()));

                XDocument xmlDoc = new XDocument(notification);

                string emailBody = Xslt.XmlTransform(xmlDoc, xslTemplate);

                SmtpClient smtp = new SmtpClient();

                smtp.Host = site.WebApplication.OutboundMailServiceInstance.Server.Name;

                //TODO: Remove google specifications for test purposes.
                //smtp.Host = "smtp.gmail.com";
                //smtp.EnableSsl = true;
                //smtp.Port = 587;
                //smtp.Credentials = new System.Net.NetworkCredential("goncalo.boleo.convex@gmail.com", "Cvx2009!");

                MailMessage message = new MailMessage();
                message.From = new MailAddress(fromEmail);
                message.Subject = tituloEmail;

                var associadosValidos = associados.Where(x => !String.IsNullOrEmpty(x.Email));

                LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                                    System.Diagnostics.TraceEventType.Information, "Notificação Inquerito",
                                    String.Format("A notificar {0} Associados..", associadosValidos.Count()));

                foreach (var associado in associadosValidos)
                {
                    string[] splitted = associado.Email.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string s in splitted)
                    {
                        string lower = s.Trim().ToLower();

                        try
                        {
                            //Commented for test purposes RSILVA
                            //message.Bcc.Add(lower);

                            //Just Testing
                            message.Bcc.Add("hrodrigosilva@gmail.com");

                            LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                                System.Diagnostics.TraceEventType.Information, "Notificação Inquerito",
                                String.Format("Associado: '{0}' Email: '{1}'", associado.Nome, associado.Email));
                        }
                        catch (Exception ex)
                        {
                            LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                                System.Diagnostics.TraceEventType.Warning, "Notificação Inquerito",
                                String.Format("Email inválido - Associado: '{0}' Email: '{1}'. Erro: {2}", associado.Nome, associado.Email, ex.Message));
                        }
                    }
                }

                // Added by gboleo, 2012-03-13 - Intranet ID78
                string[] splittedBcc = bccEmail.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in splittedBcc)
                {
                    string lower = s.Trim().ToLower();

                    try
                    {
                        //Commented for test purposes RSILVA
                        //message.Bcc.Add(lower);

                        //Just Testing
                        message.Bcc.Add("rodrigo.silva@lisboncg.com");
                    }
                    catch (Exception ex)
                    {
                        LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                            System.Diagnostics.TraceEventType.Warning, "Notificação Inquerito",
                            String.Format("Email inválido - Associado: '{0}' Email: '{1}'. Erro: {2}", "n/a", lower, ex.Message));
                    }
                }
                // Added by gboleo, 2012-03-13 - Intranet ID78

                // Removed by gboleo, 2012-03-13 - Intranet ID78
                // Deixou de fazer sentido este controlo com a introdução das notificações extra configuraveis via backoffice
                //message.Bcc.Add("daniel.simoes@convex.pt");
                // Removed by gboleo, 2012-03-13 - Intranet ID78
                message.CC.Add("hugo.rodrigues@lisboncg.com");

                message.AlternateViews.Add(CreateImagesAlternateView(emailBody, dividasWeb));

                message.Body = emailBody;
                message.IsBodyHtml = true;

                smtp.Send(message);
            }
        }
        private static AlternateView CreateImagesAlternateView(string body, SPWeb web)
        {
            Dictionary<string, LinkedResource> imgResourcesHash = new Dictionary<string, LinkedResource>();
            //List<LinkedResource> imgResources = new List<LinkedResource>();

            int imgId = 0;
            MatchCollection imgTags = Regex.Matches(body, "(?<=src=\")[^\"]+");
            foreach (Match imgTag in imgTags)
            {
                string accessUrl = HttpUtility.HtmlDecode(imgTag.Value);
                try
                {
                    if (imgResourcesHash.ContainsKey(imgTag.Value))
                        continue;
                    imgId++;
                    body = body.Replace(imgTag.Value, string.Format("cid:image{0}", imgId));

                    System.Net.WebClient x = new System.Net.WebClient();

                    if (!accessUrl.ToLower().Contains("http://"))
                    {
                        accessUrl = SPUrlUtility.CombineUrl(web.Url, accessUrl);
                    }

                    x.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    byte[] imgContents = x.DownloadData(accessUrl);
                    MemoryStream ms = new MemoryStream(imgContents);
                    string mediaType;

                    if (imgTag.Value.Contains(".gif"))
                        mediaType = MediaTypeNames.Image.Gif;
                    else
                        mediaType = MediaTypeNames.Image.Jpeg;

                    LinkedResource imgResource = new LinkedResource(ms, mediaType);
                    imgResource.ContentId = string.Format("image{0}", imgId);
                    imgResourcesHash.Add(imgTag.Value, imgResource);
                }
                catch (Exception ex)
                {
                    LogUtils.LogMessage(DivHospCategory.DivHosp_Jobs,
                                System.Diagnostics.TraceEventType.Warning, "Notificação Inquerito::CreateImagesAlternateView",
                                String.Format("Error Replacing Image '{0}' with '{1}': {2}", imgTag.Value, accessUrl, ex.Message));
                }

            }
            AlternateView view = AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, System.Net.Mime.MediaTypeNames.Text.Html);
            foreach (LinkedResource lr in imgResourcesHash.Values)
            {
                view.LinkedResources.Add(lr);
            }
            return view;
        }
    }
}

