﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>  
<%@ Register tagprefix="Webparts" namespace="Convex.Clientes.Apifarma.Internet.Webparts" assembly="Convex.Clientes.Apifarma.Internet.Webparts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d1e3b6c2eefc4c4f" %>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:webpartpageexpansion="full" meta:progid="SharePoint.WebPartPage.Document" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<div class="content_faq">

<div class="content_box_wide">
<div class="content_box_corner_faq_1"></div>
<div class="content_box_corner_faq_2"></div>
<div class="content_box_corner_3"></div>
<div class="content_box_corner_4"></div>
<div class="content_box_faq_title">Mapa do Site</div>
<div class="content_box_faq_text">
<Webparts:SiteMap AllowEdit="true" runat="server" PartImageLarge="" MissingAssembly="N&#227;o &#233; poss&#237;vel importar esta Pe&#231;a Web." IsIncluded="True" Description="Site Map" PartOrder="1" ExportMode="All" AllowConnect="False" IsVisible="True" FrameType="None" HelpLink="" IsIncludedFilter="" AllowZoneChange="True" ImportErrorMessage="N&#227;o &#233; poss&#237;vel importar esta Pe&#231;a Web." ID="SiteMap1" Title="SiteMap" ExportControlledProperties="True" SuppressWebPartChrome="False" Dir="Default" WebPart="true""></Webparts:SiteMap>


</div><!-- content_box_text end -->
</div><!-- content_box end -->

</div>

    
    <%--<SharePointWebControls:CssRegistration ID="CssRegistration3" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/pageLayouts.css %>" runat="server"/>
	
	<div class="content_top">
	     <p class="crumb">
                <asp:SiteMapPath ID="siteMapPath" Runat="server" SiteMapProvider="CurrentNavSiteMapProviderNoEncode"
                        RenderCurrentNodeAsLink="true" PathSeparatorStyle-CssClass="crumb_link" CurrentNodeStyle-CssClass="crumb_link"  NodeStyle-CssClass="crumb_link"/>
            </p>
	</div>
    <div class="content_area_padding">  
             <p class="content_title">
		    <SharePointWebControls:TextField ID="TextField1" FieldName="Title" runat="server"></SharePointWebControls:TextField>
		    </p>
            <PublishingWebControls:RichHtmlField id="Content" FieldName="PublishingPageContent" runat="server"/>
  			
    </div>--%>
     <%--<div class="content_box_bottom">
         <a class="content_back" href="javascript:window.history.back();">
			Voltar</a>
    </div>--%>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
</asp:Content>
