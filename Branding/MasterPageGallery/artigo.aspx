﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page AutoEventWireup="true" Language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CONVEX" Namespace="Convex.Sharepoint.Foundation.Webparts" Assembly="Convex.Sharepoint.Foundation.Webparts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=39eae81de3c872a1" %>

<asp:content id="PageHead" contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
<PublishingWebControls:editmodepanel runat="server" id="editmodestyles">
			<!-- Styles for edit mode only-->
			<SharePointWebControls:CssRegistration ID="CssRegistration1" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/zz2_editMode.css %>" runat="server"/>
	</PublishingWebControls:editmodepanel>
	<SharePointWebControls:CssRegistration ID="CssRegistration2" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/rca.css %>" runat="server"/>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:content>

<asp:content contentplaceholderid="PlaceHolderTitleBreadcrumb" runat="server">
	
</asp:content>

<asp:content id="Main" contentplaceholderid="PlaceHolderMain" runat="server">
<div class="content_box_wide">
		<div class="content_box_corner_1"></div>
		<div class="content_box_corner_2"></div>
		<div class="content_box_corner_3"></div>
		<div class="content_box_corner_4"></div>
		
		<div class="content_box_wide_title">
		    <CONVEX:Breadcrumb runat="server" ID="Breadcrumb1" __WebPartId="{EAAC731D-E6D5-42D8-9027-ACAFAD812123}"></CONVEX:Breadcrumb>
		</div>
		<div class="content_box_wide_text">
	<div class="list_thumbs_container">
<a class="title_thumbs" href="#">
  <SharePointWebControls:TextField ID="TextField1" FieldName="Title" runat="server"></SharePointWebControls:TextField>
<span class="date">  

<SharePointWebControls:DateTimeField ID="DateTimeField1" FieldName="ArticleStartDate" runat="server"></SharePointWebControls:DateTimeField></span>
</a>
<br />
<a href="http://www.google.com" style="float:left; display:block;">
<PublishingWebControls:RichImageField ID="RichImageField1" FieldName="PublishingPageImage" runat="server" CssClass="list_thumbs_img"></PublishingWebControls:RichImageField>
</a>
<PublishingWebControls:RichHtmlField FieldName="PublishingPageContent" runat="server" id="RichHtmlField1" CssClass="Text_Artigo">
</PublishingWebControls:RichHtmlField>
<br /><br />
<Webparts:Post runat="server" AllowEdit="True" AllowConnect="True" ConnectionID="00000000-0000-0000-0000-000000000000" Title="Post" IsIncluded="True" Dir="Default" IsVisible="True" AllowMinimize="True" ExportControlledProperties="True" ZoneID="ImportedPartZone" ID="Post1" FrameState="Normal" ExportMode="All" AllowHide="True" SuppressWebPartChrome="False" DetailLink="" ChromeType="None" HelpLink="" ImportErrorMessage="N&#227;o &#233; poss&#237;vel importar esta Pe&#231;a Web." MissingAssembly="N&#227;o &#233; poss&#237;vel importar esta Pe&#231;a Web." PartImageSmall="" AllowRemove="True" HelpMode="Modeless" FrameType="None" AllowZoneChange="True" PartOrder="1" Description="" PartImageLarge="" IsIncludedFilter="" __MarkupType="vsattributemarkup" __WebPartId="{5390efce-2c29-4532-955d-87499b3bc74b}" WebPart="true" Height="" Width=""></Webparts:Post>        
</div>
</div>
		
	
	</div>
</asp:content>

<asp:content id="PageTitle" contentplaceholderid="PlaceHolderPageTitle" runat="server">
<SharePointWebControls:FieldValue id="FieldValue1" FieldName="Title" runat="server"/>
</asp:content>

<asp:content id="PageTitleInTitleArea" contentplaceholderid="PlaceHolderPageTitleInTitleArea"
    runat="server">
<SharePointWebControls:TextField runat="server" id="TitleField" FieldName="Title"/>
</asp:content>
