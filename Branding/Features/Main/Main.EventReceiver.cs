using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Security;

namespace Branding.Features.Main
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("86915eaf-8639-421e-bedb-8a30e74be63c")]
    public class MainEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPSite siteCollection = properties.Feature.Parent as SPSite;
            if (siteCollection != null)
            {
                SPWeb topLevelSite = siteCollection.RootWeb;

                // Calculate relative path to site from Web Application root.
                string WebAppRelativePath = topLevelSite.ServerRelativeUrl;
                if (!WebAppRelativePath.EndsWith("/"))
                {
                    WebAppRelativePath += "/";
                }

                // Enumerate through each site and apply branding.
                foreach (SPWeb site in siteCollection.AllWebs)
                {
                    try
                    {
                        site.MasterUrl = WebAppRelativePath + "_catalogs/masterpage/Internet_V4.master";
                        site.CustomMasterUrl = WebAppRelativePath + "_catalogs/masterpage/Internet_V4.master";
                        //site.AlternateCssUrl = WebAppRelativePath + "Style%20Library/Apifarma/Internet_V2.css"; this is done on the masterpage itself
                        site.SiteLogoUrl = WebAppRelativePath + "Style%20Library/Apifarma/Images/logotype_bt.jpg";
                        site.UIVersion = 4;
                        site.Update();
                    }
                    // Dispose each SPWeb object
                    finally
                    {
                        if (site != null)
                        {
                            site.Dispose();
                        }
                    }
                }
            }
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            SPSite siteCollection = properties.Feature.Parent as SPSite;
            if (siteCollection != null)
            {
                SPWeb topLevelSite = siteCollection.RootWeb;

                // Calculate relative path of site from Web Application root.
                string WebAppRelativePath = topLevelSite.ServerRelativeUrl;
                if (!WebAppRelativePath.EndsWith("/"))
                {
                    WebAppRelativePath += "/";
                }

                // Enumerate through each site and remove custom branding.
                foreach (SPWeb site in siteCollection.AllWebs)
                {
                    try
                    {
                        site.MasterUrl = WebAppRelativePath + "_catalogs/masterpage/v4.master";
                        site.CustomMasterUrl = WebAppRelativePath + "_catalogs/masterpage/v4.master";
                        site.AlternateCssUrl = "";
                        site.SiteLogoUrl = "";
                        site.Update();
                    }
                    // Dispose each SPWeb object
                    finally
                    {
                        if (site != null)
                        {
                            site.Dispose();
                        }
                    }
                }
            }
        }


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
