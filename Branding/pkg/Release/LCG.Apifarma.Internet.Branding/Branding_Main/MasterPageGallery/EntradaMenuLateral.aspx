<%@ Assembly Name="Branding, Version=1.0.0.0, Culture=neutral, PublicKeyToken=cb20ac296e9949f8" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Convex" Namespace="Convex.Sharepoint.Foundation.Webparts" Assembly="Convex.Sharepoint.Foundation.Webparts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=39eae81de3c872a1" %>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:webpartpageexpansion="full" meta:progid="SharePoint.WebPartPage.Document" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
<PublishingWebControls:editmodepanel runat="server" id="editmodestyles">
	    <!-- Styles for edit mode only-->
	    <SharePointWebControls:CssRegistration ID="CssRegistration1" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/zz2_editMode.css %>" runat="server"/>
	</PublishingWebControls:editmodepanel>
	<SharePointWebControls:CssRegistration ID="CssRegistration2" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/rca.css %>" runat="server"/>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<!-- Start: Fixed left navigation -->
<div class="content_box_accordion_section">
    <div class="content_box_accordion_sharepoint_adjust">		  
	    <Convex:Navigation ID="cvxLeftNavigation" runat="server" NavigationType="Current" StartNode="Current" Depth="2" XsltFile="/Style%20Library/XSL%20Style%20Sheets/LeftNavigation.xslt" />
    </div>
</div>    
<!-- End: Fixed left navigation -->

<div class="content_box_sharepoint_adjust">            
    <div class="content_box">
	    <div class="content_box_corner_1"></div>
	    <div class="content_box_corner_2"></div>
	    <div class="content_box_corner_3"></div>
	    <div class="content_box_corner_4"></div>
		
        <!-- Start: Breadcrumb -->
		<div class="content_box_title">
		    <Convex:Breadcrumb ID="cvxBreadcrumb" runat="server" />
		</div>
        <!-- End: Breadcrumb -->
		
        <!-- Start: Content area (inside) -->
		<div class="content_box_text">
		    <PublishingWebControls:EditModePanel runat="server" id="EditModePanel1">
		        <SharePointWebControls:TextField ID="txtTitle" runat="server" FieldName="Title" ></SharePointWebControls:TextField>
		    </PublishingWebControls:EditModePanel>
            <PublishingWebControls:RichHtmlField id="txtContent" runat="server" FieldName="PublishingPageContent" /> 
            
			<WebPartPages:WebPartZone id="g_85B0A0A4F78945618F97C524E863C442" runat="server" title="Zona Interior"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone> 
		</div>
        <!-- End: Content area (inside) -->				
	</div>

</div>

<!-- Start: Content area (bottom) -->
<WebPartPages:WebPartZone id="g_86C212746E2F489FB6F141EF16F74BA3" runat="server" title="Zona Fundo"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
<!-- Start: Content area (bottom) -->
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
<SharePointWebControls:FieldValue id="FieldValue1" FieldName="Title" runat="server"/>
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
<SharePointWebControls:TextField runat="server" id="TitleField" FieldName="Title"/>
</asp:Content>
