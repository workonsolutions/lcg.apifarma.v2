<%@ Assembly Name="Branding, Version=1.0.0.0, Culture=neutral, PublicKeyToken=cb20ac296e9949f8" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls"
    Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation"
    Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="APIFARMA" Namespace="Convex.Clientes.Apifarma.Internet.Webparts"
    Assembly="Convex.Clientes.Apifarma.Internet.Webparts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d1e3b6c2eefc4c4f" %>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c"
    meta:progid="SharePoint.WebPartPage.Document" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<div class="content_box_sharepoint_adjust">
        <!-- -------------ACTUALIDADES------------- -->
        <WebPartPages:WebPartZone ID="WebPartZone1" runat="server" Title="WebPartZone1" FrameType="None"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
        <!-- -------------ACTUALIDADES------------- -->
    </div>
    <div class="content_space_11px">
    </div>
    <div class="content_box_info_section">
        <!-- -------------NEWS LETTER------------- --><!-- -------------PATROCINIOS LINKS------------- -->
        <div class="content_box_info_sharepoint_adjust">
            <WebPartPages:WebPartZone ID="WebPartZone5" runat="server" Title="WebPartZone5" FrameType="None"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
        </div>
    </div>
    <!-- content_box_info_section end -->
    <div class="content_box_dual">
        <div class="content_box_dual_corner_3">
        </div>
        <div class="content_box_dual_corner_4">
        </div>
        <div class="content_box_dual_corner_5">
        </div>
        <div class="content_box_dual_corner_6">
        </div>
        <div class="content_box_dual_container_sharepoint_adjust">
            <!-- -------------INICIATIVAS------------- -->
            <WebPartPages:WebPartZone PartStyle-CssClass="Pedro" ID="WebPartZone3" runat="server" Title="WebPartZone3" FrameType="None"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
            <!-- -------------INICIATIVAS------------- -->
        </div>
        <div class="content_space_11px">
        </div>
        <div class="content_box_dual_container_sharepoint_adjust">
            <!-- -------------INTERNACIONAL------------- -->
            <WebPartPages:WebPartZone ID="WebPartZone4" runat="server" Title="WebPartZone4" FrameType="None"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
            <!-- -------------INTERNACIONAL------------- -->
        </div>
    </div>
    <!-- content_box_dual end -->
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">

</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >

</asp:Content>
