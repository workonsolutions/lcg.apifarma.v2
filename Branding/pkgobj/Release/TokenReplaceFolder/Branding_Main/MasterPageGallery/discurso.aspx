<%@ Assembly Name="Branding, Version=1.0.0.0, Culture=neutral, PublicKeyToken=cb20ac296e9949f8" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
<PublishingWebControls:editmodepanel runat="server" id="editmodestyles">
			<!-- Styles for edit mode only-->
			<SharePointWebControls:CssRegistration ID="CssRegistration1" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/zz2_editMode.css %>" runat="server"/>
	</PublishingWebControls:editmodepanel>
	<SharePointWebControls:CssRegistration ID="CssRegistration2" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/rca.css %>" runat="server"/>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<div class="content_box_wide">
		<div class="content_box_corner_1"></div>
		<div class="content_box_corner_2"></div>
		<div class="content_box_corner_3"></div>
		<div class="content_box_corner_4"></div>
		
		<div class="content_box_wide_title">
		    <CONVEX:Breadcrumb runat="server" ID="Breadcrumb1" __WebPartId="{EAAC731D-E6D5-42D8-9027-ACAFAD812123}"></CONVEX:Breadcrumb>
		</div>
		<div class="content_box_wide_text">
			 <p class="title">
		    <SharePointWebControls:TextField ID="TextField1" FieldName="Title" runat="server"></SharePointWebControls:TextField><br />
		    <span class="date"> 
		        <SharePointWebControls:DateTimeField ID="DateTimeField1" FieldName="ArticleStartDate" runat="server"/>
		    </span>
		    </p>
		    <PublishingWebControls:EditModePanel runat="server" id="EditModePanel1"><PublishingWebControls:RichImageField ID="RichImageField1" FieldName="PublishingRollupImage" runat="server"></PublishingWebControls:RichImageField>
		        <SharePointWebControls:NoteField runat="server" ID="NoteField1" FieldName="Sinopse"></SharePointWebControls:NoteField>
		        <SharePointWebControls:RadioButtonChoiceField runat="server" ID="ChoiceField1" FieldName="Exibicao"></SharePointWebControls:RadioButtonChoiceField>
		    </PublishingWebControls:EditModePanel>
            <PublishingWebControls:RichHtmlField id="Content" FieldName="PublishingPageContent" runat="server"/> 
            
            <WebPartPages:WebPartZone ID="WebPartZone1" runat="server" Title="WebPartZone1" FrameType="None">
                    <ZoneTemplate>
                    </ZoneTemplate>
                </WebPartPages:WebPartZone>            
			<br /><br />
			<a class="bt_info" href="javascript:window.history.back();"><img class="bt_info_left" src="/_layouts/Apifarma/images/bt_info_left.png" alt="" /><img class="bt_info_right" src="/_layouts/Apifarma/images/bt_info_right.png" alt="" />Voltar <</a>
		</div>
		
		
	</div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
<SharePointWebControls:TextField runat="server" id="TitleField" FieldName="Title"/>
</asp:Content>
