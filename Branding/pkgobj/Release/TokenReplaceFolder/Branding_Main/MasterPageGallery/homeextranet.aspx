<%@ Assembly Name="Branding, Version=1.0.0.0, Culture=neutral, PublicKeyToken=cb20ac296e9949f8" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CONVEX" Namespace="Convex.Sharepoint.Foundation.Webparts"
    Assembly="Convex.Sharepoint.Foundation.Webparts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=39eae81de3c872a1" %>    
<%@ Register tagprefix="Webparts" namespace="Convex.Clientes.Apifarma.Internet.Webparts" assembly="Convex.Clientes.Apifarma.Internet.Webparts, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d1e3b6c2eefc4c4f" %>
<%@ Page Language="C#" AutoEventWireup="true"  DynamicMasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:webpartpageexpansion="full" meta:progid="SharePoint.WebPartPage.Document" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<SharePointWebControls:CssRegistration ID="CssRegistration3" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/pageLayouts.css %>" runat="server"/>
	
	<WebPartPages:SPProxyWebPartManager runat="server" id="ProxyWebPartManager"></WebPartPages:SPProxyWebPartManager>	
	
	<div class="content_box_accordion_section">
        <div class="content_box_accordion_sharepoint_adjust">
	<WebPartPages:WebPartZone id="g_E843C4004EAF414cA460382B0B851B4C" runat="server" AllowCustomization="true" AllowPersonalization="false" title="Zone Left Navigation">
		<ZoneTemplate>	
		</ZoneTemplate>
	</WebPartPages:WebPartZone> 
	
  </div>
    </div>     
    
     <div class="content_box_sharepoint_adjust">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2">
                     <WebPartPages:WebPartZone 
                        id="g_24B277125EE84f37A9B8575D7A66DB98"
                        AllowCustomization="true" 
                        AllowPersonalization="false" 
                        runat="server" title="Zona Topo"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top; padding-right: 5px; width: 50%">
                      <WebPartPages:WebPartZone 
                        id="g_BF5D11BBE87646bc8E2C7074CA9DC5A3"
                        AllowCustomization="true" 
                        AllowPersonalization="false" 
                        runat="server" title="Zona Esquerda"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
                </td>
                <td style="vertical-align: top;">
                     <WebPartPages:WebPartZone 
                        id="g_1AF4A56A55574d45812E9A248006879F"
                        AllowCustomization="true" 
                        AllowPersonalization="false" 
                        runat="server" title="Zona Direita"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <WebPartPages:WebPartZone 
                        id="g_69A3534866FD4f8888E062D90AD5C0F5"
                        AllowCustomization="true" 
                        AllowPersonalization="false" 
                        runat="server" title="Zona Fundo"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
                </td>
            </tr>
        </table>
        	</div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
</asp:Content>
