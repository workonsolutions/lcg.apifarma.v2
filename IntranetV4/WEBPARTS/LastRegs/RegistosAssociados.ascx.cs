﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Convex.Clientes.Apifarma.DivHosp.DAL;
using Convex.Clientes.Apifarma.Intranet.Helper;

using System.Linq;

namespace IntranetV4.WEBPARTS.LastRegs
{
    public partial class RegistosAssociados : System.Web.UI.UserControl
    {
        private string baseEntidadesUrl = null;
        protected string GetAssociadoIntranetUrl(object id)
        {
            if (baseEntidadesUrl == null)
            {
                baseEntidadesUrl = "http://spfte01apifarma.cloudapp.net:8080/entidades/lists/entidades/cvx/AssociadoDisplay.aspx";
                //baseEntidadesUrl = CvxUtility.CombineUrl(
                //    SPContext.Current.Site.Url,
                //    HlpIntranet.Entidades.WebUrl,
                //    HlpIntranet.Entidades.Lists.Entidades.Nome,
                //    "cvx/AssociadoDisplay.aspx");
            }
            return String.Format("{0}?ID={1}", baseEntidadesUrl, id);
        }
        public string NumeroRegistos { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (DividasHospitalaresEntities context = new DividasHospitalaresEntities())
                {
                    int numReg;
                    if (!Int32.TryParse(NumeroRegistos, out numReg))
                        numReg = 25;
                    var registos = (from r in context.RegistoSet.Include("Associado").Include("AreaNegocio")
                                    orderby r.Data descending
                                    select r).Take(numReg);

                    spGridREgistos.DataSource = registos;
                    spGridREgistos.DataBind();

                }
            }
        }
    }
}
