﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;

namespace IntranetV4.WEBPARTS.LastRegs
{
    [ToolboxItemAttribute(false)]
    public class LastRegs : Microsoft.SharePoint.WebPartPages.WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/IntranetV4.WEBPARTS/LastRegs/RegistosAssociados.ascx";

        #region Properties


        private string numeroRegistos;
        [Category("Campos")]
        [WebPartStorage(Storage.Personal)]
        [FriendlyNameAttribute("NumeroRegistos")]
        [Description("Número máximo de registos a apresentar")]
        [Personalizable]
        [WebBrowsable]
        public string NumeroRegistos
        {
            get
            {
                return numeroRegistos;
            }
            set
            {
                numeroRegistos = value;
            }
        }


        #endregion Properties


        public LastRegs()
        {
            this.ExportMode = WebPartExportMode.All;
            this.ChromeType = PartChromeType.None;
        }

        //protected override void OnInit(EventArgs e)
        //{
        //    base.OnInit(e);

        //    //get the existing ScriptManager if it exists on the page
        //    ScriptManager _AjaxManager = ScriptManager.GetCurrent(this.Page);

        //    if (_AjaxManager == null)
        //    {
        //        //create new ScriptManager and EnablePartialRendering
        //        _AjaxManager = new ScriptManager();
        //        _AjaxManager.EnablePartialRendering = true;
        //        _AjaxManager.EnableScriptLocalization = true;

        //        // Fix problem with postbacks and form actions (DevDiv 55525)
        //        // Page.ClientScript.RegisterStartupScript(typeof(AJAXSmartPart), this.ID, "_spOriginalFormAction = document.forms[0].action;", true);

        //        //tag:"form" att:"onsubmit" val:"return _spFormOnSubmitWrapper()" blocks async postbacks after the first one
        //        //not calling "_spFormOnSubmitWrapper()" breaks all postbacks
        //        //returning true all the time, somewhat defeats the purpose of the _spFormOnSubmitWrapper() which is to block repetitive postbacks, but it allows MS AJAX Extensions to work properly
        //        //its a hack that hopefully has minimal effect
        //        if (this.Page.Form != null)
        //        {
        //            //string formOnSubmitAtt = this.Page.Form.Attributes["onsubmit"];
        //            //if (!string.IsNullOrEmpty(formOnSubmitAtt) && formOnSubmitAtt == "return _spFormOnSubmitWrapper();")
        //            //{
        //            //    this.Page.Form.Attributes["onsubmit"] = "_spFormOnSubmitWrapper();";
        //            //}

        //            //add the ScriptManager as the first control in the Page.Form
        //            //I don't think this actually matters, but I did it to be consistent with how you are supposed to place the ScriptManager when used declaritevly
        //            this.Page.Form.Controls.AddAt(0, _AjaxManager);
        //        }
        //    }
        //}


        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            try
            {


                RegistosAssociados _childControl = Page.LoadControl(_ascxPath) as RegistosAssociados;
                if (_childControl != null)
                {
                    _childControl.NumeroRegistos = NumeroRegistos;
                    //_childControl.Web = Web;
                    Controls.Add(_childControl);
                }
            }
            catch (Exception ex)
            {
                //Display any error that comes up
                this.Controls.Clear();
                this.Controls.Add(new LiteralControl("An error occurred1: " + ex.Message.ToString()));
            }
        }


        /*StringBuilder renderLista = new StringBuilder();
        /// <summary>
        /// Create all your controls here for rendering.
        /// Try to avoid using the RenderWebPart() method.
        /// </summary>
        protected override void CreateChildControls()
        {
          
            if (SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                LoadControls();
            }
            else
            {
                renderLista.Append("<div class='clearfix faq'>");
                renderLista.Append("<div class='tituloFaq'><a href='#'>Pergunta</a>");
                renderLista.Append("</div><div class='txtFaq'>Resposta");
                renderLista.Append("</div></div>");
            }
        }

        private SPListItemCollection GetAllCAtegories(SPList list)
        {
            SPQuery query = new SPQuery();
            query.Query = "<OrderBy><FieldRef Name='CatOrder' /></OrderBy>";
            return list.GetItems(query);
        }
        private SPListItemCollection GetAllFaqs(SPList list)
        {
            SPQuery query = new SPQuery();
            query.Query = "<OrderBy><FieldRef Name='Category' /><FieldRef Name='FaqOrder' /></OrderBy>";
            return list.GetItems(query);
        }
         * */
        /* protected void LoadControls()
         {
             SPWeb myWeb = null;
             bool dispose = false;
             try
             {
               
                 if (String.IsNullOrEmpty(Web))
                     myWeb = SPContext.Current.Web;
                 else
                 {
                     if (SPContext.Current.Site.RootWeb.Webs[Web].Exists)
                     {
                         myWeb = SPContext.Current.Site.RootWeb.Webs[Web];
                         dispose = true;
                     }
                     else
                     {
                         renderLista.Append("Web " + Web + " não foi encontrada");
                         return;
                     }
                 }

                 SPList listaHistENews = Utils.VerificaLista(myWeb.Lists, NomeBiblioteca);


                 if (listaHistENews != null)
                 {
                     if (listaHistENews.ItemCount == 0)
                     {
                     }
                     else
                     {
                       
                         var enews = from item in listaHistENews.Items.Cast<SPListItem>()
                                     group item by item.GetTypedValue<string>("Tipo ENews") into t1
                                     select new
                                     {
                                         TipoEnews = t1.Key,
                                         Anos = from n in t1
                                                group n by n.GetTypedValue<DateTime>("Data Envio").Year into t2
                                                orderby t2.Key descending
                                                select new
                                                {
                                                    Ano = t2.Key,
                                                    Meses = from h in t2
                                                            group h by h.GetTypedValue<DateTime>("Data Envio").Month into t3
                                                            orderby t3.Key ascending
                                                            select new
                                                            {
                                                                Mes = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames[t3.Key-1],
                                                                ENews = from k in t3
                                                                        orderby k.GetTypedValue<DateTime>("Data Envio")
                                                                        select new {
                                                                            TipoENews = k.GetTypedValue<string>("Tipo ENews"),
                                                                            Titulo = k.Title,
                                                                            Url = k.File.Url,
                                                                            DataEnvio = k.GetTypedValue<DateTime>("Data Envio")
                                                                        }
                                                               
                                                            }
                                                }
                                     };

                       
                         foreach (var en in enews)
                         {
                             renderLista.AppendLine("<ul>");
                             renderLista.AppendLine("<li>");
                             renderLista.AppendFormat("<span class='histenews_tipo'>" + en.TipoEnews + "</span>");

                             foreach (var a in en.Anos)
                             {
                                 //renderLista.AppendLine("<ul>");
                                 //renderLista.AppendLine("<li>");
                                 //renderLista.AppendFormat("<span class='histenews_ano'>" + a.Ano + "</span>");                                
                               
                                 foreach (var m in a.Meses)
                                 {
                                     renderLista.AppendLine("<ul>");
                                     renderLista.AppendLine("<li>");
                                     renderLista.AppendFormat("<span class='histenews_mes'>{0}/{1}</span>", m.Mes, a.Ano);                                
                               
                                     foreach (var e in m.ENews)
                                     {
                                         renderLista.AppendLine("<ul>");
                                         renderLista.AppendLine("<li>");
                                         renderLista.AppendFormat("<a class='histenews_enews' target='_blank' href='{1}' alt='{0}'>{0} <span class='content_title_data'>{2}</span></a>",
                                            e.Titulo, SPUrlUtility.CombineUrl(listaHistENews.ParentWebUrl, e.Url), e.DataEnvio.ToShortDateString());
                                         renderLista.AppendLine("</li>");
                                         renderLista.AppendLine("</ul>");
                                     }
                                     renderLista.AppendLine("</li>");
                                     renderLista.AppendLine("</ul>");
                                 }
                                 //renderLista.AppendLine("</li>");
                                 //renderLista.AppendLine("</ul>");
                             }
                             renderLista.AppendLine("</li>");
                             renderLista.AppendLine("</ul>");
                         }
                        
                        
                     }

                  

                
                

                 }

             }
             catch (SPException es)
             {
                 renderLista.Append(es.Message);
                 renderLista.Append("</br>");
                 renderLista.Append(es.Source);
                 renderLista.Append("</br>");
             }
             finally
             {
                 if (myWeb != null && dispose)
                     myWeb.Dispose();
             }
         }



         protected override void Render(HtmlTextWriter writer)
         {
             writer.Write(renderLista);

         }
         */
    }
}
