<%@ Assembly Name="IntranetV4, Version=1.0.0.0, Culture=neutral, PublicKeyToken=b8725ae577edae68" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegistosAssociados.ascx.cs" Inherits="IntranetV4.WEBPARTS.LastRegs.RegistosAssociados" %>

<SharePoint:SPGridView Width="" runat="server" PagerStyle-CssClass="ms-formbody" ID="spGridREgistos" AllowSorting="False" AllowPaging="False" AutoGenerateColumns="False" border="0" BorderStyle="None" CssClass="ms-listviewtable" DataBoundHasBeenRaised="False" EmptyDataText="Não existem resultados a mostrar." >
<SelectedRowStyle CssClass="ms-selectednav" Font-Bold="True"></SelectedRowStyle>
<AlternatingRowStyle CssClass="ms-alternating"></AlternatingRowStyle>
     <Columns>
        <asp:TemplateField HeaderStyle-Width="100px" HeaderText="Associado" SortExpression="it.Associado.Nome">
            <ItemTemplate>
                <a href='<%# GetAssociadoIntranetUrl(DataBinder.Eval(Container.DataItem, "Associado.IdIntranet")) %>' title="Abrir ficha do Associado" target='_blank'><%# DataBinder.Eval(Container.DataItem, "Associado.Numero") %> - <%# DataBinder.Eval(Container.DataItem, "Associado.Nome") %></a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="DataInquerito" DataFormatString="{0:MMMM/yyyy}" HeaderText="Data Inquérito" SortExpression="DataInquerito" />
        <asp:BoundField DataField="Data" HeaderText="Data Carregamento" SortExpression="Data" />
        <asp:TemplateField HeaderText="Área Negócio" SortExpression="it.AreaNegocio.Nome">
            <ItemTemplate>
                <%# DataBinder.Eval(Container.DataItem, "AreaNegocio.Nome") %>
            </ItemTemplate>
        </asp:TemplateField>
        
    </Columns>
</SharePoint:SPGridView>
