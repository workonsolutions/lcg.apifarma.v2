﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Utilities;
using System.Web.UI;

namespace IntranetV4
{
    public static class CvxUtility
    {
        public static string CombineUrl(params string[] urlPaths)
        {
            string url = "";

            if (urlPaths.Length > 0)
            {
                url = urlPaths[0];
                for (int i = 1; i < urlPaths.Length; i++)
                {
                    url = SPUrlUtility.CombineUrl(url, urlPaths[i]);
                }
            }

            return url;
        }

        public static void DisableToolbarButtons(Control control, Type[] disableTypes)
        {
            foreach (Control iCtrl in control.Controls)
            {
                bool visible = true;
                foreach (Type t in disableTypes)
                {
                    if (iCtrl.GetType() == t)
                    {
                        visible = false;
                        break;
                    }
                }
                iCtrl.Visible = visible;

                if (visible) { DisableToolbarButtons(iCtrl, disableTypes); }
            }
        }
    }
}

