﻿<%@ Page Language="C#" Inherits="Convex.Clientes.Apifarma.Intranet.Entidades.PageLayouts.AssociadoDisplay, Convex.Clientes.Apifarma.Intranet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d1e3b6c2eefc4c4f" MasterPageFile="~masterurl/default.master" %>


<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    <SharePoint:ListFormPageTitle ID="ListFormPageTitle1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">    
    <SharePoint:ListItemProperty ID="lipContentType" MaxLength="40" runat="server" Property="ContentType" />:
    <SharePoint:ListItemProperty ID="ID_ItemProperty" MaxLength="40" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderPageImage" runat="server">
    <img src="/_layouts/images/blank.gif" width="1" height="1" alt="">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PlaceHolderLeftNavBar" runat="server" />
<asp:Content ID="phMain" runat="server" ContentPlaceHolderID="PlaceHolderMain">
    <ajaxToolkit:ToolkitScriptManager ID="scrManagerToolkit" EnablePartialRendering="true" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table border="0" width="100%">
                    <%-- top buttons --%>
                    <tr>
                        <td class="ms-toolbar" nowrap="nowrap">
                            <table>
                                <tr>
                                    <td width="99%" class="ms-toolbar" nowrap="nowrap">
                                        <img src="/_layouts/images/blank.gif" width="1" height="18" />
                                    </td>
                                    <td class="ms-toolbar" nowrap="nowrap">
                                        <SharePoint:SaveButton runat="server" ControlMode="Display" ID="savebutton1" />
                                    </td>
                                    <td class="ms-separator">
                                    </td>
                                    <td class="ms-toolbar" nowrap="nowrap" align="right">
                                        <SharePoint:GoBackButton runat="server" ControlMode="Display" ID="gobackbutton1" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%-- form toolbar --%>
                    <tr>
                        <td class="ms-toolbar" nowrap="nowrap">
                            <SharePoint:FormToolBar ID="formToolBar1" runat="server" ControlMode="Display" />
                        </td>
                    </tr>
                    <%-- fields --%>
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top" class="ms-formlabel" style="width: 210px;">
                                        <h3 class="ms-standardheader">
                                            <nobr><SharePoint:FieldLabel ID="lblNumeroAssociado" runat="server" ControlMode="Display" FieldName="AF_NumeroAssociado" /> </nobr>
                                        </h3>
                                    </td>
                                    <td valign="top" class="ms-formbody">
                                        <SharePoint:FormField ID="ffNumeroAssociado" runat="server" ControlMode="Display" FieldName="AF_NumeroAssociado" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="ms-formlabel">
                                        <h3 class="ms-standardheader">
                                            <nobr><SharePoint:FieldLabel ID="lblTitle" runat="server" ControlMode="Display" FieldName="Title" /> </nobr>
                                        </h3>
                                    </td>
                                    <td valign="top" class="ms-formbody" colspan="99">
                                        <SharePoint:FormField ID="ffTitle" runat="server" ControlMode="Display" FieldName="Title" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="ms-formlabel">
                                        <h3 class="ms-standardheader">
                                            <nobr><SharePoint:FieldLabel ID="lblGrupoEconomico" runat="server" ControlMode="Display" FieldName="AF_GrupoEconomico" /> </nobr>
                                        </h3>
                                    </td>
                                    <td valign="top" class="ms-formbody" colspan="99">
                                        <SharePoint:FormField ID="ffGrupoEconomico" runat="server" ControlMode="Display" FieldName="AF_GrupoEconomico" />                                         
                                        <asp:Panel ID="associadosGrupoPanel" runat="server" Visible="false">
                                            <h3 class="ms-standardheader" style="padding-top: 8px">
                                                <nobr>Outros Associados: <img id="imgAssociadosGrupo" src="/_layouts/images/plus.GIF" alt="Expandir/Colapsar" style="cursor:pointer;" onclick="ExpandCollapse('divAssociadosGrupo', 'imgAssociadosGrupo');" /></nobr>
                                            </h3>
                                      
                                            <div id="divAssociadosGrupo" style="display:none;">
                                                <SharePoint:SPGridView ID="grdAssociadosGrupo" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <%--<asp:Panel ID="associadosGrupoPanel" runat="server" Visible="false">
                                    <tr>
                                        <td valign="top" class="ms-formlabel">
                                            <h3 class="ms-standardheader">
                                                <nobr>Grupo Económico: Associados <img id="imgAssociadosGrupo" src="/_layouts/images/plus.GIF" alt="Expandir/Colapsar" style="cursor:pointer;" onclick="ExpandCollapse('divAssociadosGrupo', 'imgAssociadosGrupo');" /></nobr>
                                            </h3>
                                        </td>
                                        <td valign="top" class="ms-formbody">
                                             <div id="divAssociadosGrupo" style="display:none;">
                                                <SharePoint:SPGridView ID="grdAssociadosGrupo" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                             </div>
                                        </td>                                                
                                    </tr>
                                </asp:Panel>--%>
                                <tr>
                                    <td valign="top" class="ms-formlabel">
                                        <h3 class="ms-standardheader">
                                            <nobr><SharePoint:FieldLabel ID="lblDataInscricao" runat="server" ControlMode="Display" FieldName="AF_DataInscricao" /></nobr>
                                        </h3>
                                    </td>
                                    <td valign="top" class="ms-formbody">
                                        <SharePoint:FormField ID="ffDataInscricao" runat="server" ControlMode="Display" FieldName="AF_DataInscricao" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="ms-formlabel">
                                        <h3 class="ms-standardheader">
                                            <nobr><SharePoint:FieldLabel ID="lblDataSuspensao" runat="server" ControlMode="Display" FieldName="AF_DataSuspensao" /></nobr>
                                        </h3>
                                    </td>
                                    <td valign="top" class="ms-formbody">
                                        <SharePoint:FormField ID="ffDataSuspensao" runat="server" ControlMode="Display" FieldName="AF_DataSuspensao" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="ms-formlabel">
                                        <h3 class="ms-standardheader">
                                            <nobr><SharePoint:FieldLabel ID="lblDataCancelamento" runat="server" ControlMode="Display" FieldName="AF_DataCancelamento" /></nobr>
                                        </h3>
                                    </td>
                                    <td valign="top" class="ms-formbody">
                                        <SharePoint:FormField ID="ffDataCancelamento" runat="server" ControlMode="Display" FieldName="AF_DataCancelamento" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table border="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top" class="ms-formlabel" style="width: 210px;">
                                        <h3 class="ms-standardheader">
                                            <nobr><SharePoint:FieldLabel ID="lblCaracterizacaoActividade" runat="server" ControlMode="Display" FieldName="AF_CaracterizacaoActividade" /></nobr>
                                        </h3>
                                    </td>
                                    <td valign="top" class="ms-formbody">
                                        <SharePoint:FormField ID="ffCaracterizacaoActividade" runat="server" ControlMode="Display" FieldName="AF_CaracterizacaoActividade" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="ms-formlabel">
                                        <h3 class="ms-standardheader">
                                            <nobr>Áreas de Negócio</nobr>
                                        </h3>
                                    </td>
                                    <td valign="top" class="ms-formbody">
                                        <SharePoint:SPGridView ID="grdAreasNegocio" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <ajaxToolkit:TabContainer ID="tabContainer" runat="server" CssClass="gray" style="padding-top: 7px;" ActiveTabIndex="1">
                                <ajaxToolkit:TabPanel ID="tabPanelContactos" runat="server" HeaderText="Contactos">
                                    <contenttemplate>
                                        <table border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top" class="ms-formlabel" style="width: 210px;">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblMorada1" runat="server" ControlMode="Display" FieldName="AF_Morada1" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffMorada1" runat="server" ControlMode="Display" FieldName="AF_Morada1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblCodigoPostal" runat="server" ControlMode="Display" FieldName="AF_CodigoPostal" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffCodigoPostal" runat="server" ControlMode="Display" FieldName="AF_CodigoPostal" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblPais" runat="server" ControlMode="Display" FieldName="AF_Pais" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffPais" runat="server" ControlMode="Display" FieldName="AF_Pais" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblTelefone" runat="server" ControlMode="Display" FieldName="AF_Telefone" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffTelefone" runat="server" ControlMode="Display" FieldName="AF_Telefone" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblFax" runat="server" ControlMode="Display" FieldName="AF_Fax" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffFax" runat="server" ControlMode="Display" FieldName="AF_Fax" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblEmail" runat="server" ControlMode="Display" FieldName="AF_Email" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffEmail" runat="server" ControlMode="Display" FieldName="AF_Email" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblWebsite" runat="server" ControlMode="Display" FieldName="AF_Website" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffWebsite" runat="server" ControlMode="Display" FieldName="AF_Website" />
                                                </td>
                                            </tr>
                                        </table>
                                    </contenttemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="tabPanelColaboradores" runat="server" HeaderText="Colaboradores">
                                    <contenttemplate>
                                        <table border="0" cellspacing="0" width="100%">                                        
                                            <tr>
                                                <td valign="top" class="ms-formlabel" style="width: 210px;">
                                                    <h3 class="ms-standardheader">
                                                        <nobr>Activos</nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <div id="divContactosToolbar" runat="server" />
                                                    <SharePoint:SPGridView ID="grdContactosActivos" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                                </td>                                                
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr>Inactivos <img id="imgContactosInactivos" src="/_layouts/images/plus.GIF" alt="Expandir/Colapsar" style="cursor:pointer;" onclick="ExpandCollapse('divContactosInactivos', 'imgContactosInactivos');" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <div id="divContactosInactivos" style="display:none;">
                                                        <SharePoint:SPGridView ID="grdContactosInactivos" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                                    </div>
                                                </td>                                                
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr>Histórico <img id="imgHistoricoFuncionarios" src="/_layouts/images/plus.GIF" alt="Expandir/Colapsar" style="cursor:pointer;" onclick="ExpandCollapse('divHistoricoFuncionarios', 'imgHistoricoFuncionarios');" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">                                                    
                                                    <div id="divHistoricoFuncionarios" style="display:none;">
                                                        <div id="divFuncionariosToolbar" runat="server" />
                                                        <SharePoint:SPGridView ID="grdFuncionarios" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr>Representantes Legais (AG) <img id="imgRepresentantes" src="/_layouts/images/plus.GIF" alt="Expandir/Colapsar" style="cursor:pointer;" onclick="ExpandCollapse('divRepresentantes', 'imgRepresentantes');" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <div id="divRepresentantes" style="display:none;">
                                                        <div id="divRepresentantesToolbar" runat="server" />
                                                        <SharePoint:SPGridView ID="grdRepresentantes" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                                    </div>
                                                </td>
                                            </tr>                                        
                                        </table>
                                    </contenttemplate>
                                </ajaxToolkit:TabPanel>
                                   <ajaxToolkit:TabPanel ID="tabPanelFormandos" runat="server" HeaderText="Formandos">
                                    <contenttemplate>
                                        <table border="0" cellspacing="0" width="100%">                                        
                                            <tr>
                                                <td valign="top" class="ms-formlabel" style="width: 210px;">
                                                    <h3 class="ms-standardheader">
                                                        <nobr>Activos</nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <div id="divFormandosToolbar" runat="server" />
                                                    <SharePoint:SPDataSource id="dsFormandosActivos" runat="server" UseInternalName="true" DataSourceMode="List"></SharePoint:SPDataSource>
                                                    <SharePoint:SPGridView id="grdFormandosActivos" runat="server" AutoGenerateColumns="false" DataSourceID="dsFormandosActivos"></SharePoint:SPGridView>                                                                                                        
                                                </td>                                                
                                            </tr>  
                                            <tr>
                                                <td valign="top" class="ms-formlabel" style="width: 210px;">
                                                    <h3 class="ms-standardheader">
                                                        <nobr>Inactivos <img id="imgFormandosInactivos" src="/_layouts/images/plus.GIF" alt="Expandir/Colapsar" style="cursor:pointer;" onclick="ExpandCollapse('divFormandosInactivos', 'imgFormandosInactivos');" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <div id="divFormandosInactivos" style="display:none;">
                                                        <SharePoint:SPDataSource id="dsFormandosInactivos" runat="server" UseInternalName="true" DataSourceMode="List"></SharePoint:SPDataSource>
                                                        <SharePoint:SPGridView id="grdFormandosInactivos" runat="server" AutoGenerateColumns="false" DataSourceID="dsFormandosInactivos"></SharePoint:SPGridView>                                                                                                        
                                                    </div>
                                                </td>                                                
                                            </tr>                         
                                        </table>
                                    </contenttemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="tabPanelInfoJuridica" runat="server" HeaderText="Inf. Jurídica">
                                    <contenttemplate>
                                        <table border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top" class="ms-formlabel" style="width: 210px;">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblFormaJuridica" runat="server" ControlMode="Display" FieldName="AF_FormaJuridica" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffFormaJuridica" runat="server" ControlMode="Display" FieldName="AF_FormaJuridica" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblCapitalSocial" runat="server" ControlMode="Display" FieldName="AF_CapitalSocial" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffCapitalSocial" runat="server" ControlMode="Display" FieldName="AF_CapitalSocial" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblPaisCapitalSocial" runat="server" ControlMode="Display" FieldName="AF_PaisCapitalSocial" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffPaisCapitalSocial" runat="server" ControlMode="Display" FieldName="AF_PaisCapitalSocial" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblNif" runat="server" ControlMode="Display" FieldName="AF_Nif" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffNif" runat="server" ControlMode="Display" FieldName="AF_Nif" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblRegistoConservatoria" runat="server" ControlMode="Display" FieldName="AF_RegistoConservatoria" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffRegistoConservatoria" runat="server" ControlMode="Display" FieldName="AF_RegistoConservatoria" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblCae" runat="server" ControlMode="Display" FieldName="AF_Cae" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffCae" runat="server" ControlMode="Display" FieldName="AF_Cae" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblCae2" runat="server" ControlMode="Display" FieldName="AF_Cae2" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffCae2" runat="server" ControlMode="Display" FieldName="AF_Cae2" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblCae3" runat="server" ControlMode="Display" FieldName="AF_Cae3" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffCae3" runat="server" ControlMode="Display" FieldName="AF_Cae3" />
                                                </td>
                                            </tr>
                                        </table>
                                    </contenttemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="tabPanelContaCorrente" runat="server" HeaderText="Quotas/Conta Corrente">
                                    <contenttemplate>
                                        <table border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblEmail2" runat="server" ControlMode="Display" FieldName="AF_Email2" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffEmail2" runat="server" ControlMode="Display" FieldName="AF_Email2" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblOrdemCompra" runat="server" ControlMode="Display" FieldName="AF_OrdemCompra" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffOrdemCompra" runat="server" ControlMode="Display" FieldName="AF_OrdemCompra" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblNib" runat="server" ControlMode="Display" FieldName="AF_Nib" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffNib" runat="server" ControlMode="Display" FieldName="AF_Nib" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblDebitoDirecto" runat="server" ControlMode="Display" FieldName="AF_DebitoDirecto" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffDebitoDirecto" runat="server" ControlMode="Display" FieldName="AF_DebitoDirecto" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel" style="width: 210px;">
                                                    <h3 class="ms-standardheader">
                                                        <nobr>Volumes de Facturação</nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:SPGridView ID="grdDeclaracoesFacturacao" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr>Conta Corrente</nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:SPGridView ID="grdContaCorrente" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </contenttemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="tabPanelCorrespondencia" runat="server" HeaderText="Correspondência">
                                    <contenttemplate>
                                        <table border="0" cellspacing="0" width="100%">                                        
                                            <tr>
                                                <td valign="top" class="ms-formlabel" style="width: 210px;">
                                                    <h3 class="ms-standardheader">
                                                        <nobr>Recebida</nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:SPGridView ID="grdCorrespondenciaRecebida" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr>Enviada</nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:SPGridView ID="grdCorrespondenciaEnviada" runat="server" AutoGenerateColumns="false"></SharePoint:SPGridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblEtiquetaNomeEntidade" runat="server" ControlMode="Display" FieldName="AF_EtiquetaNomeEntidade" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffEtiquetaNomeEntidade" runat="server" ControlMode="Display" FieldName="AF_EtiquetaNomeEntidade" />
                                                </td>
                                            </tr>
                                        </table>
                                    </contenttemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="tabPanelExranet" runat="server" HeaderText="Extranet">
                                    <contenttemplate>
                                        <table border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top" class="ms-formlabel" style="width: 210px;">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblUtilizadoresExtranet" runat="server" ControlMode="Display" FieldName="AF_UtilizadoresExtranet" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffUtilizadoresExtranet" runat="server" ControlMode="Display" FieldName="AF_UtilizadoresExtranet" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblUtilizadoresExtranetContab" runat="server" ControlMode="Display" FieldName="AF_UtilizadoresExtranetContab" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffUtilizadoresExtranetContab" runat="server" ControlMode="Display" FieldName="AF_UtilizadoresExtranetContab" />
                                                </td>
                                            </tr>                                          
                                        </table>
                                    </contenttemplate>
                                </ajaxToolkit:TabPanel>     
                                  <ajaxToolkit:TabPanel ID="tabPanelDH" runat="server" HeaderText="Dívidas Hospitalares">
                                    <contenttemplate>
                                        <table border="0" cellspacing="0" width="100%">
                                             <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblUtilizadoresExtranetDividasH" runat="server" ControlMode="Display" FieldName="AF_UtilizadoresExtranetDividasH" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffUtilizadoresExtranetDividasH" runat="server" ControlMode="Display" FieldName="AF_UtilizadoresExtranetDividasH" />
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td valign="top" class="ms-formlabel">
                                                    <h3 class="ms-standardheader">
                                                        <nobr><SharePoint:FieldLabel ID="lblEmailDividasHosp" runat="server" ControlMode="Display" FieldName="AF_EmailDividasHosp" /></nobr>
                                                    </h3>
                                                </td>
                                                <td valign="top" class="ms-formbody">
                                                    <SharePoint:FormField ID="ffEmailDividasHosp" runat="server" ControlMode="Display" FieldName="AF_EmailDividasHosp" />
                                                </td>
                                            </tr>
                                        </table>
                                    </contenttemplate>
                                 </ajaxToolkit:TabPanel>                       
                            </ajaxToolkit:TabContainer>
                            <br />
                            <table border="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top" class="ms-formlabel" style="width: 210px;">
                                        <h3 class="ms-standardheader">
                                            <nobr><SharePoint:FieldLabel ID="lblObservacoes" runat="server" ControlMode="Display" FieldName="AF_Observacoes" /></nobr>
                                        </h3>
                                    </td>
                                    <td valign="top" class="ms-formbody">
                                        <SharePoint:FormField ID="ffObservacoes" runat="server" ControlMode="Display" FieldName="AF_Observacoes" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="ms-formlabel">
                                        <h3 class="ms-standardheader">
                                            <nobr><SharePoint:FieldLabel ID="lblExcluir" runat="server" ControlMode="Display" FieldName="AF_ExcluirContacto" /></nobr>
                                        </h3>
                                    </td>
                                    <td valign="top" class="ms-formbody">
                                        <SharePoint:FormField ID="ffExcluir" runat="server" ControlMode="Display" FieldName="AF_ExcluirContacto" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%-- bottom buttons --%>
                    <tr>
                        <td class="ms-toolbar" nowrap="nowrap">
                            <table>
                                <tr>
                                    <td class="ms-descriptiontext" nowrap="nowrap">
                                        <SharePoint:CreatedModifiedInfo ID="CreatedModifiedInfo1" ControlMode="Display" runat="server" />
                                    </td>
                                    <td width="99%" class="ms-toolbar" nowrap="nowrap">
                                        <img src="/_layouts/images/blank.gif" width="1" height="18" />
                                    </td>
                                    <td class="ms-toolbar" nowrap="nowrap">
                                        <SharePoint:SaveButton runat="server" ControlMode="Display" ID="savebutton2" />
                                    </td>
                                    <td class="ms-separator">
                                    </td>
                                    <td class="ms-toolbar" nowrap="nowrap" align="right">
                                        <SharePoint:GoBackButton runat="server" ControlMode="Display" ID="gobackbutton2" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
        function ExpandCollapse(objectId, imageId) {
            if (document.getElementById(objectId).style.display == "block") {
                document.getElementById(objectId).style.display = "none";
                document.getElementById(imageId).src = "/_layouts/images/plus.GIF";
            }
            else {
                document.getElementById(objectId).style.display = "block";
                document.getElementById(imageId).src = "/_layouts/images/minus.GIF";
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PlaceHolderTitleLeftBorder" runat="server">
    <table cellpadding="0" height="100%" width="100%" cellspacing="0">
        <tr>
            <td class="ms-areaseparatorleft">
                <img src="/_layouts/images/blank.gif" width="1" height="1" alt="">
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PlaceHolderTitleAreaClass" runat="server">

    <script id="onetidPageTitleAreaFrameScript">
        document.getElementById("onetidPageTitleAreaFrame").className = "ms-areaseparator";
    </script>

</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="PlaceHolderBodyAreaClass" runat="server">
    <style type="text/css">
        .ms-bodyareaframe
        {
            padding: 8px;
            border: none;
        }
        /* gboleo */
        .gray .ajax__tab_header
        {
            font-size: 11px;
            display: block;
        }
        .gray .ajax__tab_header .ajax__tab_outer
        {
            background: url(/_layouts/images/topnavunselected.gif);
            background-repeat: repeat-x;
            border-top: #c2dcff 1px solid;
            border-right: #c2dcff 1px solid;
            border-left: #c2dcff 1px solid;
            color: #3764a0;
            padding-left: 10px;
            margin-right: 3px;
        }
        .gray .ajax__tab_header .ajax__tab_inner
        {
            background: url(/_layouts/images/topnavunselected.gif);
            background-repeat: repeat-x;
            color: #3764a0;
            padding: 3px 10px 2px 0px;
            cursor: hand;
        }
        .gray .ajax__tab_hover .ajax__tab_inner
        {
            color: #000;
        }
        .gray .ajax__tab_active .ajax__tab_outer
        {
            background: url(/_layouts/images/topnavselected.gif);
            background-repeat: repeat-x;
            border-top: #79a7e3 1px solid;
            border-right: #79a7e3 1px solid;
            border-left: #79a7e3 1px solid;
            color: #000;
        }
        .gray .ajax__tab_active .ajax__tab_inner
        {
            background: url(/_layouts/images/topnavselected.gif);
            background-repeat: repeat-x;
            color: #000;
        }
        .gray .ajax__tab_body
        {
            font-size: 10pt;
            background-color: #fff;
            border-bottom: solid 1px #d7d7d7;
            border-top-width: 0;
        }
        /* /gboleo */
        </style>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="PlaceHolderBodyLeftBorder" runat="server">
    <div class='ms-areaseparatorleft'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt=""></div>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="PlaceHolderTitleRightMargin" runat="server">
    <div class='ms-areaseparatorright'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt=""></div>
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="PlaceHolderBodyRightMargin" runat="server">
    <div class='ms-areaseparatorright'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt=""></div>
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="PlaceHolderTitleAreaSeparator" runat="server" />
