﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntranetV4
{
    public enum IntranetLogCategory
    {
        Quotas_Job = 0,
        Quotas_NotificationJob = 1,
        Quotas_Notifications = 2,
        Quotas_Documents = 3,
        Quotas_Sendys = 4,
        Quotas_General = 5,
    }
}