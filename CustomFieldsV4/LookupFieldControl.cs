﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.WebControls;
using System.Web;
using Microsoft.SharePoint;
using System.Diagnostics;
using System.Web.UI;

namespace CustomFieldsV4.Sharepoint
{
    class LookupFieldControl : BaseFieldControl
    {
        private LookupField field;
        private ListBox listBox;

        public LookupFieldControl(LookupField parentField)
        {
            this.field = parentField;
            this.listBox = new ListBox();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        private static void SelectItem(ListBox lb, string value)
        {
            foreach (ListItem i in lb.Items)
            {
                if (i.Value == value)
                {
                    i.Selected = true;
                    return;
                }
            }
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            listBox = new ListBox();
            listBox.Width = new Unit("100%");
            listBox.Height = new Unit("250px");

            if (field.AllowMultipleValues)
                listBox.SelectionMode = ListSelectionMode.Multiple;



            string whiteSpaces = "    ".Replace(" ", HttpUtility.HtmlDecode("&nbsp;"));


            using (SPWeb web = SPContext.Current.Site.OpenWeb(field.LookupWebId))
            {
                #region Preencher ListBox
                SPList lookupList = web.Lists[new Guid(field.LookupList)];

                var rootItems = from i in lookupList.Items.Cast<SPListItem>()
                                orderby i.Title
                                where String.IsNullOrEmpty(i.GetFormattedValue("Pai"))
                                select i;

                foreach (var v in rootItems)
                {
                    listBox.Items.Add(new ListItem(v.GetFormattedValue(field.LookupField), v.ID.ToString()));

                    var childItems = from i in lookupList.Items.Cast<SPListItem>()
                                     orderby i.Title
                                     where i.GetFormattedValue("Pai") == v.Title
                                     select i;

                    foreach (var child in childItems)
                    {
                        listBox.Items.Add(new ListItem(whiteSpaces + child.GetFormattedValue(field.LookupField), child.ID.ToString()));
                    }

                }

                #endregion

                #region Actualizar com valor actual

                if (this.ItemFieldValue != null)
                {
                    if (field.AllowMultipleValues)
                    {
                        SPFieldLookupValueCollection lookups = (SPFieldLookupValueCollection)ItemFieldValue;
                        if (lookups.Count > 0)
                        {
                            foreach (SPFieldLookupValue l in lookups)
                            {
                                if (l.LookupId > 0)
                                {
                                    SelectItem(listBox, l.LookupId.ToString());
                                }
                            }
                        }
                    }
                    else
                    {
                        SPFieldLookupValue lookup = (SPFieldLookupValue)ItemFieldValue;

                        if (lookup.LookupId > 0)
                            listBox.SelectedValue = lookup.LookupId.ToString();
                    }


                }
                #endregion
            }
            //}
            this.Controls.Add(listBox);
        }

        public override void UpdateFieldValueInItem()
        {
            this.EnsureChildControls();

            try
            {
                if (this.listBox.SelectedItem != null)
                {
                    if (field.AllowMultipleValues)
                    {
                        SPFieldLookupValueCollection lookups = new SPFieldLookupValueCollection();

                        foreach (ListItem i in listBox.Items)
                        {
                            if (i.Selected)
                            {
                                lookups.Add(new SPFieldLookupValue(Int32.Parse(i.Value), String.Empty));
                            }
                        }
                        this.ItemFieldValue = lookups;
                    }
                    else
                    {
                        this.ItemFieldValue = new SPFieldLookupValue(Int32.Parse(listBox.SelectedValue), string.Empty);
                    }
                }
                else
                    this.ItemFieldValue = null;

            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message + ex.StackTrace, "LookupFieldControl");
                throw ex;
            }
        }

        protected override void Render(HtmlTextWriter output)
        {
            base.Render(output);
        }
    }
}


