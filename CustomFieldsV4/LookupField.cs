﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Security;
using System.Security.Permissions;

namespace CustomFieldsV4.Sharepoint
{
    class LookupField : SPFieldLookup
    {
        public LookupField(SPFieldCollection fields, string fieldName)
            : base(fields, fieldName)
        { }

        public LookupField(SPFieldCollection fields, string typeName, string displayName)
            : base(fields, typeName, displayName)
        { }

        //public override void OnAdded(SPAddFieldOptions op)
        //{
        //    base.OnAdded(op);
        //    XmlDocument schemaXmlDoc = new XmlDocument();
        //    schemaXmlDoc.LoadXml(base.SchemaXml);
        //    SetFieldAttribute(schemaXmlDoc, "Mult", "TRUE");
        //    base.SchemaXml = schemaXmlDoc.OuterXml;

        //}
        //private void SetFieldAttribute(XmlDocument xmlDocument, string name, string value)
        //{
        //    XmlElement documentElement = xmlDocument.DocumentElement;
        //    XmlAttribute attribute = documentElement.Attributes[name];
        //    if (attribute == null)
        //    {
        //        attribute = xmlDocument.CreateAttribute(name);
        //    }
        //    attribute.Value = value;
        //    documentElement.Attributes.Append(attribute);
        //}
        public override BaseFieldControl FieldRenderingControl
        {

            [SharePointPermission(SecurityAction.LinkDemand, ObjectModel = true)]
            get
            {
                LookupFieldControl ctr = new LookupFieldControl(this);
                ctr.FieldName = this.InternalName;
                return ctr;
            }
        }
    }
}