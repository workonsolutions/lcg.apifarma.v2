using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;

namespace RandomCodeMigration.Features.Feature1
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("3a00ad01-00d0-4cf7-a6ac-19a221334ed1")]
    public class Feature1EventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb web = (SPWeb)properties.Feature.Parent;
            string _DirLista = web.ServerRelativeUrl;
            _DirLista += (web.ServerRelativeUrl.Substring(web.ServerRelativeUrl.Length - 1, 1) == "/" ? "" : "/");
            _DirLista += "Lists/declaracaofacturacao";
            SPList listDeclaracaoFacturacao = web.GetList(_DirLista);
            //SPList listDeclaracaoFacturacao = web.GetList(web.ServerRelativeUrl + "/Lists/declaracaofacturacao");

            listDeclaracaoFacturacao.EventReceivers.Add(SPEventReceiverType.ItemAdded, this.GetType().Assembly.FullName, "RandomCodeMigration.ItemDeclaracaoFacturacao");
            listDeclaracaoFacturacao.EventReceivers.Add(SPEventReceiverType.ItemUpdated, this.GetType().Assembly.FullName, "RandomCodeMigration.ItemDeclaracaoFacturacao");
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
