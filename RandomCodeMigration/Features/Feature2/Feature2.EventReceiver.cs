using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;

namespace RandomCodeMigration.Features.Feature2
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("15b01f27-3823-4ba2-b093-773940d7cc4d")]
    public class Feature2EventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {

            //TODO:Para depois colocar no projecto de Settings
            //
            const int _Ano = 2013;
            const string _CampoAno = "Ano";

            using (SPWeb web = (SPWeb)properties.Feature.Parent)
            {
                try
                {


                    //Lista da DeclaracaFacturaca
                    string _DirLista = web.ServerRelativeUrl;
                    _DirLista += (web.ServerRelativeUrl.Substring(web.ServerRelativeUrl.Length - 1, 1) == "/" ? "" : "/");
                    _DirLista += "Lists/declaracaofacturacao";
                    SPList listDeclaracaoFacturacao = web.GetList(_DirLista);
                    foreach (SPListItem item in listDeclaracaoFacturacao.Items)
                    {
                        if (item[_CampoAno] != null)
                        {
                            int ValorDoCampoAno;
                            if (int.TryParse(item[_CampoAno].ToString(), out ValorDoCampoAno))
                            {
                                if (ValorDoCampoAno <= _Ano)
                                {
                                    item.ModerationInformation.Status = SPModerationStatusType.Approved;
                                    item.Update();
                                }
                            }

                        }
                    }
                    listDeclaracaoFacturacao.Update();
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }


        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
