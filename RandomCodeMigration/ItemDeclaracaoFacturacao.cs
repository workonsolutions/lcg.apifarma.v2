﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RandomCodeMigration
{
    public class ItemDeclaracaoFacturacao : SPItemEventReceiver
    {
        public override void ItemAdded(SPItemEventProperties properties)
        {
            this.EventFiringEnabled = false;
            SetNumeroAssociado(properties);
            this.EventFiringEnabled = true;
        }

        public override void ItemUpdated(SPItemEventProperties properties)
        {
            this.EventFiringEnabled = false;
            LogMessage("After 'this.EventFiringEnabled = false;'");
            SetNumeroAssociado(properties);
            LogMessage("After 'SetNumeroAssociado(properties);'");
            this.EventFiringEnabled = true;
            LogMessage("After 'this.EventFiringEnabled = true;'");
        }

        public void SetNumeroAssociado(SPItemEventProperties properties)
        {
            //SPListItem item = properties.ListItem;
            LogMessage("After 'SPListItem item = properties.ListItem;'");

            try
            {
                using (SPSite site = new SPSite(properties.SiteId))
                {
                    using (SPWeb web = site.OpenWeb("/entidades"))
                    {
                        site.AllowUnsafeUpdates = true;
                        
                        LogMessage("Inside 'try { using(...) { using(...) {'");
                        SPList listEntidades = web.Lists["Associados e Outras Entidades"];
                        LogMessage("After 'SPList listEntidades = web.Lists[\"Associados e Outras Entidades\"];'");

                        LogMessage("Before 'foreach(...)'");
                        foreach (SPListItem itemEntidades in listEntidades.Items)
                        {
                            if (itemEntidades.Title.ToString().Equals(new SPFieldLookupValue(properties.ListItem["Entidade"].ToString()).LookupValue))
                            {
                                LogMessage("Inside 'if (itemEntidades.Title.ToString().Equals(new SPFieldLookupValue(item[\"Entidade\"].ToString()).LookupValue))'");
                                properties.ListItem["Número de Associado"] = itemEntidades["Número de Associado"].ToString();
                                LogMessage("After 'item[\"Número de Associado\"] = itemEntidades[\"Número de Associado\"].ToString();'");
                                break;
                            }
                        }
                    }
                }

                base.ItemUpdated(properties);
                
                LogMessage("After 'item.Update();'");
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message + "\r\n" + ex.StackTrace + "\r\n" + ex.Source + "\r\n" + ex.InnerException.Message);
            }
        }

        private void LogMessage(string Message)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\DeclaracaoFacturacaoItemAdded.txt", true))
            {
                file.WriteLine(Message);
            }
        }
    }
}

