﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Convex.Clientes.Apifarma.Intranet;
using System.IO;
using System.Text;
using Microsoft.SharePoint.Utilities;
using Convex.OpenXml.Foundation;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Globalization;
using DocumentFormat.OpenXml;
using System.Configuration;
using Convex.Clientes.Apifarma.Intranet.Helper;
using Convex.Clientes.Apifarma.Intranet.Config;

namespace RandomCodeMigration.Layouts.RandomCodeMigration
{
    public partial class RptValoresDivida : System.Web.UI.Page
    {
        public class ReportData
        {
            public Convex.Clientes.Apifarma.Intranet.Helper.Associados.Associado Associado;
            public List<Convex.Clientes.Apifarma.Intranet.Helper.Quotas.ContaCorrente> Documentos = new List<Convex.Clientes.Apifarma.Intranet.Helper.Quotas.ContaCorrente>();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            btnPreview.Click += new EventHandler(btnPreview_Click);
            btnNotify.Click += new EventHandler(btnNotify_Click);

            dtDia.LocaleId = SPContext.Current.Web.Locale.LCID;
            dtDia.SelectedDate = DateTime.Today;

        }

        private static List<ReportData> GetReportData(DateTime date, CheckBox ChBx)
        {
            List<ReportData> reportData = new List<ReportData>();

            // Get all associados
            List<Convex.Clientes.Apifarma.Intranet.Helper.Associados.Associado> associados = new List<Convex.Clientes.Apifarma.Intranet.Helper.Associados.Associado>();
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPWeb web = SPContext.Current.Site.OpenWeb("entidades"))
                {
                    Convex.Clientes.Apifarma.Intranet.Helper.Associados a = new Convex.Clientes.Apifarma.Intranet.Helper.Associados(web);
                    associados = a.GetAssociados();
                }


                // Get documentos não pagos
                Convex.Clientes.Apifarma.Intranet.Helper.Quotas q = new Convex.Clientes.Apifarma.Intranet.Helper.Quotas();
                foreach (Convex.Clientes.Apifarma.Intranet.Helper.Associados.Associado associado in associados)
                {

                    double saldo = 0;
                    if (ChBx.Checked == false)
                        saldo = q.GetSaldoRptValDiv(associado.Id, date);
                    else
                        saldo = q.GetSaldoRptValDivComPagamentos(associado.Id, date);


                    if (saldo < 0)
                    {
                        List<Convex.Clientes.Apifarma.Intranet.Helper.Quotas.ContaCorrente> documentos = q.GetContaCorrente(associado.Id, date);

                        ReportData rd = new ReportData();
                        rd.Associado = associado;
                        rd.Associado.SaldoContaCorrente = saldo;

                        double saldoActual = Math.Abs(saldo);
                        foreach (Convex.Clientes.Apifarma.Intranet.Helper.Quotas.ContaCorrente documento in documentos)
                        {
                            if (saldoActual > 0)
                            {
                                if (documento.TipoDocumento == "AF_Joia" || documento.TipoDocumento == "AF_Quota" || documento.TipoDocumento == "AF_QuotaExtraordinaria" || documento.TipoDocumento == "AF_NotaDebito" || (documento.TipoDocumento == "AF_AcertoSaldo" && documento.Valor < 0))
                                {
                                    if (documento.DataVencimento < date)
                                    {
                                        rd.Documentos.Add(documento);
                                        saldoActual = Math.Round(saldoActual - documento.Valor, 4);
                                    }
                                }
                            }
                        }
                        reportData.Add(rd);
                    }
                }


            });
        

            return reportData;
            
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            List<ReportData> reportData = GetReportData(dtDia.IsValid ? dtDia.SelectedDate : DateTime.Today, CheckBoxPaymant);

            #region Create excel file from template and set data

            #region Create temp file from template

            SPFile file = SPContext.Current.Web.GetFile("Relatorios/Template_ValoresEmDivida.xlsx");
            Stream inputStream = file.OpenBinaryStream();

            string tempFileName = string.Format("/Temp/{0}.xlsx", Guid.NewGuid().ToString());
            string tempFullPathFileName = HttpContext.Current.Server.MapPath(tempFileName);
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (Stream outputStream = File.OpenWrite(tempFullPathFileName))
                {
                    byte[] buffer = new byte[8 * 1024];
                    int len;
                    while ((len = inputStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        outputStream.Write(buffer, 0, len);
                    }
                }

            #endregion

                #region Process data with OpenXml

                Spreadsheet excelFile = new Spreadsheet();
                excelFile.Open(tempFullPathFileName);

                Worksheet sheet = excelFile.GetWorksheet("Data");
                excelFile.SetCurrentWorksheet(sheet);

                int row = 2;
                CultureInfo culture = new CultureInfo("en-US");
                foreach (ReportData rd in reportData)
                {
                    double saldoActual = Math.Abs(rd.Associado.SaldoContaCorrente);

                    foreach (Convex.Clientes.Apifarma.Intranet.Helper.Quotas.ContaCorrente doc in rd.Documentos)
                    {
                        saldoActual = Math.Round(saldoActual - doc.Valor, 4);

                        double valorEmFalta = saldoActual >= 0 ? doc.Valor : Math.Round(doc.Valor - Math.Abs(saldoActual), 4);
                        string documento = doc.DataEmissao.ToString("yyyy-MM-dd") + ", " + doc.NomeDocumento + "-" + doc.Numero;

                        excelFile.UpdateCellValue("A" + row.ToString(), rd.Associado.Numero, CellValues.SharedString, 0);
                        excelFile.UpdateCellValue("B" + row.ToString(), rd.Associado.Nome, CellValues.SharedString, 0);
                        excelFile.UpdateCellValue("C" + row.ToString(), rd.Associado.Numero + "-" + rd.Associado.Nome, CellValues.SharedString, 0);
                        excelFile.UpdateCellValue("D" + row.ToString(), doc.NomeDocumento, CellValues.SharedString, 0);
                        excelFile.UpdateCellValue("E" + row.ToString(), doc.Numero, CellValues.SharedString, 0);
                        excelFile.UpdateCellValue("F" + row.ToString(), doc.DataEmissao.ToString(), CellValues.SharedString, 0);
                        excelFile.UpdateCellValue("G" + row.ToString(), documento, CellValues.SharedString, 0);
                        excelFile.UpdateCellValue("H" + row.ToString(), doc.Ano.ToString(), CellValues.Number, 0);
                        excelFile.UpdateCellValue("I" + row.ToString(), doc.Mes.ToString(), CellValues.Number, 0);
                        excelFile.UpdateCellValue("J" + row.ToString(), Math.Abs(doc.Valor).ToString("0.00", culture), CellValues.Number, 4);
                        excelFile.UpdateCellValue("K" + row.ToString(), Math.Abs(valorEmFalta).ToString("0.00", culture), CellValues.Number, 4);

                        row++;
                    }
                }
                row--;

                DocumentFormat.OpenXml.Spreadsheet.Table table = excelFile.GetTable(sheet, "DocumentosEmDivida");
                table.Reference = new StringValue("A1:K" + row.ToString());
                table.Save();

                excelFile.Close();

                #endregion

                // Set workaround url for postback problem
                string downloadUrl = SPUrlUtility.CombineUrl(SPContext.Current.Web.Url, "CvxCustomPages/RptDownload.aspx");
                iframeReport.Attributes["src"] = string.Format("{0}?friendlyname={1}&filename={2}&ctype={3}", downloadUrl, "ValoresEmDivida.xlsx", tempFileName, "application/vnd.ms-excel");

            #endregion
            });
        }
        private void InsertLogEntry(string msg)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                StreamWriter logs;
                logs = new StreamWriter("C:\\error.log");
                logs.WriteLine(msg);
                logs.Close();
            });
        }


        protected void btnNotify_Click(object sender, EventArgs e)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                List<ReportData> reportData = GetReportData(dtDia.IsValid ? dtDia.SelectedDate : DateTime.Today, CheckBoxPaymant);

                #region Process data to send emails

                CultureInfo culture = new CultureInfo("pt-PT");
                foreach (ReportData rd in reportData)
                {
                    double saldoActual = Math.Abs(rd.Associado.SaldoContaCorrente);

                    List<string[]> rows = new List<string[]>();
                    foreach (Convex.Clientes.Apifarma.Intranet.Helper.Quotas.ContaCorrente doc in rd.Documentos)
                    {
                        saldoActual = Math.Round(saldoActual - doc.Valor, 4);
                        double valorEmFalta = saldoActual >= 0 ? doc.Valor : Math.Round(doc.Valor - Math.Abs(saldoActual), 4);
                        string documento = doc.DataEmissao.ToString("yyyy-MM-dd") + ", " + doc.NomeDocumento + "-" + doc.Numero;

                        rows.Add(new string[]
                        {
                            doc.NomeDocumento,
                            doc.Numero,
                            doc.DataEmissao.ToString("dd-MM-yyyy"),
                            Math.Abs(doc.Valor).ToString("C2", culture),
                            Math.Abs(valorEmFalta).ToString("C2", culture)
                        });
                    }

                    string body = HlpMail.CreateMessageBody(
                                    "Apifarma.Intranet.Quotas.Reporting.RptValoresDivida",
                                    string.Format("<p><strong>Associado nº</strong>: {0}<br/><strong>Nome</strong>: {1}<br/><strong>Nº de contribuinte</strong>: {2}</p><p>Exmos. Senhores,<br/>Levamos ao vosso conhecimento que se encontra até ao momento em atraso o pagamento dos documentos indicados abaixo.</p>", rd.Associado.Numero, rd.Associado.Nome, rd.Associado.Nipc),
                                    "<p>Esperamos a rápida regularização dos documentos devidos.<br/>Se o pagamento se tiver cruzado com a nossa comunicação queiram considerar a presente sem efeito.</p><p>Na expectativa das vossas notícias, subscrevemo-nos,<br>Atentamente<br><i>Contabilidade</i></p>",
                                    new string[] { "Tipo de Documento", "Número", "Data de Emissão", "Valor do Documento", "Valor em Dívida" },
                                    new string[] { "TOTAL", "", "", "", Math.Abs(rd.Associado.SaldoContaCorrente).ToString("C2", culture) },
                                    rows
                                    );

                    if (!string.IsNullOrEmpty(rd.Associado.EmailNotificacaoContabilidade))
                    {
                        string bccAddresses = QuotasConfig.GetConfigValue(QuotasConfig.Notificacao.NotificationBccEmailsAviso);
                        try
                        {

                            if (!(ConfigurationManager.AppSettings["Environment"].ToUpper() == "PRODUCTION"))
                                HlpMail.SendMail(SPContext.Current.Site.WebApplication, ConfigurationManager.AppSettings["EmailTester"].ToLower(), ConfigurationManager.AppSettings["EmailDev"].ToLower(), string.Format("Apifarma | Aviso de Pagamento | Valor em dívida {0}", Math.Abs(rd.Associado.SaldoContaCorrente).ToString("C2", culture)), body);
                            else
                                HlpMail.SendMail(SPContext.Current.Site.WebApplication, rd.Associado.EmailNotificacaoContabilidade, bccAddresses, string.Format("Apifarma | Aviso de Pagamento | Valor em dívida {0}", Math.Abs(rd.Associado.SaldoContaCorrente).ToString("C2", culture)), body);


                        }
                        catch (Exception ex)
                        {

                            InsertLogEntry("Error: " + ex.Message);

                        }
                    }
                    else
                    {
                        //TODO
                        //Enviar email a avisar para que associados não se conseguiu enviar o email de aviso                    
                    }
                }

                #endregion
            });
        }

        //[System.Web.Services.WebMethod()]
        //public static string NotifyDebts(string day, string email)
        //{
        //    DateTime reportDate = DateTime.Today;
        //    if (!DateTime.TryParse(day, out reportDate)) 
        //    {
        //        reportDate = DateTime.Today;
        //    }
        //    List<ReportData> reportData = GetReportData(reportDate);

        //    #region Process data to send emails

        //    CultureInfo culture = new CultureInfo("pt-PT");
        //    foreach (ReportData rd in reportData)
        //    {
        //        double saldoActual = Math.Abs(rd.Associado.SaldoContaCorrente);

        //        List<string[]> rows = new List<string[]>();
        //        foreach (Helper.Quotas.ContaCorrente doc in rd.Documentos)
        //        {
        //            saldoActual -= doc.Valor;
        //            double valorEmFalta = saldoActual >= 0 ? doc.Valor : doc.Valor - Math.Abs(saldoActual);
        //            string documento = doc.DataEmissao.ToString("yyyy-MM-dd") + ", " + doc.NomeDocumento + "-" + doc.Numero;

        //            rows.Add(new string[]
        //                {
        //                    doc.NomeDocumento,
        //                    doc.Numero,
        //                    doc.DataEmissao.ToString("dd-MM-yyyy"),
        //                    Math.Abs(doc.Valor).ToString("C2", culture),
        //                    Math.Abs(valorEmFalta).ToString("C2", culture)
        //                });
        //        }

        //        string body = HlpMail.CreateMessageBody(
        //                        "Apifarma.Intranet.Quotas.Reporting.RptValoresDivida",
        //                        string.Format("Caro associado {0} ({1}), os seguintes valores encontram-se em dívida.", rd.Associado.Nome, rd.Associado.Numero),
        //                        new string[] { "Tipo de Documento", "Número", "Data de Emissão", "Valor do Documento", "Valor em Dívida" },
        //                        rows
        //                        );
        //        HlpMail.SendMail(SPContext.Current.Site.WebApplication, email, string.Format("Apifarma | Aviso de Pagamento | Valor em dívida {0}", Math.Abs(rd.Associado.SaldoContaCorrente).ToString("C2", culture)), body);
        //    }

        //    #endregion

        //    return "done";
        //}

    }



}