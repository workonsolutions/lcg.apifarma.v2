﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RandomCodeMigration
{
    public class SetNumeroAssociadoQuotas : SPItemEventReceiver
    {
        public override void ItemAdded(SPItemEventProperties properties)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\confirmation.txt", true))
            {
                file.WriteLine("Entrou");
            }

            SPListItem item = properties.ListItem;
            SPList list = properties.List;

            try
            {
                using (SPSite site = new SPSite(properties.SiteId))
                {
                    using (SPWeb web = site.OpenWeb("/entidades"))
                    {
                        SPList listEntidades = web.GetList("/Lists/entidades");

                        foreach (SPListItem itemEntidades in listEntidades.Items)
                        {
                            if (itemEntidades.Title.Equals(new SPFieldLookupValue(item["Entidade"].ToString()).LookupValue))
                            {
                                item["Número de Associado"] = itemEntidades["Número de Associado"].ToString();
                                break;
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                LogMessage(ex.Message);
            }
        }

        private void LogMessage(string Message)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Logs\QuotasItemAdded.txt", true))
            {
                file.WriteLine(Message);
            }
        }
    }
}
