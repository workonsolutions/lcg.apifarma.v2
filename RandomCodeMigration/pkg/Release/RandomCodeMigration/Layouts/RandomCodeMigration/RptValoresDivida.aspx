﻿
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RptValoresDivida.aspx.cs" Inherits="Convex.Clientes.Apifarma.Intranet.Quotas.Reporting.Pages.RptValoresDivida, Convex.Clientes.Apifarma.Intranet.Quotas.Reporting, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d1e3b6c2eefc4c4f" MasterPageFile="~masterurl/v4.master" %>

<asp:content ID="Content1" contentplaceholderid="PlaceHolderPageTitle" runat="server">
    Email de Aviso
</asp:content>
<asp:content ID="Content2" contentplaceholderid="PlaceHolderPageTitleInTitleArea" runat="server">
    Email de Aviso
</asp:content>
<%--<asp:content contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
	<SharePoint:RssLink runat="server"/>PlaceHolderAdditionalPageHead
</asp:content>
--%>
<%--<asp:Content ContentPlaceHolderId="PlaceHolderSearchArea" runat="server">
	<SharePoint:DelegateControl runat="server" ControlId="SmallSearchInputBox"/>PlaceHolderSearchArea
</asp:Content>
--%>
<asp:content ID="Content3" contentplaceholderid="PlaceHolderPageImage" runat="server">
    <img src="/_layouts/images/blank.gif" width="1" height="1" alt="">
    </asp:content>
<%--<asp:Content ContentPlaceHolderId="PlaceHolderLeftActions" runat="server">
    <SharePoint:ModifySettingsLink runat="server"/>PlaceHolderLeftActions
</asp:Content>
--%>
<asp:content ID="Content4" contentplaceholderid="PlaceHolderBodyLeftBorder" runat="server">
	<div height=100% class="ms-pagemargin"><IMG SRC="/_layouts/images/blank.gif" width="6" height="1" alt=""></div>    
</asp:content>
<asp:content ID="Content5" contentplaceholderid="PlaceHolderMain" runat="server">
<script language="javascript" type="text/javascript" src="/_layouts/jQueryLibs/jquery-1.4.2.min.js"></script>
        <table width="100%" cellpadding="0" cellspacing="0" id="onetIDListForm" style="padding:7px;">
            <tr>
                <td>
                    <table border="0" cellspacing="0" width="100%">
                        <tr>
                            <td valign="top" class="ms-formlabel" style="width: 210px;">
                                <h3 class="ms-standardheader">Dívidas até ao dia</h3>
                            </td>
                            <td  valign="top" class="ms-formbody">
                                <SharePoint:DateTimeControl id="dtDia" runat="server" DateOnly="true"></SharePoint:DateTimeControl>
                            </td>
                        </tr>
                        <tr>
                        <td valign="top" class="ms-formlabel">
                        <h3 class="ms-standardheader"> Incluir todos os Pagamentos</h3>
                        </td>
                        <td>
                            <asp:CheckBox id="CheckBoxPaymant" Checked=false runat="server" />
                             
                        </td>
                          
                        </tr>

<%--                        <tr>
                            <td valign="top" class="ms-formlabel" style="width: 210px;">
                                <h3 class="ms-standardheader">Email</h3>
                            </td>
                            <td valign="top" class="ms-formbody">
                                <asp:TextBox id="txtEmail" runat="server" MaxLength="255" Width="50%" Text="contabilidade@apifarma.pt"></asp:TextBox>
                                <br/>
                                Este endereço serve apenas para efeitos dos testes a desenvolver pela Apifarma de forma a não notificar os associados. Após o OK será substituido pelo endereço da ficha de associado.
                            </td>
                        </tr>
--%>                    </table>
                    <div style="padding-top:7px;">
                        <asp:Button id="btnPreview1" runat="server" class="ms-ButtonHeightWidth" Text="Download" />
                        <asp:Button id="btnPreview" runat="server" class="ms-ButtonHeightWidth" Text="Download" />
                        <asp:Button id="btnNotify" runat="server" class="ms-ButtonHeightWidth" Text="Enviar email" />
                    </div>
                    <img src="/_layouts/images/blank.gif" width="590" height="1" alt="">
                </td>
            </tr>
        </table>

        <iframe id="iframeReport" runat="server" src="" style="visibility:hidden; width:0px; height:0px;">
        </iframe> 

</asp:content>
<asp:content ID="Content6" contentplaceholderid="PlaceHolderBodyAreaClass" runat="server">
    <style type="text/css">
    .ms-bodyareaframe {
	    padding: 0px;
    }
    </style>
</asp:content>
<%--<asp:Content ContentPlaceHolderId="PlaceHolderPageDescription" runat="server">
    <SharePoint:ListProperty CssClass="ms-listdescription" Property="Description" runat="server"/>
    PlaceHolderPageDescription
</asp:Content>--%>
<%--<asp:Content ContentPlaceHolderId="PlaceHolderCalendarNavigator" runat="server">
    <SharePoint:SPCalendarNavigator id="CalendarNavigatorId" runat="server"/>
    PlaceHolderCalendarNavigator
</asp:Content>--%>

