﻿
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="wssuc" TagName="InputFormSection" Src="~/_controltemplates/InputFormSection.ascx" %>
<%@ Register TagPrefix="wssuc" TagName="InputFormControl" Src="~/_controltemplates/InputFormControl.ascx" %>
<%@ Register TagPrefix="wssuc" TagName="ButtonSection" Src="~/_controltemplates/ButtonSection.ascx" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" Inherits="Convex.Clientes.Apifarma.Intranet.Quotas.PageLayouts.ContaCorrente.SendEmail, Convex.Clientes.Apifarma.Intranet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d1e3b6c2eefc4c4f"
MasterPageFile="~/_layouts/application.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Envio de Email
</asp:Content>
<asp:Content ID="Content13" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    Documentos a enviar:
    <asp:Label ID="lblAssembleia" runat="server" />
</asp:Content>
<asp:Content ID="Content14" ContentPlaceHolderID="PlaceHolderAdditionalPageHead"
    runat="server">
</asp:Content>
<asp:Content ID="Content15" ContentPlaceHolderID="PlaceHolderPageDescription" runat="server">
</asp:Content>
<asp:Content ID="Content16" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <asp:Label ID="lblError" CssClass="ms-error" runat="server" Visible="false"></asp:Label>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table class="ms-pageinformation" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="top" style="padding: 10px;" width="100%" height="100px">
                            <table width="300" id="idItemHoverTable">
                                <tr>
                                    <th scope="row">
                                        Anexos:
                                    </th>
                                    <td dir="ltr">
                                        <asp:Literal runat="server" ID="ltrAttachments"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" width="100%">
                    <%-- top buttons --%>
                    <wssuc:ButtonSection runat="server" TopButtons="true" BottomSpacing="5" ShowSectionLine="false">
                        <template_buttons>
			                <asp:Button UseSubmitBehavior="false" runat="server" OnClick="BtnOk_Click" class="ms-ButtonHeightWidth" Text="<%$Resources:wss,multipages_okbutton_text%>" id="BtnNotifyTop" accesskey="<%$Resources:wss,okbutton_accesskey%>"/>
			            </template_buttons>
                    </wssuc:ButtonSection>
                    <%-- fields --%>
                    <asp:PlaceHolder ID="PanelTitle" runat="server">
                        <asp:Panel ID="PanelNotificacaoAccaoFormacao" runat="server">                            
                            <SharePoint:SPGridView ID="grdDocsToSend" runat="server" AutoGenerateColumns="false">
                                <Columns>                                  
                                     <asp:TemplateField HeaderText="Enviar">
                                         <ItemTemplate>  
                                            <asp:CheckBox ID="chkSelected" runat="server" Checked="true" />
                                         </ItemTemplate>
                                     </asp:TemplateField>                                    
                                            
                                   
                                </Columns>
                            </SharePoint:SPGridView>
                        </asp:Panel>
                    </asp:PlaceHolder>
                    <%-- bottom buttons --%>
                    <wssuc:ButtonSection runat="server">
                        <template_buttons>
			                <asp:PlaceHolder runat="server">
				                <asp:Button UseSubmitBehavior="false" runat="server" class="ms-ButtonHeightWidth" OnClick="BtnOk_Click" Text="<%$Resources:wss,multipages_okbutton_text%>" id="BtnNotifyBottom" accesskey="<%$Resources:wss,okbutton_accesskey%>"/>
			                </asp:PlaceHolder>
		                </template_buttons>
                    </wssuc:ButtonSection>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

