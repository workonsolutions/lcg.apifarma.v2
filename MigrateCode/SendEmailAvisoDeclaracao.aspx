﻿<%@ Page Language="C#" Inherits="Convex.Clientes.Apifarma.Intranet.Quotas.PageLayouts.DeclaracaoFacturacaoDetalhe.SendEmailAvisoDeclaracao, Convex.Clientes.Apifarma.Intranet, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d1e3b6c2eefc4c4f"
    MasterPageFile="~/_layouts/application.master" %>

<%@ Assembly Name="Microsoft.SharePoint.ApplicationPages, Version=12.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="wssuc" TagName="InputFormSection" Src="~/_controltemplates/InputFormSection.ascx" %>
<%@ Register TagPrefix="wssuc" TagName="InputFormControl" Src="~/_controltemplates/InputFormControl.ascx" %>
<%@ Register TagPrefix="wssuc" TagName="ButtonSection" Src="~/_controltemplates/ButtonSection.ascx" %>
<%@ Register TagPrefix="wssawc" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=12.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=12.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Envio de Aviso
</asp:Content>
<asp:Content ID="Content13" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    Associados a enviar:
    <asp:Label ID="lblAssembleia" runat="server" />
</asp:Content>
<asp:Content ID="Content14" ContentPlaceHolderID="PlaceHolderAdditionalPageHead"
    runat="server">
</asp:Content>
<asp:Content ID="Content15" ContentPlaceHolderID="PlaceHolderPageDescription" runat="server">
</asp:Content>
<asp:Content ID="Content16" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<script type="text/javascript">
//    var objImgLoader;

//    $(document).ready(function () {
//        objImgLoader = $("#imgAjaxLoader");
//        objImgLoader.hide();

//        $('input[id$="BtnNotifyTop"]').click(function () {
//            var id = "";
//            var idEntidades = "";
//            objImgLoader.show();
//            $("#log").append("A processar envio <br/>");
//            $('input[name="AssocId"]:checked').each(function () {
//                id = $(this).attr("itemId");
//                idEntidades = idEntidades + id + ";";                
//            });
//            //alert(idEntidades);

//            $.ajax(
//                {
//                    type: "POST",
//                    url: "/entidades/quotas/_layouts/Apifarma/SendEmailAvisoDeclaracao.aspx/SendEmail",
//                    data: "{ 'ids' : '" + idEntidades + "' }",
//                    contentType: "application/json; charset=utf-8",
//                    dataType: "json",
//                    success: function (msg) {
//                        //$("#log").append(msg.d + "<br/>");

//                        //                        objTxtValor.val(msg.d.toString().split(";")[0]);
//                        //                        objTxtObs.val(msg.d.toString().split(";")[1]);
//                        objImgLoader.hide();
//                        //window.location.href = msg.d;
//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                        alert("ERRRRRRRRRRRRRo");
//                        alert(thrownError);

//                        alert(xhr.status + "\n" + xhr.statusText);
//                        objImgLoader.hide();
//                    }
//                });

//            
//            //alert("aaa");
//        });
//    });
</script>
    <asp:Label ID="lblError" CssClass="ms-error" runat="server" Visible="false"></asp:Label>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table class="ms-pageinformation" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="top" style="padding: 10px;" width="100%" height="100px">
                            <table width="300" id="idItemHoverTable">
                                <tr>
                                    <td valign="top" class="ms-formlabel" style="width: 210px;">
                                        <h3 class="ms-standardheader">Ano</h3>
                                    </td>
                                    <td valign="top" class="ms-formbody">
                                        <SharePoint:InputFormTextBox id="txtAno" runat="server" MaxLength="4"></SharePoint:InputFormTextBox>
                                        <SharePoint:InputFormRequiredFieldValidator id="valRequiredAno" runat="server" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtAno" ErrorMessage="Obrigatório indicar um ano!"></SharePoint:InputFormRequiredFieldValidator>
                                        <SharePoint:InputFormRegularExpressionValidator id="valRegexAno" runat="server" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtAno" ErrorMessage="Ano inválido!" ValidationExpression="20[0-9][0-9]"></SharePoint:InputFormRegularExpressionValidator>                                 
                                    </td>
                                </tr>
                            </table>
                            <div style="padding-top:7px;">
                                <asp:Button id="btnPreview" runat="server" class="ms-ButtonHeightWidth" Text="Actualizar" />                            
                            </div>
                            <%--<div style="float:left;">
                                            <img id="imgAjaxLoader" src="/_layouts/apifarma/images/ajax-loader.gif" alt="ajax loader" style="float:left;" />
                                        </div>--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" width="100%">
                    <%-- top buttons --%>
                    <wssuc:ButtonSection runat="server" TopButtons="true" BottomSpacing="5" ShowSectionLine="false">
                        <template_buttons>
			                <asp:Button UseSubmitBehavior="false" runat="server" class="ms-ButtonHeightWidth" OnClick="BtnOk_Click" Text="<%$Resources:wss,multipages_okbutton_text%>" id="BtnNotifyTop" accesskey="<%$Resources:wss,okbutton_accesskey%>"/>
			            </template_buttons>
                    </wssuc:ButtonSection>
                    <%-- fields --%>
                    <asp:PlaceHolder ID="PanelTitle" runat="server">
                        <asp:Panel ID="PanelNotificacaoAssociadosSemDeclaracaoFacturacao" runat="server">                            
                            <SharePoint:SPGridView ID="grdAssociadosToSend" runat="server" AutoGenerateColumns="false">
                                <Columns>
                                <asp:TemplateField HeaderText="Enviar">
                                         <%--<ItemTemplate>  
                                            <input type="checkbox" ID="AssocId"  name="AssocId" checked itemId='<%# Eval("Id")%>' />                                           
                                         </ItemTemplate>--%>
                                          <ItemTemplate>  
                                            <asp:CheckBox ID="chkSelected" runat="server" Checked="true" />
                                         </ItemTemplate>
                                     </asp:TemplateField> 
                                    <SharePoint:SPBoundField DataField="Nome" HeaderText="Associado" />
                                    <SharePoint:SPBoundField DataField="EmailNotificacaoContabilidade" HeaderText="Email" />                                       
                                </Columns>
                            </SharePoint:SPGridView>
                        </asp:Panel>
                    </asp:PlaceHolder>
                    <%-- bottom buttons --%>
                    <wssuc:ButtonSection runat="server">
                        <template_buttons>
			                <asp:PlaceHolder runat="server">                                                               
				                <asp:Button UseSubmitBehavior="false" runat="server" class="ms-ButtonHeightWidth" OnClick="BtnOk_Click" Text="<%$Resources:wss,multipages_okbutton_text%>" id="BtnNotifyBottom" accesskey="<%$Resources:wss,okbutton_accesskey%>"/>
			                </asp:PlaceHolder>
		                </template_buttons>
                    </wssuc:ButtonSection>
                    
                </table>
                <div id="log" >
                                            
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
